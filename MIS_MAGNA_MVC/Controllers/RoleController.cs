﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
namespace MIS_MAGNA_MVC.Controllers
{
    public class RoleController : Controller
    {
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // GET: Role
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_m_role", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column()
                {
                    name = "sAddFilter",
                    data = "sAddFilter",
                    search = new Search()
                    {
                        value = "",
                        regex = ""
                    },
                    searchable = true,
                    orderable = false
                };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM (Select m_role_uid, m_role_id, m_company_id, m_role_code, m_role_name, m_role_flag, created_by, created_at, last_edited_by, last_edited_at from tb_m_role) AS t";

            var sFixedFilter = "";

            var sOrder_by = " t.m_role_id DESC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s); 

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public class m_role_d : tb_m_role_d
        {
            public string menumodule { get; set; }
            public int moduleorder { get; set; }
            public string menutype { get; set; }
            public int typeorder { get; set; }
            public string menuname { get; set; }
            public int menuorder { get; set; }
            public string menuurl { get; set; }
        }

        // GET: Role/Form/id
        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_m_role tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_m_role();
                tbl.m_role_uid = Guid.NewGuid();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_m_role.Find(id);
            }
            if (tbl == null)
                return HttpNotFound();
            ViewBag.action = action;
            InitDDL();
            return View(tbl); 
        }

        private void InitDDL()
        {
            var menumodule = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>("SELECT menumodule sfield, (CASE menumodule WHEN 'Setup' THEN 1 WHEN 'Purchasing' THEN 2 WHEN 'Inventory' THEN 3 WHEN 'Marketing' THEN 4 WHEN 'Production' THEN 5 WHEN 'Accounting' THEN 6 WHEN 'Fixed Assets' THEN 7 WHEN 'WIP Log' THEN 8 ELSE 9 END) ifield FROM tb_formmenu m WHERE menuflag='ACTIVE' GROUP BY menumodule ORDER BY ifield").ToList(), "sfield", "sfield");
            ViewBag.menumodule = menumodule;

            var menutype = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>("SELECT menutype sfield, (CASE menutype WHEN 'Master' THEN 1 WHEN 'Transaction' THEN 2 WHEN 'Report' THEN 3 ELSE 4 END) ifield FROM tb_formmenu m WHERE menuflag='ACTIVE' GROUP BY menutype ORDER BY ifield").ToList(), "sfield", "sfield");
            ViewBag.menutype = menutype;
        }

        [HttpPost]
        public ActionResult GetDataDetails(string menumodule, string menutype)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT 0 m_role_d_seq, menuoid m_menu_id, menumodule, menutype, 0 typeorder, menuname, menuorder, ('~/' + menucontroller + '/' + menuview) menuurl FROM tb_formmenu WHERE menuflag='ACTIVE' AND menumodule='" + menumodule + "' AND menutype='" + menutype + "' AND menuoid NOT IN (SELECT m_menu_id FROM tb_m_role_d /*WHERE m_role_id<>*/) ORDER BY menuorder";
                var tbl = db.Database.SqlQuery<m_role_d>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() + sSql }, JsonRequestBehavior.AllowGet);
            }
            return js; 
        }

        [HttpPost]
        public ActionResult FillDetailData(Guid? m_role_uid)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.Database.SqlQuery<m_role_d>($"SELECT m_role_d_seq, d.m_role_d_id, m.m_role_id, d.m_menu_id, mn.menuorder, mn.menumodule, mn.menutype, 0 typeorder, mn.menuname, ('~/' + menucontroller + '/' + menuview) menuurl FROM tb_m_role m Inner join tb_m_role_d d ON d.m_role_id=m.m_role_id Inner Join tb_formmenu mn ON mn.menuoid=d.m_menu_id Where menuflag='ACTIVE' AND m.m_role_uid='{m_role_uid}' Order By m_role_d_seq").ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        //[HttpPost]
        //public JsonResult SetDataDetails(List<m_role_d> tbl)
        //{
        //    Session["tb_formroledtl"] = tbl;
        //    return Json("", JsonRequestBehavior.AllowGet);
        //}

        // POST: Role/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_m_role tbl, List<m_role_d> dtDtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            var servertime = GetServerTime();

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.m_company_id = db.tb_m_company.FirstOrDefault().m_company_id;
                            tbl.last_edited_at = tbl.created_at;
                            tbl.last_edited_by = tbl.created_by;
                            db.tb_m_role.Add(tbl);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.m_company_id = db.tb_m_company.FirstOrDefault().m_company_id;
                            tbl.last_edited_at = servertime;
                            tbl.last_edited_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.tb_m_role_d.Where(a => a.m_role_id == tbl.m_role_id);
                            db.tb_m_role_d.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        var tbldtl = new List<tb_m_role_d>();
                        foreach (var item in dtDtl)
                        {
                            var new_dtl = (tb_m_role_d)MappingTable(new tb_m_role_d(), item);
                            new_dtl.m_role_id = tbl.m_role_id;
                            new_dtl.created_by = tbl.last_edited_by;
                            new_dtl.created_at = tbl.last_edited_at;
                            tbldtl.Add(new_dtl);
                        }
                        db.tb_m_role_d.AddRange(tbldtl); 

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.m_role_uid.ToString();
                        msg = "Data Already Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors) 
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />"; 
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);  
        }

        // POST: PurchaseReturnService/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_m_role tbl = db.tb_m_role.Find(uid);
            var servertime = GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.tb_m_role_d.Where(a => a.m_role_id == tbl.m_role_id );
                        db.tb_m_role_d.RemoveRange(trndtl);
                        db.tb_m_role.Remove(tbl);
                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}