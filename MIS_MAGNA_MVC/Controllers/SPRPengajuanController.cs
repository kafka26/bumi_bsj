﻿using CrystalDecisions.CrystalReports.Engine;
using MIS_MAGNA_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using System.Collections;
namespace MIS_MAGNA_MVC.Controllers
{
    public class SPRPengajuanController : Controller
    {
        // GET: SPRPengajuan 
        private QL_MIS_MAGNAEntities db; private string sSql = "";
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]; 
        private static readonly int dp_account_id = GetCOAInterface(new List<string> { "PIUTANG DP" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int kt_account_id = GetCOAInterface(new List<string> { "PIUTANG KT" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int shm_account_id = GetCOAInterface(new List<string> { "PIUTANG SHM" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int kpr_account_id = GetCOAInterface(new List<string> { "PIUTANG KPR" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int jual_account_id = GetCOAInterface(new List<string> { "HUTANG PENJUALAN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int hutang_refund_id = GetCOAInterface(new List<string> { "HUTANG REFUND" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int t_refund_cost_account_id = GetCOAInterface(new List<string> { "ADMIN REFUND" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int t_refund_cost1_account_id = GetCOAInterface(new List<string> { "BIAYA LAIN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int m_account_id_titipan = GetCOAInterface(new List<string> { "UM TITIPAN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int piutang_acoount_id_pb = GetCOAInterface(new List<string>() { "PIUTANG_PENINGKATAN_BANGUNAN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int hutang_acoount_id_pb = GetCOAInterface(new List<string>() { "HUTANG_PENINGKATAN_BANGUNAN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int piutang_acoount_id_jb = GetCOAInterface(new List<string>() { "PIUTANG_JASA_BANGUN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int hutang_acoount_id_jb = GetCOAInterface(new List<string>() { "HUTANG_JASA_BANGUN" }).FirstOrDefault()?.m_account_id ?? 0;
        public SPRPengajuanController() { db = new QL_MIS_MAGNAEntities(); db.Database.CommandTimeout = 0; } 

        public class t_refund : tb_t_refund
        {
            public int total_angsuran { get; set; }
            public decimal t_spr_price { get; set; }
            public decimal r_piutang_sisa_amt { get; set; }
            public decimal r_piutang_pay_amt { get; set; }
            public string t_spr_no { get; set; }
            public string m_item_name { get; set; }
            public string m_proyek_name { get; set; }
            public string m_type_unit_name { get; set; }
            public string m_cluster_name { get; set; }
            public string m_cust_name { get; set; }
        }

        public class r_piutang:tb_r_piutang
        {   
            public string t_spr_no { get; set; }  
            public Decimal r_piutang_sisa_amt { get; set; }
        }

        #region SPR Pengajuan

        [HttpPost]
        public ActionResult GetShowCoa(tb_t_refund tbl)
        {
            JsonResult js = null; string sResult = "";
            try
            {
                var sText = ""; var new_jurnal = new List<m_account>();
                var coa_penjualan = db.tb_m_account.Where(x => x.m_account_id == jual_account_id);
                var coa_refund = db.tb_m_account.Where(x => x.m_account_id == hutang_refund_id);
                var sList = GetDataPiutang(tbl, "crud").Where(c => c.m_cust_id == tbl.m_cust_id && c.m_item_id == tbl.m_item_id);
                // Piutang Hutang Penjualan
                new_jurnal.Add(new m_account { m_account_id = jual_account_id, m_account_code = coa_penjualan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_penjualan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = tbl.t_refund_price, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });

                decimal amt_titipan = tbl.t_refund_pay_ppn_amt + tbl.t_refund_pay_bphtb_amt + tbl.t_refund_pay_balik_nama_amt;
                if (amt_titipan > 0m)
                {
                    var coa_titipan = db.tb_m_account.Where(x => x.m_account_id == m_account_id_titipan);
                    new_jurnal.Add(new m_account { m_account_id = m_account_id_titipan, m_account_code = coa_titipan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_titipan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = amt_titipan, m_account_dbcr = "C", m_account_note = "" });
                }

                if (tbl.t_refund_sisa_peningkatan_amt > 0m)
                {
                    var hutang_amt_pb = sList.Where(x => new List<string> { "tb_t_peningkatan_d_pb", "tb_t_peningkatan_h_pb" }.Contains(x.r_piutang_ref_table)).ToList().Sum(x => x.r_piutang_amt);
                    var coa_hutang_pb = db.tb_m_account.Where(x => x.m_account_id == hutang_acoount_id_pb);
                    var coa_piutang_pb = db.tb_m_account.Where(x => x.m_account_id == piutang_acoount_id_pb);

                    new_jurnal.Add(new m_account { m_account_id = hutang_acoount_id_pb, m_account_code = coa_hutang_pb.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_hutang_pb.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = hutang_amt_pb, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });

                    new_jurnal.Add(new m_account { m_account_id = piutang_acoount_id_pb, m_account_code = coa_piutang_pb.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_piutang_pb.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_sisa_peningkatan_amt, m_account_dbcr = "D", m_account_note = "" });
                }

                if (tbl.t_refund_sisa_jasa_bangun_amt > 0m)
                {
                    var hutang_amt_jb = sList.Where(x => new List<string> { "tb_t_peningkatan_d_jb", "tb_t_peningkatan_h_jb" }.Contains(x.r_piutang_ref_table)).ToList().Sum(x => x.r_piutang_amt);
                    var coa_hutang_jb = db.tb_m_account.Where(x => x.m_account_id == hutang_acoount_id_jb);
                    var coa_piutang_jb = db.tb_m_account.Where(x => x.m_account_id == piutang_acoount_id_jb);

                    new_jurnal.Add(new m_account { m_account_id = hutang_acoount_id_jb, m_account_code = coa_hutang_jb.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_hutang_jb.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = hutang_amt_jb, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });

                    new_jurnal.Add(new m_account { m_account_id = piutang_acoount_id_jb, m_account_code = coa_piutang_jb.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_piutang_jb.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_sisa_jasa_bangun_amt, m_account_dbcr = "C", m_account_note = "" });
                }

                if ((decimal)tbl.t_refund_sisa_amt_dp > 0m)
                {
                    // Piutang DP
                    var coa_dp = db.tb_m_account.Where(x => x.m_account_id == dp_account_id);
                    new_jurnal.Add(new m_account { m_account_id = dp_account_id, m_account_code = coa_dp.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_dp.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = (decimal)tbl.t_refund_sisa_amt_dp + tbl.t_refund_utj_amt, m_account_dbcr = "C", m_account_note = "" });
                }

                if (tbl.t_refund_sisa_amt_kt > 0m)
                {
                    // Piutang KT
                    var coa_kt = db.tb_m_account.Where(x => x.m_account_id == kt_account_id);
                    new_jurnal.Add(new m_account { m_account_id = kt_account_id, m_account_code = coa_kt.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_kt.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_sisa_amt_kt, m_account_dbcr = "C" });
                }

                if (tbl.t_refund_sisa_amt_kpr > 0m)
                {
                    // Piutang KPR
                    var coa_kpr = db.tb_m_account.Where(x => x.m_account_id == kpr_account_id);
                    new_jurnal.Add(new m_account { m_account_id = kpr_account_id, m_account_code = coa_kpr.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_kpr.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_sisa_amt_kpr, m_account_dbcr = "C", m_account_note = "" });
                }

                // Insert Jurnal Akun BIAYA LAIN 
                if (tbl.t_refund_disc_amt > 0m)
                {
                    var coa_disc = db.tb_m_account.Where(x => x.m_account_id == t_refund_cost1_account_id);
                    new_jurnal.Add(new m_account { m_account_id = t_refund_cost1_account_id, m_account_code = coa_disc.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_disc.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_disc_amt + tbl.t_refund_cost_other_amt, m_account_dbcr = "C", m_account_note = "" });
                }


                // Insert Jurnal Akun BIAYA ADM (PENDAPATAN)
                if (tbl.t_refund_cost_amt + tbl.t_refund_cost_other_amt > 0m)
                {
                    var coa_pendapatan = db.tb_m_account.Where(x => x.m_account_id == t_refund_cost_account_id);
                    new_jurnal.Add(new m_account { m_account_id = t_refund_cost_account_id, m_account_code = coa_pendapatan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_pendapatan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_cost_amt + tbl.t_refund_cost_other_amt, m_account_dbcr = "C", m_account_note = "" });
                } 

                // Insert Jurnal Akun BIAYA LAIN (LAIN)
                if (tbl.t_refund_cost_other1_amt > 0m)
                {
                    var coa_pendapatan = db.tb_m_account.Where(x => x.m_account_id == t_refund_cost1_account_id);
                    new_jurnal.Add(new m_account { m_account_id = t_refund_cost1_account_id, m_account_code = coa_pendapatan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_pendapatan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_cost_other1_amt, m_account_dbcr = "C", m_account_note = "" });
                }

                new_jurnal.Add(new m_account { m_account_id = hutang_refund_id, m_account_code = coa_refund.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_refund.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_total_amt - (tbl.t_refund_disc_amt > 0m ? tbl.t_refund_disc_amt : 0m), m_account_dbcr = "C", m_account_note = "" });

                sResult = "ganti_harga"; var stbl = new_jurnal.ToList();
                sText = "Apakah anda yakin akan ganti harga unit?";
                js = Json(new { result = sResult, stext = $"{sText}", stbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        // GET: SuratPesananRumah
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            var isCollapse = "True";
            InitAdvFilterIndex(modfil, "tb_t_refund", false);
            ViewBag.isCollapse = isCollapse;
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( SELECT h.t_refund_uid, h.t_refund_id, h.m_company_id, m_company_name, t_refund_date, h.t_refund_no, sp.t_spr_no, p.m_proyek_name, c.m_cust_name, i.m_item_name, t_refund_type, t_refund_note, t_refund_status FROM tb_t_refund h INNER JOIN tb_m_company cmp ON cmp.m_company_id=h.m_company_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = h.m_proyek_id INNER JOIN tb_m_cust c ON c.m_cust_id = h.m_cust_id INNER JOIN tb_m_item i ON i.m_item_id = h.m_item_id INNER JOIN tb_t_spr sp ON sp.t_spr_id = h.t_refund_ref_id Where t_refund_type!='Batal SPR' ) AS t";
            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, "", " t.t_refund_date DESC");

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        } 

        [HttpPost]
        public ActionResult GetDataSPR(string t_refund_type) {
            JsonResult js = null; 
            try
            {
                var r_piutang_ref_table = ""; var r_piutang_type = "";
                // UPDATE TBL R_PIUTANG
                if (t_refund_type.Contains("DP"))
                {
                    r_piutang_ref_table = "AND r_piutang_ref_table IN ('tb_t_angsuran_dp_d', 'tb_t_spr_dp', 'tb_t_spr_utj', 'tb_t_utj') ";
                    r_piutang_type = "AND r_piutang_type = 'DP'";
                    t_refund_type = "DP";
                }
                if (t_refund_type.Contains("KPR"))
                {
                    r_piutang_ref_table = "AND r_piutang_ref_table IN ('tb_t_spr', 'tb_t_angsuran_kpr_d')";
                    r_piutang_type = "AND r_piutang_type = 'KPR'";
                    t_refund_type = "KPR";
                }
                if (t_refund_type.Contains("KT"))
                {
                    r_piutang_ref_table = "AND r_piutang_ref_table IN ('tb_t_angsuran_kt_d', 'tb_t_spr_kt')";
                    r_piutang_type = "AND r_piutang_type = 'KT'";
                    t_refund_type = "KT";
                } 

                sSql = $@"select s.t_spr_awal_id, s.t_spr_id t_refund_ref_id, s.t_spr_no, s.m_item_id
                , m_item_name, s.m_proyek_id, m_proyek_name, s.m_cust_id, m_cust_name, m_type_unit_name, 'tb_t_spr' t_refund_ref_table
                , r.r_piutang_pay_amt t_refund_pay_amt, s.t_spr_price, r.r_piutang_amt-r.r_piutang_pay_amt r_piutang_sisa_amt 
                from tb_t_spr s
                inner join tb_m_proyek ss on ss.m_proyek_id=s.m_proyek_id
                inner join tb_m_item i on i.m_item_id=s.m_item_id
                inner join tb_m_type_unit xx on xx.m_type_unit_id=i.m_item_type_unit_id
                inner join tb_m_cust c on c.m_cust_id=s.m_cust_id
                Inner Join ( 
                    select r.m_item_id, r.m_cust_id, Isnull(SUM(r_piutang_amt), 0.0) r_piutang_amt
                    , Isnull((select(SUM(p.r_piutang_pay_amt)) from tb_r_piutang p where p.r_piutang_type = 'PAYMENT' 
                    AND p.m_cust_id = r.m_cust_id AND p.m_item_id = r.m_item_id and p.r_piutang_ref_type!='DISKON' 
                    {r_piutang_ref_table}), 0.0) r_piutang_pay_amt
                    , Isnull((select(SUM(p.r_piutang_disc_amt)) from tb_r_piutang p where p.r_piutang_type = 'PAYMENT' 
                    AND p.m_cust_id = r.m_cust_id AND p.m_item_id = r.m_item_id {r_piutang_ref_table}
                    and p.r_piutang_ref_type='DISKON' ), 0.0) r_piutang_disc_amt
                    , '{t_refund_type}' r_piutang_type from tb_r_piutang r where r.r_piutang_status='Post' 
                    AND r_piutang_type != 'PAYMENT' {r_piutang_type} Group By r.m_item_id, r.m_cust_id 
                ) r ON r.m_item_id=s.m_item_id 
                AND r.m_cust_id=s.m_cust_id where i.m_item_flag = 'Terjual' AND s.t_spr_status = 'Post' 
                /*AND r.r_piutang_amt - isnull(r.r_piutang_pay_amt, 0.0) > 0*/
                AND s.t_spr_id NOT IN (select t_refund_ref_id from tb_t_refund group by t_refund_ref_id) order by c.m_cust_code";
                var stbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (stbl == null || stbl.Count <= 0) { js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet); } 
                else { js = Json(new { result = "", stbl }, JsonRequestBehavior.AllowGet); js.MaxJsonLength = Int32.MaxValue; }
            }
            catch (Exception e) { js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet); }
            return js;
        }

        private string sQuery(tb_t_refund tbl, string sQuery = "")
        {
            sQuery = $@"select r.m_item_id, r.m_cust_id, c.m_cust_name, i.m_item_name
            , r_piutang_ref_table, SUM(r_piutang_amt) r_piutang_amt
            , SUM(r_piutang_pay_amt) r_piutang_pay_amt, SUM(r_piutang_disc_amt) r_piutang_disc_amt
            , Isnull(SUM(r_piutang_amt) - SUM(r_piutang_pay_amt), 0.0) r_piutang_sisa_amt 
            from tb_r_piutang r Inner Join tb_m_cust c ON c.m_cust_id=r.m_cust_id Inner Join tb_m_item i ON i.m_item_id=r.m_item_id  
            where r.r_piutang_status='Post' AND Isnull(r.r_piutang_ref_type, '')!='DISKON' 
            AND r.r_piutang_disc_amt=0 AND r.m_item_id={tbl.m_item_id} AND r.m_cust_id={tbl.m_cust_id}
            Group By c.m_cust_name, r.m_item_id, i.m_item_name, r.r_piutang_account_id, r_piutang_ref_table
            , r.m_cust_id Order By c.m_cust_name, m_item_name, r_piutang_ref_table";
            return sQuery;
        }

        [HttpPost]
        public ActionResult GetDataItem(tb_t_refund tbl)
        {
            JsonResult js = null;
            try
            { 
                var stbl = new List<t_refund>();
                var sList = GetDataPiutang(tbl, "crud").Where(c => c.m_cust_id == tbl.m_cust_id && c.m_item_id == tbl.m_item_id);
                var dtList = db.Database.SqlQuery<r_piutang>(sQuery(tbl)).ToList();

                decimal t_refund_utj_amt = sList.Where(x => x.r_piutang_type == "UTJ").Sum(x => x.r_piutang_amt) > 0 ? sList.Where(x => x.r_piutang_type == "UTJ").Sum(x => x.r_piutang_amt) : 0m;

                decimal t_refund_amt_dp = sList.Where(x => x.r_piutang_type == "DP").Sum(x => x.r_piutang_amt) > 0 ? sList.Where(x => x.r_piutang_type == "DP").Sum(x => x.r_piutang_amt) : 0m;
                // CEK DATA KPR 
                decimal t_refund_amt_kpr = sList.Where(x => x.r_piutang_type == "KPR").Sum(x => x.r_piutang_amt) > 0 ? sList.Where(x => x.r_piutang_type == "KPR").Sum(x => x.r_piutang_amt) : 0;
                // CEK DATA KT 
                decimal t_refund_amt_kt = sList.Where(x => x.r_piutang_type == "KT").Sum(x => x.r_piutang_amt) > 0 ? sList.Where(x => x.r_piutang_type == "KT").Sum(x => x.r_piutang_amt) : 0;

                decimal t_refund_amt_shm = sList.Where(x => x.r_piutang_type == "SHM").Sum(x => x.r_piutang_amt) > 0 ? sList.Where(x => x.r_piutang_type == "SHM").Sum(x => x.r_piutang_amt) : 0;
                if (sList != null || sList.Count() > 0)
                { 
                    // Insert Jurnal Akun PIUTANG UANG MUKA
                    if (tbl.t_refund_type.Contains("DP"))
                    {
                        stbl.Add(new t_refund
                        {
                            t_refund_utj_amt = t_refund_utj_amt,
                            t_refund_pay_amt = sList.Sum(x => x.r_piutang_pay_amt),
                            t_refund_disc_amt = sList.Sum(x => x.r_piutang_disc_amt),
                            t_refund_total_amt = sList.Sum(x => x.r_piutang_pay_amt),
                            t_refund_pay_amt_dp = sList.Sum(x => x.r_piutang_pay_amt),
                            t_refund_disc_amt_dp = sList.Sum(x => x.r_piutang_disc_amt),
                            t_refund_sisa_amt_dp = t_refund_amt_dp - tbl.t_refund_pay_amt_dp,
                            t_refund_sisa_amt_kt = 0m,
                            t_refund_sisa_amt_kpr = 0m,
                            t_refund_sisa_amt_shm = 0m,
                            t_refund_total_angsuran = sList.Count(),
                        });
                    }

                    if (tbl.t_refund_type.Contains("KPR"))
                    {
                        stbl.Add(new t_refund
                        {
                            t_refund_utj_amt = t_refund_utj_amt,
                            t_refund_pay_amt = sList.Sum(x => x.r_piutang_pay_amt),
                            t_refund_pay_amt_kpr = sList.Sum(x => x.r_piutang_pay_amt),
                            t_refund_disc_amt_kpr = sList.Sum(x => x.r_piutang_disc_amt),
                            t_refund_disc_amt = sList.Sum(x => x.r_piutang_disc_amt),
                            t_refund_total_amt = sList.Sum(x => x.r_piutang_pay_amt),
                            t_refund_sisa_amt_dp = 0m,
                            t_refund_sisa_amt_kt = t_refund_amt_kt - tbl.t_refund_pay_amt_kt,
                            t_refund_sisa_amt_kpr = 0m,
                            t_refund_sisa_amt_shm = 0m,
                            t_refund_total_angsuran = sList.Count(),
                        });
                    }

                    if (tbl.t_refund_type.Contains("KT"))
                    {
                        stbl.Add(new t_refund
                        {
                            t_refund_utj_amt = t_refund_utj_amt,
                            t_refund_pay_amt = sList.Sum(x => x.r_piutang_pay_amt),
                            t_refund_pay_amt_kt = sList.Sum(x => x.r_piutang_pay_amt),
                            t_refund_disc_amt_kt = sList.Sum(x => x.r_piutang_disc_amt),
                            t_refund_disc_amt = sList.Sum(x => x.r_piutang_disc_amt),
                            t_refund_total_amt = sList.Sum(x => x.r_piutang_pay_amt),
                            t_refund_sisa_amt_dp = 0m,
                            t_refund_sisa_amt_kt = 0m,
                            t_refund_sisa_amt_kpr = t_refund_amt_kpr - tbl.t_refund_pay_amt_kpr,
                            t_refund_sisa_amt_shm = 0m,
                            t_refund_total_angsuran = sList.Count(),
                        });
                    }
                    else 
                    {
                        // CEK DATA UANG MUKA  
                        Decimal t_refund_pay_ppn_amt = db.Database.SqlQuery<Decimal>($"select Isnull(SUM(r_piutang_amt) - SUM(r_piutang_pay_amt), 0.0) from tb_r_piutang r where r.r_piutang_status='Post' AND r.r_piutang_ref_type!='DISKON' AND r.r_piutang_disc_amt=0 AND r_piutang_ref_table IN ('tb_t_titipan') And m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND r_piutang_ref_type='PPN' Group By r.m_item_id, r.m_cust_id")?.FirstOrDefault() ?? 0m;

                        Decimal t_refund_pay_peningkatan_amt = sList.FirstOrDefault(x => new List<string> { "tb_t_peningkatan_d_pb", "tb_t_peningkatan_h_pb" }.Contains(x.r_piutang_ref_table))?.r_piutang_pay_amt ?? 0m;

                        Decimal t_refund_sisa_peningkatan_amt = sList.FirstOrDefault(x => new List<string> { "tb_t_peningkatan_h_pb", "tb_t_peningkatan_d_pb" }.Contains(x.r_piutang_ref_table))?.r_piutang_sisa_amt ?? 0m;


                        Decimal t_refund_pay_jasa_bangun_amt = sList.FirstOrDefault(x => new List<string> { "tb_t_peningkatan_h_jb", "tb_t_peningkatan_d_jb" }.Contains(x.r_piutang_ref_table))?.r_piutang_pay_amt ?? 0m;

                        Decimal t_refund_sisa_jasa_bangun_amt = sList.FirstOrDefault(x => new List<string> { "tb_t_peningkatan_h_jb", "tb_t_peningkatan_d_jb" }.Contains(x.r_piutang_ref_table))?.r_piutang_sisa_amt ?? 0m;

                        stbl.Add(new t_refund
                        {
                            t_refund_utj_amt = t_refund_utj_amt,
                            t_refund_pay_amt = sList.Sum(x => x.r_piutang_pay_amt),
                            t_refund_pay_amt_dp = sList.Where(x => x.r_piutang_type == "DP" && x.r_piutang_pay_amt > 0m).Sum(x => x.r_piutang_pay_amt),
                            t_refund_disc_amt_dp = sList.Where(x => x.r_piutang_type == "DP" && x.r_piutang_disc_amt > 0m).Sum(x => x.r_piutang_disc_amt),
                            t_refund_pay_amt_kt = sList.Where(x => x.r_piutang_type == "KT" && x.r_piutang_pay_amt > 0m).Sum(x => x.r_piutang_pay_amt),
                            t_refund_disc_amt_kt = sList.Where(x => x.r_piutang_type == "KT" && x.r_piutang_disc_amt > 0m).Sum(x => x.r_piutang_disc_amt),
                            t_refund_pay_amt_kpr = sList.Where(x => x.r_piutang_type == "KPR" && x.r_piutang_pay_amt > 0m).Sum(x => x.r_piutang_pay_amt),
                            t_refund_disc_amt_kpr = sList.Where(x => x.r_piutang_type == "KPR" && x.r_piutang_disc_amt > 0m).Sum(x => x.r_piutang_disc_amt),
                            t_refund_disc_amt = sList.Sum(x => x.r_piutang_disc_amt), 
                            t_refund_sisa_amt_dp = t_refund_amt_dp - sList.Where(x => x.r_piutang_type == "DP" && x.r_piutang_pay_amt > 0m).Sum(x => x.r_piutang_pay_amt),
                            t_refund_sisa_amt_kt = t_refund_amt_kt - sList.Where(x => x.r_piutang_type == "KT" && x.r_piutang_pay_amt > 0m).Sum(x => x.r_piutang_pay_amt),
                            t_refund_sisa_amt_kpr = t_refund_amt_kpr - sList.Where(x => x.r_piutang_type == "KPR" && x.r_piutang_pay_amt > 0m).Sum(x => x.r_piutang_pay_amt),
                            t_refund_total_angsuran = sList.Count(),
                            t_refund_pay_ppn_amt = t_refund_pay_ppn_amt,
                            
                            t_refund_pay_peningkatan_amt = t_refund_pay_peningkatan_amt,
                            t_refund_sisa_peningkatan_amt= t_refund_sisa_peningkatan_amt,
                            t_refund_pay_jasa_bangun_amt = t_refund_pay_jasa_bangun_amt,
                            t_refund_sisa_jasa_bangun_amt = t_refund_sisa_jasa_bangun_amt,
                            t_refund_total_amt = sList.Sum(x => x.r_piutang_pay_amt),
                        });
                    }
                }
                  
                if (stbl == null || stbl.Count <= 0) { js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet); } 
                else { js = Json(new { result = "", stbl }, JsonRequestBehavior.AllowGet); js.MaxJsonLength = Int32.MaxValue; }
            }
            catch (Exception e) { js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet); }
            return js; 
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_t_refund tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_refund();
                tbl.t_refund_uid = Guid.NewGuid();
                tbl.t_refund_date = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.t_refund_status = "In Process";
                tbl.t_refund_account_id = GetCOAInterface(new List<string> { "HUTANG REFUND" }).FirstOrDefault().m_account_id;
                tbl.t_refund_cost_account_id= GetCOAInterface(new List<string> { "ADMIN REFUND" }).FirstOrDefault().m_account_id;
                tbl.m_item_new_id = 0;
                tbl.t_refund_price_new = 0m;
                tbl.t_refund_disc_amt_shm = 0m;
                tbl.t_refund_cost1_account_id = 0;
                tbl.m_item_new_id = 0;
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_refund.Find(id);
            }

            if (tbl.t_refund_status != "In Process") { ViewBag.r_gl_ref_table = db.tb_r_gl.FirstOrDefault(gl => gl.r_gl_ref_no == tbl.t_refund_no)?.r_gl_ref_table ?? ""; }

            ViewBag.action = action;
            var getCompany = db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList();
            if (action == "New Data")
                getCompany = getCompany.Where(m => m.flag == "ACTIVE").ToList();
            ViewBag.m_company_id = new SelectList(getCompany, "valuefield", "textfield", tbl.m_company_id);

            ViewBag.t_refund_account_id = new SelectList(GetCOAInterface(new List<string> { "HUTANG REFUND" }), "m_account_id", "m_account_name", tbl.t_refund_account_id);
            ViewBag.t_refund_cost_account_id = new SelectList(GetCOAInterface(new List<string> { "ADMIN REFUND" }), "m_account_id", "m_account_name", tbl.t_refund_cost_account_id);
            ViewBag.t_refund_cost1_account_id = new SelectList(GetCOAInterface(new List<string> { "BIAYA LAIN" }), "m_account_id", "m_account_name", tbl.t_refund_cost1_account_id);

            ViewBag.t_spr_no = (tbl.t_refund_ref_id != 0 ? db.tb_t_spr.FirstOrDefault(x => x.t_spr_id == tbl.t_refund_ref_id).t_spr_no : "");
            ViewBag.m_cust_name = (tbl.m_cust_id != 0 ? db.tb_m_cust.FirstOrDefault(x => x.m_cust_id == tbl.m_cust_id).m_cust_name : "");
            ViewBag.m_item_name = (tbl.m_item_id != 0 ? db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id).m_item_name : "");
            ViewBag.m_proyek_name = (tbl.m_proyek_id != 0 ? db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == tbl.m_proyek_id).m_proyek_name : "");
            return View(tbl);
        } 

        [HttpPost]
        public ActionResult InitDDLAccount(string t_refund_type_bayar, string action)
        {
            JsonResult js = null;
            try
            {
                var tbl = GetAccountDataToList(db, action, "NONE", 0, sVar: new List<string> { (t_refund_type_bayar == "KM" ? "KAS" : "BANK") });
                if (tbl == null && tbl.Count() <= 0)
                    tbl = GetAccountDataToList(db, action, "NONE", 0);
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_refund tbl, string action, List<m_account> dtDtl)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = new Guid();
            var servertime = GetServerTime(); var sListType = new List<string>() { "Ganti Unit", "Ganti Harga" };

            if (string.IsNullOrEmpty(tbl.t_refund_note)) tbl.t_refund_note = "";
            if (string.IsNullOrEmpty(tbl.t_refund_bank_name)) tbl.t_refund_bank_name = "";
            if (string.IsNullOrEmpty(tbl.t_refund_norek)) tbl.t_refund_norek = "";
            if (string.IsNullOrEmpty(tbl.t_refund_atas_nama)) tbl.t_refund_atas_nama = "";
            if (tbl.t_refund_ref_id == 0) { msg += "Silahkan pilih customer!<br/>"; }
            if (tbl.m_cust_id == 0) { msg += "Silahkan pilih customer!<br/>"; }
            if (tbl.m_item_id == 0) { msg += "Silahkan pilih unit!<br/>"; }
            if (tbl.t_refund_account_id == 0) { msg += "Silahkan Pilih Akun!<br/>"; }
            if (tbl.t_refund_cost_account_id == 0) { msg += "Silahkan Pilih Biaya!<br/>"; }
            if (tbl.t_refund_total_amt <= 0) { msg += "uang Kembali tidak boleh minus!<br/>"; }
            if (tbl.t_refund_type == "Batal SPR" && tbl.t_refund_type_bayar == "Angsuran" && tbl.t_refund_total_angsuran == 0) msg += "Silahkan isi total angsuran!<br/>";
            if (tbl.t_refund_date < GetCutOffdate()) { msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_refund_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />"; }
                
            if (string.IsNullOrEmpty(msg))
            {
                if (tbl.t_refund_status == "Revised") tbl.t_refund_status = "In Process";
                if (tbl.t_refund_status == "Post")
                {
                    tbl.t_refund_no = GenerateTransNo($"REF", "t_refund_no", "tb_t_refund", tbl.m_company_id, tbl.t_refund_date.ToString("MM/dd/yyyy"));
                    tbl.posted_by = Session["UserID"].ToString();
                    tbl.posted_at = servertime;
                }
                else tbl.t_refund_no = "";
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.t_refund_total_amt = tbl.t_refund_total_amt - (tbl.t_refund_disc_amt > 0m ? tbl.t_refund_disc_amt : 0m);
                        if (action == "New Data")
                        {
                            tbl.t_refund_price_new = 0m;
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            tbl.t_refund_flag = false;
                            db.tb_t_refund.Add(tbl); 
                        }
                        else
                        {
                            tbl.updated_at = GetServerTime();
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;                            
                        }
                        db.SaveChanges();

                        if (tbl.t_refund_status == "Post")
                        {
                            if (sListType.Contains(tbl.t_refund_type)) { msg = PostJurnalBatal(tbl, dtDtl); } 
                            else { msg = PostJurnalGantiTenor(tbl, dtDtl); } 
                            if (!string.IsNullOrEmpty(msg))
                            {
                                objTrans.Rollback(); result = "failed";
                                return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                            } 
                            //Block Post Transaction
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_refund_uid, "tb_t_refund_h", tbl.t_refund_id, tbl.updated_by, tbl.updated_at));
                            var dt_spr = db.tb_t_spr.FirstOrDefault(s => s.t_spr_id == tbl.t_refund_ref_id);
                            dt_spr.t_spr_status = "Cancel";
                            db.Entry(dt_spr).State = EntityState.Modified;
                        } 

                        db.SaveChanges();
                        objTrans.Commit();

                        hdrid = tbl.t_refund_uid; result = "success";
                        if (tbl.t_refund_status == "Post") msg = $"data telah diposting dengan nomor transaksi {tbl.t_refund_no}";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        public List<r_piutang> GetDataPiutang(tb_t_refund tbl, string action = "")
        {
            string msg = string.Empty; var r_piutang_ref_table = ""; var r_piutang_type = ""; var sWhere = "";
            // UPDATE TBL R_PIUTANG
            if (tbl.t_refund_type.Contains("DP"))
            {
                r_piutang_ref_table = "AND r_piutang_ref_table IN ('tb_t_angsuran_dp_d', 'tb_t_spr_dp', 'tb_t_spr_utj', 'tb_t_utj') ";
                r_piutang_type = "AND r_piutang_type = 'DP'";
            }
            if (tbl.t_refund_type.Contains("KPR")) { 
                r_piutang_ref_table = "AND r_piutang_ref_table IN ('tb_t_spr', 'tb_t_angsuran_kpr_d')"; 
                r_piutang_type = "AND r_piutang_type = 'KPR'"; 
            }
            if (tbl.t_refund_type.Contains("KT")) { 
                r_piutang_ref_table = "AND r_piutang_ref_table IN ('tb_t_angsuran_kt_d', 'tb_t_spr_kt')"; 
                r_piutang_type = "AND r_piutang_type = 'KT'";
            }

            if (action == "crud") { sWhere = $"AND m_cust_id = {tbl.m_cust_id} AND r.m_item_id = {tbl.m_item_id}"; }

            sSql = $@"select 'data;ist' slist, r_piutang_amt-isnull(r_piutang_pay_amt, 0.0) r_piutang_sisa_amt
                , * from ( 
                    select r_piutang_id, r_piutang_ref_id, r.m_cust_id, r.m_item_id, r.r_piutang_account_id
                    , r.r_piutang_type, (r_piutang_amt) r_piutang_amt, r.r_piutang_ref_table, r.r_piutang_status 
                    , Isnull((select(SUM(p.r_piutang_pay_amt)) 
                        from tb_r_piutang p where p.r_piutang_ref_table = r.r_piutang_ref_table AND r.r_piutang_ref_id = p.r_piutang_ref_id 
                        AND p.r_piutang_type = 'PAYMENT' AND p.r_piutang_status='Post' AND Isnull(p.r_piutang_ref_type, '') != 'DISKON' 
                        AND p.m_cust_id = r.m_cust_id AND p.m_item_id = r.m_item_id), 0.0) r_piutang_pay_amt
                    , Isnull((select (SUM(p.r_piutang_disc_amt)) from tb_r_piutang p where p.r_piutang_ref_table=r.r_piutang_ref_table 
                    AND r.r_piutang_ref_id=p.r_piutang_ref_id AND p.r_piutang_type='PAYMENT' AND p.m_cust_id=r.m_cust_id 
                    AND p.m_item_id=r.m_item_id AND p.r_piutang_status='Post' 
                    AND Isnull(p.r_piutang_ref_type, '') = 'DISKON'), 0.0) r_piutang_disc_amt
                    from tb_r_piutang r where r.r_piutang_status='Post' {sWhere} AND r_piutang_type != 'PAYMENT' {r_piutang_ref_table} 
                ) r Where 1=1 {r_piutang_type} ";
            var sList = db.Database.SqlQuery<r_piutang>(sSql).ToList();
            return sList;
        }

        private string PostJurnalGantiTenor(tb_t_refund tbl, List<m_account> dtDtl, string msg = "", int seq = 1)
        {            
            // Update tb_r_piutang            
            try
            {
                var sList = GetDataPiutang(tbl, "crud").Where(x => x.r_piutang_pay_amt > 0);
                foreach (var items in sList.Where(x => (x.r_piutang_amt - x.r_piutang_pay_amt) == x.r_piutang_amt))
                {
                    var dt_ar = db.tb_r_piutang.Where(x => x.r_piutang_ref_id == items.r_piutang_ref_id && x.r_piutang_id == items.r_piutang_id);
                    foreach (var item in dt_ar)
                    {
                        item.r_piutang_status = "Cancel";
                        db.Entry(item).State = EntityState.Modified;
                    }
                }

                if (dtDtl.Any())
                {
                    foreach (var item in dtDtl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_refund", tbl.t_refund_id, tbl.t_refund_no, tbl.t_refund_date, item.m_account_id, item.m_account_dbcr, (item.m_account_dbcr == "D" ? item.m_account_debet_amt : item.m_account_credit_amt), $"SPR {tbl.t_refund_type}", Session["UserID"].ToString(), GetServerTime(), tbl.m_cust_id)); db.SaveChanges();
                    }
                } 
                //Akhir Insert Jurnal Akunting
            }
            catch (Exception e)
            {
                msg = e.ToString();
            }
            db.SaveChanges();
            return msg;
        }

        private string PostJurnalBatal(tb_t_refund tbl, List<m_account> dtDtl, string msg = "", int seq = 1)
        { 
            try
            {
                var dt_unit = db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id);
                dt_unit.m_item_flag = "Open";
                db.Entry(dt_unit).State = EntityState.Modified;

                if (dtDtl.Any())
                {
                    foreach (var item in dtDtl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_refund", tbl.t_refund_id, tbl.t_refund_no, tbl.t_refund_date, item.m_account_id, item.m_account_dbcr, (item.m_account_dbcr == "D" ? item.m_account_debet_amt : item.m_account_credit_amt), $"SPR {tbl.t_refund_type}", Session["UserID"].ToString(), GetServerTime(), tbl.m_cust_id)); db.SaveChanges();
                    }
                }
                var sList = GetDataPiutang(tbl, "crud").Where(x => x.m_cust_id == tbl.m_cust_id && x.m_item_id == tbl.m_item_id);
                if (sList != null)
                {
                    var dt_ar = db.tb_r_piutang.Where(x => x.m_cust_id == tbl.m_cust_id && x.m_item_id == tbl.m_item_id);
                    foreach (var item in dt_ar)
                    {
                        item.r_piutang_status = "Cancel";
                        db.Entry(item).State = EntityState.Modified;
                    }
                }
                //Akhir Insert Jurnal Akunting
            }
            catch (Exception e)
            {
                msg = e.ToString();
            }
            db.SaveChanges();
            return msg; 
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int t_refund_id, string revisi)
        {
            tb_t_refund tbl = db.tb_t_refund.Find(t_refund_id); 
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var dt_unit = db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id);
                        dt_unit.m_item_flag = "Terjual";
                        db.Entry(dt_unit).State = EntityState.Modified;

                        var dt_spr = db.tb_t_spr.FirstOrDefault(s => s.t_spr_id == tbl.t_refund_ref_id);
                        dt_spr.t_spr_status = "Post";

                        db.Entry(dt_spr).State = EntityState.Modified;
                        db.tb_t_refund.Remove(tbl);

                        db.SaveChanges();
                        objTrans.Commit();                         
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Print Out
        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            string rptname = "PrintRefund";
            var id = int.Parse(ids); var type = "";
            var tbl = db.tb_t_refund.FirstOrDefault(x => x.t_refund_id == id);
            if (tbl.t_refund_type == "Batal SPR") type = "Batal";
            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), $"rpt{rptname}{type}.rpt"));

            sSql = $"SELECT * FROM tb_v_print_refund WHERE t_refund_id IN ({ids}) ORDER BY t_refund_id";
            var dtRpt = new ClassConnection().GetDataTable(sSql, "data");

            report.SetDataSource(dtRpt);
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
        #endregion
    }
}
