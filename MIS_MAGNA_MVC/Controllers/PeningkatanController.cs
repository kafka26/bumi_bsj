﻿using MIS_MAGNA_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using System.Collections;
using static MIS_MAGNA_MVC.Controllers.UangMukaController;
using System.Security;
using static MIS_MAGNA_MVC.Controllers.KreditPemilikanRumahController;

namespace MIS_MAGNA_MVC.Controllers
{
    public class PeningkatanController : Controller
    {
        // GET: Peningkatan
        private QL_MIS_MAGNAEntities db;
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        public string sSql = "";
        public string sWhere = "";
        public class t_peningkatan_d : tb_t_peningkatan_d
        { 
            public string m_item_name { get; set; }
            public string m_cust_name { get; set; }
            public string m_proyek_name { get; set; }
        }

        public PeningkatanController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        #region Peningkatan Mutu 
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.periodstart = GetServerTime().ToString("01/dd/yyyy");
            ViewBag.periodend = GetServerTime().ToString("MM/dd/yyyy");
            ViewBag.Title = "Peningkatan Mutu";
            return View();
        }

        [HttpPost]
        public ActionResult GetDataIndex(string periodstart, string periodend)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT * FROM ( Select h.*, p.m_proyek_name, c.m_cust_name, i.m_item_name, s.t_spr_no FROM tb_t_peningkatan_h h INNER JOIN tb_m_company cmp ON cmp.m_company_id=h.m_company_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = h.m_proyek_id INNER JOIN tb_m_cust c ON c.m_cust_id = h.m_cust_id INNER JOIN tb_m_item i ON i.m_item_id = h.m_item_id INNER JOIN tb_t_spr s ON s.t_spr_id = h.t_spr_id ) AS t Order By created_at DESC";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_t_peningkatan_h tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_peningkatan_h();
                tbl.t_peningkatan_uid = Guid.NewGuid();
                tbl.t_peningkatan_date = GetServerTime();
                tbl.t_peningkatan_due_date = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.t_peningkatan_status = "In Process";
                tbl.m_company_id = db.tb_m_company.FirstOrDefault()?.m_company_id ?? 0;
                tbl.t_peningkatan_angsuran = 1;
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_peningkatan_h.Find(id);
            }

            ViewBag.action = action;
            FillAdditionalField(tbl, action);
            return View(tbl);
        }

        private void FillAdditionalField(tb_t_peningkatan_h tbl, string action)
        {             
            ViewBag.m_cust_name = (tbl.m_cust_id != 0 ? db.tb_m_cust.FirstOrDefault(x => x.m_cust_id == tbl.m_cust_id).m_cust_name : "");
            ViewBag.m_item_name = (tbl.m_item_id != 0 ? db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id).m_item_name : "");
            ViewBag.m_proyek_name = (tbl.m_proyek_id != 0 ? db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == tbl.m_proyek_id).m_proyek_name : "");
            ViewBag.t_spr_no = (tbl.t_spr_id != 0 ? db.tb_t_spr.FirstOrDefault(x => x.t_spr_id == tbl.t_spr_id).t_spr_no : "");
            ViewBag.t_peningkatan_ref_no = "";           
            ViewBag.t_titipan_masuk_amt = 0m;
            ViewBag.t_titipan_pay_amt = 0m;
            ViewBag.t_titipan_os_amt = 0m;
            var sList = new List<string> { "PB", "JB" };
            ViewBag.t_peningkatan_ref_type = new SelectList(GetTypeTitipan().Where(x => sList.Contains(x.Value)).ToList(), "Value", "Text", tbl.t_peningkatan_ref_type);
            ViewBag.m_account_debet_id = new SelectList(GetCOAInterface(new List<string>() { "PIUTANG_PENINGKATAN_BANGUNAN" }), "m_account_id", "m_account_name", tbl.m_account_debet_id);
            ViewBag.m_account_credit_id = new SelectList(GetCOAInterface(new List<string>() { "HUTANG_PENINGKATAN_BANGUNAN" }), "m_account_id", "m_account_name", tbl.m_account_credit_id);
        }

        [HttpPost]
        public ActionResult GetAccount(string t_peningkatan_ref_type = "")
        {
            JsonResult js = null;
            try
            { 
                var db = GetCOAInterface(new List<string>() { "PIUTANG_PENINGKATAN_BANGUNAN" });
                var cr = GetCOAInterface(new List<string>() { "HUTANG_PENINGKATAN_BANGUNAN" });
                if (t_peningkatan_ref_type == "JB")
                {
                    db = GetCOAInterface(new List<string>() { "PIUTANG_JASA_BANGUN" });
                    cr = GetCOAInterface(new List<string>() { "HUTANG_JASA_BANGUN" });
                }
                js = Json(new { result = "", db, cr }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue; 
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        #endregion

        #region Get data trn
        [HttpPost]
        public ActionResult GetShowCoa(tb_t_peningkatan_h stbl, string sText = "")
        {
            JsonResult js = null;
            try
            {
                var new_jurnal = new List<m_account>();
                // Piutang Hutang Penjualan
                new_jurnal.Add(new m_account { m_account_id = stbl.m_account_debet_id, m_account_code = db.tb_m_account.FirstOrDefault(x => x.m_account_id == stbl.m_account_debet_id)?.m_account_code ?? "", m_account_desc = db.tb_m_account.FirstOrDefault(x => x.m_account_id == stbl.m_account_debet_id)?.m_account_desc ?? "", m_account_debet_amt = stbl.t_peningkatan_amt, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });
                  
                // Hutang Refund
                new_jurnal.Add(new m_account { m_account_id = stbl.m_account_credit_id, m_account_code = db.tb_m_account.FirstOrDefault(x => x.m_account_id == stbl.m_account_credit_id)?.m_account_code ?? "", m_account_desc = db.tb_m_account.FirstOrDefault(x => x.m_account_id == stbl.m_account_credit_id)?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = stbl.t_peningkatan_amt, m_account_dbcr = "C" , m_account_note = "" }); 

                var tbl = new_jurnal.ToList();
                if (tbl.Count > 0) { 
                    sText = "Apakah anda yakin akan melanjutkan?";
                    js = Json(new { result = "", stext = $"{sText}", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
                else
                {
                    sText = "Tidak ada data yang bisa di proses..";
                    js = Json(new { result = "-", stext = $"{sText}", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataCust(string t_titipan_type_bayar)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select distinct c.m_cust_id, c.m_cust_code, c.m_cust_name, c.m_cust_addr, s.m_proyek_id, s.t_spr_id, s.t_spr_awal_id, s.t_spr_no, i.m_item_id, i.m_item_name, (select p.m_proyek_name from tb_m_proyek p where p.m_proyek_id=s.m_proyek_id) m_proyek_name from tb_m_cust c inner join tb_t_spr s on s.m_cust_id = c.m_cust_id Inner Join tb_m_item i ON i.m_item_id=s.m_item_id AND i.m_proyek_id=s.m_proyek_id Where t_spr_status='Post'";
                 
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataItem(int m_cust_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"select c.m_cust_id, c.m_cust_name, i.m_proyek_id, p.m_proyek_name, i.m_item_id, i.m_item_name, tu.m_type_unit_name, isnull(s.t_spr_awal_id, 0) t_spr_awal_id, s.t_spr_id, s.t_spr_no, format(s.t_spr_date, 'dd/MM/yyyy') t_spr_date from tb_m_cust c inner join tb_t_spr s on s.m_cust_id = c.m_cust_id inner join tb_m_item i on i.m_item_id = s.m_item_id inner join tb_m_type_unit tu on tu.m_type_unit_id = i.m_item_type_unit_id inner join tb_m_proyek p on p.m_proyek_id = i.m_proyek_id where s.t_spr_status = 'Post' and s.m_cust_id = {m_cust_id}";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(tb_t_peningkatan_h tbl, string t_peningkatan_due_date, string result = "", int t_peningkatan_d_seq = 0)
        {
            List<t_peningkatan_d> dtl = new List<t_peningkatan_d>();
            try
            {
                var t_peningkatan_ref_type = "";
                int maxloop = 1; decimal amtGen = 0; t_peningkatan_d_seq = 0;
                if (tbl.t_peningkatan_type_bayar.ToLower() == "angsuran") { maxloop = tbl.t_peningkatan_angsuran; }
                if (tbl.t_peningkatan_ref_type == "PB") { t_peningkatan_ref_type = "Peningkatan"; }
                else { t_peningkatan_ref_type = "Jasa Bangun"; }
                decimal dValue = Math.Round(tbl.t_peningkatan_amt_bln, 0);
                decimal amtTotal = Math.Round(tbl.t_peningkatan_amt, 0);
                for (var i = 0; i < maxloop; i++)
                {
                    if (tbl.t_peningkatan_type_bayar == "Angsuran") { t_peningkatan_d_seq = i + 1; }
                    dtl.Add(new t_peningkatan_d()
                    {
                        t_peningkatan_d_seq = i + 1,
                        m_item_name = db.tb_m_item.FirstOrDefault()?.m_item_name ?? "",
                        t_peningkatan_d_note = $"{(tbl.t_peningkatan_type_bayar == "Angsuran" ? $"{t_peningkatan_ref_type} {i + 1}" : t_peningkatan_ref_type + " " + tbl.t_peningkatan_type_bayar)}",
                        t_peningkatan_due_date = DateTime.Parse(t_peningkatan_due_date).AddMonths(i),
                        t_peningkatan_d_amt = (i == (maxloop - 1) ? (amtTotal - amtGen) : dValue),
                        t_peningkatan_d_status = "Post"
                    });
                    amtGen += dValue;
                }

            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            JsonResult js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int t_peningkatan_id, string m_item_name)
        {
            JsonResult js = null;
            try
            {
                sSql = $"select '{m_item_name}' m_item_name, * from tb_t_peningkatan_d Where t_peningkatan_h_id={t_peningkatan_id} Order By t_peningkatan_d_seq";
                var tbl = db.Database.SqlQuery<t_peningkatan_d>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0) { js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet); }
                else { js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet); js.MaxJsonLength = Int32.MaxValue; }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString(), sSql }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }
        #endregion

        #region Save Data
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_peningkatan_h tbl, List<t_peningkatan_d> dtDtl, List<m_account> dtcoa, string action)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = ""; int seq = 1;
            var sReturnNo = ""; var sReturnState = ""; var servertime = GetServerTime();
            var t_peningkatan_amt = 0m;
            if (tbl.t_spr_id == 0) msg += "Silahkan Pilih No. Surat Pesanan Rumah ! <br />";
            if (dtDtl == null) msg += "Silahkan Isi Data Detail ! <br />";
            if (dtDtl != null && dtDtl.Count > 0)
            {
                foreach (var item in dtDtl) { t_peningkatan_amt += item.t_peningkatan_d_amt; }
                if (tbl.t_peningkatan_amt != t_peningkatan_amt)
                    msg += $"<strong>Nominal Total Angsuran {string.Format("{0:#,0.00}", Convert.ToDecimal(t_peningkatan_amt))} harus sama dengan {string.Format("{0:#,0.00}", Convert.ToDecimal(tbl.t_peningkatan_amt))}</strong> <br />";
            }

            if (dtDtl == null || dtDtl.Count <= 0) { msg += "Silakan Generated data detail dulu"; } 
            if (tbl.t_peningkatan_status == "Post")
            {
                tbl.t_peningkatan_no = GenerateTransNo($"PM", "t_peningkatan_no", "tb_t_peningkatan_h", tbl.m_company_id, tbl.t_peningkatan_date.ToString("MM/dd/yyyy"));
                tbl.posted_at = servertime;
                tbl.posted_by = Session["UserID"].ToString();
            }
            else { tbl.t_peningkatan_no = ""; }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.created_at = servertime;
                            tbl.created_by = Session["UserID"].ToString();
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.tb_t_peningkatan_h.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            //cek data yg dihapus
                            var d_id = dtDtl.Select(s => s.t_peningkatan_h_id).ToList();
                            var dt_d = db.tb_t_peningkatan_d.Where(a => a.t_peningkatan_h_id == tbl.t_peningkatan_id && !d_id.Contains(a.t_peningkatan_d_id));
                            if (dt_d != null && dt_d.Count() > 0) db.tb_t_peningkatan_d.RemoveRange(dt_d); 
                        }
                        db.SaveChanges();

                        tb_t_peningkatan_d tDtl;
                        foreach(var item in dtDtl)
                        {
                            var cek_data = db.tb_t_peningkatan_d.Where(a => a.t_peningkatan_d_id == item.t_peningkatan_d_id);
                            tDtl = (tb_t_peningkatan_d)MappingTable(new tb_t_peningkatan_d(), item);
                            if (cek_data != null && cek_data.Count() > 0)
                                tDtl = db.tb_t_peningkatan_d.FirstOrDefault(a => a.t_peningkatan_d_id == item.t_peningkatan_d_id);
                            tDtl.t_peningkatan_h_id = tbl.t_peningkatan_id; 
                            tDtl.created_by = tbl.updated_by;
                            tDtl.created_at = tbl.updated_at; 
                            if (cek_data != null && cek_data.Count() > 0) db.Entry(tDtl).State = EntityState.Modified;
                            else { db.tb_t_peningkatan_d.Add(tDtl); } 
                        }
                        db.SaveChanges();

                        if (tbl.t_peningkatan_status == "Post")
                        {
                            //Block Post Transaction
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_peningkatan_uid, "tb_t_peningkatan_h", tbl.t_peningkatan_id, tbl.updated_by, tbl.updated_at));
                            var dtldata = db.tb_t_peningkatan_d.Where(d => d.t_peningkatan_h_id == tbl.t_peningkatan_id).ToList();
                            foreach (var item in dtldata)
                            {
                                db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, tbl.t_peningkatan_ref_type, (tbl.t_peningkatan_type_bayar == "Angsuran" ? $"tb_t_peningkatan_d_{tbl.t_peningkatan_ref_type.ToLower()}" : $"tb_t_peningkatan_h_{tbl.t_peningkatan_ref_type.ToLower()}"), (tbl.t_peningkatan_type_bayar == "Angsuran" ? item.t_peningkatan_d_id : tbl.t_peningkatan_id), tbl.t_peningkatan_no, item.t_peningkatan_due_date, item.t_peningkatan_due_date, Convert.ToDateTime("1/1/1900".ToString()), tbl.m_account_debet_id, item.t_peningkatan_d_amt, 0m, item.t_peningkatan_d_note, tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0, tbl.t_spr_id, tbl.t_spr_awal_id, "PENINGKATAN", "PENINGKATAN", tbl.t_peningkatan_id));
                            }

                            foreach (var item in dtcoa)
                            {
                                db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_peningkatan_h", tbl.t_peningkatan_id, tbl.t_peningkatan_no, tbl.t_peningkatan_date, item.m_account_id, item.m_account_dbcr, (item.m_account_dbcr == "D" ? item.m_account_debet_amt : item.m_account_credit_amt), $"SALDO {tbl.t_peningkatan_ref_type.ToUpper()}", tbl.created_by.ToString(), GetServerTime(), tbl.m_cust_id));
                            }
                        }

                        db.SaveChanges(); objTrans.Commit();
                        hdrid = tbl.t_peningkatan_id.ToString();
                        if (tbl.t_peningkatan_status == "Post")
                        {
                            sReturnNo = tbl.t_peningkatan_no;
                            sReturnState = "terposting";
                        }
                        else
                        {
                            sReturnNo = "draft " + tbl.t_peningkatan_id.ToString();
                            sReturnState = "tersimpan";
                        }
                        msg = "Data " + sReturnState + " dengan No. " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback(); var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += $"Entity of type {eve.Entry.Entity.GetType().Name} in state {eve.Entry.State} has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                err += $"- Property: {ve.PropertyName}, Error: {ve.ErrorMessage} <br />";
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_t_peningkatan_h tbl = db.tb_t_peningkatan_h.Find(id);
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data tidak ditemukan!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.tb_t_peningkatan_d.Where(a => a.t_peningkatan_h_id == tbl.t_peningkatan_id);
                        db.tb_t_peningkatan_d.RemoveRange(trndtl); db.SaveChanges(); 
                        db.tb_t_peningkatan_h.Remove(tbl); db.SaveChanges(); 
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Print Out
        public ActionResult PrintReport(int ids)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile"); 
            string rptname = $"PrintKBM";
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("userID", Session["UserID"].ToString()); 
            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), $"rpt{rptname}.rpt"));
            sSql = $"SELECT * FROM tb_v_print_peningkatan WHERE t_kbm_h_id IN ({ids}) ORDER BY t_kbm_h_id";
            var dt = new ClassConnection().GetDataTable(sSql, "datatable");
            report.SetDataSource(dt);
            if (rptparam.Count > 0) foreach (var item in rptparam) report.SetParameterValue(item.Key, item.Value);

            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", $"inline; filename={rptname}.pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
        #endregion
    }
}