﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace MIS_MAGNA_MVC.Controllers
{
    public class UrgentController : Controller
    {
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public UrgentController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        public class r_urgent_d : tb_r_urgent_d
        {
            public int r_urgent_d_seq { get; set; }
            public string r_urgent_d_pay_flag { get; set; }
        }

        private void InitDDL(tb_r_urgent_h tbl, string action) { }

        [HttpPost]
        public ActionResult FillDetailData(string id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT 0 r_urgent_d_seq, r_urgent_d_id, r_urgent_d_pay_type, r_urgent_d_pay_date, r_urgent_d_transfer_date, r_urgent_d_pay_amt, r_urgent_d_pay_note, 'update' r_urgent_d_pay_flag FROM tb_r_urgent_d d WHERE r_urgent_h_id = {id} ORDER BY r_urgent_d_id";
                var tbl = db.Database.SqlQuery<r_urgent_d>(sSql).ToList();
                var seq = 0;
                foreach (var item in tbl) item.r_urgent_d_seq = ++seq;
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {                
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            InitAdvFilterIndex(modfil, "tb_r_urgent_h", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE isapptrans=0 AND isposttrans=0 ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( SELECT h.r_urgent_h_uid, h.r_urgent_h_id, h.m_cust_name, h.m_unit_name, h.m_unit_type, p.m_proyek_name, FORMAT(r_urgent_h_amt, '#,##0.00') r_urgent_h_amt, r_urgent_h_status FROM tb_r_urgent_h h INNER JOIN tb_m_proyek p ON p.m_proyek_id = h.m_proyek_id ) AS t";

            var sFixedFilter = "";
            var sOrder_by = " t.r_urgent_h_id DESC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res)
            {
                // simple remapping adding extra info to found dataset
                result.Add(s);
            };

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_r_urgent_h tbl;

            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_r_urgent_h();
                tbl.r_urgent_h_uid = Guid.NewGuid();
                ViewBag.t_usage_bbm_date = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                tbl.r_urgent_h_status = "In Process";
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_r_urgent_h.Find(id);
                ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == tbl.m_proyek_id).m_proyek_name;
                var r_urgent_d_pay_amt = 0M;
                var dt_d = db.tb_r_urgent_d.Where(x => x.r_urgent_h_id == tbl.r_urgent_h_id);
                if (dt_d != null && dt_d.Count() > 0) r_urgent_d_pay_amt = db.tb_r_urgent_d.Where(x => x.r_urgent_h_id == tbl.r_urgent_h_id).Sum(x => x.r_urgent_d_pay_amt);
                ViewBag.r_urgent_h_sisa_amt = tbl.r_urgent_h_amt - tbl.r_urgent_h_accum_amt /*- r_urgent_d_pay_amt*/;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl, action);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(int? r_urgent_h_id, decimal r_urgent_h_accum_amt_temp, decimal r_urgent_h_um_amt_temp, decimal r_urgent_h_utj_amt_temp, decimal r_urgent_h_lebih_tanah_temp, string r_urgent_h_faktur_no, List<r_urgent_d> dtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            
            if (dtl != null)
            {
                foreach (var item in dtl)
                {
                    if (string.IsNullOrEmpty(item.r_urgent_d_pay_type)) msg += "Silahkan isi tipe pembayaran!<br/>";
                    if (item.r_urgent_d_pay_amt <= 0) msg += "Jumlah pembayaran harus lebih besar dari 0!<br/>";
                    if (string.IsNullOrEmpty(item.r_urgent_d_pay_note)) msg += "Silahkan isi keterangan!<br/>";
                }
            }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (r_urgent_h_id != null)
                        {
                            var dt_h = db.tb_r_urgent_h.FirstOrDefault(x => x.r_urgent_h_id == r_urgent_h_id);
                            dt_h.r_urgent_h_faktur_no = r_urgent_h_faktur_no;
                            dt_h.r_urgent_h_accum_amt_temp = r_urgent_h_accum_amt_temp;
                            dt_h.r_urgent_h_um_amt_temp = r_urgent_h_um_amt_temp;
                            dt_h.r_urgent_h_utj_amt_temp = r_urgent_h_utj_amt_temp;
                            dt_h.r_urgent_h_lebih_tanah_temp = r_urgent_h_lebih_tanah_temp;
                            db.Entry(dt_h).State = EntityState.Modified;
                        }

                        var tbldtl = new List<tb_r_urgent_d>();
                        if (dtl != null)
                        {
                            foreach (var item in dtl)
                            {
                                var cek_data = db.tb_r_urgent_d.Where(a => a.r_urgent_d_id == item.r_urgent_d_id);
                                var new_dtl = (tb_r_urgent_d)ClassFunction.MappingTable(new tb_r_urgent_d(), item);
                                if (cek_data != null && cek_data.Count() > 0)
                                {
                                    new_dtl = db.tb_r_urgent_d.FirstOrDefault(a => a.r_urgent_d_id == item.r_urgent_d_id);
                                    new_dtl.r_urgent_d_pay_type = item.r_urgent_d_pay_type;
                                    new_dtl.r_urgent_d_transfer_date = item.r_urgent_d_transfer_date;
                                    new_dtl.r_urgent_d_pay_date = item.r_urgent_d_pay_date;
                                    new_dtl.r_urgent_d_pay_amt = item.r_urgent_d_pay_amt;
                                    new_dtl.r_urgent_d_pay_note = item.r_urgent_d_pay_note;
                                }
                                new_dtl.r_urgent_h_id = (int)r_urgent_h_id;
                                new_dtl.created_by = Session["UserID"].ToString();
                                new_dtl.created_at = ClassFunction.GetServerTime();

                                if (cek_data != null && cek_data.Count() > 0) db.Entry(new_dtl).State = EntityState.Modified;
                                else tbldtl.Add(new_dtl);
                            }
                        }
                        if (tbldtl != null && tbldtl.Count() > 0) db.tb_r_urgent_d.AddRange(tbldtl);
                        //cek data yg dihapus
                        var d_id = dtl.Select(s => s.r_urgent_d_id).ToList();
                        var dt_d = db.tb_r_urgent_d.Where(a => a.r_urgent_h_id == r_urgent_h_id && !d_id.Contains(a.r_urgent_d_id));
                        if (dt_d != null && dt_d.Count() > 0) db.tb_r_urgent_d.RemoveRange(dt_d);
                        db.SaveChanges();

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = r_urgent_h_id.ToString();
                        msg = "Data Already Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_r_urgent_h tbl = db.tb_r_urgent_h.Find(id);
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data tidak ditemukan!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.tb_r_urgent_h.Remove(tbl);
                        db.SaveChanges();

                        var trndtl = db.tb_r_urgent_d.Where(a => a.r_urgent_h_id == tbl.r_urgent_h_id);
                        db.tb_r_urgent_d.RemoveRange(trndtl); db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            string rptname = "KwitansiPembayaran";
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("userID", Session["UserID"].ToString());

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rpt" + rptname + ".rpt"));
            sSql = $"SELECT * FROM tb_v_print_urgent WHERE r_urgent_d_id IN ({ids}) ORDER BY r_urgent_d_id";
            var dt = new ClassConnection().GetDataTable(sSql, "datatable");
            report.SetDataSource(dt);
            if (rptparam.Count > 0)
                foreach (var item in rptparam)
                    report.SetParameterValue(item.Key, item.Value);

            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}