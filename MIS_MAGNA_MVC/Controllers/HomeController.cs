﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using MIS_MAGNA_MVC.Models;
using System.Linq.Expressions;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.GlobalFunctions; 
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
//using LinqKit;

namespace MIS_MAGNA_MVC.Controllers
{
    public class HomeController : Controller
    {
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class notifclass
        {
            public int notifqty { get; set; }
            public string notiftype { get; set; }
            public string notiftitle { get; set; }
            public string notifmsg { get; set; }
            public string notifaction { get; set; }
            public string notifctrl { get; set; }
            public string notifcss { get; set; }
            public string notiflink { get; set; }
        }

        [HttpPost]
        public ActionResult GetNotification(string userID)
        {
            List<notifclass> dtnotif = null; 
            var trnName = new List<string> { "MATERIAL REQUEST", "SALES ORDER", "SALES ORDER", "DELIVERY ORDER", "SALES INVOICE", "PR MATERIAL", "PO MATERIAL" };
            var notifctrl = new List<string> { "MatTransfer", "SOMaterial", "DeliveryOrder", "Shipment", "ARMaterial", "PO", "MR"};
            var notiftitle = new List<string> { "Transfer", "Update Estimate Shipment Date", "Delivery Order", "Shipment", "Posted", "Purchase Order", "Material Recieved" };
            var tblName = new List<string> { "QL_t_mreq_h", "QL_t_so_h", "QL_t_so_h", "QL_t_do_h", "QL_t_ar_h", "QL_t_pr_h", "QL_t_po_h" };
            var status = new List<string> { "('Post', 'Approved')", "('Post', 'Approved', 'Complete')", "('Post', 'Approved')", "('Post', 'Approved')", "('In Process')", "('Post', 'Approved')", "('Post', 'Approved')" };
            var addQuery = new List<string> { "", "", "", "", "", " AND t_pr_type='RAW MATERIAL'", " AND t_po_type='RAW MATERIAL'" };
            var notifaction = new List<string> { "Form", "Index", "Form", "Form", "Index" , "Form", "Form" }; 
            return Json(dtnotif, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "profile");
            return View();
        }
        
        [HttpPost]
        public ActionResult GetDataKBM()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT 0 r_piutang_seq, * FROM dbo.GetReportHarianMasuk(format(GETDATE(), 'MM/dd/yyyy 00:00:00'), format(GETDATE(), 'MM/dd/yyyy 23:59:59')) r Order By m_proyek_name, r_kb_date ";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl.Count > 0) {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataSPR()
        {
            JsonResult js = null;
            try
            {
                sSql = $"select 0 r_piutang_seq, spr.t_spr_no, spr.t_spr_date, p.m_proyek_code, c.m_cust_name, i.m_item_name, t.m_type_unit_name, o.m_item_name m_item_old_name, t1.m_type_unit_name m_type_old_unit_name, spr.t_spr_price from tb_t_spr spr Inner Join tb_m_item i ON i.m_item_id=spr.m_item_id Inner Join tb_m_type_unit t ON t.m_type_unit_id=i.m_item_type_unit_id Inner Join tb_m_cust c ON spr.m_cust_id=c.m_cust_id Inner Join tb_m_proyek p ON p.m_proyek_id=spr.m_proyek_id Inner Join tb_m_item o ON o.m_item_id=spr.m_item_old_id Inner Join tb_m_type_unit t1 ON t1.m_type_unit_id=o.m_item_type_unit_id Where m_item_old_id<>0 ";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl.Count > 0)
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataCust(string ref_code = "")
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT 0 r_piutang_seq, c.m_cust_id, c.m_cust_uid, c.m_cust_code, c.m_cust_name, c.m_cust_addr, c.m_cust_nik, c.m_cust_gender, m_cust_npwp, d.m_cust_old_name, d.last_edited_at, d.last_edited_by FROM tb_m_cust c Outer Apply (select top 1 * from tb_m_cust_hist h Where h.m_cust_id=c.m_cust_id Order By h.m_cust_d_id desc) d where 1=1 ";
                if (ref_code != "") sSql += $" AND c.m_cust_code='{ref_code}'";
                else sSql += " AND d.m_cust_old_name is not null";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl.Count > 0)
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult Chat()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetNotifContractso()
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "Select con.m_contract_name [Contract Dec], Format(con.m_contract_date1, 'dd/MM/yyyy') [Periode 1], Format(con.m_contract_date2, 'dd/MM/yyyy') [Periode 1], c.m_cust_code [Cust. Code], c.m_cust_name [Customer], i.m_item_code [Code], i.m_item_long_desc [Material], Convert(varchar,cast(con.m_contract_target_qty as money), 3) [Qty Target], Convert(varchar,cast(con.m_contract_accum_qty as money), 3) [Qty SO], Isnull(con.m_contract_note, '') [Note] From tb_m_contract con Inner Join tb_m_cust c ON c.m_cust_id=con.m_cust_id Inner Join tb_m_item i ON i.m_item_id=con.m_item_id Order By con.m_contract_date1";
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "data");
                if (tbl.Rows.Count > 0)
                {
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            //if (col.ColumnName == "Qty Target")
                            //    item = "<a href='" + Url.Action("Form", "MR") + item + "'>Create MR</a>";
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName)) tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data not found";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetNotifMaterialReceived()
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                if (!checkPagePermission("MR", (List<RoleDetail>)Session["Role"]))
                {
                    sSql = "SELECT * FROM (SELECT dbo.GetPrefixName('SUPPLIER', h.m_supp_id) [Supplier], t_po_no [PO No.], FORMAT(t_po_date, 'MM/dd/yyyy') [PO Date], t_po_h_note [Header Note], CONCAT(m_item_long_desc, ' ', m_composition, ' ', m_item_size) [Material Description], FORMAT(t_po_arr_date,'MM/dd/yyyy') [Arrival Date], FORMAT(DATEDIFF(D, FORMAT(GETDATE(),'MM/dd/yyyy'), FORMAT(t_po_arr_date,'MM/dd/yyyy')), '') + ' Days Later' [Days], '#' + FORMAT(h.t_po_h_id, '') + '#' + FORMAT(h.m_supp_id, '') + '#' + m_supp_name + '#' + t_po_type [Action] FROM QL_t_po_h h INNER JOIN QL_t_po_d d ON h.t_po_h_id=d.t_po_h_id INNER JOIN tb_m_item i ON i.m_item_id=d.t_po_item_ref_id INNER JOIN tb_m_supp s ON s.m_supp_id=h.m_supp_id WHERE t_po_h_status='Approved' AND ISNULL(t_po_d_status, '')='' AND DATEDIFF(D, FORMAT(t_po_arr_date,'MM/dd/yyyy'), FORMAT(GETDATE(),'MM/dd/yyyy')) BETWEEN -7 AND 0) dt ORDER BY [Days]";
                    DataTable tbl = new ClassConnection().GetDataTable(sSql, "data");
                    if (tbl.Rows.Count > 0)
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbl.Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in tbl.Columns)
                            {
                                var item = dr[col].ToString();
                                if (col.ColumnName == "Action") item = "<a href='" + Url.Action("Form", "MR") + item + "'>Create MR</a>";
                                row.Add(col.ColumnName, item);
                                if (!tblcols.Contains(col.ColumnName)) tblcols.Add(col.ColumnName);
                            }
                            tblrows.Add(row);
                        }
                    }
                    else
                        result = "Data not found";
                }
                else
                    result = "No Notif";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetNotifUpdate()
        {
            var result = "";
            JsonResult js = null;
            List<string> tbl = new List<string>();

            try
            {
                sSql = "SELECT (FORMAT(o_notif_date, 'dd MMM yyyy') + ' - ' + o_notif_note) notifnote FROM QL_o_notif WHERE DATEDIFF(DAY, o_notif_date, GETDATE()) BETWEEN 0 AND 6 ORDER BY o_notif_date, o_notif_id";
                tbl = db.Database.SqlQuery<string>(sSql).ToList();
                if (tbl.Count <= 0)
                    result = "Data tidak ditemukan";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string nomor, string tablename, int tableoid)
        {
            return Json(ShowCOAPostingNew(nomor, CompnyCode, tablename, tableoid), JsonRequestBehavior.AllowGet);
        }
    }
}