﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using static MIS_MAGNA_MVC.GlobalFunctions;

namespace MIS_MAGNA_MVC.Controllers
{
    public class MemoJurnalController : Controller
    {
        // GET: MemoJurnal
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        #region Memo Jurnal
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_t_memo_h", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column()
                {
                    name = "sAddFilter",
                    data = "sAddFilter",
                    search = new Search() { value = "", regex = "" },
                    searchable = true,
                    orderable = false
                };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( Select t_memo_h_id, t_memo_h_uid, m_company_id, t_memo_no, t_memo_date, t_memo_note, t_memo_status, created_by, created_at, updated_by, updated_at, posted_by, posted_at from tb_t_memo_h ) AS t";
            var sFixedFilter = "";
            var sOrder_by = " t.t_memo_no ASC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        private void InitDDLAndField(tb_t_memo_h tbl, string action)
        {
            //DDL
            var getCompany = db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList();
            if (action == "New Data") getCompany = getCompany.Where(m => m.flag == "ACTIVE").ToList();
            ViewBag.m_company_id = new SelectList(getCompany, "valuefield", "textfield", tbl.m_company_id); 
            //Field
            ViewBag.m_item_name = db.tb_m_item.FirstOrDefault(w => w.m_item_id == tbl.m_item_id)?.m_item_name ?? "";
            ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(w => w.m_proyek_id == tbl.m_proyek_id)?.m_proyek_name ?? ""; 
        }

        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            tb_t_memo_h tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_memo_h();
                tbl.t_memo_h_uid = Guid.NewGuid();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.t_memo_date = GetServerTime();
                tbl.t_memo_status = "In Process"; 
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_memo_h.FirstOrDefault(p => p.t_memo_h_uid == id);
            }
            if (tbl == null) return HttpNotFound();
            ViewBag.action = action;
            InitDDLAndField(tbl, action);
            return View(tbl);
        } 

        public ActionResult GenerateNo(DateTime t_memo_date, int m_company_id)
        {
            var sJson = GenerateTransNo($"MM", "t_memo_no", "tb_t_memo_h", m_company_id, t_memo_date.ToString("MM/dd/yyyy"));
            return Json(sJson);
        }

        [HttpPost]
        public ActionResult GetUnitData(int m_company_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT rp.m_cust_id, m_cust_name, rp.m_proyek_id, m_proyek_name, rp.m_item_id, m_item_name, SUM(Round(r_piutang_amt, 0)) r_piutang_amt, Isnull((Select SUM(r.r_piutang_pay_amt) From tb_r_piutang r Where r.m_item_id=rp.m_item_id AND r.m_proyek_id=rp.m_proyek_id AND r.t_spr_awal_id=rp.t_spr_awal_id AND r.r_piutang_type='Payment'), 0.0) r_piutang_pay_amt FROM tb_r_piutang rp INNER JOIN tb_m_cust c ON c.m_cust_id = rp.m_cust_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = rp.m_proyek_id INNER JOIN tb_m_item i ON i.m_item_id = rp.m_item_id WHERE rp.m_company_id={m_company_id} AND r_piutang_type!='Payment' AND r_piutang_status='Post' GROUP BY rp.m_cust_id, m_cust_name, rp.m_proyek_id, m_proyek_name, rp.m_item_id, m_item_name, rp.t_spr_awal_id ";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class t_memo_d : tb_t_memo_d
        {
            public string m_account_code { get; set; }
            public string m_account_name { get; set; }
        }

        public ActionResult GetDataDetails()
        {
            JsonResult js = null;
            try
            {
                List<string> interface_name = new List<string> { "JURNAL UMUM" };
                var coa_temp = GetCOAInterface(interface_name);
                List<t_memo_d> tbl = new List<t_memo_d>();
                if (coa_temp != null && coa_temp.Count > 0)
                {
                    foreach (var item in coa_temp)
                    {
                        tbl.Add(new t_memo_d
                        {
                            t_memo_seq = 0,
                            t_memo_ref_type = "",
                            t_memo_ref_id = 0,
                            t_memo_account_id = item.m_account_id,
                            m_account_code = item.m_account_code,
                            m_account_name = item.m_account_name,
                            t_memo_debet_amt = 0,
                            t_memo_credit_amt = 0,
                            t_memo_d_note ="",
                        });
                    }
                }
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult FillDetailData(int t_memo_h_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select d.t_memo_seq, d.t_memo_d_id, d.t_memo_account_id, a.m_account_code, a.m_account_desc m_account_name, d.t_memo_debet_amt, d.t_memo_credit_amt, ISNULL(d.t_memo_d_note, '') t_memo_d_note from tb_t_memo_d d Inner Join tb_m_account a On a.m_account_id = d.t_memo_account_id Where d.t_memo_h_id = {t_memo_h_id} order by t_memo_seq";
                var tbl = db.Database.SqlQuery<t_memo_d>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_memo_h tbl, List<t_memo_d> dtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";
            var servertime = GetServerTime();
            if (tbl.t_memo_date < GetCutOffdate())
                msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_memo_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />";
            decimal t_memo_debet_amt = 0m;
            decimal t_memo_credit_amt = 0m;
            if (string.IsNullOrEmpty(tbl.t_memo_note)) tbl.t_memo_note = ""; 
            if (dtl != null)
            {
                foreach (var item in dtl)
                {
                    t_memo_debet_amt += item.t_memo_debet_amt;
                    t_memo_credit_amt += item.t_memo_credit_amt;
                }
            }

            if (t_memo_debet_amt != t_memo_credit_amt) msg += "Nominal yang di input harus balance!<br/>";
            if(t_memo_debet_amt == 0 && t_memo_credit_amt==0) msg += "Nominal Total debet dan credit tidak boleh nol balance!<br/>";
            tbl.t_memo_no = GenerateTransNo($"MJ", "r_kb_no", "tb_r_kb", tbl.m_company_id, tbl.t_memo_date.ToString("MM/dd/yyyy"));
            if (tbl.t_memo_status == "Post")
            {
                tbl.t_memo_no = GenerateTransNo($"MJ", "t_memo_no", "tb_t_memo_h", tbl.m_company_id, tbl.t_memo_date.ToString("MM/dd/yyyy"));
                tbl.posted_at = servertime;
                tbl.posted_by = Session["UserID"].ToString();
            }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            db.tb_t_memo_h.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            //cek data yg dihapus
                            var d_id = dtl.Select(s => s.t_memo_d_id).ToList();
                            var dt_d = db.tb_t_memo_d.Where(a => a.t_memo_h_id == tbl.t_memo_h_id && !d_id.Contains(a.t_memo_h_id));
                            if (dt_d != null && dt_d.Count() > 0) db.tb_t_memo_d.RemoveRange(dt_d);
                        }
                        db.SaveChanges();

                        if (tbl.t_memo_status == "Post")
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_memo_h_uid, "tb_t_memo_h", tbl.t_memo_h_id, tbl.updated_by, servertime));

                        tb_t_memo_d tbldtl;
                        if (dtl != null)
                        {
                            foreach (var item in dtl)
                            {
                                var cek_data = db.tb_t_memo_d.Where(a => a.t_memo_d_id == item.t_memo_d_id);
                                tbldtl = (tb_t_memo_d)MappingTable(new tb_t_memo_d(), item);
                                if (cek_data != null && cek_data.Count() > 0)
                                    tbldtl = db.tb_t_memo_d.FirstOrDefault(a => a.t_memo_d_id == item.t_memo_d_id);
                                tbldtl.t_memo_h_id = tbl.t_memo_h_id;
                                tbldtl.t_memo_ref_id = 0;
                                tbldtl.t_memo_ref_type = item.t_memo_ref_type ?? "";
                                tbldtl.t_memo_d_note = item.t_memo_d_note ?? "";
                                tbldtl.updated_by = tbl.updated_by;
                                tbldtl.updated_at = servertime;

                                if (cek_data != null && cek_data.Count() > 0) db.Entry(tbldtl).State = EntityState.Modified;
                                else { db.tb_t_memo_d.Add(tbldtl); }
                                db.SaveChanges();
                            }
                        }

                        if (tbl.t_memo_status == "Post")
                        {
                            var seq = 1;
                            var tbl_note = $"MEMO (NO: {tbl.t_memo_no} | NOTE : {tbl.t_memo_note}";
                            foreach (var item in dtl)
                            {
                                if(item.t_memo_debet_amt> 0)
                                    db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, db.tb_m_account.Where(a => a.m_account_group == "PROYEK" && a.m_account_id == item.t_memo_account_id).Count() > 0 ? tbl.m_proyek_id : 0, tbl.m_item_id, seq++, "tb_t_memo_h", tbl.t_memo_h_id, tbl.t_memo_no, tbl.t_memo_date, item.t_memo_account_id, "D", item.t_memo_debet_amt, tbl_note +" | NOTE : "+ item.t_memo_d_note, tbl.created_by, servertime));

                                if (item.t_memo_credit_amt > 0)
                                    db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, db.tb_m_account.Where(a => a.m_account_group == "PROYEK" && a.m_account_id == item.t_memo_account_id).Count() > 0 ? tbl.m_proyek_id : 0, tbl.m_item_id, seq++, "tb_t_memo_h", tbl.t_memo_h_id, tbl.t_memo_no, tbl.t_memo_date, item.t_memo_account_id, "C", item.t_memo_credit_amt, tbl_note + " | NOTE : " + item.t_memo_d_note, tbl.created_by, servertime));
                            } 
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.t_memo_h_id.ToString();
                        if (tbl.t_memo_status == "Post")
                        {
                            sReturnNo = tbl.t_memo_no;
                            sReturnState = "terposting";
                        }
                        else
                        {
                            sReturnNo = "draft " + tbl.t_memo_h_id.ToString();
                            sReturnState = "tersimpan";
                        }
                        msg = "Data " + sReturnState + " dengan No. " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_t_memo_h tbl = db.tb_t_memo_h.Find(id);
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data tidak ditemukan!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.tb_t_memo_d.Where(a => a.t_memo_h_id == tbl.t_memo_h_id);
                        db.tb_t_memo_d.RemoveRange(trndtl); db.SaveChanges();

                        db.tb_t_memo_h.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
