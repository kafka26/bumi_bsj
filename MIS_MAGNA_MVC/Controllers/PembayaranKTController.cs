﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class PembayaranKTController : Controller
    {
        // GET: KBM
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public PembayaranKTController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        private void InitDDLAndField(tb_t_kbm_h tbl, string action)
        {
            //DDL
            var getCompany = db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList();
            if (action == "New Data")
                getCompany = getCompany.Where(m => m.flag == "ACTIVE").ToList();
            ViewBag.m_company_id = new SelectList(getCompany, "valuefield", "textfield", tbl.m_company_id);
            var sVar = "KAS_SISTEM";
            if (action == "Update Data")
            {
                if (tbl.t_kbm_type_bayar == "KM")
                    sVar = "KAS_SISTEM"; 
                else
                    sVar = "BANK_SISTEM";
            }
            var t_kbm_account_id = new SelectList(GetCOAInterface(new List<string> { sVar }), "m_account_id", "m_account_name", tbl.t_kbm_account_id);
            ViewBag.t_kbm_account_id = t_kbm_account_id;

            //Field
            ViewBag.m_item_name = db.tb_m_item.FirstOrDefault(w => w.m_item_id == tbl.m_item_id)?.m_item_name ?? "";
            ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(w => w.m_proyek_id == tbl.m_proyek_id)?.m_proyek_name ?? "";
            ViewBag.m_cust_name = db.tb_m_cust.FirstOrDefault(w => w.m_cust_id == tbl.m_cust_id)?.m_cust_name ?? "";
            ViewBag.t_refund_no = db.tb_t_refund.FirstOrDefault(w => w.t_refund_id == tbl.t_refund_id)?.t_refund_no ?? "";

            var r_piutang_amt = (tbl.m_cust_id != 0 ? db.Database.SqlQuery<decimal>($"SELECT ISNULL(ROUND(SUM(r_piutang_amt), 0), 0.0) FROM tb_r_piutang WHERE m_cust_id = {tbl.m_cust_id} AND m_item_id = {tbl.m_item_id} AND r_piutang_type = 'KT' AND r_piutang_status='Post'").FirstOrDefault() : 0);
            ViewBag.r_piutang_amt = r_piutang_amt;

            var r_piutang_pay_amt = (tbl.m_cust_id != 0 ? db.Database.SqlQuery<decimal>($"SELECT ISNULL(SUM(r_piutang_pay_amt), 0.0) FROM tb_r_piutang WHERE m_cust_id = {tbl.m_cust_id} AND m_item_id = {tbl.m_item_id} AND r_piutang_type = 'PAYMENT' AND r_piutang_status='Post' AND r_piutang_ref_table IN ('tb_t_spr_kt', 'tb_t_angsuran_kt_d') AND r_piutang_pay_ref_date <= '{tbl.t_kbm_date}'").FirstOrDefault() : 0);
            ViewBag.r_piutang_pay_amt = r_piutang_pay_amt;
            ViewBag.r_piutang_amt_sisa = r_piutang_amt - r_piutang_pay_amt;
        }

        [HttpPost]
        public ActionResult InitDDLAccount(string t_kbm_type_bayar, string action, string flag_tunai)
        {
            JsonResult js = null;
            try
            {
                var sVar = "";
                if (t_kbm_type_bayar == "KM") sVar = "KAS_SISTEM";
                else sVar = "BANK_SISTEM";

                var tbl = GetCOAInterface(new List<string> { sVar });
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        } 

        [HttpPost]
        public ActionResult GetUnitData(int m_company_id, string t_kbm_no)
        {
            JsonResult js = null;
            try
            {
                var trans_type = "KT"; var tb_t_spr = "tb_t_spr_kt";
                var tb_t_angsuran = "tb_t_angsuran_kt_d";
                sSql = $"Select * from dbo.GetDataKartu('{trans_type}', '{t_kbm_no}', '{tb_t_spr}', '{tb_t_angsuran}') r WHERE (Round(r.total_piutang, 0) - Round(r.r_piutang_pay_amt, 0)) > 0 ORDER BY m_cust_name";                
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() + sSql }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class t_kbm_d : tb_t_kbm_d
        {
            public string m_account_code { get; set; }
            public string m_account_name { get; set; }
            public DateTime r_piutang_ref_date { get; set; }
            public DateTime r_piutang_ref_due_date { get; set; }
            public string t_spr_no { get; set; }
            public decimal r_piutang_amt { get; set; }
            public string flag_tunai { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(int m_company_id, int m_item_id, int t_kbm_h_id, int m_cust_id, int t_spr_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT * FROM dbo.GetDtlBayar('KT', {t_kbm_h_id}, {m_company_id}, {m_cust_id}, {m_item_id}, {t_spr_id}) Order By r_piutang_ref_id ";
                var tbl = db.Database.SqlQuery<t_kbm_d>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select * from tb_v_Details_kbm WHERE t_kbm_h_id = {id} AND r_piutang_status='Post' AND t_spr_status='Post' Order By t_kbm_d_seq"; 
                var tbl = db.Database.SqlQuery<t_kbm_d>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            InitAdvFilterIndex(modfil, "tb_t_kbm_h", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE isapptrans=0 AND isposttrans=0 ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( SELECT h.t_kbm_uid, h.t_kbm_h_id, t_kbm_no, h.t_kbm_date, ISNULL((SELECT m_cust_name FROM tb_m_cust c WHERE c.m_cust_id=h.m_cust_id),'') m_cust_name, ISNULL((SELECT m_item_name FROM tb_m_item i WHERE i.m_item_id=h.m_item_id),'') m_unit_name, ISNULL((SELECT m_account_desc FROM tb_m_account i WHERE i.m_account_id=h.t_kbm_account_id),'') m_account_name, t_kbm_ref_no, t_kbm_h_note, t_kbm_h_status, created_at FROM tb_t_kbm_h h WHERE ISNULL((SELECT TOP 1 t_kbm_ref_table FROM tb_t_kbm_d WHERE t_kbm_h_id = h.t_kbm_h_id), '') IN ('tb_t_angsuran_kt_d','tb_t_spr_kt') AND h.m_item_id IN (select m_item_id from tb_r_piutang r WHere h.m_item_id=r.m_item_id AND r_piutang_ref_table IN ('tb_t_angsuran_kt_d','tb_t_spr_kt')) AND t_kbm_h_status != 'Force Closed' ) AS t ";

            var sFixedFilter = "";
            var sOrder_by = " t.created_at DESC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_t_kbm_h tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_kbm_h();
                tbl.t_kbm_uid = Guid.NewGuid();
                tbl.t_kbm_date = GetServerTime();
                tbl.created_at = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.t_kbm_h_status = "In Process";
                tbl.t_kbm_ref_type = "KT";
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_kbm_h.Find(id);
            }

            if (tbl == null) return HttpNotFound();
            ViewBag.flag_tunai = "";
            ViewBag.action = action;
            InitDDLAndField(tbl, action);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_kbm_h tbl, List<t_kbm_d> dtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";
            var servertime = GetServerTime();
            if (tbl.t_kbm_date < GetCutOffdate())
                msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_kbm_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />";
            if (tbl.m_item_id == 0) msg += "Silahkan Pilih Unit!<br/>";
            if (tbl.t_kbm_account_id == 0) msg += "Silahkan Pilih Akun!<br/>";

            if (string.IsNullOrEmpty(tbl.t_kbm_no)) tbl.t_kbm_no = "";
            if (string.IsNullOrEmpty(tbl.t_kbm_ref_no)) tbl.t_kbm_ref_no = "";
            if (string.IsNullOrEmpty(tbl.t_kbm_h_note)) tbl.t_kbm_h_note = "";

            if (dtl != null)
            {
                foreach (var item in dtl)
                {
                    if (item.t_kbm_d_account_id == 0) msg += "Akun Tidak Tersedia!<br/>";
                    if (item.t_kbm_d_amt == 0) msg += "Jumlah Tidah boleh 0!<br/>";
                    if (tbl.t_kbm_h_status == "Post")
                    {
                        var sisa_piutang = db.tb_r_piutang.Where(x => x.r_piutang_ref_id == item.t_kbm_ref_id && x.r_piutang_ref_table == item.t_kbm_ref_table && x.r_piutang_status == "Post").Sum(y => y.r_piutang_amt - y.r_piutang_pay_amt);
                        if (sisa_piutang <= 0)
                            msg += $"keterangan {item.t_kbm_d_note} sudah tidak ada sisa tagihan, silahkan pilih angsuran yg lain!<br/>";
                    }
                }
            }

            if (tbl.t_kbm_h_status == "Post")
            {
                tbl.t_kbm_no = GenerateTransNo($"{tbl.t_kbm_type_bayar}", "r_kb_no", "tb_r_kb", tbl.m_company_id, tbl.t_kbm_date.ToString("MM/dd/yyyy"));
                tbl.posted_at = servertime;
                tbl.posted_by = Session["UserID"].ToString();
            }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            db.tb_t_kbm_h.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            //cek data yg dihapus
                            var d_id = dtl.Select(s => s.t_kbm_d_id).ToList();
                            var dt_d = db.tb_t_kbm_d.Where(a => a.t_kbm_h_id == tbl.t_kbm_h_id && !d_id.Contains(a.t_kbm_d_id));
                            if (dt_d != null && dt_d.Count() > 0) db.tb_t_kbm_d.RemoveRange(dt_d);
                        }
                        db.SaveChanges();

                        if (tbl.t_kbm_h_status == "Post")
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_kbm_uid, "tb_t_kbm_h", tbl.t_kbm_h_id, tbl.updated_by, tbl.updated_at));

                        tb_t_kbm_d tbldtl;
                        if (dtl != null)
                        {
                            foreach (var item in dtl)
                            {
                                var cek_data = db.tb_t_kbm_d.Where(a => a.t_kbm_d_id == item.t_kbm_d_id);
                                tbldtl = (tb_t_kbm_d)MappingTable(new tb_t_kbm_d(), item);
                                if (cek_data != null && cek_data.Count() > 0)
                                    tbldtl = db.tb_t_kbm_d.FirstOrDefault(a => a.t_kbm_d_id == item.t_kbm_d_id);
                                tbldtl.t_kbm_h_id = tbl.t_kbm_h_id;
                                tbldtl.t_kbm_d_note = item.t_kbm_d_note ?? "";
                                tbldtl.created_by = tbl.updated_by;
                                tbldtl.created_at = tbl.updated_at;
                                tbldtl.t_kbm_d_netto = item.t_kbm_d_amt - item.t_kbm_disc_amt;
                                if (cek_data != null && cek_data.Count() > 0) db.Entry(tbldtl).State = EntityState.Modified;
                                else { db.tb_t_kbm_d.Add(tbldtl); }
                                db.SaveChanges();

                                if (tbl.t_kbm_h_status == "Post")
                                {
                                    db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "PAYMENT", item.t_kbm_ref_table, item.t_kbm_ref_id, item.t_spr_no, item.r_piutang_ref_date, item.r_piutang_ref_due_date, tbl.t_kbm_date, item.t_kbm_d_account_id, 0m, item.t_kbm_d_amt, item.t_kbm_d_note ?? "", tbl.created_by, tbl.created_at, "tb_t_kbm_d", tbldtl.t_kbm_d_id, tbl.t_kbm_no, tbl.t_kbm_account_id, item.t_kbm_disc_amt, tbl.t_spr_id, tbl.t_spr_awal_id));

                                    db.tb_r_kb.Add(Insert_R_KB(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "tb_t_kbm_d", tbldtl.t_kbm_d_id, tbl.t_kbm_type_bayar, tbl.t_kbm_no, tbl.t_kbm_date, tbl.t_kbm_account_id, item.t_kbm_d_account_id, item.t_kbm_d_amt - item.t_kbm_disc_amt, 0m, item.t_kbm_d_note ?? "", tbl.created_by, tbl.created_at, tbl.t_spr_id, tbl.t_spr_awal_id, tbl.t_kbm_h_id, tbl.t_kbm_type));
                                    db.SaveChanges();
                                }
                            }

                            // INSERT AUTO JURNAL 
                            if (tbl.t_kbm_h_status == "Post")
                            {
                                var seq = 1;
                                var tbl_note = "Pembayaran Angsuran KT";
                                db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbm_h", tbl.t_kbm_h_id, tbl.t_kbm_no, tbl.t_kbm_date, tbl.t_kbm_account_id, "D", tbl.t_kbm_h_amt, tbl_note ?? "", tbl.created_by, tbl.created_at, tbl.m_cust_id));
                                foreach (var item in dtl)
                                {
                                    if (item.t_kbm_disc_amt > 0)
                                    {
                                        var m_account_id = GetCOAInterface(new List<string> { "POTONGAN KT" }).ToList().FirstOrDefault().m_account_id;
                                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbm_h", tbl.t_kbm_h_id, tbl.t_kbm_no, tbl.t_kbm_date, m_account_id, "D", item.t_kbm_disc_amt, item.t_kbm_d_note ?? "", tbl.created_by, tbl.created_at, tbl.m_cust_id));
                                    }

                                    db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbm_h", tbl.t_kbm_h_id, tbl.t_kbm_no, tbl.t_kbm_date, item.t_kbm_d_account_id, "C", item.t_kbm_d_amt, item.t_kbm_d_note ?? "", tbl.created_by, tbl.created_at, tbl.m_cust_id));
                                }
                                db.SaveChanges();
                            }
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.t_kbm_h_id.ToString();
                        if (tbl.t_kbm_h_status == "Post")
                        {
                            sReturnNo = tbl.t_kbm_no;
                            sReturnState = "terposting";
                        }
                        else
                        {
                            sReturnNo = "draft " + tbl.t_kbm_h_id.ToString();
                            sReturnState = "tersimpan";
                        }
                        msg = "Data " + sReturnState + " dengan No. " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_t_kbm_h tbl = db.tb_t_kbm_h.Find(id);
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data tidak ditemukan!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.tb_t_kbm_d.Where(a => a.t_kbm_h_id == tbl.t_kbm_h_id);
                        db.tb_t_kbm_d.RemoveRange(trndtl); db.SaveChanges();

                        db.tb_t_kbm_h.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            string rptname = "PrintKBM";
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("userID", Session["UserID"].ToString());

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rpt" + rptname + ".rpt"));
            sSql = $"SELECT * FROM tb_v_print_kbm WHERE t_kbm_h_id IN ({ids}) ORDER BY t_kbm_h_id";
            var dt = new ClassConnection().GetDataTable(sSql, "datatable");
            report.SetDataSource(dt);
            if (rptparam.Count > 0)
                foreach (var item in rptparam)
                    report.SetParameterValue(item.Key, item.Value);

            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}