﻿using MIS_MAGNA_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MIS_MAGNA_MVC.Controllers
{
    public class ClassAdvanceFilter
    {
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        public ModelFilter modfil { get; private set; }
        public string tblname { get; private set; }
        public bool isapproval { get; private set; }
        public SelectList filterddl { get; private set; }
        public string filtertext { get; private set; }
        public bool isperiodchecked { get; private set; }
        public DateTime filterperiodfrom { get; private set; }
        public DateTime filterperiodto { get; private set; }
        public SelectList filterstatus { get; private set; }

        public ClassAdvanceFilter(ModelFilter mfModalFilter, string sTableName, bool bIsApproval)
        {
            this.modfil = mfModalFilter;
            this.tblname = sTableName;
            this.isapproval = bIsApproval;

            filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT fieldlabel valuefield, tblfield textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);

            filtertext = modfil.filtertext;

            isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("dd/MM/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            filterperiodfrom = modfil.filterperiodfrom;

            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("dd/MM/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            filterperiodto = modfil.filterperiodto;

            filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
        }
    }
}