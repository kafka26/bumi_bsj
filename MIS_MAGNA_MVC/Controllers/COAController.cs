﻿using CrystalDecisions.CrystalReports.Engine;
using MIS_MAGNA_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc; 
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class COAController : Controller
    {
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class listgroup
        {
            public int groupoid { get; set; }
            public string groupdesc { get; set; }
        }

        #region Master COA

        
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            InitAdvFilterIndex(modfil, "tb_m_employee", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column()
                {
                    name = "sAddFilter",
                    data = "sAddFilter",
                    search = new Search()
                    {
                        value = "",
                        regex = ""
                    },
                    searchable = true,
                    orderable = false
                };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM( SELECT m_account_uid, m_account_id, m_account_code, m_account_desc, m_account_type, active_flag, created_at, created_by, last_edited_at, last_edited_by FROM tb_m_account a ) AS t";

            var sFixedFilter = "";
            var sOrder_by = " t.m_account_code";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res)
            {
                // simple remapping adding extra info to found dataset
                result.Add(s);
            };

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        private void FillAdditionalField(tb_m_account tbl)
        {
            ViewBag.m_company_id = new SelectList(db.tb_m_company.Where(a => a.active_flag == "ACTIVE").ToList(), "m_company_id", "m_company_name", tbl.m_company_id);

            var iParentOid = tbl.m_account_parent_id;
            if (iParentOid != 0)
            {
                var ParentCOA = (from a in db.tb_m_account
                                 where a.m_account_id == iParentOid
                                 select new
                                 {
                                     parentcode = a.m_account_code,
                                     parentdesc = a.m_account_desc
                                 }).FirstOrDefault();
                ViewBag.acctgparentcode = ParentCOA.parentcode;
                ViewBag.acctgparentdesc = ParentCOA.parentdesc;
                ViewBag.acctgcode1 = ParentCOA.parentcode;
                tbl.m_account_code = tbl.m_account_code.Replace(ParentCOA.parentcode, "");
            }
        } 

        // GET: COA/Create
        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_m_account tbl = new tb_m_account();
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_m_account();
                tbl.m_account_uid = Guid.NewGuid();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.m_curr_id = 1;
                tbl.m_account_level = 1;
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_m_account.Find(id);
            }

            if (tbl == null)
                return HttpNotFound();

            ViewBag.action = action;
            FillAdditionalField(tbl);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_m_account tbl, string action, string acctgcode2)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            var account_code = acctgcode2;
            tbl.m_account_group = "-";
            if (action == "New Data")
            {
                if (db.tb_m_account.Where(a => a.m_account_code == account_code).Count() > 0)
                    msg += " - This Code have been used before!";
            }
            else
            {
                if (db.tb_m_account.Where(a => a.m_account_code == account_code & a.m_account_id != tbl.m_account_id).Count() > 0)
                    msg += "This Code have been used before!";
            }
            if (string.IsNullOrEmpty(tbl.m_account_note)) tbl.m_account_note = "";

            if (msg == "")
            {
                tbl.m_account_code = account_code;
                tbl.m_account_dbcr = "-";
                tbl.m_account_report_lvl = "0";

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.last_edited_at = tbl.created_at;
                            tbl.last_edited_by = tbl.created_by;
                            db.tb_m_account.Add(tbl);
                        }
                        else
                        {
                            tbl.last_edited_at = GetServerTime();
                            tbl.last_edited_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.m_account_id.ToString();
                        msg = "Data Already Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid? m_account_uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_m_account list = db.tb_m_account.Find(m_account_uid);

            string result = "success";
            string msg = "";

            //var tbl_usage = db.GetTableUsage(new List<string> { "m_account_id", "m_account_1_id" }, new List<string> { "tb_m_account" });
            //if (tbl_usage != null && tbl_usage.Count() > 0)
            //{
            //    foreach (var item in tbl_usage)
            //    {
            //        var check_data = db.Database.SqlQuery<int>($"SELECT COUNT(*) FROM {item.table_name} WHERE {item.column_name}={list.m_account_id}").FirstOrDefault();
            //        if (check_data > 0)
            //        {
            //            msg += $"this data can't be delete, because using in another transaction!<br />";
            //            result = "failed";
            //        }
            //    }
            //}

            if (list == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.tb_m_account.Remove(list);
                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetParentCOA(string m_account_type)
        {
            List<tb_m_account> tbl = new List<tb_m_account>();
            sSql = "SELECT * FROM tb_m_account WHERE active_flag='ACTIVE' AND m_account_type='" + m_account_type + "' ORDER BY m_account_code";
            tbl = db.Database.SqlQuery<tb_m_account>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        #endregion

        #region Print Out
        public ActionResult PrintReport(string stype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptCOA.rpt"));

            sSql = "SELECT m_account_uid, m_account_id, m_account_code, m_account_desc, m_account_type, active_flag, created_at, created_by, last_edited_at, last_edited_by FROM tb_m_account a Order By m_account_code";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "tb_m_account");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("sHeader", "CHART OF ACCOUNT");
            report.SetParameterValue("userID", Session["UserID"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            if (stype == "PDF")
            {
                Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", "ChartOfAccount.pdf");
            }
            else
            {
                Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/excel", "ChartOfAccount.xls");
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
        #endregion
    }
}