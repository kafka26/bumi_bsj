﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace MIS_MAGNA_MVC.Controllers
{
    public class FormFilterController : Controller
    {
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class formfilter
        {
            public int seq { get; set; }
            public string tblname { get; set; }
            public string tblfield { get; set; }
            public string fieldlabel { get; set; }
        }

        private void InitDDL(string tablename)
        {
            sSql = "SELECT name valuefield, name textfield FROM sys.tables WHERE (name LIKE 'QL_mst%' OR name LIKE'tb_m_%' OR name LIKE'QL_t_%') AND name NOT LIKE '%dtl%' AND name NOT LIKE '%_d' ORDER BY textfield";
            var tblname = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>(sSql).ToList(), "valuefield", "textfield", tablename);
            ViewBag.tblname = tblname;
        }

        // GET: Role
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            return View();
        }

        [HttpPost]
        public ActionResult GetDataList()
        {
            var msg = "";

            sSql = "SELECT tblname, tblname [Table Name], COUNT(-1) [Field Count] FROM tb_formfilterddl r GROUP BY tblname";

            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tb_formfilterddl");

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "Table Name")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + dr["tblname"].ToString() + "/" + CompnyCode, "FormFilter") + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    JsonResult js = Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                    return js;
                }
                else
                    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // GET: Role/Form/id
        public ActionResult Form(string id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            string action = "New Data";
            List<formfilter> tbl = null;
            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(cmp))
            {
                tbl = new List<formfilter>();
                Session["tb_formfilterddl"] = null;
            }
            else
            {
                action = "Update Data";
                sSql = "SELECT 0 seq, tblname, tblfield, fieldlabel FROM tb_formfilterddl WHERE tblname='" + id + "'";
                tbl = db.Database.SqlQuery<formfilter>(sSql).ToList();
                for (int i = 0; i < tbl.Count; i++)
                {
                    tbl[i].seq = i + 1;
                }

                Session["tb_formfilterddl"] = tbl;
            }

            if (tbl == null)
                return HttpNotFound();

            ViewBag.action = action;
            InitDDL(id);
            return View(tbl);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string tablename)
        {
            var result = "";
            JsonResult js = null;
            List<formfilter> tbl = new List<formfilter>();
            try
            {
                sSql = "SELECT 0 seq, col.name tblfield, '' fieldlabel FROM sys.columns col INNER JOIN sys.objects obj ON col.object_id=obj.object_id WHERE obj.name='" + tablename + "' ORDER BY obj.name, col.column_id";
                tbl = db.Database.SqlQuery<formfilter>(sSql).ToList();
                for (int i = 0; i < tbl.Count; i++)
                {
                    tbl[i].seq = i + 1;
                }
                if (tbl.Count == 0)
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<formfilter> tbl)
        {
            Session["tb_formfilterddl"] = tbl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["tb_formfilterddl"] == null)
            {
                Session["tb_formfilterddl"] = new List<formfilter>();
            }

            List<formfilter> tbl = (List<formfilter>)Session["tb_formfilterddl"];
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // POST: Role/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_formfilterddl tbl, string action, string tblname)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            List<formfilter> dtDtl = (List<formfilter>)Session["tb_formfilterddl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            if (dtDtl != null || dtDtl.Count() > 0)
            {
                for (var i = 0; i < dtDtl.Count(); i++)
                {
                    if (dtDtl[i].tblfield == "" || dtDtl[i].tblfield == null)
                        ModelState.AddModelError("", "Please fill Field Name!");
                    if (dtDtl[i].fieldlabel == "" || dtDtl[i].fieldlabel == null)
                        ModelState.AddModelError("", "Please fill Label Name!");
                }
            }

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var icheck = db.tb_formfilterddl.Count();
                        tbl.oid = 0;
                        if (icheck > 0)
                            tbl.oid = db.tb_formfilterddl.Max(m => m.oid) + 1;
                        if (action == "Update Data")
                            db.tb_formfilterddl.RemoveRange(db.tb_formfilterddl.Where(a => a.tblname == tblname));
                        tb_formfilterddl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new tb_formfilterddl();
                            tbldtl.oid = tbl.oid++;
                            tbldtl.tblname = tbl.tblname;
                            tbldtl.tblfield = dtDtl[i].tblfield;
                            tbldtl.fieldlabel = dtDtl[i].fieldlabel;
                            db.tb_formfilterddl.Add(tbldtl);
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "";
                            }
                        }
                        ModelState.AddModelError("", err);
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl.tblname);
            return View(tbl);
        }

        // POST: PurchaseReturnService/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string tablename)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            string result = "success";
            string msg = "";

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.tb_formfilterddl.Where(a => a.tblname == tablename);
                        db.tb_formfilterddl.RemoveRange(trndtl);

                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}