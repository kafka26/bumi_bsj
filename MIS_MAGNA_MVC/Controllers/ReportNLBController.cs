﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using System.Xml.Linq;
using Microsoft.AspNet.SignalR;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Web.Services.Description;
using System.Xml;

namespace MIS_MAGNA_MVC.Controllers
{
    public class ReportNLBController : Controller
    {
        // GET: ReportNLB
        private QL_MIS_MAGNAEntities db;
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        public ReportNLBController() { db = new QL_MIS_MAGNAEntities(); db.Database.CommandTimeout = 0; }

        public class sFilter
        {
            public string ddlyear { get; set; }
            public string ddlmonth { get; set; }
            public string bulan { get; set; }
            public string ddltype { get; set; }
            public string r_kb_kelompok { get; set; }
            public decimal r_kb_saldo { get; set; }
        }
         
        #region Laporan Neraca
        public ActionResult Neraca()
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Neraca", (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile"); 
            ViewBag.ddlyear = new SelectList(GetTahun(), "Value", "Text", GetServerTime().ToString("yyyy"));
            ViewBag.ddlmonth = new SelectList(GetBulan(), "Value", "Text", GetServerTime().ToString("MM"));
            return View();
        } 

        private List<m_coa> getData(sFilter param)
        {
            sSql = $@"Declare @sPeriodeMonthYear as Varchar(30); Declare @sPeriodeYear as Varchar(30);
            set @sPeriodeMonthYear='{param.ddlyear}{param.ddlmonth}' set @sPeriodeYear='{param.ddlyear}'
            Select * from (
                select m_account_id coa_id, m_account_code coa_code, m_account_desc coa_name
                , m_account_type coa_type, group2, group1, '' t_periode, '' grp1code, '' grp2code, 0.0 total_aktiva, 0.0 total_pasiva
		        , case when a.m_account_code='3301' then /*Laba di tahan*/ (amt_gl) +
                Isnull(( 
	                SELECT SUM(ISNULL(gl.r_gl_debet_amt, 0.0) - ISNULL(gl.r_gl_credit_amt, 0.0)) * (-1) amt FROM tb_m_account ax
	                Outer Apply (	 
		                SELECT rx.r_gl_debet_amt, rx.r_gl_credit_amt FROM tb_r_gl rx
		                WHERE rx.r_gl_account_id=ax.m_account_id and
		                format(rx.r_gl_date, 'yyyy') < cast(@sPeriodeYear as int) 
                ) AS gl 
                WHERE ax.m_account_id NOT IN ( SELECT x.m_account_parent_id FROM tb_m_account x) AND ax.m_account_report_type = 'LABA RUGI'
                ), 0.0)
                when a.m_account_code='3302' then /*Laba Tahun Berjalan*/
                    (
	                Isnull(( 
		                SELECT SUM(ISNULL(gl.r_gl_debet_amt, 0.0) - ISNULL(gl.r_gl_credit_amt, 0.0)) * (-1) amt 
		                FROM tb_m_account ax
		                Outer Apply (	 
			                SELECT rx.r_gl_debet_amt, rx.r_gl_credit_amt FROM tb_r_gl rx where
			                cast(format(rx.r_gl_date, 'yyyy') as int) = cast(@sPeriodeYear as int) and
			                cast(format(rx.r_gl_date, 'yyyyMM') as int) < cast(@sPeriodeMonthYear as int)
			                and rx.r_gl_account_id=ax.m_account_id
	                ) AS gl 
	                WHERE ax.m_account_id NOT IN ( SELECT x.m_account_parent_id FROM tb_m_account x) AND ax.m_account_report_type = 'LABA RUGI'
	                ), 0.0) 
                    )
                    when a.m_account_code='3303' then /*Laba Bulan Ini*/
                    (
	                Isnull(( 
		                SELECT SUM(ISNULL(gl.r_gl_debet_amt, 0.0) - ISNULL(gl.r_gl_credit_amt, 0.0)) * (-1) amt 
		                FROM tb_m_account ax
		                Outer Apply (	 
			                SELECT rx.r_gl_debet_amt, rx.r_gl_credit_amt FROM tb_r_gl rx
			                WHERE cast(format(rx.r_gl_date, 'yyyyMM') as int) = cast(@sPeriodeMonthYear as int)
			                and rx.r_gl_account_id=ax.m_account_id
	                ) AS gl 
	                WHERE ax.m_account_id NOT IN ( SELECT x.m_account_parent_id FROM tb_m_account x) AND ax.m_account_report_type = 'LABA RUGI'
	                ), 0.0) 
                    ) else amt_gl End amt 
                from (
	                select a.m_account_id, a.m_account_code, a.m_account_desc, a.m_account_type 
	                , ISNULL((SELECT x1.m_account_desc FROM tb_m_account x1 WHERE x1.m_account_id = (SELECT x.m_account_parent_id FROM tb_m_account x WHERE x.m_account_id = a.m_account_parent_id)), '') group1
	                , ISNULL((SELECT x.m_account_desc FROM tb_m_account x WHERE x.m_account_id = a.m_account_parent_id), '') group2
	                , iif(a.m_account_group='UMUM', '', isnull(m_proyek_code, '')) m_proyek_code
	                , isnull((sum(r.r_gl_debet_amt) - sum(r.r_gl_credit_amt)), 0.0) * iif(a.m_account_type='AKTIVA', 1, -1 ) amt_gl 
	                from tb_m_account a 
	                outer apply ( 
		                select (select x.m_proyek_code from tb_m_proyek x where x.m_proyek_id=r.m_proyek_id) m_proyek_code, r.r_gl_debet_amt, r.r_gl_credit_amt
		                from tb_r_gl r where r.r_gl_account_id=a.m_account_id and Format(r_gl_date, 'yyyyMM')<=cast(@sPeriodeMonthYear as int)
	                ) r	where a.m_account_report_type='NERACA' and a.m_account_id NOT IN (SELECT x.m_account_parent_id FROM tb_m_account x) AND a.m_account_report_type IN ('NERACA')  
	                Group By a.m_account_id, a.m_account_code, a.m_account_desc, iif(a.m_account_group='UMUM', '', isnull(m_proyek_code, '')), a.m_account_type, a.m_account_parent_id
                ) a 
                union all
                select 0 coa_id, '' coa_code, '' coa_name, 'AKTIVA' coa_type, 'saldo' group2, '' group1
                , '' t_periode, '' grp1code, '' grp2code, 0.0 total_aktiva, 0.0 total_pasiva, SUM(gl.r_gl_debet_amt)-sum(gl.r_gl_credit_amt) amt
                from tb_r_gl gl inner join tb_m_account xx on xx.m_account_id=gl.r_gl_account_id 
                where format(gl.r_gl_date, 'yyyy') <= cast(@sPeriodeYear as int) and xx.m_account_code='3302'
            ) r order by coa_code "; 
            var tbl = db.Database.SqlQuery<m_coa>(sSql).ToList();
            return tbl;
        }

        public static string raw_query(sFilter param)
        {
            var sSql = $@"Declare @sPeriodeMonthYear as Varchar(30); Declare @sPeriodeYear as Varchar(30);
            set @sPeriodeMonthYear='{param.ddlyear}{param.ddlmonth}' set @sPeriodeYear='{param.ddlyear}'
            Select * from (
                select m_account_id coa_id, m_account_code coa_code, Concat(m_account_desc,' ',m_proyek_code) coa_name
                , m_account_type coa_type, group2, group1, '' t_periode, '' grp1code, '' grp2code, 0.0 total_aktiva, 0.0 total_pasiva
		        , case when a.m_account_code='3301' then /*Laba di tahan*/ isnull(amt_gl, 0.0) +
                Isnull(( 
	                SELECT SUM(ISNULL(gl.r_gl_debet_amt, 0.0) - ISNULL(gl.r_gl_credit_amt, 0.0)) * (-1) amt FROM tb_m_account ax
	                Outer Apply (	 
		                SELECT rx.r_gl_debet_amt, rx.r_gl_credit_amt FROM tb_r_gl rx
		                WHERE rx.r_gl_account_id=ax.m_account_id and
		                format(rx.r_gl_date, 'yyyy') < cast(@sPeriodeYear as int) 
                    ) AS gl 
                    WHERE ax.m_account_id NOT IN ( SELECT x.m_account_parent_id FROM tb_m_account x) AND ax.m_account_report_type = 'LABA RUGI'
                    ), 0.0)
                when a.m_account_code='3302' then /*Laba Tahun Berjalan*/
                    isnull(amt_gl, 0.0) + (
	                    Isnull(( 
		                    SELECT SUM(ISNULL(gl.r_gl_debet_amt, 0.0) - ISNULL(gl.r_gl_credit_amt, 0.0)) * (-1) amt 
		                    FROM tb_m_account ax
		                    Outer Apply (	 
			                    SELECT rx.r_gl_debet_amt, rx.r_gl_credit_amt FROM tb_r_gl rx where
			                    cast(format(rx.r_gl_date, 'yyyy') as int) = cast(@sPeriodeYear as int) and
			                    cast(format(rx.r_gl_date, 'yyyyMM') as int) <= cast(@sPeriodeMonthYear as int)
			                    and rx.r_gl_account_id=ax.m_account_id
	                    ) AS gl 
	                    WHERE ax.m_account_id NOT IN ( SELECT x.m_account_parent_id FROM tb_m_account x) AND ax.m_account_report_type = 'LABA RUGI'
	                    ), 0.0) 
                    )
                when a.m_account_code='3303' then /*Laba Bulan Ini*/ 0.0 else amt_gl End amt
 
                from (
	                select a.m_account_id, a.m_account_code, a.m_account_desc, a.m_account_type 
	                , ISNULL((SELECT x1.m_account_desc FROM tb_m_account x1 WHERE x1.m_account_id = (SELECT x.m_account_parent_id FROM tb_m_account x WHERE x.m_account_id = a.m_account_parent_id)), '') group1
	                , ISNULL((SELECT x.m_account_desc FROM tb_m_account x WHERE x.m_account_id = a.m_account_parent_id), '') group2
	                , iif(a.m_account_group='UMUM', '', isnull(m_proyek_code, '')) m_proyek_code
	                , isnull((sum(r.r_gl_debet_amt) - sum(r.r_gl_credit_amt)), 0.0) * iif(a.m_account_type='AKTIVA', 1, -1 ) amt_gl 
	                from tb_m_account a 
	                outer apply ( 
		                select (select x.m_proyek_code from tb_m_proyek x where x.m_proyek_id=r.m_proyek_id) m_proyek_code, r.r_gl_debet_amt, r.r_gl_credit_amt
		                from tb_r_gl r where r.r_gl_account_id=a.m_account_id and Format(r_gl_date, 'yyyyMM')<=cast(@sPeriodeMonthYear as int)
	                ) r	where a.m_account_report_type='NERACA' and a.m_account_id NOT IN (SELECT x.m_account_parent_id FROM tb_m_account x) AND a.m_account_report_type IN ('NERACA')  
	                Group By a.m_account_id, a.m_account_code, a.m_account_desc, iif(a.m_account_group='UMUM', '', isnull(m_proyek_code, '')), a.m_account_type, a.m_account_parent_id
                ) a 
                /*union all
                select 0 coa_id, '' coa_code, '' coa_name, 'AKTIVA' coa_type, 'saldo' group2, '' group1
                , '' t_periode, '' grp1code, '' grp2code, 0.0 total_aktiva, 0.0 total_pasiva, SUM(gl.r_gl_debet_amt)-sum(gl.r_gl_credit_amt) amt
                from tb_r_gl gl inner join tb_m_account xx on xx.m_account_id=gl.r_gl_account_id 
                where format(gl.r_gl_date, 'yyyy') <= cast(@sPeriodeYear as int) and xx.m_account_code='3302'*/
            ) r order by coa_code";
            return sSql;
        }

        public class m_coa
        {
            public int coa_id { get; set; } 
            public Decimal amt { get; set; }
            public Decimal total_aktiva { get; set; }
            public Decimal total_pasiva { get; set; }
            public String group1 { get; set; }
            public String group2 {  get; set; }
            public String coa_type { get; set; }
            public String coa_code { get; set; }
            public String coa_name { get; set; } 
        }

        private DataTable GetDT(DataTable dtRpt, List<m_coa> tbl)
        {
            DataRow addRow = dtRpt.NewRow();
            var tbl_aktiva = tbl.Where(x => x.coa_type == "AKTIVA").GroupBy(x => new { coa_type = x.coa_type });
            foreach (var item in tbl_aktiva)
            {
                addRow["m_account_type"] = item.Key.coa_type; 
                addRow["m_account_desc"] = $"TOTAL {item.Key.coa_type}"; 
                addRow["gl_amt"] = tbl.Where(x => x.group2 == "AKTIVA").Sum(x => x.amt);
                dtRpt.Rows.Add(addRow); addRow = dtRpt.NewRow();
            }
            dtRpt.AcceptChanges();
            return dtRpt;
        }

        [HttpPost]
        public ActionResult ViewReportNeraca(sFilter param, string reporttype)
        {
            var rptfile = ""; var rptname = "";
            rptfile = $"rptNeraca{reporttype.Replace("PDF", "")}";
            rptname = $"LAPORAN_NERACA_{param.ddlyear}_{param.ddlmonth}"; 
            var sPeriodeMonthYear= $"{param.ddlyear}{param.ddlmonth}"; 
            var sPeriodeYear = $"{param.ddlyear}"; 
            var periode = param.bulan.ToUpper() + " " + sPeriodeYear.ToString();
            var dtRpt = new ClassConnection().GetDataTable(raw_query(param), "tbl");
             
            for (int i = 0; i < dtRpt.Rows.Count; i++)
            {     
                dtRpt.Rows[i]["total_aktiva"] = dtRpt.Compute("SUM(amt)", "coa_type = 'AKTIVA'");
                dtRpt.Rows[i]["total_pasiva"] = dtRpt.Compute("SUM(amt)", "coa_type = 'PASIVA'");
            }

            dtRpt.AcceptChanges(); DataView new_dr = dtRpt.DefaultView;
            // FILTER AKTIVA            
            new_dr.RowFilter = "coa_type in ('AKTIVA')";
            new_dr.Sort = "coa_code";
            DataTable dtaktiva = new_dr.ToTable();
            new_dr.RowFilter = "";

            // FILTER PASIVA
            new_dr.RowFilter = "coa_type in ('PASIVA')";
            DataTable dtpasiva = new_dr.ToTable();
            new_dr.Sort = "coa_code";
            new_dr.RowFilter = "";
            var irequest = new ReportRequest_New()
            {
                rptDataSource = new List<DataTable> { dtRpt, dtaktiva, dtpasiva },
                rptFile = rptfile,
                rptPaperSizeEnum = 9,
                rptPaperOrientationEnum = 1,
                rptExportType = reporttype,
                rptParam = new Dictionary<string, string>()
            };

            irequest.rptParam.Add("periode", periode);
            irequest.rptParam.Add("UserID", Session["UserID"].ToString()); 
            irequest.rptParam.Add("company_name", db.tb_m_company.FirstOrDefault().m_company_name);
            var rpt_id = GenerateReport_New(irequest);
            return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet); 
        }

        [HttpPost]
        public ActionResult ViewReportNeraca1(sFilter param, string reporttype)
        {
            var rptfile = ""; var rptname = "";
            rptfile = $"rptNeraca{reporttype}";
            rptname = $"LAPORAN_NERACA_{param.ddlyear}_{param.ddlmonth}";
            rptfile = rptfile.Replace("PDF", "");

            var periode = param.bulan.ToUpper() + " " + param.ddlyear.ToString();
            DateTime start_date = new DateTime(param.ddlyear.ToInteger(), param.ddlmonth.ToInteger(), 1);
            DateTime end_date = new DateTime(param.ddlyear.ToInteger(), param.ddlmonth.ToInteger(), DateTime.DaysInMonth(param.ddlyear.ToInteger(), param.ddlmonth.ToInteger()));
            decimal laba_berjalan = db.Database.SqlQuery<decimal>($"(select SUM(r_gl_debet_amt)-sum(r_gl_credit_amt) from tb_r_gl where r_gl_account_id=52 and r_gl_ref_table='tb_sa')")?.FirstOrDefault() ?? 0m;
            decimal total_aktiva = db.Database.SqlQuery<decimal>($"select Isnull(SUM(amt), 0.0) + {laba_berjalan} from dbo.GetReportNeraca('{start_date}','{end_date}') nc where m_account_type='AKTIVA'").FirstOrDefault();
            decimal total_pasiva = db.Database.SqlQuery<decimal>($"select Isnull(SUM(amt), 0.0) from dbo.GetReportNeraca('{start_date}','{end_date}') nc where m_account_type='PASIVA'").FirstOrDefault();

            sSql = $"select *, ({total_aktiva}) total_aktiva, ({total_pasiva}) total_pasiva from dbo.GetReportNeraca('{start_date}','{end_date}') nc where m_account_type='AKTIVA' order by m_account_code";
            DataTable data = new ClassConnection().GetDataTable(sSql, "data");

            sSql = $"select *, ({total_aktiva}) total_aktiva, ({total_pasiva}) total_pasiva from dbo.GetReportNeraca('{start_date}','{end_date}') nc where m_account_type='AKTIVA' order by m_account_code";
            DataTable data2 = new ClassConnection().GetDataTable(sSql, "data");

            sSql = $"select *, ({total_aktiva}) total_aktiva, ({total_pasiva}) total_pasiva from dbo.GetReportNeraca('{start_date}','{end_date}') nc where m_account_type='PASIVA' order by m_account_code";
            DataTable data3 = new ClassConnection().GetDataTable(sSql, "data");

            //decimal total_aktiva = decimal.Parse(data2.Compute("SUM(amt)", "").ToString());
            //decimal total_pasiva = decimal.Parse(data3.Compute("SUM(amt)", "").ToString());

            var irequest = new ReportRequest_New()
            {
                rptDataSource = new List<DataTable> { data, data2, data3 },
                rptFile = rptfile,
                rptPaperSizeEnum = 9,
                rptPaperOrientationEnum = 1,
                rptExportType = reporttype,
                rptParam = new Dictionary<string, string>()
            };

            irequest.rptParam.Add("periode", periode);
            irequest.rptParam.Add("UserID", Session["UserID"].ToString());
            var rpt_id = GenerateReport_New(irequest);
            return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Laporan Laba Rugi
        public ActionResult LabaRugi()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Neraca", (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.ddltype = new SelectList(new Dictionary<string, string>() {["LabaRugi"] = "Laba Rugi", ["LabaRugiProyek"] = "Laba Rugi Per Proyek" }, "Key", "Value");
            ViewBag.ddlyear = new SelectList(GetTahun(), "Value", "Text", GetServerTime().ToString("yyyy"));
            ViewBag.ddlmonth = new SelectList(GetBulan(), "Value", "Text", GetServerTime().ToString("MM"));
            return View();
        }

        [HttpPost]
        public ActionResult ViewReportLabaRugi(sFilter param, string reporttype)
        {
            var rptfile = ""; var rptname = "";
            if (param.ddltype == "LabaRugi") rptfile = $"rptLabaRugi{reporttype}";
            else rptfile = $"rptLabaRugiProyek{reporttype}";
            rptname = $"LAPORAN_LABA_RUGI_{param.ddlyear}_{param.ddlmonth}";
            rptfile = rptfile.Replace("PDF", "");

            var periode = param.bulan.ToUpper() + " " + param.ddlyear.ToString();
            DateTime start_date = new DateTime(param.ddlyear.ToInteger(), param.ddlmonth.ToInteger(), 1);
            DateTime end_date = new DateTime(param.ddlyear.ToInteger(), param.ddlmonth.ToInteger(), DateTime.DaysInMonth(param.ddlyear.ToInteger(), param.ddlmonth.ToInteger()));

            if (param.ddltype == "LabaRugi")
            {
                sSql = $"select * from dbo.GetReportLabaRugi('{start_date}','{end_date}') nc where m_account_type='PENDAPATAN' order by m_account_code";
                DataTable data = new ClassConnection().GetDataTable(sSql, "data");

                sSql = $"select * from dbo.GetReportLabaRugi('{start_date}','{end_date}') nc where m_account_type='BIAYA' order by m_account_code";
                DataTable data2 = new ClassConnection().GetDataTable(sSql, "data");

                decimal laba_bersih = db.Database.SqlQuery<decimal>($"select sum(amt) from dbo.GetReportLabaRugi('{start_date}','{end_date}') where m_account_type IN('PENDAPATAN','BIAYA')")?.FirstOrDefault() ?? 0;

                var irequest = new ReportRequest_New()
                {
                    rptDataSource = new List<DataTable> { data, data2 },
                    rptFile = rptfile,
                    rptPaperSizeEnum = 9,
                    rptPaperOrientationEnum = 1,
                    rptExportType = reporttype,
                    rptParam = new Dictionary<string, string>()
                };

                irequest.rptParam.Add("periode", periode);
                irequest.rptParam.Add("UserID", Session["UserID"].ToString());
                irequest.rptParam.Add("laba_bersih", laba_bersih.ToString());
                var rpt_id = GenerateReport_New(irequest);
                return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                sSql = $"select * from dbo.GetReportLabaRugi('{start_date}','{end_date}') nc where m_account_type IN('PENDAPATAN','BIAYA') order by m_account_code";
                DataTable data = new ClassConnection().GetDataTable(sSql, "data");

                var irequest = new ReportRequest_New()
                {
                    rptDataSource = new List<DataTable> { data },
                    rptFile = rptfile,
                    rptPaperSizeEnum = 9,
                    rptPaperOrientationEnum = 1,
                    rptExportType = reporttype,
                    rptParam = new Dictionary<string, string>()
                };

                irequest.rptParam.Add("periode", periode);
                irequest.rptParam.Add("UserID", Session["UserID"].ToString());
                var rpt_id = GenerateReport_New(irequest);
                return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Arus Kas
        public ActionResult ArusKas()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/ArusKas", (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.ddlyear = new SelectList(GetTahun(), "Value", "Text", GetServerTime().ToString("yyyy"));
            ViewBag.ddlmonth = new SelectList(GetBulan(), "Value", "Text", GetServerTime().ToString("MM"));
            return View();
        }

        [HttpPost]
        public ActionResult ViewReport(sFilter param, string reporttype)
        { 
            var rptfile = $"RptAcctgArusKas{reporttype}";
            var rptname = $"LAPORAN_ARUS_KAS_{param.bulan.ToUpper() + "_" + param.ddlyear.ToString()}";
            rptfile = rptfile.Replace("PDF", ""); 
            sSql = $"Select * From dbo.GetArusKas({1}, {param.ddlmonth}, {param.ddlyear}, '{param.bulan.ToUpper()}') r Order By seq, m_account_code";
            var sList = db.Database.SqlQuery<sFilter>(sSql).ToList();
            decimal saldo_awal_amt = sList.Where(x => x.r_kb_kelompok == "SALDO AWAL").Sum(x=>x.r_kb_saldo);
            decimal saldo_akhir_amt = sList.Where(x => x.r_kb_kelompok == "SALDO AKHIR").Sum(x => x.r_kb_saldo);
            decimal selisih_amt = saldo_awal_amt - saldo_akhir_amt;
            var sPeriode = $"{GetServerTime().ToString("dd") + " " + param.bulan.ToString() + " " + GetServerTime().ToString("yyyy")} " ;
            var iSize = 14; var iLans = 1;
            var irequest = new ReportRequest()
            {
                rptFile = rptfile,
                rptQuery = sSql,
                rptPaperSizeEnum = iSize,
                rptPaperOrientationEnum = iLans,
                rptExportType = reporttype,
                rptParam = new Dictionary<string, string>()
            };
            irequest.rptParam.Add("Periode", param.bulan.ToUpper() + " - " + param.ddlyear.ToString());
            irequest.rptParam.Add("UserID", Session["UserID"].ToString());
            irequest.rptParam.Add("selisih_amt", selisih_amt.ToString());
            irequest.rptParam.Add("sPeriode", sPeriode);
            var rpt_id = GenerateReport(irequest);
            return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}