﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class LegalitasController : Controller
    {
        // GET: Legalitas
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        #region Master Customer
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_t_legalitas", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column()
                {
                    name = "sAddFilter",
                    data = "sAddFilter",
                    search = new Search()
                    {
                        value = "",
                        regex = ""
                    },
                    searchable = true,
                    orderable = false
                };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( Select l.t_legalitas_uid, s.t_spr_no, s.t_spr_date, c.m_cust_name, i.m_item_name, l.t_legalitas_date, l.created_by, m_cust_addr, l.created_at, t_legalitas_status From tb_t_legalitas l Inner Join tb_t_spr s ON s.t_spr_id = l.t_spr_id Inner Join tb_m_item i ON s.m_item_id = i.m_item_id Inner Join tb_m_cust c ON c.m_cust_id = s.m_cust_id ) AS t";
            var sFixedFilter = "";
            var sOrder_by = " t.t_spr_no";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_t_legalitas tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_legalitas();
                tbl.t_legalitas_uid = Guid.NewGuid();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();                
                tbl.m_company_id = db.tb_m_company.FirstOrDefault(d => d.active_flag == "ACTIVE").m_company_id;
                tbl.t_legalitas_date = GetServerTime();
                tbl.t_legalitas_party2_birth_date = GetServerTime();
                tbl.t_legalitas_heirs1_birth_date = GetServerTime();
                tbl.t_legalitas_heirs2_birth_date = GetServerTime();
                tbl.t_legalitas_status = "In Process";
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_legalitas.FirstOrDefault(p => p.t_legalitas_uid == id);
            }
            if (tbl == null) return HttpNotFound();
            ViewBag.action = action;
            additionalFields(tbl, action);
            return View(tbl);
        }

        private void additionalFields(tb_t_legalitas tbl, string action)
        {          
            var spr = db.tb_t_spr.Where(x => x.t_spr_id == tbl.t_spr_id);
            if (spr.Any())
            {
                ViewBag.m_cust_name = db.tb_m_cust.FirstOrDefault(c => c.m_cust_id == tbl.m_cust_id)?.m_cust_name ?? "";
                ViewBag.t_spr_no = spr.FirstOrDefault()?.t_spr_no ?? ""; 
                ViewBag.m_item_name = db.tb_m_item.FirstOrDefault(x => x.m_item_id ==spr.FirstOrDefault().m_item_id)?.m_item_name.ToUpper() ?? "";
                ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(c =>c.m_proyek_id==spr.FirstOrDefault().m_proyek_id)?.m_proyek_name ?? "";
            }

        }

        [HttpPost]
        public ActionResult GetDataItem()
        {
            JsonResult js = null;
            try
            {
                sSql = $"select t_spr_id, t_spr_no, m_item_name, m_proyek_name, s.m_cust_id, s.m_item_id, m_cust_name, m_type_unit_name, m_cluster_name, t_spr_price, t_spr_status from tb_v_spr s Inner Join ( Select r.m_cust_id, r.m_item_id, SUM(r_piutang_amt) r_piutang_amt, Isnull((Select SUM(r_piutang_pay_amt) from tb_r_piutang x Where x.r_piutang_type='PAYMENT' AND x.r_piutang_status=r.r_piutang_status AND x.m_cust_id=r.m_cust_id AND r.m_item_id=x.m_item_id), 0.0) r_piutang_pay_amt from tb_r_piutang r Where r_piutang_type!='PAYMENT' AND r_piutang_status='Post' Group By r.m_cust_id, r.m_item_id, r.r_piutang_status Having SUM(r_piutang_amt)-Isnull((Select SUM(r_piutang_pay_amt) from tb_r_piutang x Where x.r_piutang_type='PAYMENT' AND x.r_piutang_status=r.r_piutang_status AND x.m_cust_id=r.m_cust_id AND r.m_item_id=x.m_item_id), 0.0) > 0 ) r ON s.m_cust_id=r.m_cust_id AND s.m_item_id=r.m_item_id where t_spr_status='Post' AND t_spr_id NOT IN ( select t_refund_ref_id from tb_t_refund ) Order By m_item_name";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_legalitas tbl, string action)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = ""; var servertime = GetServerTime();
            if (tbl.t_spr_id == 0) msg += "Silahkan pilih SPR!<br/>"; 
            if (string.IsNullOrEmpty(tbl.t_legalitas_kuasa)) msg += "Silahkan isi Nama Kuasa!<br/>";
            if (string.IsNullOrEmpty(tbl.t_legalitas_party2_name)) msg += "Silahkan isi Nama Lengkap Pihak ke 2!<br/>";
            if (string.IsNullOrEmpty(tbl.t_legalitas_party2_nik)) msg += "Silahkan isi No. KTP Pihak ke 2!<br/>";
            if (string.IsNullOrEmpty(tbl.t_legalitas_party2_addr)) msg += "Silahkan isi Alamat lengkap Pihak ke 2!<br/>";
            if (string.IsNullOrEmpty(tbl.t_legalitas_party2_birth_city)) msg += "Silahkan isi tempat lahir Pihak ke 2!<br/>";
            if (string.IsNullOrEmpty(tbl.t_legalitas_party2_birth_job)) msg += "Silahkan isi pekerjaan Pihak ke 2!<br/>";
            if (string.IsNullOrEmpty(tbl.t_legalitas_heirs1_name)) msg += "Silahkan isi nama lengkap ahli waris 1!<br/>";
            if (string.IsNullOrEmpty(tbl.t_legalitas_heirs1_nik)) msg += "Silahkan isi No. KTP ahli waris 1!<br/>";
            if (string.IsNullOrEmpty(tbl.t_legalitas_heirs1_addr)) msg += "Silahkan isi Alamat lengkap ahli waris 1!<br/>";
            if (string.IsNullOrEmpty(tbl.t_legalitas_heirs1_birth_city)) msg += "Silahkan isi tempat lahir ahli waris 1!<br/>";
            if (string.IsNullOrEmpty(tbl.t_legalitas_heirs1_birth_job)) msg += "Silahkan isi pekerjaan ahli waris 1!<br/>";

            if (string.IsNullOrEmpty(msg))
            {
                if (tbl.t_legalitas_status == "Revised") tbl.t_legalitas_status = "In Process";
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.created_at = tbl.created_at;
                            tbl.created_by = tbl.created_by;
                            tbl.last_edited_at = GetServerTime();
                            tbl.last_edited_by = Session["UserID"].ToString();
                            db.tb_t_legalitas.Add(tbl);
                        }
                        else
                        {
                            tbl.last_edited_at = GetServerTime();
                            tbl.last_edited_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                        }
                        db.SaveChanges();

                        if (tbl.t_legalitas_status == "Post")
                        {
                            var tb_spr = db.tb_t_spr.FirstOrDefault(x=>x.t_spr_id==tbl.t_spr_id && x.m_cust_id==tbl.m_cust_id && x.t_spr_status=="Post");
                            tb_spr.t_spr_status = "Complete"; 
                            db.Entry(tb_spr).State = EntityState.Modified;
                            var item = db.tb_m_item.FirstOrDefault(x=>x.m_item_id==tbl.m_item_id);
                            item.m_item_flag = "Complete";
                            //Block Post Transaction
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_legalitas_uid, "tb_t_legalitas", tbl.t_legalitas_id, tbl.last_edited_by, tbl.last_edited_at));
                             
                            if (!string.IsNullOrEmpty(msg))
                            {
                                objTrans.Rollback(); result = "failed";
                                return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        db.SaveChanges();
                        objTrans.Commit();

                        hdrid = tbl.t_legalitas_id.ToString(); result = "success";
                        if (tbl.t_legalitas_status == "Post") msg = $"data telah diposting dengan nomor transaksi {tbl.t_legalitas_party2_name}";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int t_legalitas_id)
        {

            tb_t_legalitas tbl = db.tb_t_legalitas.FirstOrDefault(x=>x.t_legalitas_id == t_legalitas_id);
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.tb_t_legalitas.Remove(tbl);
                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}