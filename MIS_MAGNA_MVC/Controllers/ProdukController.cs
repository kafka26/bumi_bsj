﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq; 
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
namespace MIS_MAGNA_MVC.Controllers
{
    public class ProdukController : Controller
    {
        // GET: Produk
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        #region Master Produk
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile"); 
            return View();
        } 

        [HttpPost]
        public ActionResult GetDataIndex()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT * FROM ( Select m_item_id, m.m_item_uid, m.m_item_name, p.m_proyek_name, p.m_proyek_addr, u.m_type_unit_name, m.m_item_luas_bangunan, m.m_item_luas_tanah, m.m_item_flag, m.created_by, m.created_at, m.last_edited_by, m.last_edited_at From tb_m_item m Inner Join tb_m_proyek p ON p.m_proyek_id=m.m_proyek_id Inner Join tb_m_type_unit u ON u.m_type_unit_id=m.m_item_type_unit_id Where m_item_type = 'Unit' ) AS t Order By m_item_name";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_m_item tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_m_item();
                tbl.m_item_uid = Guid.NewGuid();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_m_item.FirstOrDefault(p => p.m_item_uid == id);
            }
            if (tbl == null) return HttpNotFound();
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        private string GetCluster(int m_proyek_id, int m_company_id)
        { 
            return $"select m_cluster_id ifield, m_cluster_name sfield from tb_m_cluster Where m_proyek_id={m_proyek_id} AND m_company_id={m_company_id} Order By m_cluster_name";
        }

        private void InitDDL(tb_m_item tbl)
        {
            ViewBag.m_company_id = new SelectList(db.tb_m_company.Where(a => a.active_flag == "ACTIVE").ToList(), "m_company_id", "m_company_name", tbl.m_company_id);
            ViewBag.m_proyek_id = new SelectList(db.tb_m_proyek.Where(a => a.m_proyek_flag == "OPEN").ToList(), "m_proyek_id", "m_proyek_name", tbl.m_proyek_id);
            ViewBag.m_item_type_unit_id = new SelectList(db.tb_m_type_unit.Where(a => a.m_type_unit_flag == "ACTIVE").ToList(), "m_type_unit_id", "m_type_unit_name", tbl.m_item_type_unit_id);
            ViewBag.m_cluster_id = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>(GetCluster(tbl.m_proyek_id, tbl.m_company_id)).ToList(), "ifield", "sfield", tbl.m_cluster_id);
        }

        [HttpPost]
        public ActionResult GetDataCluster(int m_proyek_id, int m_company_id)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.Database.SqlQuery<ReportModels.DDLDoubleField>(GetCluster(m_proyek_id, m_company_id)).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_m_item tbl, string action)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            var servertime = GetServerTime();
          
            if (action != "New Data")
            {
                var sitem = db.tb_m_item.AsNoTracking().Where(x=>x.m_item_id==tbl.m_item_id).ToList();
                if (tbl.m_item_flag != sitem.FirstOrDefault(x=>x.m_item_id==tbl.m_item_id).m_item_flag)
                    msg += $"Maaf, status unit {tbl.m_item_name} masih dalam status {sitem.FirstOrDefault(x => x.m_item_id == tbl.m_item_id).m_item_flag}, ganti pilihan status menjadi {tbl.m_item_flag}!<br />";
            }

            var item = db.Database.SqlQuery<tb_m_item>($"select * from tb_m_item ").ToList();
            if (action == "New Data")
            {
                var sCekItem = item.Where(x=>x.m_item_name==tbl.m_item_name && x.m_proyek_id==tbl.m_proyek_id && x.m_cluster_id==tbl.m_cluster_id).ToList();
                if (sCekItem.Count()>0)
                {
                    var mPro = db.tb_m_proyek.Where(x=>x.m_proyek_id==tbl.m_proyek_id)?.FirstOrDefault().m_proyek_name ?? "";
                    var mCluster = db.tb_m_cluster.Where(x=>x.m_cluster_id==tbl.m_cluster_id)?.FirstOrDefault().m_cluster_name ??"";
                    msg += $"Maaf, Nama Unit <strong>{tbl.m_item_name}</strong> dengan proyek <strong> {mPro} </strong> dan cluster <strong>{mCluster}</strong> sudah ada, ganti unit dengan nama yang lain!<br />";
                }
            } 

            if (string.IsNullOrEmpty(tbl.m_item_name)) msg += "Tolong, Isi Nama Kota..!<br />";
            tbl.m_item_type = "Unit"; tbl.m_account_id = 0;

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.last_edited_at = tbl.created_at;
                            tbl.last_edited_by = tbl.created_by;
                            db.tb_m_item.Add(tbl);
                        }
                        else
                        {
                            tbl.last_edited_at = servertime;
                            tbl.last_edited_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.m_item_uid.ToString();
                        msg = "Data Already Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}