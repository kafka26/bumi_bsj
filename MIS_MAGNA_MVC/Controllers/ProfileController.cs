﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Text.RegularExpressions; 
namespace MIS_MAGNA_MVC.Controllers
{
    public class ProfileController : Controller
    {
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        // GET: Profile
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            InitAdvFilterIndex(modfil, "tb_m_user", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE isapptrans=0 AND isposttrans=0 ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM( SELECT m_user_uid, m_user_id, m_user_login, active_flag, created_at FROM tb_m_user ) AS t";

            var sFixedFilter = "";

            var sOrder_by = " t.m_user_login";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res)
            {
                // simple remapping adding extra info to found dataset
                result.Add(s);
            };

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        private void SetUserSession(string userid)
        {
            var tbl = (from m in db.tb_m_user join c in db.tb_m_company on m.m_company_id equals c.m_company_id where m.m_user_login == userid.ToLower() select new { m.m_user_uid, m.m_user_login, c.m_company_code, m.m_user_type }).FirstOrDefault();
            //db.QL_mstprof.Where(o => o.profoid.ToLower().Equals(clog.userid.ToLower())).FirstOrDefault();

            Session["UserID"] = tbl.m_user_login;
            Session["UserName"] = tbl.m_user_login;
            Session["CompnyCode"] = tbl.m_company_code;
            Session["CompnyCodeHdr"] = tbl.m_company_code;
            Session["UserGuid"] = tbl.m_user_uid;
            Session["ApprovalLimit"] = 0;

            string sSql = "";
            if (tbl.m_user_type == "Admin")
                sSql = "SELECT menutype formtype, menuname formname, menucontroller formaddress, menumodule formmodule, menuorder formnumber, menuview formmenu, '' formimage FROM tb_formmenu WHERE menuflag='ACTIVE' ORDER BY menumodule, menutype, menuorder";
            else
                sSql = $"SELECT DISTINCT menutype formtype, menuname formname, menucontroller formaddress, menumodule formmodule, menuorder formnumber, menuview formmenu, '' formimage, ur.m_role_id, u.m_user_id, m.menuoid FROM tb_m_user u Inner Join tb_m_user_d ur on u.m_user_id=ur.m_user_id INNER JOIN tb_m_role_d rd ON rd.m_role_id=ur.m_role_id INNER JOIN tb_formmenu m ON m.menuoid=rd.m_menu_id WHERE menuflag='ACTIVE' AND u.m_user_login='{tbl.m_user_login}' ORDER BY menumodule, menutype, menuorder";
            Session["Role"] = db.Database.SqlQuery<RoleDetail>(sSql).ToList();
            List<RoleDetail> dtRoleTmp = db.Database.SqlQuery<RoleDetail>(sSql).ToList();
            List<RoleDetail> dtRole = new List<RoleDetail>();
            for (int i = 0; i < dtRoleTmp.Count; i++)
            {
                var pathcheck = dtRoleTmp[i].formaddress;
                if (dtRoleTmp[i].formtype.ToUpper() == "REPORT")
                    pathcheck = "ReportForm/" + dtRoleTmp[i].formaddress;
                var sfilepath = Server.MapPath("~/Controllers/" + pathcheck + "Controller.cs");
                if (System.IO.File.Exists(sfilepath))
                {
                    var item = new RoleDetail();
                    item.formtype = dtRoleTmp[i].formtype;
                    item.formname = dtRoleTmp[i].formname;
                    item.formaddress = dtRoleTmp[i].formaddress;
                    item.formmodule = dtRoleTmp[i].formmodule;
                    item.formnumber = dtRoleTmp[i].formnumber;
                    item.formmenu = dtRoleTmp[i].formmenu;
                    item.formimage = dtRoleTmp[i].formimage;
                    dtRole.Add(item);
                }
                else
                {
                    pathcheck = dtRoleTmp[i].formaddress;
                    sfilepath = Server.MapPath("~/Controllers/" + pathcheck + "Controller.cs");
                    if (System.IO.File.Exists(sfilepath))
                    {
                        var item = new RoleDetail();
                        item.formtype = dtRoleTmp[i].formtype;
                        item.formname = dtRoleTmp[i].formname;
                        item.formaddress = dtRoleTmp[i].formaddress;
                        item.formmodule = dtRoleTmp[i].formmodule;
                        item.formnumber = dtRoleTmp[i].formnumber;
                        item.formmenu = dtRoleTmp[i].formmenu;
                        item.formimage = dtRoleTmp[i].formimage;
                        dtRole.Add(item);
                    }
                }
            }
            Session["Role"] = dtRole;
            if (tbl.m_user_type == "Admin")
                sSql = "SELECT menucontroller formaddress, 'Yes' special FROM tb_formmenu WHERE menuflag='ACTIVE'";
            else
                sSql = "SELECT DISTINCT menucontroller formaddress, isspecial special FROM tb_m_user_d ur INNER JOIN tb_m_user u ON u.m_user_id=ur.m_role_id INNER JOIN tb_m_role_d rd ON rd.m_role_id=ur.m_role_id INNER JOIN tb_formmenu m ON m.menuoid=rd.m_menu_id WHERE menuflag='ACTIVE' AND u.m_user_login='" + tbl.m_user_login + "' AND isspecial='Yes'";

            //sSql = "SELECT DISTINCT menucontroller formaddress, isspecial special FROM tb_formuserrole ur INNER JOIN tb_formroledtl rd ON rd.roleoid=ur.roleoid INNER JOIN tb_formmenu m ON m.menuoid=rd.menuoid WHERE menuflag='ACTIVE' AND ur.profoid='" + tbl.m_user_login + "' AND isspecial='Yes'";
            Session["SpecialAccess"] = db.Database.SqlQuery<RoleSpecial>(sSql).ToList();
        }

        // GET: /Profile/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View(new DataLogin());
        }

        // POST: /Profile/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(DataLogin clog)
        {
            if (ModelState.IsValid)
            {
                string userpassword = db.tb_m_user.Where(o => o.m_user_login.ToLower().Equals(clog.userid)).Any() ? db.tb_m_user.Where(o => o.m_user_login.ToLower().Equals(clog.userid)).FirstOrDefault().m_user_pass : "";

                if (string.IsNullOrEmpty(userpassword))
                    ModelState.AddModelError("", "Incorrect Login Parameter");
                else
                {
                    if (clog.userpwd.ToLower().Equals(userpassword.ToLower()))
                    {
                        SetUserSession(clog.userid);
                        ClassFunction.clearFilesTemp();
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Incorrect Password");
                    }
                }
            }
            return View(clog);
        }

        [HttpPost]
        public ActionResult GetUserLogin(DataLogin clog)
        {
            var result = "success"; 
            var msg = "";
            var userid = "";
            var poctype = "";
            var postdata = "GetUserLogin(sUserID=" + clog.userid + ",sUserPassword=" + clog.userpwd + ")";
            
            try
            {
                //string userpassword = db.QL_mstprof.Where(o => o.profoid.ToLower().Equals(clog.userid)).Any() ? db.QL_mstprof.Where(o => o.profoid.ToLower().Equals(clog.userid)).FirstOrDefault().profpass : "";

                string userpassword = db.tb_m_user.Where(o => o.m_user_login.ToLower().Equals(clog.userid)).Any() ? db.tb_m_user.Where(o => o.m_user_login.ToLower().Equals(clog.userid)).FirstOrDefault().m_user_pass : "";

                //string userpassword = db.tb_m_user.Where(o => o.m_user_login.ToLower().Equals(clog.userid) && (clog.userid.ToUpper() == "ADMIN" ? o.m_user_type.Contains("") : !o.m_user_type.ToUpper().Contains("NONE"))).Any() ? db.QL_mstprof.Where(o => o.profoid.ToLower().Equals(clog.userid) && (clog.userid.ToUpper() == "ADMIN" ? o.poctype.Contains("") : !o.poctype.ToUpper().Contains("NONE"))).FirstOrDefault().profpass : "";

                if (string.IsNullOrEmpty(userpassword))
                {
                    result = "fail";
                    msg += "Incorrect Login Parameter\n";
                }                    
                else
                {
                    if (clog.userpwd.ToLower().Equals(userpassword.ToLower()))
                    {
                        var tbl = db.tb_m_user.Where(o => o.m_user_login.ToLower().Equals(clog.userid.ToLower())).FirstOrDefault();
                        userid = tbl.m_user_login;
                        poctype = tbl.m_user_type;                        
                    }
                    else
                    {
                        result = "fail";
                        msg += "Incorrect Password\n";
                    }
                }                
            }
            catch (Exception e)
            {
                result = "fail";
                msg = e.ToString();
            }

            JsonResult js = Json(new { result, msg, postdata, userid, poctype }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: /Profile/Logout
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Login", "Profile");
        }

        // GET: /Profile/NotAuthorize
        public ActionResult NotAuthorize()
        {
            return View();
        }

        // GET: /Profile/Update/UserID
        public ActionResult Update(Guid? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_m_user tbl;
            tbl = db.tb_m_user.Find(Session["UserGuid"]);
            string action = "Update Data";
            tbl = db.tb_m_user.Find(Session["UserGuid"]);
            FillAdditionalField(tbl);
            if (tbl == null)
                return HttpNotFound();
            ViewBag.action = action;
            return View(tbl);
        }

        // POST: Profile/Update
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(tb_m_user tbl, string action, string cbpasswordvalue, string newprofpass, string confirmprofpass)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            if (cbpasswordvalue== "true")
            { 
                if (string.IsNullOrEmpty(newprofpass))
                    msg += "New pass This field is required.";
                if (string.IsNullOrEmpty(confirmprofpass))
                    msg += "Confirm pass This field is required.";
                if (!string.IsNullOrEmpty(newprofpass) && !string.IsNullOrEmpty(confirmprofpass) && newprofpass.ToUpper() != confirmprofpass.ToUpper())
                    msg += "Confirm New Password didn't match with New Password."; 
            }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.m_company_id = db.tb_m_company.FirstOrDefault().m_company_id;
                        tbl.m_user_pass = newprofpass;
                        tbl.last_edited_at = ClassFunction.GetServerTime();
                        tbl.last_edited_by = Session["UserID"].ToString();
                        db.Entry(tbl).State = EntityState.Modified; 

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.m_user_id.ToString();
                        msg = "Data Already Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);

            //var oldpassword = db.QL_mstprof.Where(p => p.profoid == tbl.profoid).Select(p => p.profpass).FirstOrDefault();
            //if (cbemailvalue)
            //{
            //    if (string.IsNullOrEmpty(profemail))
            //        ModelState.AddModelError("profemail", "This field is required.");
            //    else
            //    {
            //        var oreg = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);
            //        Match m = oreg.Match(profemail);
            //        if (!m.Success)
            //            ModelState.AddModelError("profemail", "Email Address format is wrong.");
            //    }
            //} 
        }

        public class m_user_d : tb_m_user_d
        { 
            public string m_role_code { get; set; }
            public string m_role_name { get; set; }
        }

        // GET: /Profile/Form/id
        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_m_user tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_m_user();
                tbl.m_user_uid = Guid.NewGuid();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_m_user.Find(id);
            }
            FillAdditionalField(tbl);
            if (tbl == null)
                return HttpNotFound();
            ViewBag.action = action; 
            return View(tbl); 
        }

        //private void InitDDL(QL_mstprof tbl)
        //{
        //    sSql = "SELECT * FROM tb_m_division WHERE activeflag='ACTIVE'";
        //    if (Session["CompnyCode"].ToString() != CompnyCode)
        //        sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
        //    sSql += " ORDER BY divname";
        //    //var cmpcode = new SelectList(db.Database.SqlQuery<tb_m_division>(sSql).ToList(), "m_division_id", "m_division_name", tbl.cmpcode);
        //    //ViewBag.cmpcode = cmpcode;
        //}

        private void FillAdditionalField(tb_m_user tbl)
        {
            ViewBag.m_employee_code = ""; ViewBag.m_user_name = "";
            if (tbl.m_employee_id != 0)
            {
                var addfield = db.tb_m_employee.Where(x => x.m_employee_id == tbl.m_employee_id).FirstOrDefault();
                ViewBag.m_employee_code = addfield.m_employee_code;
                ViewBag.m_employee_name = addfield.m_employee_name;
            }
        }

        [HttpPost]
        public ActionResult GetPersonData( int m_user_id)
        {
            var result = "";
            JsonResult js = null;
            List<tb_m_employee> tbl = new List<tb_m_employee>();
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + 'p.' + name FROM sys.syscolumns WHERE id=OBJECT_ID('tb_m_employee') AND name<>'cardno' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();

            try
            {
                sSql = $"SELECT {cols} FROM tb_m_employee p WHERE p.m_employee_id NOT IN (SELECT m_employee_id FROM tb_m_user WHERE m_user_id <> {m_user_id})";
                tbl = db.Database.SqlQuery<tb_m_employee>(sSql).ToList();
                if (tbl.Count == 0) result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString() + sSql;
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(int m_user_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT 0 m_user_d_seq, m_role_id, m_role_code, m_role_name, 'Yes' isspecial FROM tb_m_role WHERE m_role_flag='ACTIVE' AND m_role_id NOT IN (SELECT m_role_id FROM tb_m_user_d WHERE m_user_id = {m_user_id}) Order By m_role_id";
                var tbl = db.Database.SqlQuery<m_user_d>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() + sSql }, JsonRequestBehavior.AllowGet);
            }
            return js; 
        }
         
        public ActionResult FillDetailData(int m_user_id)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.Database.SqlQuery<m_user_d>($"Select d.m_role_id, m_user_d_seq, r.m_role_code, r.m_role_name, d.isspecial From tb_m_user_d d Inner Join tb_m_user m ON m.m_user_id=d.m_user_id Inner Join tb_m_role r ON r.m_role_id=d.m_role_id Where m.m_user_id = {m_user_id} Order By m_user_d_seq").ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        // POST: Role/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_m_user tbl, List<m_user_d> dtl, string action)
        {
            var msg = ""; var result = "failed"; var hdrid = new Guid(); var servertime = ClassFunction.GetServerTime(); 
            if (tbl.m_employee_id == 0) msg += "Please select Employee!<br />";
            if (string.IsNullOrEmpty(tbl.m_user_pass)) msg += "Please input password!<br />";
            if (tbl.m_user_type != "Admin")
            {
                if (dtl == null || dtl.Count <= 0) msg += "Please fill detail Process!<br />";
                else
                {
                    foreach (var item in dtl)
                    {
                        if (item.m_role_id == 0)
                            msg += $"Please select Output role name!<br />";
                    }
                }
            }
            if (string.IsNullOrEmpty(msg))
            { 
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.m_company_id= db.tb_m_company.FirstOrDefault().m_company_id;
                            tbl.last_edited_at = tbl.created_at;
                            tbl.last_edited_by = tbl.created_by;
                            db.tb_m_user.Add(tbl);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.m_company_id = db.tb_m_company.FirstOrDefault().m_company_id;
                            tbl.last_edited_at = servertime;
                            tbl.last_edited_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.tb_m_user_d.Where(a => a.m_user_id == tbl.m_user_id);
                            db.tb_m_user_d.RemoveRange(trndtl);
                            db.SaveChanges(); 
                        }

                        var tbldtl = new List<tb_m_user_d>();
                        if (dtl != null && dtl.Count() > 0)
                        {
                            foreach (var item in dtl)
                            {
                                var new_dtl = (tb_m_user_d)ClassFunction.MappingTable(new tb_m_user_d(), item);
                                new_dtl.m_user_id = tbl.m_user_id;
                                new_dtl.m_company_id = db.tb_m_company.FirstOrDefault().m_company_id;
                                new_dtl.created_by = tbl.last_edited_by;
                                new_dtl.created_at = tbl.last_edited_at;
                                tbldtl.Add(new_dtl);
                            }
                            db.tb_m_user_d.AddRange(tbldtl);
                        }

                        db.SaveChanges();  
                        objTrans.Commit();

                        hdrid = tbl.m_user_uid; result = "success";
                        dtl = null;
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid, dtl }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult IsSessionExpired()
        {
            return Json(Session["UserID"] == null, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoginAgain(string userid, string password)
        {
            var result = "";

            var match = db.tb_m_user.Where(x => x.m_user_login.ToLower() == userid.ToLower()
                                                     && x.m_user_pass == password
                                                     && x.active_flag == "ACTIVE")
                                         .Any();

            if (match)
            {
                SetUserSession(userid);
            }
            else
            {
                result = "Incorrect login data.";
            }

            var js = Json(new { result }, JsonRequestBehavior.AllowGet);

            js.MaxJsonLength = Int32.MaxValue;

            return js;
        }
    }
}