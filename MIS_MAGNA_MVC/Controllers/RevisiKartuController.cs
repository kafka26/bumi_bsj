﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;
using MIS_MAGNA_MVC.Models;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;


namespace MIS_MAGNA_MVC.Controllers
{
    public class RevisiKartuController : Controller
    {
        // GET: RevisiKartu
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public RevisiKartuController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        #region Revisi Data Kartu
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.Title = "Revisi Data Karti Kontrol";
            return View();
        } 

        [HttpPost]
        public ActionResult GetDataSPR()
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select 0 r_piutang_seq, * from ( select * from tb_v_spr_rev_unit ) r ";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl.Count > 0)
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class t_spr : tb_t_spr
        {
            public decimal r_piutang_amt { get; set; }
            public decimal r_piutang_pay_amt { get; set; }
            public string type_revisi { get; set; }
        }

        public class r_piutang_d : tb_r_piutang
        {
            public int r_piutang_pay_id { get; set; } 
        }

        public ActionResult Form(int item_id, int t_spr_id = 0)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            tb_t_spr tbl;
            ViewBag.Title = "Revisi Data Kartu Kontrol";
            tbl = db.tb_t_spr.Where(r => r.t_spr_id == t_spr_id && r.m_item_id == item_id).FirstOrDefault();   
            ViewBag.m_item_name = db.tb_m_item.Where(i => i.m_item_id == tbl.m_item_id)?.FirstOrDefault().m_item_name ?? "";
            ViewBag.m_cust_name = db.tb_m_cust.Where(i => i.m_cust_id == tbl.m_cust_id)?.FirstOrDefault().m_cust_name ?? "";
            ViewBag.m_proyek_code = db.tb_m_proyek.Where(i => i.m_proyek_id == tbl.m_proyek_id)?.FirstOrDefault().m_proyek_code ?? "";
             
            return View(tbl);
        }

        [HttpPost]
        public ActionResult FillDetailData(t_spr param)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT 0 r_piutang_seq, r.m_item_id, r.r_piutang_type, r.r_piutang_pay_id, r.r_piutang_id, r.r_piutang_ref_id, r.r_piutang_note, r.r_piutang_ref_date, r.r_piutang_pay_ref_date, r.r_piutang_amt, r.r_piutang_pay_amt, Isnull(r.r_piutang_pay_ref_id, 0) r_piutang_pay_ref_id, Isnull(r.r_piutang_pay_ref_no, '') r_piutang_pay_ref_no FROM tb_v_kartu_detail r WHERE r.m_item_id = {param.m_item_id} AND r.m_cust_id = {param.m_cust_id} AND r.m_proyek_id = {param.m_proyek_id} Order By r.m_cust_id, r.t_spr_id, r.m_item_id, r.seq, CASE WHEN r_piutang_ref_table IN ( 'tb_t_spr_dp', 'tb_t_angsuran_dp_d') THEN 2 WHEN r_piutang_ref_table IN('tb_t_spr_dp_disc') THEN 2 WHEN r_piutang_ref_table IN('tb_t_spr_kt', 'tb_t_angsuran_kt_d') THEN 3 WHEN r_piutang_ref_table IN('tb_t_kt_disc') THEN 3 WHEN r_piutang_ref_table IN ('tb_t_spr', 'tb_t_angsuran_kpr_d') THEN 4 WHEN r_piutang_ref_table IN('tb_t_kpr_disc') THEN 4 ELSE 5 END, r_piutang_ref_date, r_piutang_ref_id, r.r_piutang_pay_ref_date";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tb_v_kartu_detail"));
                if (tbl != null || tbl.Count > 0)
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = sSql + e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost, ActionName("SimpanData")]
        [ValidateAntiForgeryToken]
        public ActionResult SimpanData(List<r_piutang_d> dtDtl)
        {     
            string result = "success"; string msg = ""; 
            if (dtDtl == null) { result = "failed"; msg = "Data can't be found!"; } 
            if (string.IsNullOrEmpty(msg))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    { 
                        if (dtDtl.Count() > 0)
                        {
                            foreach (var item in dtDtl)
                            {
                                var rPiutang = db.tb_r_piutang.FirstOrDefault(x=> x.r_piutang_id== item.r_piutang_pay_id && x.r_piutang_type=="PAYMENT");
                                rPiutang.r_piutang_pay_ref_date = item.r_piutang_pay_ref_date;
                                db.Entry(rPiutang).State = EntityState.Modified; 
                            }
                            db.SaveChanges();
                        } 
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
 

        #endregion



    }
}