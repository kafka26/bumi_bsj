﻿using MIS_MAGNA_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine; 
using static MIS_MAGNA_MVC.Controllers.ClassFunction;  
namespace MIS_MAGNA_MVC.Controllers
{
    public class SPRController : Controller
    {
        // GET: SPR
        private QL_MIS_MAGNAEntities db;
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class t_promo : tb_t_promo { public int m_promo_seq { get; set; } public string m_promo_name { get; set; } }

        public class tb_marketing : tb_m_marketing { public int seq { get; set; } }

        public class dt_gl { public decimal gl_amt { get; set; } public int account_id { get; set; } }

        public SPRController() { db = new QL_MIS_MAGNAEntities(); db.Database.CommandTimeout = 0; }

        // GET: SuratPesananRumah
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            var isCollapse = "True";
            InitAdvFilterIndex(modfil, "tb_t_spr", false);
            ViewBag.isCollapse = isCollapse;
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false }; model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( SELECT h.t_spr_uid, h.t_spr_id, h.m_company_id, m_company_name, t_spr_date, h.t_spr_no, p.m_proyek_name, c.m_cust_name, i.m_item_name, t_spr_note, t_spr_status, t_spr_type_trans, iif(h.is_promo=1, 'Ya', 'Tidak') is_promo_name FROM tb_t_spr h INNER JOIN tb_m_company cmp ON cmp.m_company_id=h.m_company_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = h.m_proyek_id INNER JOIN tb_m_cust c ON c.m_cust_id = h.m_cust_id INNER JOIN tb_m_item i ON i.m_item_id = h.m_item_id Where t_spr_type_trans IN('Baru') ) AS t";

            // action inside a standard controller
            int filteredResultsCount; int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, "", " t.t_spr_date DESC");

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var item in res) { result.Add(item); }

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        [HttpPost]
        public ActionResult GetDataItem(int m_company_id)
        {
            JsonResult js = null;
            try
            {
                var tbl = (from i in db.tb_m_item
                           join p in db.tb_m_proyek on i.m_proyek_id equals p.m_proyek_id
                           join c in db.tb_m_cluster on i.m_cluster_id equals c.m_cluster_id
                           join t in db.tb_m_type_unit on i.m_item_type_unit_id equals t.m_type_unit_id
                           where new List<string> { "Booking", "Open" }.Contains(i.m_item_flag) 
                           select new { i.m_item_id, i.m_item_name, p.m_proyek_id, p.m_proyek_name, c.m_cluster_name, t.m_type_unit_name, i.m_item_luas_bangunan, i.m_item_luas_standart, i.m_item_luas_tanah }).ToList();
                if (tbl == null || tbl.Count <= 0) { js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet); } 
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataCust(int m_company_id)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.tb_m_cust.Where(x => x.m_cust_flag == "ACTIVE").Select(x => new { x.m_company_id, x.m_cust_id, x.m_cust_name, x.m_cust_addr }).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataUtj(int m_company_id)
        {
            JsonResult js = null;
            try
            {
                var tbl = (from u in db.tb_t_utj
                           join i in db.tb_m_item on u.m_item_id equals i.m_item_id
                           join p in db.tb_m_proyek on u.m_proyek_id equals p.m_proyek_id
                           join c in db.tb_m_cluster on i.m_cluster_id equals c.m_cluster_id
                           join t in db.tb_m_type_unit on i.m_item_type_unit_id equals t.m_type_unit_id
                           join cs in db.tb_m_cust on u.m_cust_id equals cs.m_cust_id
                           where u.t_utj_status == "Post" && u.m_company_id == m_company_id
                           select new { u.t_utj_id, u.t_utj_no, u.t_utj_date, u.t_utj_type_bayar, u.t_utj_amt, u.m_cust_id, cs.m_cust_name, i.m_item_id, i.m_item_name, p.m_proyek_id, p.m_proyek_name, c.m_cluster_name, t.m_type_unit_name, i.m_item_luas_bangunan, i.m_item_luas_standart, i.m_item_luas_tanah }).ToList();

                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int t_spr_id, int m_cust_id)
        {
            sSql = $"Select 0 m_promo_seq, x1.m_item_name m_promo_name, * from tb_t_promo x inner join tb_m_item x1 ON x1.m_item_id=x.m_promo_id Where t_spr_id={t_spr_id} order by m_promo_id";
            return getJsResult(sSql);
        }

        public static List<SelectListItem> GetList1()
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem { Text = "Tidak", Value = "Tidak" },
                new SelectListItem { Text = "Ya", Value = "Ya" }, 
            };
            return result;
        }

        public static List<SelectListItem> GetList2()
        {
            var result = new List<SelectListItem>
            { 
                new SelectListItem { Text = "Tunai", Value = "Tunai" },
                new SelectListItem { Text = "Angsuran", Value = "Angsuran" },
            };
            return result;
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) { return RedirectToAction("NotAuthorize", "Profile"); }

            tb_t_spr tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_spr();
                tbl.t_spr_uid = Guid.NewGuid();
                tbl.t_spr_date = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.t_spr_status = "In Process";
                tbl.t_spr_type_trans = "Baru";
                tbl.t_utj_amt2 = 0m;
                tbl.t_dp_amt2 = 0m;
                tbl.t_kt_amt2 = 0m;
                tbl.t_shm_amt2 = 0m;
                tbl.t_piutang_amt2 = 0m;
                tbl.m_item_old_id = 0;
                tbl.t_spr_promo_dp_amt = 0m;
                tbl.t_spr_promo_id = 0;
                tbl.m_company_id = db.tb_m_company.FirstOrDefault()?.m_company_id ?? 0;
                tbl.is_promo = false; 
                tbl.t_spr_promo_id = 0;
                tbl.t_spr_promo_dp_type = "";
                tbl.t_spr_promo_dp_amt = 0m;
                tbl.t_spr_promo_dp_persen = 0;
                tbl.t_spr_promo_dp_note = "";
                tbl.t_dp_type = "Tidak";
                tbl.t_kt_type = "Tidak";
                tbl.t_shm_type = "Tidak";
                tbl.t_piutang_type_bayar = "Tunai";
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_spr.Find(id);
            }

            ViewBag.action = action;
            var t_dp_account_id = new SelectList(GetCOAInterface(new List<string> { "PIUTANG DP" }), "m_account_id", "m_account_name", tbl.t_dp_account_id);
            ViewBag.t_dp_account_id = t_dp_account_id;
            var t_kt_account_id = new SelectList(GetCOAInterface(new List<string> { "PIUTANG KT" }), "m_account_id", "m_account_name", tbl.t_kt_account_id);
            ViewBag.t_kt_account_id = t_kt_account_id;
            var t_shm_account_id = new SelectList(GetCOAInterface(new List<string> { "PIUTANG SHM" }), "m_account_id", "m_account_name", tbl.t_shm_account_id);
            ViewBag.t_shm_account_id = t_shm_account_id;
            ViewBag.m_marketing_id = new SelectList(db.Database.SqlQuery<tb_marketing>($"Select * From (select m_marketing_id, m_marketing_name, 1 seq from tb_m_marketing Union ALL select 0 m_marketing_id, 'Lain-Lain' m_marketing_name, 0 seq) m Order By seq").ToList(), "m_marketing_id", "m_marketing_name", tbl.m_marketing_id);
            ViewBag.m_promo_id = new SelectList((db.tb_m_item.Where(x => x.m_item_flag == "ACTIVE" && x.m_item_type == "Promo").ToList()), "m_item_id", "m_item_name");

            ViewBag.m_cust_name = (tbl.m_cust_id != 0 ? db.tb_m_cust.FirstOrDefault(x => x.m_cust_id == tbl.m_cust_id).m_cust_name : "");
            ViewBag.m_item_name = (tbl.m_item_id != 0 ? db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id).m_item_name : "");
            ViewBag.m_proyek_name = (tbl.m_proyek_id != 0 ? db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == tbl.m_proyek_id).m_proyek_name : "");
            ViewBag.t_utj_no = (tbl.t_utj_id != 0 ? db.tb_t_utj.FirstOrDefault(x => x.t_utj_id == tbl.t_utj_id).t_utj_no : "");
            ViewBag.t_promo_total_angsuran = 1m;
            ViewBag.t_promo_amt = 0m;
            ViewBag.t_promo_total_amt = 0m;
            ViewBag.m_promo_seq = 0;

            ViewBag.t_dp_type = new SelectList(GetList1(), "Value", "Text", tbl.t_dp_type);
            ViewBag.t_dp_type_bayar = new SelectList(GetList2(), "Value", "Text", tbl.t_dp_type_bayar);

            ViewBag.t_kt_type = new SelectList(GetList1(), "Value", "Text", tbl.t_kt_type);
            ViewBag.t_kt_type_bayar = new SelectList(GetList2(), "Value", "Text", tbl.t_kt_type_bayar);

            ViewBag.t_shm_type = new SelectList(GetList1(), "Value", "Text", tbl.t_shm_type);
            ViewBag.t_shm_type_bayar = new SelectList(GetList2(), "Value", "Text", tbl.t_shm_type_bayar);
            ViewBag.t_piutang_type_bayar = new SelectList(GetList2(), "Value", "Text", tbl.t_piutang_type_bayar);
            return View(tbl);
        } 

        private static string GenTransNo(string prefix, string field_no, string tablename, int m_company_id, string transdate = "", QL_MIS_MAGNAEntities db = null)
        {
            var sNo = $"{prefix}-{(string.IsNullOrEmpty(transdate) ? GetServerTime().ToString("yyyy.MM") : DateTime.Parse(transdate).ToString("yyyy.MM"))}-";
            var sSql = $"SELECT ISNULL(MAX(CAST(RIGHT({field_no}, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM {tablename} WHERE {field_no} LIKE '{sNo}%' AND t_spr_type_trans='Baru'";
            if (db == null) db = new QL_MIS_MAGNAEntities();
            var result = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (result == 0) result = 1;
            return sNo + GenNumberString(result, 4);
        }
               
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_spr tbl, List<t_promo> dtl, string action)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed";
            var hdrid = ""; var servertime = GetServerTime();
            int r_piutang_ref_id = db.tb_r_piutang.AsNoTracking().ToList().Max(x => x.r_piutang_id);
            if (string.IsNullOrEmpty(tbl.t_utj_type)) { tbl.t_utj_type = ""; }
            if (string.IsNullOrEmpty(tbl.t_spr_promo_dp_type)) { tbl.t_spr_promo_dp_type = ""; }
            if (string.IsNullOrEmpty(tbl.t_spr_promo_dp_note)) { tbl.t_spr_promo_dp_note = ""; }
            if (string.IsNullOrEmpty(tbl.t_spr_note)) { tbl.t_spr_note = ""; }
            if (string.IsNullOrEmpty(tbl.t_shm_status)) { tbl.t_shm_status = ""; }

            //if (tbl.t_spr_date < GetCutOffdate())
            //    msg += $"Silahkan pilih Tgl. Transfer {tbl.t_spr_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />";

            if (tbl.m_cust_id == 0) { msg += "Silahkan pilih customer!<br/>"; }
            if (tbl.m_item_id == 0) { msg += "Silahkan pilih unit!<br/>"; }
            if (tbl.m_marketing_id == 0 && string.IsNullOrEmpty(tbl.m_marketing_name)) { msg += "Silahkan isi nama marketing!<br/>"; }  

            if (tbl.is_promo == true) {
                if (dtl == null || dtl.Count() <= 0) { msg += $"<strong>Silakan pilih detail promo dulu..!!</strong><br/>"; } 
                else if (dtl.Count() > 0)
                {
                    foreach (var item in dtl) { if (item.t_promo_total_amt == 0m) { msg += $"<strong>SPR ada promo dan nominal promo tidak boleh nol..!!</strong><br/>"; } }
                }
            } 

            if (tbl.t_dp_type == "Ya")
            {
                if (tbl.t_dp_type_bayar == "Angsuran" && tbl.t_dp_total_angsuran <= 0) { msg += "Total angsuran uang muka harus lebih dari 0!<br>"; }
                if (tbl.t_dp_amt <= 0) { msg += "Jumlah uang muka harus lebih dari 0!<br>"; }
            }
            else { tbl.t_dp_amt = 0m; tbl.t_dp_type_bayar = "-"; }

            if (tbl.t_kt_type == "Ya")
            {
                if (tbl.t_kt_type_bayar == "Angsuran" && tbl.t_kt_total_angsuran <= 0) { msg += "Total angsuran kelebihan tanah harus lebih dari 0!<br>"; }
                if (tbl.t_kt_amt <= 0) { msg += "Jumlah kelebihan tanah harus lebih dari 0!<br>"; }
            }
            else { tbl.t_kt_amt = 0m; tbl.t_kt_type_bayar = "-"; }

            if (tbl.t_shm_type == "Ya")
            {
                if (tbl.t_shm_type_bayar == "Angsuran" && tbl.t_shm_total_angsuran <= 0) { msg += "Total angsuran SHM harus lebih dari 0!<br>"; }
                if (tbl.t_shm_amt <= 0) { msg += "Jumlah SHM harus lebih dari 0!<br>"; }
            }
            else { tbl.t_shm_amt = 0m; tbl.t_shm_type_bayar = "-"; }

            if (tbl.t_piutang_type_bayar == "Angsuran")
            {
                if (tbl.t_piutang_total_angsuran <= 0) { msg += "Total angsuran KPR harus lebih dari 0!<br>"; }
                if (tbl.t_piutang_amt <= 0) { msg += "Jumlah KPR harus lebih dari 0!<br>"; }
                if (tbl.t_spr_total_amt <= 0) { msg += "Total SPR harus lebih dari 0!<br>"; }
            }
            else if (tbl.t_spr_total_amt < 0) { msg += "Total SPR tidak boleh kurang dari 0!<br>"; }
            if (string.IsNullOrEmpty(msg))
            {
                if (tbl.t_spr_status == "Revised") { tbl.t_spr_status = "In Process"; }
                if (tbl.t_spr_status == "Post")
                {
                    tbl.t_spr_no = GenTransNo($"SPR", "t_spr_no", "tb_t_spr", 0, tbl.t_spr_date.ToString("MM/dd/yyyy"));
                    tbl.posted_by = Session["UserID"].ToString();
                    tbl.posted_at = servertime;
                }
                else { tbl.t_spr_no = ""; }
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.t_dp_amt_old = 0; tbl.t_piutang_amt_old = 0; tbl.t_kt_amt_old = 0;
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            db.tb_t_spr.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = GetServerTime();
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                        }
                        db.SaveChanges();

                        if (tbl.is_promo)
                        {
                            msg = InsertPromo(tbl, dtl);
                            if (!string.IsNullOrEmpty(msg)) { objTrans.Rollback(); result = "failed"; return Json(new { result, msg }, JsonRequestBehavior.AllowGet); }
                        } 

                        if (tbl.t_spr_status == "Post")
                        {
                            tbl.t_spr_reff_id = 0;
                            tbl.t_spr_awal_id = tbl.t_spr_id;
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var dt_unit = db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id);
                            dt_unit.m_item_flag = "Terjual";
                            db.Entry(dt_unit).State = EntityState.Modified;

                            if (tbl.t_utj_id != 0)
                            {
                                var dt_utj = db.tb_t_utj.FirstOrDefault(x => x.t_utj_id == tbl.t_utj_id);
                                dt_utj.t_utj_status = "Complete";
                                db.Entry(dt_utj).State = EntityState.Modified;

                                var r_piutang = db.tb_r_piutang.AsNoTracking().Where(x => x.m_cust_id == tbl.m_cust_id && x.m_item_id == tbl.m_item_id && x.r_piutang_type == "UTJ" && x.r_piutang_ref_id == tbl.t_utj_id);
                                foreach(var items in r_piutang)
                                {
                                    items.t_spr_id = tbl.t_spr_id;
                                    items.t_spr_awal_id = tbl.t_spr_id;
                                    db.Entry(items).State = EntityState.Modified;
                                    db.SaveChanges();
                                } 
                            }

                            //Block Post Transaction
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_spr_uid, "tb_t_spr", tbl.t_spr_id, tbl.updated_by, tbl.updated_at));

                            // --> DP
                            if (tbl.t_dp_type == "Ya" && tbl.t_dp_type_bayar == "Tunai" && tbl.t_dp_amt > 0)
                            {
                                var dp = db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "DP", "tb_t_spr_dp", r_piutang_ref_id + 1, tbl.t_spr_no, tbl.t_spr_date, tbl.t_spr_date, Convert.ToDateTime("1/1/1900".ToString()), tbl.t_dp_account_id, tbl.t_dp_amt, 0m, "Uang Muka", tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0m, tbl.t_spr_id, tbl.t_spr_awal_id)); db.SaveChanges();
                                r_piutang_ref_id = dp.r_piutang_id;
                            }
                            // --> KT
                            if (tbl.t_kt_type == "Ya" && tbl.t_kt_type_bayar == "Tunai" && tbl.t_kt_amt > 0)
                            {
                                var kt = db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "KT", "tb_t_spr_kt", r_piutang_ref_id + 1, tbl.t_spr_no, tbl.t_spr_date, tbl.t_spr_date, Convert.ToDateTime("1/1/1900".ToString()), tbl.t_kt_account_id, tbl.t_kt_amt, 0m, "Kelebihan Tanah", tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0m, tbl.t_spr_id, tbl.t_spr_awal_id)); db.SaveChanges(); r_piutang_ref_id = kt.r_piutang_id;
                            }
                            // --> SHM
                            if (tbl.t_shm_type == "Ya" && tbl.t_shm_type_bayar == "Tunai" && tbl.t_shm_amt > 0)
                            {
                                var shm = db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "SHM", "tb_t_spr_shm", r_piutang_ref_id + 1, tbl.t_spr_no, tbl.t_spr_date, tbl.t_spr_date, Convert.ToDateTime("1/1/1900".ToString()), tbl.t_shm_account_id, tbl.t_shm_amt, 0m, "SHM", tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0m, tbl.t_spr_id, tbl.t_spr_awal_id)); db.SaveChanges(); r_piutang_ref_id = shm.r_piutang_id;
                                tbl.t_shm_status = "Post";
                            }
                            // --> KPR
                            if (tbl.t_piutang_type_bayar == "Tunai" && tbl.t_piutang_amt > 0)
                            {
                                db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "KPR", "tb_t_spr", r_piutang_ref_id + 1, tbl.t_spr_no, tbl.t_spr_date, tbl.t_spr_date, Convert.ToDateTime("1/1/1900".ToString()), db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == tbl.m_proyek_id).m_account_id, tbl.t_piutang_amt, 0m, "KPR Tunai", tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0m, tbl.t_spr_id, tbl.t_spr_awal_id)); db.SaveChanges();
                            } 

                            // --> Insert Jurnal
                            msg = PostJurnal(tbl);
                            if (!string.IsNullOrEmpty(msg))
                            {
                                objTrans.Rollback(); result = "failed";
                                return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        db.SaveChanges();
                        objTrans.Commit();

                        hdrid = tbl.t_spr_id.ToString(); result = "success";
                        if (tbl.t_spr_status == "Post") { msg = $"data telah diposting dengan nomor transaksi {tbl.t_spr_no}"; }
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += $"Entity of type {eve.Entry.Entity.GetType().Name} in state {eve.Entry.State} has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors) { msg += $"- Property: {ve.PropertyName}, Error: {ve.ErrorMessage}<br />"; }
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        } 

        private string InsertPromo(tb_t_spr tbl, List<t_promo> dtl, string msg = "")
        { 
            try
            {
                var d_id = dtl.Select(s => s.t_promo_id).ToList();
                var dt_d = db.tb_t_promo.Where(a => a.t_spr_id == tbl.t_spr_id && !d_id.Contains(a.t_promo_id));
                if (dt_d != null && dt_d.Count() > 0) {
                    var cek_kartu = db.tb_r_promo.AsNoTracking().Where(x => (dt_d.Select(xx => xx.t_promo_id)).Contains(x.t_promo_id));
                    if (cek_kartu.Count() > 0) foreach (var item in dtl) { msg += $"<strong>Promo {item.m_promo_name} sudah ada di kartu..!!</strong><br/>"; }
                    else { db.tb_t_promo.RemoveRange(dt_d); }
                } 
                
                if (dtl != null)
                {
                    tb_t_promo tbldtl;
                    foreach (var item in dtl)
                    {
                        var rPromo = db.tb_r_promo.AsNoTracking().Where(x => x.t_promo_id == item.t_promo_id && x.t_spr_id == tbl.t_spr_id);
                        var cek_data = db.tb_t_promo.Where(a => a.t_promo_id == item.t_promo_id);
                        tbldtl = (tb_t_promo)MappingTable(new tb_t_promo(), item);
                        if (cek_data != null && cek_data.Count() > 0) { tbldtl = db.tb_t_promo.FirstOrDefault(a => a.t_promo_id == item.t_promo_id); }
                        tbldtl.t_promo_date = GetServerTime();
                        tbldtl.t_promo_no = tbl.t_spr_no;
                        tbldtl.t_spr_id = tbl.t_spr_id;
                        tbldtl.t_spr_awal_id = tbl.t_spr_id;
                        tbldtl.m_item_id = tbl.m_item_id;
                        tbldtl.m_proyek_id = tbl.m_proyek_id;
                        tbldtl.m_cust_id = tbl.m_cust_id;
                        tbldtl.t_promo_status = (item.t_promo_type_bayar == "Tunai" ? "Post" : "");
                        tbldtl.t_promo_note = $"{db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbldtl.m_promo_id).m_item_name} ({item.t_promo_type_bayar})" ;
                        tbldtl.created_by = tbl.updated_by;
                        tbldtl.created_at = tbl.created_at;
                        tbldtl.updated_at = tbl.updated_at;
                        tbldtl.updated_by = tbl.updated_by;

                        if (cek_data != null && cek_data.Count() > 0) {
                            if (rPromo == null || rPromo.Count() <= 0) { db.Entry(tbldtl).State = EntityState.Modified; } 
                        }
                        else { db.tb_t_promo.Add(tbldtl); } db.SaveChanges();
                        
                        if (rPromo == null || rPromo.Count() <= 0)
                        {   
                            if (item.t_promo_type_bayar == "Tunai" && tbl.t_spr_status == "Post")
                            {
                                db.tb_r_promo.Add(Insert_R_Promo(tbl.m_company_id, tbldtl.t_promo_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, tbl.t_spr_id, tbl.t_spr_id, (int)item.m_promo_id, "Promo", "tb_t_promo", tbldtl.t_promo_id, tbl.t_spr_no, tbldtl.t_promo_date, db.tb_m_item.FirstOrDefault(x => x.m_item_id == item.m_promo_id)?.m_account_id ?? 0, tbldtl.t_promo_total_amt, new DateTime(1900, 1, 1), tbldtl.t_promo_status, tbldtl.created_by, tbldtl.created_at, tbldtl.t_promo_note ?? "Promo Tunai")); db.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.ToString(); 
            }
            db.SaveChanges(); return msg;
        }

        private string PostJurnal(tb_t_spr tbl, string msg = "")
        {  
            try
            {
                var new_dt_gl = new List<dt_gl>();
                var dp_account_id = GetCOAInterface(new List<string> { "PIUTANG DP" }).FirstOrDefault()?.m_account_id ?? 0;
                var kt_account_id = GetCOAInterface(new List<string> { "PIUTANG KT" }).FirstOrDefault()?.m_account_id ?? 0;
                var shm_account_id = GetCOAInterface(new List<string> { "PIUTANG SHM" }).FirstOrDefault()?.m_account_id ?? 0;
                var kpr_account_id = GetCOAInterface(new List<string> { "PIUTANG KPR" }).FirstOrDefault()?.m_account_id ?? 0;
                var jual_account_id = GetCOAInterface(new List<string> { "HUTANG PENJUALAN" }).FirstOrDefault()?.m_account_id ?? 0;
                var total_ar = tbl.t_dp_amt + tbl.t_kt_amt + tbl.t_shm_amt + tbl.t_piutang_amt;
                if (tbl.t_dp_amt != 0) new_dt_gl.Add(new dt_gl { gl_amt = tbl.t_dp_amt, account_id = dp_account_id });
                if (tbl.t_kt_amt != 0) new_dt_gl.Add(new dt_gl { gl_amt = tbl.t_kt_amt, account_id = kt_account_id });
                if (tbl.t_shm_amt != 0) new_dt_gl.Add(new dt_gl { gl_amt = tbl.t_shm_amt, account_id = shm_account_id });

                new_dt_gl.Add(new dt_gl { gl_amt = tbl.t_piutang_amt, account_id = kpr_account_id });
                new_dt_gl.Add(new dt_gl { gl_amt = total_ar * -1, account_id = jual_account_id });
                var seq = 0; var gl_note = tbl.t_spr_note == "" ? $"SPR {tbl.t_spr_type_trans.ToUpper()}" : tbl.t_spr_note;
                if (new_dt_gl != null && new_dt_gl.Count() > 0)
                {
                    var db_cr = "";
                    foreach (var item in new_dt_gl)
                    {
                        if (item.gl_amt > 0) { db_cr = "D"; } else { db_cr = "C"; }
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_spr", tbl.t_spr_id, tbl.t_spr_no, tbl.t_spr_date, item.account_id, db_cr, Math.Abs(item.gl_amt), gl_note, tbl.created_by, tbl.created_at, tbl.m_cust_id));
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.ToString();
            }
            db.SaveChanges(); return msg;
        }

        [HttpPost, ActionName("UpdatePromo")]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePromo(int id, bool is_promo, List<t_promo> dtl, string msg = "")
        {
            tb_t_spr tbl = db.tb_t_spr.Find(id); string result = "success";
            if (tbl == null) { result = "failed"; msg = "Data can't be found!"; }
            if (dtl == null && is_promo) { msg += $"<strong>Silakan pilih detail promo dulu..!!</strong><br/>"; }
            if (is_promo && msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.is_promo = is_promo;
                        db.Entry(tbl).State = EntityState.Modified;
                        db.SaveChanges();
                        msg = InsertPromo(tbl, dtl);
                        if (!string.IsNullOrEmpty(msg)) { 
                            objTrans.Rollback(); 
                            result = "failed"; return Json(new { result, msg }, JsonRequestBehavior.AllowGet); 
                        }
                        db.SaveChanges(); msg = tbl.t_spr_no; objTrans.Commit(); 
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback(); result = "failed"; msg = ex.ToString();
                    } 
                }
            } 
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet); 
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tb_t_spr tbl = db.tb_t_spr.Find(id); string result = "success"; string msg = string.Empty;
            if (tbl == null) { result = "failed"; msg = "Data can't be found!"; }
            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try {
                        var trndtl = db.tb_r_promo.Where(a => a.t_spr_id == tbl.t_spr_id && a.m_item_id == tbl.m_item_id && a.m_cust_id == tbl.m_cust_id);
                        db.tb_t_spr.Remove(tbl); db.SaveChanges(); objTrans.Commit();
                    }
                    catch (Exception ex) { objTrans.Rollback(); result = "failed"; msg = ex.ToString(); }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("UpdateUTJ")]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUTJ(int id, int t_utj_id, string t_utj_type, decimal t_utj_amt)
        {

            tb_t_spr tbl = db.tb_t_spr.Find(id);
            tb_r_piutang rPiutang = db.tb_r_piutang.FirstOrDefault(r => r.m_item_id == tbl.m_item_id && r.m_cust_id == tbl.m_cust_id);
            string result = "";
            string msg = "";
            if (tbl == null) result = "failed"; msg = "Data can't be found!";
            if (result == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.t_utj_id = t_utj_id;
                        tbl.t_utj_type = t_utj_type;
                        tbl.t_utj_amt = t_utj_amt;
                        db.Entry(tbl).State = EntityState.Modified;
                        rPiutang.t_spr_id = tbl.t_spr_id;
                        rPiutang.t_spr_awal_id = tbl.t_spr_id;
                        db.Entry(rPiutang).State = EntityState.Modified;
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback(); result = "failed"; msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            string rptname = "PrintSPR";
            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), $"rpt{rptname}.rpt"));

            sSql = $"SELECT * FROM tb_v_print_spr WHERE t_spr_id IN ({ids}) ORDER BY t_spr_id";
            var dtRpt = new ClassConnection().GetDataTable(sSql, "data");

            report.SetDataSource(dtRpt);
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}