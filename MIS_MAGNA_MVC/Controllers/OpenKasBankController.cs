﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class OpenKasBankController : Controller
    {
        // GET: OpenKasBank
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        #region OpenKasBank

        public OpenKasBankController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.periodstart = GetServerTime();
            ViewBag.periodend = GetServerTime();
            return View();
        }

        public class tb_r_kb_h : tb_r_kb
        { 
            public string m_account_code { get; set; }
            public string m_account_desc { get; set; }
            public string t_kbm_type { get; set; }
            public decimal r_kb_amt { get; set; }
            public string stype { get; set; } 
        }

        [HttpPost]
        public ActionResult GetDataDetails(string r_kb_ref_table, string periodstart, string periodend)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select * from tb_v_r_kb r Where r_kb_ref_table='{r_kb_ref_table}' /*AND r_kb_date >= Cast('{periodstart} 0:0:0' as Datetime) AND r_kb_date <= Cast('{periodend} 23:59:59' as datetime)*/ Order By r.r_kb_no";
                var tbl = db.Database.SqlQuery<tb_r_kb_h>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            { 
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(List<tb_r_kb_h> tbl, string r_kb_ref_table)
        {
            var msg = ""; var result = "failed";
            var hdrid = new Guid();
            if (tbl == null || tbl.Count <= 0) msg += "Please fill detail Process!<br />";

            if (string.IsNullOrEmpty(msg))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (tbl != null && tbl.Count() > 0)
                        {
                            foreach (var item in tbl)
                            {
                                if (item.r_kb_ref_table == "tb_t_kbk_d")
                                {    
                                    var t_kbk_h = db.tb_t_kbk_h.Where(a => a.t_kbk_h_id == item.r_kb_h_ref_id).FirstOrDefault();
                                    t_kbk_h.t_kbk_no = "";
                                    t_kbk_h.updated_at = GetServerTime();
                                    t_kbk_h.updated_by = Session["UserID"].ToString();
                                    t_kbk_h.t_kbk_h_status = "In Process";
                                    db.Entry(t_kbk_h).State = EntityState.Modified;

                                    var r_gl = db.tb_r_gl.Where(r => r.r_gl_ref_id == item.r_kb_h_ref_id && r.r_gl_ref_table == "tb_t_kbk_h").ToList();
                                    db.tb_r_gl.RemoveRange(r_gl);

                                    var r_kb = db.tb_r_kb.Where(r => r.r_kb_h_ref_id == item.r_kb_h_ref_id && r.r_kb_ref_table == "tb_t_kbk_d").ToList();
                                    if (r_kb != null) db.tb_r_kb.RemoveRange(r_kb);

                                    var kbk_d = db.tb_t_kbk_d.Where(k => k.t_kbk_h_id == item.r_kb_h_ref_id).ToList();
                                    foreach(var items in kbk_d)
                                    { 
                                        if (item.stype == "REFUND")
                                        {
                                            var r_hutang = db.tb_r_hutang.Where(r => r.r_hutang_pay_ref_id == items.t_kbk_d_id && r.r_hutang_pay_ref_no == item.r_kb_no && r.r_hutang_pay_ref_table == item.r_kb_ref_table).FirstOrDefault();
                                            if (r_hutang != null) db.tb_r_hutang.Remove(r_hutang);
                                        }
                                    }
                                }

                                if (item.r_kb_ref_table == "tb_t_kbm_d")
                                {   
                                    var t_kbm_h = db.tb_t_kbm_h.Where(a => a.t_kbm_h_id == item.r_kb_h_ref_id).FirstOrDefault();
                                    t_kbm_h.t_kbm_no = "";
                                    t_kbm_h.updated_at = GetServerTime();
                                    t_kbm_h.updated_by = Session["UserID"].ToString();
                                    t_kbm_h.t_kbm_h_status = "In Process";
                                    db.Entry(t_kbm_h).State = EntityState.Modified;
                                    var r_gl = db.tb_r_gl.Where(r => r.r_gl_ref_id == item.r_kb_h_ref_id && r.r_gl_ref_table == "tb_t_kbm_h");
                                    db.tb_r_gl.RemoveRange(r_gl);

                                    var kbm_d = db.tb_t_kbm_d.Where(k => k.t_kbm_h_id == item.r_kb_h_ref_id).ToList();
                                    foreach (var items in kbm_d)
                                    {
                                        var r_kb = db.tb_r_kb.Where(r => r.r_kb_ref_id == items.t_kbm_d_id && r.r_kb_ref_table == r_kb_ref_table).FirstOrDefault();
                                        if (r_kb != null) db.tb_r_kb.Remove(r_kb);
                                        if (item.stype == "SPR")
                                        {
                                            var r_piutang = db.tb_r_piutang.Where(r => r.r_piutang_pay_ref_id == items.t_kbm_d_id && r.r_piutang_pay_ref_no == item.r_kb_no && r.r_piutang_pay_ref_table == item.r_kb_ref_table);
                                            if (r_piutang != null) db.tb_r_piutang.RemoveRange(r_piutang);
                                        } 
                                    } 
                                   
                                }
                            }
                        }
                        db.SaveChanges();
                        objTrans.Commit();
                        result = "success";
                        tbl = null;
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid, tbl }, JsonRequestBehavior.AllowGet);
        } 

        #endregion
    }
}