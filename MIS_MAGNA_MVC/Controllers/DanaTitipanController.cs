﻿using MIS_MAGNA_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using System.Collections;
using Microsoft.Ajax.Utilities;

namespace MIS_MAGNA_MVC.Controllers
{
    public class DanaTitipanController : Controller
    {
        private QL_MIS_MAGNAEntities db;
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        public string sSql = "";
        public string sWhere = ""; 
        public DanaTitipanController() { db = new QL_MIS_MAGNAEntities(); db.Database.CommandTimeout = 0; }

        public class t_titipan : tb_t_titipan
        {
            public decimal t_titipan_debet_amt { get; set; }
            public decimal t_titipan_credit_amt { get; set; }
            public decimal t_titipan_os_amt { get; set; }
        }

        public class dt_gl
        {
            public decimal r_gl_amt { get; set; }
            public int m_account_id { get; set; }
            public string r_gl_db_cr { get; set; }
        }

        #region Dana Titipan 
        // GET: Titipan
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.periodstart = GetServerTime().ToString("01/dd/yyyy");
            ViewBag.periodend = GetServerTime().ToString("MM/dd/yyyy");
            ViewBag.Title = "Dana Titipan";
            return View();
        }

        [HttpPost]
        public ActionResult GetDataIndex(string periodstart, string periodend)
        {
            sSql = $"select * from ( select h.*, Case When t_titipan_type ='PPN' then 'Titipan Pajak Pembelian' When t_titipan_type ='OTHER' then 'Lain - Lain' When t_titipan_type ='BALIK NAMA' then 'BALIK NAMA' Else 'BPHTB' END t_titipan_type_ref, p.m_proyek_name, c.m_cust_name, i.m_item_name, s.t_spr_no FROM tb_t_titipan h INNER JOIN tb_m_company cmp ON cmp.m_company_id=h.m_company_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = h.m_proyek_id INNER JOIN tb_m_cust c ON c.m_cust_id = h.m_cust_id INNER JOIN tb_m_item i ON i.m_item_id = h.m_item_id INNER JOIN tb_t_spr s ON s.t_spr_id = h.t_spr_id WHERE h.t_titipan_trans_type='TITIPAN' AND Isnull(h.t_titipan_flag, '')='') AS t where 1=1 Order By created_at DESC";
            return getJsResult(sSql);
        }

        [HttpPost]
        public ActionResult GetDataCust(string t_titipan_type_bayar, string t_titipan_type)
        {
            var sList_type_bayar = new List<string>() { "PENGEMBALIAN", "REALISASI" };
            sSql = $"Select distinct c.m_cust_id, c.m_cust_code, c.m_cust_name, c.m_cust_addr, s.m_proyek_id, s.t_spr_id, s.t_spr_awal_id, s.t_spr_no, i.m_item_id, i.m_item_name, (select p.m_proyek_name from tb_m_proyek p where p.m_proyek_id=s.m_proyek_id) m_proyek_name from tb_m_cust c inner join tb_t_spr s on s.m_cust_id = c.m_cust_id Inner Join tb_m_item i ON i.m_item_id=s.m_item_id AND i.m_proyek_id=s.m_proyek_id ";
            if (sList_type_bayar.Contains(t_titipan_type_bayar))
            {
                sSql += $"INNER JOIN ( SELECT r.m_cust_id, r.m_item_id, r.t_spr_id, r.r_piutang_amt - Isnull(SUM(b.r_piutang_pay_amt), 0.0) r_piutang_sisa_amt	FROM tb_r_piutang r LEFT JOIN tb_r_piutang b ON b.r_piutang_ref_id = r.r_piutang_ref_id	AND r.r_piutang_ref_table = b.r_piutang_ref_table AND r.r_piutang_trans_type=b.r_piutang_trans_type AND b.r_piutang_type IN ('REALISASI', 'PENGEMBALIAN') AND b.r_piutang_ref_type = r.r_piutang_ref_type where r.r_piutang_ref_table = 'tb_t_titipan' AND r.r_piutang_type = 'PENERIMAAN' AND r.r_piutang_ref_type = '{t_titipan_type}' AND r.r_piutang_trans_type='TITIPAN' AND r.r_piutang_status = 'Post' GROUP BY r.m_cust_id, r.m_item_id, r.t_spr_id, r.r_piutang_amt ) r ON r.m_cust_id = c.m_cust_id AND r.m_item_id = i.m_item_id AND r.t_spr_id = s.t_spr_id";
            }
            sSql += $" where s.t_spr_status = 'Post' and c.m_cust_flag = 'ACTIVE' {(t_titipan_type_bayar == "REALISASI" ? "AND r_piutang_sisa_amt >= 0" : "")} Order By c.m_cust_code";
            return getJsResult(sSql);
        }

        [HttpPost]
        public ActionResult GetDataItem(int m_cust_id)
        {
            sSql = $"select c.m_cust_id, c.m_cust_name, i.m_proyek_id, p.m_proyek_name, i.m_item_id, i.m_item_name, tu.m_type_unit_name, isnull(s.t_spr_awal_id, 0) t_spr_awal_id, s.t_spr_id, s.t_spr_no, format(s.t_spr_date, 'dd/MM/yyyy') t_spr_date from tb_m_cust c inner join tb_t_spr s on s.m_cust_id = c.m_cust_id inner join tb_m_item i on i.m_item_id = s.m_item_id inner join tb_m_type_unit tu on tu.m_type_unit_id = i.m_item_type_unit_id inner join tb_m_proyek p on p.m_proyek_id = i.m_proyek_id where s.t_spr_status = 'Post' And s.m_cust_id = {m_cust_id}";
            return getJsResult(sSql);
        }

        [HttpPost]
        public ActionResult GetDataRef(string t_titipan_type, int m_item_id)
        {
            sSql = $"select t.t_titipan_id, t.t_titipan_no, format(t.t_titipan_date, 'dd/MM/yyyy') t_titipan_date, t.t_titipan_type_bayar, t.t_titipan_type, t.t_titipan_amt t_titipan_masuk_amt, r_piutang_amt t_titipan_amt, Isnull(r_piutang_pay_amt, 0.0) t_titipan_pay_amt, Isnull(r_piutang_sisa_amt, 0.0) t_titipan_os_amt, t.t_titipan_note, s.t_spr_no, s.t_spr_id, s.t_spr_awal_id FROM tb_t_titipan t INNER JOIN tb_t_spr s ON s.m_item_id = t.m_item_id AND s.m_cust_id = t.m_cust_id AND s.t_spr_id = t.t_spr_id INNER JOIN ( select r.m_cust_id, r.m_item_id, r.t_spr_id, r.r_piutang_amt, Isnull(SUM(b.r_piutang_pay_amt), 0.0) r_piutang_pay_amt, r.r_piutang_amt - Isnull(SUM(b.r_piutang_pay_amt), 0.0) r_piutang_sisa_amt FROM tb_r_piutang r LEFT JOIN tb_r_piutang b ON b.r_piutang_ref_id = r.r_piutang_ref_id AND r.r_piutang_ref_table = b.r_piutang_ref_table AND r.r_piutang_trans_type=b.r_piutang_trans_type AND b.r_piutang_type IN ('REALISASI', 'PENGEMBALIAN') AND b.r_piutang_ref_type = '{t_titipan_type}' WHERE r.r_piutang_ref_table = 'tb_t_titipan' AND r.r_piutang_status = 'Post' AND r.r_piutang_trans_type='TITIPAN' GROUP BY r.m_cust_id, r.m_item_id, r.t_spr_id, r.r_piutang_amt ) r ON r.m_cust_id = t.m_cust_id AND r.m_item_id = t.m_item_id AND r.t_spr_id = s.t_spr_id WHERE t.t_titipan_status = 'Post' AND t.t_titipan_trans_type='TITIPAN' AND r_piutang_sisa_amt > 0 AND t.m_item_id = {m_item_id} AND t.t_titipan_type = '{t_titipan_type}' AND t.t_titipan_type_bayar = 'PENERIMAAN'";
            return getJsResult(sSql);
        }

        [HttpPost]
        public ActionResult GetRefundData(int t_spr_awal_id, int m_cust_id, int m_item_id, int t_titipan_debet_account_id)
        {
            sSql = $@"select r_hutang_ref_id t_refund_id, h.r_hutang_ref_no t_refund_no, h.r_hutang_ref_date t_refund_date
            , p.m_proyek_code m_proyek_name, c.m_cust_name, i.m_item_name, h.r_hutang_amt t_refund_amt
            , h.r_hutang_amt - Isnull(r.r_hutang_pay_amt, 0.0) t_refund_sisa_amt, h.r_hutang_account_id
            , h.m_item_old_id from tb_r_hutang h Inner Join tb_m_cust c ON c.m_cust_id=h.m_cust_id 
            Inner Join tb_m_proyek p ON p.m_proyek_id=h.m_proyek_id 
            Inner Join tb_m_item i ON i.m_item_id=h.m_item_id Outer Apply ( 
            select Isnull(SUM(r.r_hutang_pay_amt), 0.0) r_hutang_pay_amt from tb_r_hutang r Where r.r_hutang_type='PAYMENT' 
                AND r.r_hutang_ref_table=h.r_hutang_ref_table AND r.r_hutang_ref_id=h.r_hutang_ref_id 
            ) r Where h.r_hutang_type='Refund' AND r_hutang_ref_table in ('tb_t_spr_new', 'tb_t_refund')
            AND h.r_hutang_amt - Isnull(r.r_hutang_pay_amt, 0.0) > 0.0 AND h.m_cust_id={m_cust_id} 
            /*AND i.m_item_id={m_item_id} AND h.t_spr_awal_id={t_spr_awal_id} 
            AND h.r_hutang_account_id={t_titipan_debet_account_id}*/ Order By h.r_hutang_ref_no";
            return getJsResult(sSql);
        }

        [HttpPost]
        public ActionResult GetAccount(string t_titipan_type_bayar, bool t_titipan_ref_type)
        {
            JsonResult js = null;
            try
            {
                //var var_db = "KAS_BANK_TITIPAN"; var var_cr = "UM TITIPAN";
                var var_db = new List<string>();
                var var_cr = new List<string>();
                var var_profit = new List<string>() { "PENDAPATAN" };
                if (t_titipan_type_bayar == "REALISASI")
                {
                    var_db.Add("UM TITIPAN");
                    var_cr.Add("BIAYA TITIPAN");
                }
                else if (t_titipan_type_bayar == "PENGEMBALIAN")
                {
                    var_db.Add("UM TITIPAN");
                    var_cr.Add("BANK_SISTEM");
                    var_cr.Add("KAS_SISTEM");
                }
                else if (t_titipan_type_bayar == "PENERIMAAN")
                {
                    var_cr.Add("UM TITIPAN");
                    if (t_titipan_ref_type == true) { var_db.Add("HUTANG LAIN"); }
                    else { var_db.Add("BANK_SISTEM"); var_db.Add("KAS_SISTEM"); }
                }

                var db = GetCOAInterface(var_db);
                var cr = GetCOAInterface(var_cr);
                var crp = GetCOAInterface(var_profit);

                js = Json(new { result = "", db, cr, crp }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue; 
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        private void InitDDL(tb_t_titipan tbl)
        {
            var sList = new List<string>() { "PB", "JB" };
            var getCompany = db.tb_m_company.Select(m => new { m.m_company_id, m.m_company_name, m.active_flag }).ToList();
            ViewBag.m_company_id = new SelectList(getCompany, "m_company_id", "m_company_name", tbl.m_company_id);
            ViewBag.t_titipan_type = new SelectList(GetTypeTitipan().Where(x => !sList.Contains(x.Value)).ToList(), "Value", "Text", tbl.t_titipan_type);

            var var_db = new List<string>();
            var var_cr = new List<string>();
            var var_profit = new List<string>() { "PENDAPATAN" };
            if (tbl.t_titipan_type_bayar == "REALISASI")
            {
                var_db.Add("UM TITIPAN");
                var_cr.Add("BIAYA TITIPAN");
            }
            else if (tbl.t_titipan_type_bayar == "PENERIMAAN")
            {
                var_cr.Add("UM TITIPAN");
                if (tbl.t_titipan_ref_type == true) { var_db.Add("HUTANG LAIN"); }
                else { var_db.Add("BANK_SISTEM"); var_db.Add("KAS_SISTEM"); }
            } 

            ViewBag.t_titipan_debet_account_id = new SelectList(GetCOAInterface(var_db), "m_account_id", "m_account_name", tbl.t_titipan_debet_account_id);
            ViewBag.t_titipan_credit_account_id = new SelectList(GetCOAInterface(var_cr), "m_account_id", "m_account_name", tbl.t_titipan_credit_account_id);
            ViewBag.t_titipan_profit_account_id = new SelectList(GetCOAInterface(var_profit), "m_account_id", "m_account_name", tbl.t_titipan_profit_account_id);
        }

        private static string GenTransNo(string prefix, string field_no, string tablename, int m_company_id, string transdate = "", QL_MIS_MAGNAEntities db = null)
        {
            var sNo = $"{prefix}-{(string.IsNullOrEmpty(transdate) ? GetServerTime().ToString("yyyy.MM") : DateTime.Parse(transdate).ToString("yyyy.MM"))}-";
            var sSql = $"select ISNULL(MAX(CAST(RIGHT({field_no}, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM {tablename} where {field_no} LIKE '{sNo}%' ";
            if (db == null) db = new QL_MIS_MAGNAEntities();
            var result = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (result == 0) result = 1;
            return sNo + GenNumberString(result, 4);
        }
         
        [HttpPost]
        public ActionResult GetType(string t_titipan_type_bayar)
        {    
            JsonResult js = null;
            try
            {
                var sList = new List<string>() { "PB", "JB" };
                var tbl = GetTypeTitipan().Where(x => !sList.Contains(x.Value)).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_t_titipan tbl;
            string action = "New Data";
            ViewBag.Title = "Dana Titipan";
            if (id == null)
            {
                tbl = new tb_t_titipan();
                tbl.t_titipan_uid = Guid.NewGuid();
                tbl.t_titipan_date = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.t_titipan_status = "In Process";
                tbl.t_titipan_trans_type = "TITIPAN";
                tbl.t_kb_no = "-";
                tbl.t_titipan_no = GenTransNo($"TC", "t_titipan_no", "tb_t_titipan", tbl.m_company_id, tbl.t_titipan_date.ToString("MM/dd/yyyy"));
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_titipan.Find(id);
            }

            ViewBag.action = action;
            InitDDL(tbl);
            decimal t_titipan_debet_amt = GetDataTitipan(tbl, 1).FirstOrDefault()?.t_titipan_debet_amt ?? 0m;
            decimal t_titipan_credit_amt = GetDataTitipan(tbl, -1).FirstOrDefault()?.t_titipan_credit_amt ?? 0m;

            ViewBag.m_cust_name = (tbl.m_cust_id != 0 ? db.tb_m_cust.FirstOrDefault(x => x.m_cust_id == tbl.m_cust_id).m_cust_name : "");
            ViewBag.m_item_name = (tbl.m_item_id != 0 ? db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id).m_item_name : "");
            ViewBag.m_proyek_name = (tbl.m_proyek_id != 0 ? db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == tbl.m_proyek_id).m_proyek_name : "");
            ViewBag.t_spr_no = (tbl.t_spr_id != 0 ? db.tb_t_spr.FirstOrDefault(x => x.t_spr_id == tbl.t_spr_id).t_spr_no : "");
            ViewBag.t_titipan_ref_no = GetDataTitipan(tbl, 1).FirstOrDefault()?.t_titipan_no ?? "";
            if (action == "Update Data" && tbl.t_titipan_type_bayar == "PENERIMAAN")
                ViewBag.tableoid = db.tb_r_kb.FirstOrDefault(r => r.r_kb_no == tbl.t_kb_no)?.r_kb_h_ref_id ?? 0;
            else if (action == "Update Data" && tbl.t_titipan_type_bayar == "PENERIMAAN" && tbl.t_titipan_ref_type == true) { ViewBag.tableoid = tbl.t_titipan_id; }

            ViewBag.t_refund_no = db.tb_r_hutang.FirstOrDefault(x => x.r_hutang_ref_id == tbl.r_hutang_ref_id && x.r_hutang_ref_table == "tb_t_spr_new" && x.r_hutang_type != "PAYMENT")?.r_hutang_ref_no ?? "";
            ViewBag.t_titipan_masuk_amt = t_titipan_debet_amt;
            ViewBag.t_titipan_pay_amt = t_titipan_credit_amt;
            ViewBag.t_titipan_os_amt = t_titipan_debet_amt - t_titipan_credit_amt - tbl.t_titipan_amt;
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_titipan tbl, string action, string t_titipan_ref_no = "")
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            var sList_type_bayar = new List<string>() { "PENERIMAAN", "REALISASI" };
            var msg = ""; var result = "failed"; var type_bayar = "";
            var hdrid = ""; var servertime = GetServerTime();
            var m_account_id = tbl.t_titipan_debet_account_id;
            if (tbl.t_titipan_date < GetCutOffdate())
                msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_titipan_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />";
            if (tbl.m_cust_id == 0) msg += "Silahkan pilih customer !<br/>";
            if (tbl.m_item_id == 0) msg += "Silahkan pilih unit !<br/>";
            if (string.IsNullOrEmpty(tbl.t_titipan_type)) msg += "Silahkan pilih tipe !<br/>";
            if (string.IsNullOrEmpty(tbl.t_titipan_type_bayar)) msg += "Silahkan pilih tipe bayar !<br/>";
            else if (tbl.t_titipan_ref_id == 0 && tbl.t_titipan_type_bayar == "REALISASI") { msg += "Silahkan pilih no. referensi !<br/>"; }

            if (tbl.r_hutang_ref_id == 0) { tbl.t_titipan_ref_type = false; }
            else if (sList_type_bayar.Contains(tbl.t_titipan_type_bayar))
                if (tbl.t_titipan_ref_type == true && tbl.r_hutang_ref_id == 0) { msg += "Silahkan pilih No. kelebihan bayar !<br/>"; }

            if (tbl.t_titipan_amt <= 0) msg += "Nominal harus lebih dari 0 !<br/>";
            if (tbl.t_titipan_debet_account_id == 0) msg += "Silahkan pilih account debet !<br/>";
            if (tbl.t_titipan_credit_account_id == 0) msg += "Silahkan pilih account credit !<br/>";
            if (tbl.t_titipan_type_bayar == "PENERIMAAN") m_account_id = tbl.t_titipan_debet_account_id;

            var dt_t = db.Database.SqlQuery<t_titipan>($"select *, t_titipan_debet_amt-(t_titipan_credit_amt + {tbl.t_titipan_amt}) t_titipan_os_amt from ( select s.t_titipan_id, t_titipan_amt t_titipan_debet_amt, Isnull(( select SUM(t_titipan_amt) from tb_t_titipan r where r.t_titipan_type_min=-1 AND r.t_titipan_type_bayar='{tbl.t_titipan_type_bayar}' AND r.m_item_id=s.m_item_id AND s.m_cust_id=r.m_cust_id AND r.t_titipan_ref_id=s.t_titipan_id AND r.t_titipan_id!={tbl.t_titipan_id} ), 0.0) t_titipan_credit_amt, s.t_titipan_status from tb_t_titipan s where t_titipan_type_bayar='PENERIMAAN' AND t_titipan_type ='{tbl.t_titipan_type}' AND t_titipan_type_min=1 AND s.t_titipan_id={tbl.t_titipan_ref_id} AND s.m_item_id={tbl.m_item_id} AND s.m_cust_id={tbl.m_cust_id}) t").ToList();

            var kb_no = db.Database.SqlQuery<listcoa>($"select h.m_interface_name m_account_var, d.m_account_id from tb_m_interface_h h Inner Join tb_m_interface_d d ON h.m_interface_h_id=d.m_interface_h_id WHere h.m_interface_name <> 'KAS_BANK_TITIPAN'").ToList();

            if (tbl.t_titipan_type_bayar == "PENERIMAAN" && tbl.t_titipan_ref_type == false)
                type_bayar = kb_no.Where(d => d.m_account_id == tbl.t_titipan_debet_account_id).FirstOrDefault().m_account_var == "BANK_SISTEM" ? "BM" : "KM";

            if (string.IsNullOrEmpty(msg))
            {
                tbl.t_titipan_note = tbl.t_titipan_note ?? "";
                if (tbl.t_titipan_type_bayar == "PENERIMAAN") tbl.t_titipan_type_min = 1;
                else if (tbl.t_titipan_type_bayar == "REALISASI") tbl.t_titipan_type_min = -1;
                if (tbl.t_titipan_status == "Revised") tbl.t_titipan_status = "In Process";
                tbl.t_titipan_type_min = (tbl.t_titipan_type_bayar == "PENERIMAAN") ? 1 : -1;

                if (tbl.t_titipan_status == "Post")
                {
                    tbl.t_titipan_no = GenTransNo($"TC", "t_titipan_no", "tb_t_titipan", tbl.m_company_id, tbl.t_titipan_date.ToString("MM/dd/yyyy"));
                    tbl.posted_by = Session["UserID"].ToString();
                    tbl.posted_at = servertime;

                    if (tbl.t_titipan_type_bayar == "PENERIMAAN" && tbl.t_titipan_ref_type == false)
                        tbl.t_kb_no = GenerateTransNo($"{type_bayar}", "r_kb_no", "tb_r_kb", tbl.m_company_id, tbl.t_titipan_date.ToString("MM/dd/yyyy"));
                    else if (tbl.t_titipan_type_bayar == "PENERIMAAN" && tbl.t_titipan_ref_type == true)
                        tbl.t_kb_no = db.tb_r_hutang.FirstOrDefault(x => x.m_item_id == tbl.m_item_id && x.m_cust_id == tbl.m_cust_id && x.r_hutang_ref_table == "tb_t_spr_new" && x.r_hutang_ref_id == tbl.r_hutang_ref_id && x.r_hutang_type == "Refund")?.r_hutang_ref_no ?? "";
                    else tbl.t_kb_no = t_titipan_ref_no; 
                }
                tbl.t_titipan_profit_account_id = (tbl.t_titipan_type_bayar != "REALISASI" ? 0 : tbl.t_titipan_profit_account_id);
                if (tbl.t_titipan_type_bayar == "REALISASI" && dt_t.Count() > 0)
                {
                    var dt_ref = dt_t.FirstOrDefault();
                    tbl.t_titipan_profit_amt = dt_ref.t_titipan_os_amt < 0 ? dt_ref.t_titipan_os_amt * -1 : 0m;
                    tbl.t_titipan_profit_account_id = dt_ref.t_titipan_os_amt < 0 ? tbl.t_titipan_profit_account_id : 0;
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            tbl.t_titipan_flag = "";
                            db.tb_t_titipan.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = GetServerTime();
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                        }
                        db.SaveChanges();

                        if (tbl.t_titipan_status == "Post")
                        {
                            // --> Insert Jurnal
                            msg = PostJurnal(ref db, tbl, type_bayar, dt_t);
                            if (!string.IsNullOrEmpty(msg))
                            {
                                objTrans.Rollback(); result = "failed";
                                return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        db.SaveChanges(); objTrans.Commit(); 
                        hdrid = tbl.t_titipan_id.ToString(); result = "success";
                        if (tbl.t_titipan_status == "Post") { msg = $"data telah diposting dengan nomor transaksi {tbl.t_titipan_no}"; }
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors) { msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />"; } 
                        }
                    }
                    catch (Exception ex) { objTrans.Rollback(); msg = ex.ToString(); }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        private string PostJurnal(ref QL_MIS_MAGNAEntities db, tb_t_titipan tbl, string type_bayar, List<t_titipan> dt_t)
        {
            int seq = 1; int t_kb_d_id = 0; int ref_id = 0;
            var msg = ""; var ref_d_table = ""; var t_titipan_note = "";
            var ref_no = ""; var ref_table = ""; var new_dt_gl = new List<dt_gl>();
            var sList_type_bayar = new List<string>() { "PENERIMAAN" };
            decimal r_piutang_amt = 0m; decimal r_piutang_pay_amt = 0m;

            try
            {
                db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_titipan_uid, "tb_t_titipan", tbl.t_titipan_id, tbl.updated_by, tbl.updated_at));
                var kbm = new tb_t_kbm_h(); var kbmd = new tb_t_kbm_d();
                var kbk = new tb_t_kbk_h(); var kbkd = new tb_t_kbk_d();
                if (sList_type_bayar.Contains(tbl.t_titipan_type_bayar) && tbl.t_titipan_ref_type == false)
                {
                    kbm.t_kbm_uid = tbl.t_titipan_uid;
                    kbm.t_kbm_no = tbl.t_kb_no;
                    kbm.t_kbm_ref_no = tbl.t_titipan_no;
                    kbm.t_kbm_type_bayar = type_bayar;
                    kbm.t_kbm_ref_type = "TITIPAN";
                    kbm.t_kbm_date = tbl.t_titipan_date;
                    kbm.t_spr_id = tbl.t_spr_id;
                    kbm.t_spr_awal_id = tbl.t_spr_awal_id;
                    kbm.m_cust_id = tbl.m_cust_id;
                    kbm.m_item_id = tbl.m_item_id;
                    kbm.m_proyek_id = tbl.m_proyek_id;
                    kbm.t_kbm_account_id = tbl.t_titipan_debet_account_id;
                    kbm.t_kbm_h_amt = tbl.t_titipan_amt;
                    kbm.t_kbm_h_status = tbl.t_titipan_status;
                    kbm.t_kbm_h_note = tbl.t_titipan_note;
                    kbm.t_kbm_type = "Proyek";
                    kbm.posted_at = GetServerTime();
                    kbm.posted_by = tbl.posted_by;
                    kbm.created_at = tbl.t_titipan_date;
                    kbm.created_by = tbl.created_by;
                    kbm.updated_at = tbl.updated_at;
                    kbm.updated_by = tbl.updated_by;
                    kbm.m_promo_id = 0;
                    db.tb_t_kbm_h.Add(kbm);
                    db.SaveChanges();

                    kbmd.t_kbm_d_account_id = tbl.t_titipan_credit_account_id;
                    kbmd.t_kbm_ref_id = tbl.t_titipan_id;
                    kbmd.t_kbm_h_id = kbm.t_kbm_h_id;
                    kbmd.t_kbm_ref_table = "tb_t_titipan";
                    kbmd.t_kbm_d_seq = 1;
                    kbmd.t_kbm_disc_amt = 0m;
                    kbmd.t_kbm_d_netto = tbl.t_titipan_amt;
                    kbmd.t_kbm_d_note = tbl.t_titipan_note;
                    kbmd.created_at = tbl.created_at;
                    kbmd.created_by = tbl.created_by;
                    db.tb_t_kbm_d.Add(kbmd);
                    db.SaveChanges();

                    db.tb_r_kb.Add(Insert_R_KB(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "tb_t_kbm_d", tbl.t_titipan_id, type_bayar, tbl.t_kb_no, tbl.t_titipan_date, tbl.t_titipan_debet_account_id, tbl.t_titipan_credit_account_id, tbl.t_titipan_amt, 0m, tbl.t_titipan_note ?? "", tbl.created_by, tbl.t_titipan_date, tbl.t_spr_id, tbl.t_spr_awal_id, kbm.t_kbm_h_id, kbm.t_kbm_type));
                    db.SaveChanges();

                    ref_table = "tb_t_kbm_h"; ref_d_table = "tb_t_kbm_d";
                    t_kb_d_id = kbmd.t_kbm_d_id; ref_no = tbl.t_kb_no; ref_id = kbm.t_kbm_h_id;
                    r_piutang_amt = tbl.t_titipan_amt; r_piutang_pay_amt = 0m;
                    t_titipan_note = $"Penerimaan {tbl.t_titipan_no}";
                } 

                if (!sList_type_bayar.Contains(tbl.t_titipan_type_bayar))
                {  
                    ref_table = "tb_t_titipan"; ref_d_table = "tb_t_titipan";
                    ref_no = tbl.t_titipan_no; ref_id = tbl.t_titipan_id; 
                    r_piutang_amt = 0m; r_piutang_pay_amt = tbl.t_titipan_amt;
                    t_titipan_note = $"Realisasi {tbl.t_titipan_no}";
                }
                else if (sList_type_bayar.Contains(tbl.t_titipan_type_bayar) && tbl.t_titipan_ref_type == true)
                {
                    var dt_ref = db.tb_r_hutang.AsNoTracking().FirstOrDefault(x => x.r_hutang_ref_id == tbl.r_hutang_ref_id); 
                    db.tb_r_hutang.Add(Insert_R_Hutang(tbl.m_company_id, tbl.m_cust_id, dt_ref.m_proyek_id, dt_ref.m_item_id, "PAYMENT", dt_ref.r_hutang_ref_table, tbl.r_hutang_ref_id, dt_ref.r_hutang_ref_no, dt_ref.r_hutang_ref_date, dt_ref.r_hutang_ref_date, tbl.t_titipan_date, dt_ref?.r_hutang_account_id ?? 0, 0m, tbl.t_titipan_amt, t_titipan_note, tbl.created_by, tbl.created_at, "tb_t_titipan", tbl.t_titipan_id, tbl.t_titipan_no, tbl.t_titipan_credit_account_id, dt_ref.t_spr_id, dt_ref.t_spr_awal_id, m_item_old_id: dt_ref?.m_item_old_id ?? 0));

                    ref_table = "tb_t_titipan"; ref_d_table = "tb_t_titipan";
                    ref_no = tbl.t_titipan_no; ref_id = tbl.t_titipan_id;
                    r_piutang_amt = tbl.t_titipan_amt; r_piutang_pay_amt = 0m;
                    t_titipan_note = $"Penerimaan dari hutang lebih bayar {tbl.t_titipan_no}";
                }

                db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, tbl.t_titipan_type_bayar, "tb_t_titipan", (tbl.t_titipan_type_bayar == "PENERIMAAN" ? tbl.t_titipan_id : tbl.t_titipan_ref_id), tbl.t_titipan_no, tbl.t_titipan_date, tbl.t_titipan_date, tbl.t_titipan_date, tbl.t_titipan_credit_account_id, r_piutang_amt, r_piutang_pay_amt, tbl.t_titipan_note == "" ? t_titipan_note : tbl.t_titipan_note, tbl.created_by, tbl.created_at, ref_d_table, t_kb_d_id, ref_no, tbl.t_titipan_debet_account_id, 0m, tbl.t_spr_id, tbl.t_spr_awal_id, tbl.t_titipan_type, "TITIPAN", tbl.t_titipan_id));

                if (dt_t.Count() > 0)
                {
                    var dt = db.tb_t_titipan.AsNoTracking().FirstOrDefault(x => x.t_titipan_type_bayar == "PENERIMAAN" && x.t_titipan_id == tbl.t_titipan_ref_id);
                    dt.t_titipan_status = dt_t.FirstOrDefault().t_titipan_os_amt <= 0 ? "Complete" : dt.t_titipan_status;
                    db.Entry(dt).State = EntityState.Modified;
                }

                new_dt_gl.Add(new dt_gl { r_gl_amt = tbl.t_titipan_amt, m_account_id = tbl.t_titipan_debet_account_id, r_gl_db_cr = "D" });
                new_dt_gl.Add(new dt_gl { r_gl_amt = tbl.t_titipan_amt, m_account_id = tbl.t_titipan_credit_account_id, r_gl_db_cr = "C" }); 
                if (tbl.t_titipan_type_bayar == "REALISASI" && tbl.t_titipan_profit_amt > 0m)
                {
                    new_dt_gl.Add(new dt_gl { 
                        r_gl_amt = tbl.t_titipan_amt - tbl.t_titipan_profit_amt,
                        m_account_id = tbl.t_titipan_credit_account_id, 
                        r_gl_db_cr = "C"
                    });
                    new_dt_gl.Add(new dt_gl { 
                        r_gl_amt = tbl.t_titipan_profit_amt, 
                        m_account_id = tbl.t_titipan_profit_account_id, 
                        r_gl_db_cr = "C" 
                    });
                }

                var gl_note = tbl.t_titipan_note == "" ? $"DANA TITIPAN {tbl.t_titipan_type_bayar}" : tbl.t_titipan_note;
                if (new_dt_gl != null && new_dt_gl.Count() > 0)
                {
                    foreach (var item in new_dt_gl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, ref_table, ref_id, ref_no, tbl.t_titipan_date, item.m_account_id, item.r_gl_db_cr, item.r_gl_amt, gl_note, tbl.created_by, tbl.created_at, tbl.m_cust_id));
                    }
                }
                db.SaveChanges();
            }
            catch (Exception e) { msg = e.ToString(); }
            return msg;
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string revisi)
        {
            tb_t_titipan tbl = db.tb_t_titipan.Find(id);
            string result = "success";
            string msg = "";
            if (tbl == null) { result = "failed"; msg = "Data can't be found!"; }
            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try { db.tb_t_titipan.Remove(tbl); db.SaveChanges(); objTrans.Commit(); }
                    catch (Exception ex) { objTrans.Rollback(); result = "failed"; msg = ex.ToString(); }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Print Out
        public ActionResult PrintReport(int ids)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            string rptname = $"RptPrintTitipan";
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("userID", Session["UserID"].ToString());

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), $"{rptname}.rpt"));
            sSql = $"select * from tb_v_print_titipan where t_kbm_h_id IN ({ids}) ORDER BY t_kbm_h_id";
            var dt = new ClassConnection().GetDataTable(sSql, "datatable");
            report.SetDataSource(dt);
            if (rptparam.Count > 0) foreach (var item in rptparam) report.SetParameterValue(item.Key, item.Value);

            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", $"inline; filename={rptname}.pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
        #endregion

        public class sFilter
        {
            public string ddltype { get; set; }
            public int ddlproyek { get; set; }
            public string periodstart { get; set; }
            public string periodend { get; set; }
            public string filtercust { get; set; }
            public string ddlfilter { get; set; }
        }

        #region Rekap Titipan
        public ActionResult RekapTitipan()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/RekapTitipan", (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            var proyek = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "ALL" } }.Union(db.tb_m_proyek.Where(x => x.m_proyek_flag == "OPEN").Select(x => new SelectListItem() { Value = x.m_proyek_id.ToString(), Text = x.m_proyek_name })).ToList();
            ViewBag.ddlproyek = new SelectList(proyek, "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult GetModalData(sFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                if (modaltype.ToUpper() == "CUST")
                    sSql = $"select Distinct 0 seq, m_cust_code [Kode], m_cust_name Customer, t_spr_no [No. SPR], m_item_name [Unit] from dbo.GetRekapTitipan('{param.periodstart} 0:0:0', '{param.periodend} 23:59:59') Where t_titipan_type!='saldo'";
                if (param.ddlproyek != 0) sSql += $" AND m_proyek_id = {param.ddlproyek}";
                DataTable tbl = new ClassConnection().GetDataTable(sSql, $"tblModal{modaltype}");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString() + sSql;
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ViewReport(sFilter param, string reporttype)
        {
            var rptfile = ""; var rptname = ""; var sField = "";
            var sGroupBy = ""; var sOrderBy = "";
            var Periode = $"{param.periodstart.ToDateTime().ToString("dd/MM/yyyy")}-{param.periodend.ToDateTime().ToString("dd/MM/yyyy")}";

            rptfile = $"RptRekapTitipan{param.ddltype}{reporttype}";
            rptname = $"REKAP_TITIPAN_{param.ddltype}_{Periode}";
            rptfile = rptfile.Replace("PDF", "");
            sField = $"*";
            sOrderBy = " Order By Case When t_titipan_type_bayar = 'SALDO' Then 1 When t_titipan_type_bayar = 'PENERIMAAN' then 2 When t_titipan_type_bayar = 'REALISASI' then 3 Else 4 End, m_cust_id, m_item_name, t_titipan_date";

            sSql += $"select {sField} from dbo.GetRekapTitipan('{param.periodstart} 0:0:0', '{param.periodend} 23:59:59') Where 1=1";

            if (param.ddlproyek != 0) sSql += $" AND m_proyek_id = {param.ddlproyek}";
            if (!string.IsNullOrEmpty(param.filtercust))
                sSql += $" AND {param.ddlfilter} IN ('{param.filtercust.Replace(";", "', '")}')";
            sSql += sGroupBy + sOrderBy;

            var PaperSize = 9;
            var irequest = new ReportRequest()
            {
                rptQuery = sSql,
                rptFile = rptfile,
                rptPaperSizeEnum = PaperSize,
                rptPaperOrientationEnum = 1,
                rptExportType = reporttype,
                rptParam = new Dictionary<string, string>()
            };

            irequest.rptParam.Add("Periode", Periode);
            irequest.rptParam.Add("UserID", Session["UserID"].ToString());
            var rpt_id = GenerateReport(irequest);
            return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}