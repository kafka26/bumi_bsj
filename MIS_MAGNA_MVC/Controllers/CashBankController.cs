﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class CashBankController : Controller
    {
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class sFilter
        {
            public int ddlcompany { get; set; }
            public int ddlcoa { get; set; }
            public string ddltype { get; set; }
            public string ddlkbtype { get; set; }
            public string ddltypebook { get; set; }
            public string periodstart { get; set; }
            public string periodend { get; set; }
            public string filterno { get; set; }
            public List<string> ddllistcoa { get; set; }
        }

        #region Get data
        [HttpPost]
        public ActionResult GetModalDatakb(sFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = $" Select Distinct 0 seq, r_kb_no [No. Transaksi], Format(r_kb_date, 'dd/MM/yyyy') Tanggal from tb_r_kb Where r_kb_date>=Cast('{param.periodstart} 23:59:59' as datetime) And r_kb_date<=Cast('{param.periodend} 00:00:00' as datetime) ";
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblModal" + modaltype);
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString() + sSql;
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetModalData(sFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = $"Select Distinct 0 seq, r_kb_no [No. Transaksi], Format(r_kb_date, 'dd/MM/yyyy') Tanggal from dbo.GetReportBiaya('{param.periodstart} 00:00:00', '{param.periodend} 23:59:59' ) Where r_kb_ref_table!='SALDO'";
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblModal" + modaltype);
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString() + sSql;
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDDLCOA(string ddlkbtype)
        {
            JsonResult js = null;
            try
            {
                var tbl = GetCOAInterface(new List<string> { ddlkbtype });
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }
        #endregion

        #region Report Daily
        public ActionResult Report()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.ddllistcoa = new SelectList(GetCOAInterface(new List<string> { "KAS_BANK_SISTEM" }), "m_account_id", "m_account_name");
            return View();
        }

        [HttpPost]
        public ActionResult ViewReportDaily(sFilter param, string reporttype)
        {
            var sField = ""; var sGroupBy = ""; var sOrderBy = "";
            var rptfile = $"RptAcctgDaily{param.ddltype + reporttype}";
            var rptname = $"LAPORAN_DAILY_TGL_{param.periodstart + "_" + param.periodend.ToDateTime()}";
            rptfile = rptfile.Replace("PDF", "");

            if (new List<string> { "SUMMARY" }.Contains(param.ddltype))
            {
                sField = $"r_kb_seq, r_kb_date, r_kb_no, r_kb_account_id, m_account_code, m_account_desc, m_proyek_name, m_item_name, m_cust_name, SUM(r_kb_debet_amt) r_kb_debet_amt, SUM(r_kb_credit_amt) r_kb_credit_amt, r_kb_saldo_amt";
                sGroupBy = " Group By r_kb_date, r_kb_no, r_kb_account_id, m_account_code, m_account_desc, m_proyek_name, m_item_name, m_cust_name, r_kb_saldo_amt, r_kb_seq";
                sOrderBy = " Order By m_account_code, r_kb_seq, r_kb_date ";
            }
            else
            {
                sField = $" r_kb_seq, r_kb_date, r_kb_no, r_kb_account_id, m_account_code, m_account_desc, m_account_d_code, m_account_d_desc, m_proyek_name, m_item_name, Isnull(m_cust_name, r_kb_note) m_cust_name, r_kb_note, r_kb_debet_amt, r_kb_credit_amt, r_kb_saldo_amt, r_kb_ref_table, r_rpt_type, r_cb_type";
                sOrderBy = " Order By m_account_code, r_kb_seq, r_kb_date ";
            }
            sSql = $" Select {sField} From dbo.GetReportCB( '{param.periodstart} 00:00:00', '{param.periodend} 23:59:59' ) Where 1=1";
            if (!string.IsNullOrEmpty(param.filterno)) sSql += $" AND r_kb_no IN ('{param.filterno.Replace(";", "', '")}')";
            if (param.ddllistcoa != null && param.ddllistcoa.Count() > 0)
            {
                var coaid = string.Join(",", param.ddllistcoa);
                sSql += $" AND r_kb_account_id IN ({coaid})";
            }
            if (param.ddlkbtype != "KAS_BANK_SISTEM") sSql += $" AND r_cb_type = '{param.ddlkbtype}'";
            sSql += sGroupBy + sOrderBy;
            var iSize = 14; var iLans = 1;
            var irequest = new ReportRequest()
            {
                rptFile = rptfile,
                rptQuery = sSql,
                rptPaperSizeEnum = iSize,
                rptPaperOrientationEnum = iLans,
                rptExportType = reporttype,
                rptParam = new Dictionary<string, string>()
            };
            irequest.rptParam.Add("Periode", param.periodstart + " - " + param.periodend);
            irequest.rptParam.Add("UserID", Session["UserID"].ToString());
            var rpt_id = GenerateReport(irequest);
            return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Report Pengeluaran
        public ActionResult ReportCB()
        { 
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/ReportCB", (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.ddllistcoa = new SelectList(GetCOAInterface(new List<string> { "PENGELUARAN" }), "m_account_id", "m_account_name"); 
            return View();
        }

        [HttpPost]
        public ActionResult ViewReport(sFilter param, string reporttype)
        { 
            var rptfile = $"RptAcctg{param.ddltypebook + param.ddltype + reporttype}";
            var rptname = $"LAPORAN_{param.ddltypebook.ToUpper()}_TGL_{param.periodstart + "_" + param.periodend.ToDateTime()}";
            rptfile = rptfile.Replace("PDF", ""); 
            
            sSql = $" Select * From dbo.GetReportBiaya('{param.periodstart} 00:00:00', '{param.periodend} 23:59:59') Where 1=1";
            if (!string.IsNullOrEmpty(param.filterno)) sSql += $" AND r_kb_no IN ('{param.filterno.Replace(";", "', '")}')";
            if (param.ddllistcoa != null && param.ddllistcoa.Count() > 0)
            {
                var coaid = string.Join(",", param.ddllistcoa);
                sSql += $" AND r_kb_d_account_id IN ({coaid})";
            }

            sSql += " Order By m_account_code, m_account_d_code, r_kb_seq, r_kb_date";
            var iSize = 14; var iLans = 1;
            var irequest = new ReportRequest()
            {
                rptFile = rptfile,
                rptQuery = sSql,
                rptPaperSizeEnum = iSize,
                rptPaperOrientationEnum = iLans,
                rptExportType = reporttype,
                rptParam = new Dictionary<string, string>()
            };
            irequest.rptParam.Add("Periode", param.periodstart + " - " + param.periodend);
            irequest.rptParam.Add("UserID", Session["UserID"].ToString());
            var rpt_id = GenerateReport(irequest);
            return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet);
        }
         
        #endregion
    }
}