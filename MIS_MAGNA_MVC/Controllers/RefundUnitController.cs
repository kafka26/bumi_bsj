﻿using CrystalDecisions.CrystalReports.Engine;
using MIS_MAGNA_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using Microsoft.Owin.Security.Provider;

namespace MIS_MAGNA_MVC.Controllers
{
    public class RefundUnitController : Controller
    {
        // GET: RefundUnit
        private QL_MIS_MAGNAEntities db;
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = ""; 
        //private int m_account_id_peningkatan = 0; 
        private static readonly int dp_account_id = GetCOAInterface(new List<string> { "PIUTANG DP" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int kt_account_id = GetCOAInterface(new List<string> { "PIUTANG KT" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int shm_account_id = GetCOAInterface(new List<string> { "PIUTANG SHM" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int kpr_account_id = GetCOAInterface(new List<string> { "PIUTANG KPR" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int jual_account_id = GetCOAInterface(new List<string> { "HUTANG PENJUALAN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int hutang_refund_id = GetCOAInterface(new List<string> { "HUTANG REFUND" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int t_refund_cost_account_id = GetCOAInterface(new List<string> { "ADMIN REFUND" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int t_refund_cost1_account_id = GetCOAInterface(new List<string> { "BIAYA LAIN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int m_account_id_titipan = GetCOAInterface(new List<string> { "UM TITIPAN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int piutang_acoount_id_pb = GetCOAInterface(new List<string>() { "PIUTANG_PENINGKATAN_BANGUNAN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int hutang_acoount_id_pb = GetCOAInterface(new List<string>() { "HUTANG_PENINGKATAN_BANGUNAN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int piutang_acoount_id_jb = GetCOAInterface(new List<string>() { "PIUTANG_JASA_BANGUN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int hutang_acoount_id_jb = GetCOAInterface(new List<string>() { "HUTANG_JASA_BANGUN" }).FirstOrDefault()?.m_account_id ?? 0;

        public RefundUnitController() { db = new QL_MIS_MAGNAEntities(); db.Database.CommandTimeout = 0; }

        #region Function List Model

        public class r_piutang : tb_r_piutang
        { 
            public string r_piutang_no { get; set; }
            public decimal t_utj_amt { get; set; }
            public Decimal r_piutang_sisa_amt { get; set; }
        }

        public class t_refund : tb_t_refund { }

        #endregion

        #region Refund Unit
        // GET: SuratPesananRumah
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile"); 
            var isCollapse = "True"; InitAdvFilterIndex(modfil, "tb_t_refund", false);
            ViewBag.isCollapse = isCollapse;
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl; 
            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001") { modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1); } 
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001") { modfil.filterperiodto = GetServerTime(); } 
            ViewBag.filterperiodto = modfil.filterperiodto; 
            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false }; model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "select * from ( SELECT h.t_refund_uid, h.t_refund_id, h.m_company_id, m_company_name, t_refund_date, h.t_refund_no, sp.t_spr_no, p.m_proyek_name, c.m_cust_name, i.m_item_name, t_refund_type, t_refund_note, t_refund_status, h.t_refund_ref_table FROM tb_t_refund h INNER JOIN tb_m_company cmp ON cmp.m_company_id=h.m_company_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = h.m_proyek_id INNER JOIN tb_m_cust c ON c.m_cust_id = h.m_cust_id INNER JOIN tb_m_item i ON i.m_item_id = h.m_item_id Left JOIN tb_t_spr sp ON sp.t_spr_id = h.t_refund_ref_id Where t_refund_type='Batal SPR'  ) AS t";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, "", " t.t_refund_date DESC"); 
            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) { result.Add(s); }
            return Json(new { draw = model.draw, recordsTotal = totalResultsCount, recordsFiltered = filteredResultsCount, data = result });
        }

        private string sQuery(tb_t_refund tbl, string sQuery = "")
        {
            sQuery = $@"select r.m_item_id, r.m_cust_id, c.m_cust_name, i.m_item_name
            , r_piutang_ref_table, SUM(r_piutang_amt) r_piutang_amt
            , SUM(r_piutang_pay_amt) r_piutang_pay_amt, SUM(r_piutang_disc_amt) r_piutang_disc_amt
            , Isnull(SUM(r_piutang_amt) - SUM(r_piutang_pay_amt), 0.0) r_piutang_sisa_amt 
            from tb_r_piutang r Inner Join tb_m_cust c ON c.m_cust_id=r.m_cust_id Inner Join tb_m_item i ON i.m_item_id=r.m_item_id  
            where r.r_piutang_status='Post' AND Isnull(r.r_piutang_ref_type, '')!='DISKON' 
            AND r.r_piutang_disc_amt=0 AND r.m_item_id={tbl.m_item_id} AND r.m_cust_id={tbl.m_cust_id}
            Group By c.m_cust_name, r.m_item_id, i.m_item_name, r.r_piutang_account_id, r_piutang_ref_table
            , r.m_cust_id Order By c.m_cust_name, m_item_name, r_piutang_ref_table";
            return sQuery;
        }

        private decimal amtrefund(tb_t_refund tbl, Decimal amt = 0m, string ref_type = "")
        {
            if (ref_type == "pay_amt")
            {
                ref_type = $@"SELECT Isnull(Case When t_refund_type IN ('Ganti Harga', 'Ganti Unit') Then f.t_refund_pay_amt Else 
                f.t_refund_utj_amt + f.t_refund_pay_amt_dp + f.t_refund_pay_amt_kpr + f.t_refund_pay_amt_kt 
                End, 0.0) t_refund_total_amt FROM tb_t_refund f inner join tb_t_spr spr on f.t_refund_id = spr.t_refund_id
                Where f.t_refund_type NOT IN ('Batal SPR') AND f.m_cust_id = {tbl.m_cust_id} AND f.t_spr_awal_id = f.t_spr_awal_id";
            }
            else
            {
                ref_type = $@"SELECT Isnull(f.t_refund_{ref_type}, 0.0) FROM tb_t_refund f inner join tb_t_spr spr on f.t_refund_id = spr.t_refund_id
                Where f.t_refund_type NOT IN ('Batal SPR') AND f.m_cust_id = {tbl.m_cust_id} AND f.t_spr_awal_id = f.t_spr_awal_id";
            }
            amt = db.Database.SqlQuery<Decimal>(ref_type)?.FirstOrDefault() ?? 0m;
            return amt;
        }

        private decimal amtdisc(tb_t_refund tbl, Decimal amt = 0m, string ref_type = "")
        {
            ref_type = $"Select Isnull(SUM(r_piutang_disc_amt), 0.0) from tb_r_piutang where m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND r_piutang_disc_amt>0 AND r_piutang_ref_table IN ('tb_t_angsuran{ref_type}_d', 'tb_t_spr{ref_type}')";
            amt = db.Database.SqlQuery<Decimal>(ref_type)?.FirstOrDefault() ?? 0m;
            if (ref_type == "kpr") {
                ref_type = $"Select Isnull(SUM(r_piutang_disc_amt), 0.0) from tb_r_piutang where m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND r_piutang_disc_amt>0 AND r_piutang_ref_table IN ('tb_t_angsuran_{ref_type}_d', 'tb_t_spr')";
            }            
            return amt;
        }

        public List<t_refund> GetListPiutang(tb_t_refund tbl)
        {    
            var stbl = new List<t_refund>();
            var sList = db.Database.SqlQuery<r_piutang>(sQuery(tbl)).ToList();
            Decimal t_refund_pay_amt = sList.Sum(x => x.r_piutang_pay_amt) + amtrefund(tbl, ref_type: "pay_amt");
            Decimal t_refund_utj_amt = sList.FirstOrDefault(x => new List<string> { "tb_t_spr_utj", "tb_t_utj" }.Contains(x.r_piutang_ref_table))?.r_piutang_pay_amt ?? 0m;
            t_refund_utj_amt = t_refund_utj_amt + amtrefund(tbl, ref_type: "utj_amt"); ;
            //amt dp
            Decimal t_refund_pay_amt_dp = sList.FirstOrDefault(x => new List<string> { "tb_t_angsuran_dp_d", "tb_t_spr_dp" }.Contains(x.r_piutang_ref_table))?.r_piutang_pay_amt ?? 0m;
            t_refund_pay_amt_dp = t_refund_pay_amt_dp + amtrefund(tbl, ref_type: "pay_amt_dp");
            Decimal t_refund_disc_amt_dp = amtdisc(tbl, ref_type: "_dp");
            Decimal t_refund_sisa_amt_dp = sList.FirstOrDefault(x => new List<string> { "tb_t_angsuran_dp_d", "tb_t_spr_dp" }.Contains(x.r_piutang_ref_table))?.r_piutang_sisa_amt ?? 0m;
            // amt kpr
            Decimal kpr = amtrefund(tbl, ref_type: "pay_amt_kpr");
            Decimal t_refund_pay_amt_kpr = sList.FirstOrDefault(x => new List<string> { "tb_t_angsuran_kpr_d", "tb_t_spr" }.Contains(x.r_piutang_ref_table))?.r_piutang_pay_amt ?? 0m;
            t_refund_pay_amt_kpr = t_refund_pay_amt_kpr + amtrefund(tbl, ref_type: "pay_amt_kpr");

            Decimal t_refund_disc_amt_kpr = amtdisc(tbl, ref_type: "kpr");

            Decimal t_refund_sisa_amt_kpr = sList.FirstOrDefault(x => new List<string> { "tb_t_angsuran_kpr_d", "tb_t_spr" }.Contains(x.r_piutang_ref_table))?.r_piutang_sisa_amt ?? 0m;

            //amt kt
            Decimal t_refund_pay_amt_kt = sList.FirstOrDefault(x => new List<string> { "tb_t_angsuran_kt_d", "tb_t_spr_kt" }.Contains(x.r_piutang_ref_table))?.r_piutang_pay_amt ?? 0m + amtrefund(tbl, ref_type: "pay_amt_kt");

            Decimal t_refund_disc_amt_kt = amtdisc(tbl, ref_type: "_kt");

            Decimal t_refund_sisa_amt_kt = sList.FirstOrDefault(x => new List<string> { "tb_t_angsuran_kt_d", "tb_t_spr_kt" }.Contains(x.r_piutang_ref_table))?.r_piutang_sisa_amt ?? 0m;

            // amt shm
            Decimal t_refund_pay_amt_shm = sList.FirstOrDefault(x => new List<string> { "tb_t_angsuran_shm_d", "tb_t_spr_shm" }.Contains(x.r_piutang_ref_table))?.r_piutang_pay_amt ?? 0m + amtrefund(tbl, ref_type: "pay_amt_shm");

            Decimal t_refund_disc_amt_shm = amtdisc(tbl, ref_type: "_shm");

            Decimal t_refund_sisa_amt_shm = sList.FirstOrDefault(x => new List<string> { "tb_t_angsuran_shm_d", "tb_t_spr_shm" }.Contains(x.r_piutang_ref_table))?.r_piutang_sisa_amt ?? 0m;

            stbl.Add(new t_refund
            {
                t_refund_pay_amt = t_refund_pay_amt,
                t_refund_utj_amt = t_refund_utj_amt,
                t_refund_pay_amt_dp = t_refund_pay_amt_dp,
                t_refund_disc_amt_dp = t_refund_disc_amt_dp,
                t_refund_sisa_amt_dp = t_refund_sisa_amt_dp,
                t_refund_pay_amt_kpr = t_refund_pay_amt_kpr,
                t_refund_disc_amt_kpr = t_refund_disc_amt_kpr,
                t_refund_sisa_amt_kpr = t_refund_sisa_amt_kpr,
                t_refund_pay_amt_kt = t_refund_pay_amt_kt,
                t_refund_disc_amt_kt = t_refund_disc_amt_kt,
                t_refund_sisa_amt_kt = t_refund_sisa_amt_kt,
                t_refund_pay_amt_shm = t_refund_pay_amt_shm,
                t_refund_disc_amt_shm = t_refund_disc_amt_shm,
                t_refund_sisa_amt_shm = t_refund_sisa_amt_shm,

                t_refund_pay_ppn_amt = db.Database.SqlQuery<Decimal>($"select Isnull(SUM(r_piutang_amt) - SUM(r_piutang_pay_amt), 0.0) from tb_r_piutang r where r.r_piutang_status='Post' AND r.r_piutang_ref_type!='DISKON' AND r.r_piutang_disc_amt=0 AND r_piutang_ref_table IN ('tb_t_titipan') And m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND r_piutang_ref_type='PPN' Group By r.m_item_id, r.m_cust_id")?.FirstOrDefault() ?? 0m,

                t_refund_pay_bphtb_amt = db.Database.SqlQuery<Decimal>($"select Isnull(SUM(r_piutang_amt) - SUM(r_piutang_pay_amt), 0.0) from tb_r_piutang r where r.r_piutang_status='Post' AND r.r_piutang_ref_type!='DISKON' AND r.r_piutang_disc_amt=0 AND r_piutang_ref_table IN ('tb_t_titipan') And m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND r_piutang_ref_type='BPHTB' Group By r.m_item_id, r.m_cust_id")?.FirstOrDefault() ?? 0m,

                t_refund_pay_balik_nama_amt = db.Database.SqlQuery<Decimal>($"select Isnull(SUM(r_piutang_amt) - SUM(r_piutang_pay_amt), 0.0) from tb_r_piutang r where r.r_piutang_status='Post' AND r.r_piutang_ref_type!='DISKON' AND r.r_piutang_disc_amt=0 AND r_piutang_ref_table IN ('tb_t_titipan') And m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND r_piutang_ref_type='BL' Group By r.m_item_id, r.m_cust_id")?.FirstOrDefault() ?? 0m,

                t_refund_pay_peningkatan_amt = sList.FirstOrDefault(x => new List<string> { "tb_t_peningkatan_d_pb", "tb_t_peningkatan_h_pb" }.Contains(x.r_piutang_ref_table))?.r_piutang_pay_amt ?? 0m,

                t_refund_disc_peningkatan_amt = db.Database.SqlQuery<Decimal>($"Select Isnull(SUM(r_piutang_disc_amt), 0.0) from tb_r_piutang where m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND r_piutang_disc_amt>0 AND r_piutang_ref_table IN ('tb_t_peningkatan_h_pb', 'tb_t_peningkatan_d_pb')")?.FirstOrDefault() ?? 0m,

                t_refund_sisa_peningkatan_amt = sList.FirstOrDefault(x => new List<string> { "tb_t_peningkatan_h_pb", "tb_t_peningkatan_d_pb" }.Contains(x.r_piutang_ref_table))?.r_piutang_sisa_amt ?? 0m,

                t_refund_pay_jasa_bangun_amt = sList.FirstOrDefault(x => new List<string> { "tb_t_peningkatan_h_jb", "tb_t_peningkatan_d_jb" }.Contains(x.r_piutang_ref_table))?.r_piutang_pay_amt ?? 0m,

                t_refund_disc_jasa_bangun_amt = db.Database.SqlQuery<Decimal>($"Select Isnull(SUM(r_piutang_disc_amt), 0.0) from tb_r_piutang where m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND r_piutang_disc_amt>0 AND r_piutang_ref_table IN ('tb_t_peningkatan_d_jb', 'tb_t_peningkatan_h_jb')")?.FirstOrDefault() ?? 0m,

                t_refund_sisa_jasa_bangun_amt = sList.FirstOrDefault(x => new List<string> { "tb_t_peningkatan_d_jb", "tb_t_peningkatan_h_jb" }.Contains(x.r_piutang_ref_table))?.r_piutang_sisa_amt ?? 0m,

                t_refund_disc_amt = db.Database.SqlQuery<Decimal>($"Select Isnull(SUM(r_piutang_disc_amt), 0.0) from tb_r_piutang where m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND r_piutang_disc_amt>0 ")?.FirstOrDefault() ?? 0m,

                t_refund_total_amt = t_refund_pay_amt - db.Database.SqlQuery<Decimal>($"Select Isnull(SUM(r_piutang_disc_amt), 0.0) from tb_r_piutang where m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND r_piutang_disc_amt>0 ")?.FirstOrDefault() ?? 0m,
                t_refund_total_angsuran = sList.Count(),
            });
            return stbl;
        }

        [HttpPost]
        public ActionResult GetDataPiutang(tb_t_refund tbl)
        {
            JsonResult js = null;
            try
            {
                var stbl = GetListPiutang(tbl);
                if (stbl == null || stbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", stbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e) { js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet); }
            return js;
        }

        [HttpPost]
        public ActionResult GetShowCoa(tb_t_refund tbl, string sText = "")
        {
            JsonResult js = null;
            try
            {
                var new_jurnal = new List<m_account>();
                var sListPiutang = new List<string> { "tb_t_peningkatan_d_pb", "tb_t_peningkatan_h_pb", "tb_t_peningkatan_d_jb", "tb_t_peningkatan_h_jb", "tb_t_titipan" };
                var coa_penjualan = db.tb_m_account.Where(x => x.m_account_id == jual_account_id);
                var coa_refund = db.tb_m_account.Where(x => x.m_account_id == hutang_refund_id);               
                var sList = db.Database.SqlQuery<r_piutang>(sQuery(tbl)).ToList();

                // Piutang Hutang Penjualan
                new_jurnal.Add(new m_account { m_account_id = jual_account_id, m_account_code = coa_penjualan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_penjualan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = sList.Where(x => !sListPiutang.Contains(x.r_piutang_ref_table)).Sum(x => x.r_piutang_amt), m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });

                if (tbl.t_refund_sisa_amt_dp > 0m)
                {
                    // Piutang DP
                    var coa_dp = db.tb_m_account.Where(x => x.m_account_id == dp_account_id);
                    new_jurnal.Add(new m_account { m_account_id = dp_account_id, m_account_code = coa_dp.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_dp.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = (Decimal)tbl.t_refund_sisa_amt_dp , m_account_dbcr = "C", m_account_note = "" });
                }

                if (tbl.t_refund_sisa_amt_kt > 0m)
                {
                    // Piutang KT
                    var coa_kt = db.tb_m_account.Where(x => x.m_account_id == kt_account_id);
                    new_jurnal.Add(new m_account { m_account_id = kt_account_id, m_account_code = coa_kt.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_kt.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_sisa_amt_kt, m_account_dbcr = "C", m_account_note = "" });
                }

                if (tbl.t_refund_sisa_amt_kpr > 0m)
                {
                    // Piutang KPR
                    var coa_kpr = db.tb_m_account.Where(x => x.m_account_id == kpr_account_id);
                    new_jurnal.Add(new m_account { m_account_id = kpr_account_id, m_account_code = coa_kpr.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_kpr.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_sisa_amt_kpr, m_account_dbcr = "C", m_account_note = "" });
                }

                // Insert Jurnal Akun BIAYA LAIN 
                if (tbl.t_refund_disc_amt > 0m)
                {
                    var coa_disc = db.tb_m_account.Where(x => x.m_account_id == t_refund_cost1_account_id);
                    new_jurnal.Add(new m_account { m_account_id = t_refund_cost1_account_id, m_account_code = coa_disc.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_disc.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_disc_amt + tbl.t_refund_cost_other_amt, m_account_dbcr = "C", m_account_note = "" });
                }

                decimal amt_titipan = tbl.t_refund_pay_ppn_amt + tbl.t_refund_pay_bphtb_amt + tbl.t_refund_pay_balik_nama_amt;
                if (amt_titipan > 0m)
                {
                    var coa_titipan = db.tb_m_account.Where(x => x.m_account_id == m_account_id_titipan);
                    new_jurnal.Add(new m_account { m_account_id = m_account_id_titipan, m_account_code = coa_titipan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_titipan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = amt_titipan, m_account_credit_amt = 0, m_account_dbcr = "D", m_account_note = "" });
                }

                // Insert Jurnal Akun BIAYA ADM (PENDAPATAN)
                if (tbl.t_refund_cost_amt + tbl.t_refund_cost_other_amt > 0m)
                {
                    var coa_pendapatan = db.tb_m_account.Where(x => x.m_account_id == t_refund_cost_account_id);
                    new_jurnal.Add(new m_account { m_account_id = t_refund_cost_account_id, m_account_code = coa_pendapatan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_pendapatan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_cost_amt + tbl.t_refund_cost_other_amt, m_account_dbcr = "C", m_account_note = "" });
                }

                if (tbl.t_refund_sisa_peningkatan_amt > 0m)
                {
                    var hutang_amt_pb = sList.Where(x => new List<string> { "tb_t_peningkatan_d_pb", "tb_t_peningkatan_h_pb" }.Contains(x.r_piutang_ref_table)).ToList().Sum(x => x.r_piutang_amt);
                    var coa_hutang_pb = db.tb_m_account.Where(x => x.m_account_id == hutang_acoount_id_pb);
                    var coa_piutang_pb = db.tb_m_account.Where(x => x.m_account_id == piutang_acoount_id_pb);

                    new_jurnal.Add(new m_account { m_account_id = hutang_acoount_id_pb, m_account_code = coa_hutang_pb.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_hutang_pb.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = hutang_amt_pb, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });

                    new_jurnal.Add(new m_account { m_account_id = piutang_acoount_id_pb, m_account_code = coa_piutang_pb.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_piutang_pb.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_sisa_peningkatan_amt, m_account_dbcr = "C", m_account_note = "" });
                }

                if (tbl.t_refund_sisa_jasa_bangun_amt > 0m)
                {
                    var hutang_amt_jb = sList.Where(x => new List<string> { "tb_t_peningkatan_d_jb", "tb_t_peningkatan_h_jb" }.Contains(x.r_piutang_ref_table)).ToList().Sum(x => x.r_piutang_amt);
                    var coa_hutang_jb = db.tb_m_account.Where(x => x.m_account_id == hutang_acoount_id_jb);
                    var coa_piutang_jb = db.tb_m_account.Where(x => x.m_account_id == piutang_acoount_id_jb);

                    new_jurnal.Add(new m_account { m_account_id = hutang_acoount_id_jb, m_account_code = coa_hutang_jb.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_hutang_jb.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = hutang_amt_jb, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });

                    new_jurnal.Add(new m_account { m_account_id = piutang_acoount_id_jb, m_account_code = coa_piutang_jb.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_piutang_jb.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_sisa_jasa_bangun_amt, m_account_dbcr = "C", m_account_note = "" });
                }

                // Insert Jurnal Akun BIAYA LAIN (LAIN)
                if (tbl.t_refund_cost_other1_amt > 0m)
                {
                    var coa_pendapatan = db.tb_m_account.Where(x => x.m_account_id == t_refund_cost1_account_id);
                    new_jurnal.Add(new m_account { m_account_id = t_refund_cost1_account_id, m_account_code = coa_pendapatan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_pendapatan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_cost_other1_amt, m_account_dbcr = "C", m_account_note = "" });
                }

                new_jurnal.Add(new m_account { m_account_id = hutang_refund_id, m_account_code = coa_refund.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_refund.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = tbl.t_refund_total_amt, m_account_dbcr = "C", m_account_note = "" });
                var stbl = new_jurnal.ToList(); 
                sText = "Apakah anda yakin akan refund unit?";
                js = Json(new { result = "", stext = $"{sText}", stbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e) { js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet); }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataItem()
        {
            sSql = $@"SELECT s.t_spr_awal_id, s.t_spr_no, s.m_item_id, m_item_name, s.m_proyek_id, m_proyek_name, s.m_cust_id
                , m_cust_name, m_type_unit_name, m_cluster_name, Isnull(utj.r_piutang_amt_utj, 0.0) t_utj_amt, s.t_spr_price
                , 0 total_angsuran, 'tb_t_spr' t_refund_ref_table, s.t_spr_id t_refund_ref_id, 'tb_t_spr' t_refund_ref_type, s.t_spr_price r_piutang_amt
                , Isnull(r.r_piutang_pay_amt, 0.0) + Isnull(re.t_refund_total_amt, 0.0) r_piutang_pay_amt
                , r_piutang_sisa_amt, isnull(disc.r_piutang_disc_amt, 0.0) r_piutang_disc_amt 
                FROM tb_v_spr s 
                OUTER APPLY ( SELECT Sum(r.r_piutang_amt) r_piutang_amt, SUM(r_piutang_pay_amt) r_piutang_pay_amt
                , Sum(r.r_piutang_amt) - SUM(r_piutang_pay_amt) r_piutang_sisa_amt 
                FROM tb_r_piutang r WHERE r.m_cust_id = s.m_cust_id AND r.m_item_id = s.m_item_id AND r.m_proyek_id = s.m_proyek_id 
                AND r.t_spr_id = s.t_spr_id AND r_piutang_ref_type != 'DISKON' 
                AND r_piutang_ref_table IN ( 'tb_t_spr', 'tb_t_angsuran_kpr_d', 'tb_t_spr_kt', 'tb_t_angsuran_kt_d', 'tb_t_spr_dp', 'tb_t_angsuran_dp_d', 'tb_t_angsuran_shm_d', 'tb_t_spr_shm' ) 
                ) r
                OUTER APPLY ( SELECT Sum(r.r_piutang_disc_amt) r_piutang_disc_amt 
                FROM tb_r_piutang r WHERE r.m_cust_id = s.m_cust_id AND r.m_item_id = s.m_item_id 
                AND r.m_proyek_id = s.m_proyek_id AND r.t_spr_id = s.t_spr_id AND r_piutang_ref_type = 'DISKON' 
                AND r_piutang_ref_table IN('tb_t_spr', 'tb_t_angsuran_kpr_d', 'tb_t_spr_kt', 'tb_t_angsuran_kt_d'
                , 'tb_t_spr_dp', 'tb_t_angsuran_dp_d', 'tb_t_angsuran_shm_d', 'tb_t_spr_shm' ) ) disc 
                OUTER APPLY ( SELECT Sum(r.r_piutang_amt) r_piutang_amt_utj FROM tb_r_piutang r 
                WHERE r.m_cust_id = s.m_cust_id AND r.m_item_id = s.m_item_id AND r.m_proyek_id = s.m_proyek_id AND r.t_spr_id = s.t_spr_id 
                AND r_piutang_ref_type = '' AND r_piutang_ref_table IN ('tb_t_spr_utj', 'tb_t_utj' ) ) utj 
                OUTER APPLY ( SELECT Isnull(t.t_refund_total_amt, 0.0) t_refund_total_amt, t.t_refund_utj_amt
                , Isnull(t.t_refund_cost_amt, 0.0) + Isnull(t.t_refund_cost_other_amt, 0.0) + Isnull(t.t_refund_cost_other1_amt, 0.0) t_refund_total_cost_amt
                FROM tb_t_refund t WHERE s.t_refund_id = t.t_refund_id AND s.m_cust_id = s.m_cust_id ) re 
                WHERE s.t_spr_status = 'Post' AND s.t_spr_id NOT IN ( SELECT t_refund_ref_id FROM tb_t_refund WHERE t_refund_type = 'Batal SPR' ) ORDER BY m_cust_name";
            return getJsResult(sSql);
        } 

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_t_refund tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_refund();
                tbl.t_refund_uid = Guid.NewGuid();
                tbl.t_refund_date = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.t_refund_account_id = GetCOAInterface(new List<string> { "HUTANG REFUND" }).FirstOrDefault().m_account_id;
                tbl.created_at = GetServerTime();
                tbl.t_refund_status = "In Process";
                tbl.t_refund_cost1_account_id = 0;
                tbl.t_refund_cost_other1_amt = 0m;
                tbl.t_refund_ref_table = "tb_t_spr";
                tbl.m_item_new_id = 0;
                tbl.t_refund_flag = false;
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_refund.Find(id); 
            }

            ViewBag.action = action; 
            var getCompany = db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList();
            if (action == "New Data") { getCompany = getCompany.Where(m => m.flag == "ACTIVE").ToList(); } 
            ViewBag.m_company_id = new SelectList(getCompany, "valuefield", "textfield", tbl.m_company_id); 
            var t_refund_account_id = new SelectList(GetCOAInterface(new List<string> { "HUTANG REFUND" }), "m_account_id", "m_account_name", tbl.t_refund_account_id);
            ViewBag.t_refund_account_id = t_refund_account_id;
             var t_refund_cost_account_id = new SelectList(GetCOAInterface(new List<string> { "ADMIN REFUND" }), "m_account_id", "m_account_name", tbl.t_refund_cost_account_id);

            ViewBag.t_refund_cost_account_id = t_refund_cost_account_id;
            ViewBag.t_spr_no = (tbl.t_refund_ref_id != 0 ? db.tb_t_spr.FirstOrDefault(x => x.t_spr_id == tbl.t_refund_ref_id).t_spr_no : "");
            ViewBag.t_utj_amt = (tbl.t_refund_ref_id != 0 ? db.tb_t_spr.FirstOrDefault(x => x.t_spr_id == tbl.t_refund_ref_id).t_utj_amt : 0m);
            ViewBag.m_cust_name = (tbl.m_cust_id != 0 ? db.tb_m_cust.FirstOrDefault(x => x.m_cust_id == tbl.m_cust_id).m_cust_name : "");
            ViewBag.m_item_name = (tbl.m_item_id != 0 ? db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id).m_item_name : "");
            ViewBag.m_proyek_name = (tbl.m_proyek_id != 0 ? db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == tbl.m_proyek_id).m_proyek_name : "");
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_refund tbl, string action, List<m_account> dtDtl)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) { return RedirectToAction("NotAuthorize", "Profile"); }

            var msg = ""; var result = "failed"; var hdrid = new Guid(); 
            var servertime = GetServerTime();
            if(GetCOAInterface(new List<string> { "HUTANG PENJUALAN" }).ToList().Count <=0)
                msg += "Akun Untuk HUTANG PENJUALAN belum di setting di master interface..!<br/>";

            if (tbl.t_refund_date < GetCutOffdate()) { msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_refund_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />"; }
                
            if (tbl.t_refund_cost_amt > 0m || tbl.t_refund_cost_other_amt > 0m) { if (tbl.t_refund_cost_account_id <= 0) { msg += "Akun untuk Biaya admin REFUND belum di setting di master interface..!<br/>"; } }

            if (action == "New Data")
            {
                var spr = db.Database.SqlQuery<r_piutang>($"Select spr.t_spr_no r_piutang_no, r.t_refund_id r_piutang_id, r.t_refund_status r_piutang_status from tb_v_spr spr inner join tb_t_refund r ON r.t_refund_ref_id=spr.t_spr_id where r.t_refund_ref_id={tbl.t_refund_ref_id}");
                var ref_id = db.Database.SqlQuery<int>($"select COUNT(*) from tb_t_refund where t_refund_ref_id={tbl.t_refund_ref_id} AND t_refund_type='Batal SPR'")?.FirstOrDefault() ?? 0;
                if (ref_id > 0)
                {
                    msg += $"Maaf, Nomer SPR <strong>{spr.FirstOrDefault().r_piutang_no}</strong> sudah pernah di Refund => nomer Draft <strong>{spr.FirstOrDefault().r_piutang_id} </strong> dan status <strong>{spr.FirstOrDefault().r_piutang_status}</strong>";
                }   
            } 

            if (string.IsNullOrEmpty(tbl.t_refund_note)) { tbl.t_refund_note = ""; }
            if (string.IsNullOrEmpty(tbl.t_refund_norek)) { tbl.t_refund_norek = ""; }
            if (string.IsNullOrEmpty(tbl.t_refund_atas_nama)) { tbl.t_refund_atas_nama = ""; }
            if (string.IsNullOrEmpty(tbl.t_refund_bank_name)) { tbl.t_refund_bank_name = ""; }
            if (tbl.t_refund_ref_id == 0) { msg += "Silahkan pilih No. SPR!<br/>"; }
            if (tbl.m_cust_id == 0) { msg += "Silahkan pilih customer!<br/>"; }
            if (tbl.m_item_id == 0) { msg += "Silahkan pilih unit!<br/>"; }
            if (tbl.t_refund_account_id == 0) { msg += "Silahkan Pilih Akun!<br/>"; }
            if (tbl.t_refund_cost_account_id == 0) { msg += "Silahkan Pilih Biaya!<br/>"; }
            //if (tbl.t_refund_total_amt<=0) msg += "uang Kembali tidak boleh minus!<br/>";
            if (tbl.t_refund_type == "Batal SPR" && tbl.t_refund_type_bayar == "Angsuran" && tbl.t_refund_total_angsuran == 0) { msg += "Silahkan isi total angsuran!<br/>"; }

            if (string.IsNullOrEmpty(msg))
            {
                if (tbl.t_refund_status == "Revised") { tbl.t_refund_status = "In Process"; }
                if (tbl.t_refund_status == "Post")
                {
                    tbl.t_refund_account_id = GetCOAInterface(new List<string> { "HUTANG REFUND" }).FirstOrDefault().m_account_id;
                    tbl.t_refund_no = GenerateTransNo($"REF", "t_refund_no", "tb_t_refund", tbl.m_company_id, tbl.t_refund_date.ToString("MM/dd/yyyy"));
                    tbl.posted_by = Session["UserID"].ToString();
                    tbl.posted_at = servertime;
                }
                else { tbl.t_refund_no = ""; }
                
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    { 
                        if (action == "New Data")
                        {
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            tbl.t_refund_price_new = 0m;
                            db.tb_t_refund.Add(tbl); 
                        }
                        else
                        {
                            tbl.updated_at = GetServerTime();
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                        }
                        db.SaveChanges();

                        if (tbl.t_refund_status == "Post")
                        {
                            var status = "Closed";
                            var dt_unit = db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id);
                            dt_unit.m_item_flag = "Open";
                            db.Entry(dt_unit).State = EntityState.Modified;

                            //Block Post Transaction
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_refund_uid, "tb_t_refund_h", tbl.t_refund_id, tbl.updated_by, tbl.updated_at));

                            if (tbl.t_refund_type == "Batal SPR")
                            {
                                // --> spr
                                var dt_spr = db.tb_t_spr.FirstOrDefault(s => s.t_spr_id == tbl.t_refund_ref_id);
                                dt_spr.t_spr_status = status;
                                db.Entry(dt_spr).State = EntityState.Modified;

                                // --> angsuran kpr
                                if (dt_spr.t_piutang_type_bayar == "Angsuran")
                                {
                                    var angs_kpr_h = db.tb_t_angsuran_kpr_h.Where(s => s.t_spr_id == tbl.t_refund_ref_id);
                                    if (angs_kpr_h != null || angs_kpr_h.Count() > 0)
                                    {
                                        var dt_angs_kpr_h = db.tb_t_angsuran_kpr_h.FirstOrDefault(s => s.t_spr_id == tbl.t_refund_ref_id);
                                        if (dt_angs_kpr_h != null)
                                        {
                                            dt_angs_kpr_h.t_angsuran_kpr_h_status = status;
                                            db.Entry(dt_angs_kpr_h).State = EntityState.Modified;

                                            var dt_angs_kpr_d = db.tb_t_angsuran_kpr_d.Where(x => x.t_angsuran_kpr_h_id == dt_angs_kpr_h.t_angsuran_kpr_h_id);
                                            foreach (var item in dt_angs_kpr_d) { 
                                                item.t_angsuran_kpr_d_status = status; db.Entry(item).State = EntityState.Modified; 
                                            }
                                        }
                                    }
                                }
                                db.SaveChanges();
                            }

                            if (tbl.t_refund_type == "Batal SPR" && tbl.t_refund_type_bayar == "Tunai")
                            {
                                db.tb_r_hutang.Add(Insert_R_Hutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "Refund", "tb_t_refund", tbl.t_refund_id, tbl.t_refund_no, tbl.t_refund_date, tbl.t_refund_date, Convert.ToDateTime("1/1/1900".ToString()), tbl.t_refund_account_id, tbl.t_refund_total_amt, 0m, $"Refund {tbl.t_refund_type_bayar}", tbl.created_by, tbl.created_at, "", 0, "", 0, t_spr_id: tbl.t_refund_ref_id, t_spr_awal_id: tbl.t_spr_awal_id));
                            }

                            var refund_batal = new string[] { "Batal SPR" };
                            if (refund_batal.Contains(tbl.t_refund_type))
                            {
                                msg = PostJurnal(tbl, dtDtl);
                                if (!string.IsNullOrEmpty(msg)) { objTrans.Rollback(); result = "failed"; return Json(new { result, msg }, JsonRequestBehavior.AllowGet); } 
                                var dt_ar = db.tb_r_piutang.Where(x => x.r_piutang_status == "Post" && x.m_item_id == tbl.m_item_id && x.m_cust_id==tbl.m_cust_id);
                                if (dt_ar != null) { foreach (var item in dt_ar) { item.r_piutang_status = status; db.Entry(item).State = EntityState.Modified; } }
                            }
                        }

                        db.SaveChanges(); objTrans.Commit(); 
                        hdrid = tbl.t_refund_uid; result = "success";
                        if (tbl.t_refund_status == "Post") { msg = $"data telah diposting dengan nomor transaksi {tbl.t_refund_no}"; }
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += $"Entity of type {eve.Entry.Entity.GetType().Name} in state {eve.Entry.State} has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors) { msg += $"- Property: {ve.PropertyName}, Error: {ve.ErrorMessage}<br />"; }
                        }
                    }
                    catch (Exception ex) { objTrans.Rollback(); msg = ex.ToString(); }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        private string PostJurnal(tb_t_refund tbl, List<m_account> dtDtl, string msg = "", int seq = 1)
        {
            try
            {
                var dt_unit = db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id);
                dt_unit.m_item_flag = "Open"; db.Entry(dt_unit).State = EntityState.Modified;

                if (dtDtl.Any())
                {
                    foreach (var item in dtDtl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_refund", tbl.t_refund_id, tbl.t_refund_no, tbl.t_refund_date, item.m_account_id, item.m_account_dbcr, (item.m_account_dbcr == "D" ? item.m_account_debet_amt : item.m_account_credit_amt), $"SPR {tbl.t_refund_type}", Session["UserID"].ToString(), GetServerTime(), tbl.m_cust_id)); db.SaveChanges();
                    }
                }
                var sList = db.tb_r_piutang.Where(x => x.m_item_id == tbl.m_item_id && x.m_cust_id == tbl.m_cust_id && x.m_proyek_id == tbl.m_proyek_id).ToList();
                if (sList != null && sList.Count() > 0) { foreach (var item in sList) { item.r_piutang_status = "Closed"; db.Entry(item).State = EntityState.Modified; } }
                //Akhir Insert Jurnal Akunting
            }
            catch (Exception e) { msg = e.ToString(); }
            db.SaveChanges();
            return msg;
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int t_refund_id)
        { 
            tb_t_refund tbl = db.tb_t_refund.Find(t_refund_id);
            string result = "success"; string msg = "";
            if (tbl == null) { result = "failed"; msg = "Data can't be found!"; }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try { db.tb_t_refund.Remove(tbl); db.SaveChanges(); objTrans.Commit(); }
                    catch (Exception ex) { objTrans.Rollback(); result = "failed"; msg = ex.ToString(); }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Print Out
        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); } 
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) { return RedirectToAction("NotAuthorize", "Profile"); }
            string rptname = "PrintRefund";
            var id = int.Parse(ids); var type = "";
            var tbl = db.tb_t_refund.FirstOrDefault(x => x.t_refund_id == id);
            if (tbl.t_refund_type == "Batal SPR") { type = "Batal"; }
            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), $"rpt{rptname}{type}.rpt"));

            sSql = $"select * from tb_v_print_refund WHERE t_refund_id IN ({ids}) ORDER BY t_refund_id";
            var dtRpt = new ClassConnection().GetDataTable(sSql, "data");

            report.SetDataSource(dtRpt);
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing) { if (disposing) { db.Dispose(); } base.Dispose(disposing); }
        #endregion
    }
}