﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using System.Security.Cryptography.Xml;

namespace MIS_MAGNA_MVC.Controllers
{
    public class RealisasiPromoController : Controller
    {
        // GET: KBK
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        public class t_kbk_d : tb_t_kbk_d
        {
            public string m_account_code { get; set; }
            public string m_account_desc { get; set; }
            public string m_item_name { get; set; }
            public string r_promo_ref_no { get; set; }
            public DateTime r_promo_ref_date { get; set; }
            public Decimal r_promo_amt { get; set; }
        }
        public RealisasiPromoController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_t_kbk_h", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE isapptrans=0 AND isposttrans=0 ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( SELECT h.t_kbk_uid, h.t_kbk_h_id, t_kbk_no, h.t_kbk_date, ISNULL((SELECT m_item_name FROM tb_m_item i WHERE i.m_item_id=h.m_item_id),'') m_unit_name, ISNULL((SELECT m_account_desc FROM tb_m_account i WHERE i.m_account_id=h.t_kbk_account_id),'') m_account_name, t_kbk_ref_no, t_kbk_h_note, t_kbk_h_status FROM tb_t_kbk_h h Where t_kbk_type='Promo' ) AS t";
            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, "", "t.t_kbk_h_id DESC");

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        [HttpPost]
        public ActionResult InitDDLAccount(string t_kbk_type_bayar, string action)
        {
            JsonResult js = null;
            try
            {
                var sVar = "";
                if (t_kbk_type_bayar == "KK") sVar = "KAS_SISTEM";
                else sVar = "BANK_SISTEM";
                var tbl = GetCOAInterface(new List<string> { sVar });
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetUnitData()
        {
            JsonResult js = null;
            try
            { 
                sSql = $"SELECT rp.m_cust_id, ii.m_item_name m_promo_name, m_cust_name, rp.m_proyek_id, m_proyek_name, rp.m_item_id, i.m_item_name, rp.t_spr_id, rp.t_spr_awal_id, rp.t_promo_id, rp.m_promo_id, SUM(r_promo_amt) r_promo_amt, Isnull((select SUM(r_promo_pay_amt) from tb_r_promo xx Where xx.t_promo_id=rp.t_promo_id AND xx.m_promo_id=rp.m_promo_id AND rp.m_cust_id=xx.m_cust_id AND xx.r_promo_type!='Promo'), 0.0) r_promo_pay_amt FROM tb_r_promo rp INNER JOIN tb_m_cust c ON c.m_cust_id = rp.m_cust_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = rp.m_proyek_id INNER JOIN tb_m_item i ON i.m_item_id = rp.m_item_id INNER JOIN tb_m_item ii ON ii.m_item_id = rp.m_promo_id INNER JOIN tb_t_spr spr ON spr.t_spr_id = rp.t_spr_id WHERE ISNULL(spr.is_promo, 0) = 1 AND spr.t_spr_status = 'Post' AND r_promo_status='Post' AND rp.r_promo_type='Promo' GROUP BY rp.m_cust_id, m_cust_name, rp.m_proyek_id, m_proyek_name, rp.m_item_id, i.m_item_name, ii.m_item_name, rp.t_spr_id,rp.m_promo_id, rp.t_spr_awal_id,rp.t_promo_id Having SUM(r_promo_amt) - Isnull((select SUM(r_promo_pay_amt) from tb_r_promo xx Where xx.t_promo_id=rp.t_promo_id AND xx.m_promo_id=rp.m_promo_id AND rp.m_cust_id=xx.m_cust_id AND xx.r_promo_type!='Promo'), 0.0) > 0 Order By m_cust_name";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetShowCoa(tb_t_kbk_h stbl, List<t_kbk_d> dtl)
        {
            JsonResult js = null; string sResult = "";
            try
            {
                var new_jurnal = new List<m_account>();
                var coa_penjualan = db.tb_m_account.Where(x => x.m_account_id == stbl.t_kbk_account_id);
                var m_item_name = db.tb_m_item.FirstOrDefault(x => x.m_item_id == stbl.m_item_id).m_item_name;
                // Piutang Hutang Penjualan 
                if (dtl != null || dtl.Count() > 0) 
                {
                    foreach (var items in dtl)
                    {
                        new_jurnal.Add(new m_account { m_account_id = items.t_kbk_d_account_id, m_account_code = items.m_account_code, m_account_desc = items.m_account_desc, m_account_debet_amt = items.t_kbk_d_amt, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = (items.t_kbk_d_note == "" ? "-" : items.t_kbk_d_note) });
                    }
                }
                new_jurnal.Add(new m_account { m_account_id = stbl.t_kbk_account_id, m_account_code = coa_penjualan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_penjualan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = stbl.t_kbk_h_amt, m_account_dbcr = "C", m_account_note = $"Realisasi Promo unit : {m_item_name}" });
                var tbl = new_jurnal.ToList();

                sResult = "Berikut jurnal yang akan terbentuk jika anda akan melanjutkan transaksi dengan klik tombol Post data :"; tbl = new_jurnal.ToList(); 
                js = Json(new { result = sResult, tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(tb_t_kbk_h stbl)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select 0 t_kbk_d_seq, x.r_promo_ref_no, xx.t_promo_type_bayar, x.r_promo_ref_id t_kbk_ref_id, x.m_item_id, x.m_proyek_id, x.m_cust_id, x.r_promo_account_id t_kbk_d_account_id, x.r_promo_ref_table t_kbk_ref_table, x.r_promo_ref_date, x.r_promo_ref_no, x1.m_item_name, x2.m_account_code, x2.m_account_desc, x.r_promo_amt - Isnull((select SUM(r_promo_pay_amt) from tb_r_promo xx Where xx.m_cust_id=x.m_cust_id AND x.m_promo_id=xx.m_promo_id AND xx.r_promo_type='Payment' AND x.r_promo_status=xx.r_promo_status), 0.0) r_promo_amt, x.r_promo_amt - Isnull((select SUM(r_promo_pay_amt) from tb_r_promo xx Where xx.m_cust_id=x.m_cust_id AND x.m_promo_id=xx.m_promo_id AND xx.r_promo_type='Payment' AND x.r_promo_status=xx.r_promo_status AND xx.r_promo_ref_id=x.r_promo_ref_id), 0.0) r_promo_sisa_amt, x.r_promo_amt - Isnull((select SUM(r_promo_pay_amt) from tb_r_promo xx Where xx.m_cust_id=x.m_cust_id AND x.m_promo_id=xx.m_promo_id AND xx.r_promo_type='Payment'  AND xx.r_promo_ref_id=x.r_promo_ref_id), 0.0) t_kbk_d_amt, Concat(x1.m_item_name, xx.t_promo_type_bayar) t_kbk_d_note from tb_r_promo x Inner Join tb_t_promo xx ON xx.t_promo_id=x.t_promo_id Inner Join tb_m_item x1 ON x1.m_item_id=x.m_promo_id Inner Join tb_m_account x2 ON x2.m_account_id=x.r_promo_account_id where x.r_promo_type='Promo' AND x.r_promo_amt - Isnull((select SUM(r_promo_pay_amt) from tb_r_promo xx Where xx.m_cust_id=x.m_cust_id AND x.m_promo_id=xx.m_promo_id AND xx.r_promo_type='Payment' AND x.r_promo_status=xx.r_promo_status AND xx.r_promo_ref_id=x.r_promo_ref_id), 0.0) > 0.0 AND x.r_promo_status='Post' AND x.t_spr_id={stbl.t_spr_id} AND x.m_cust_id={stbl.m_cust_id} AND x.m_item_id={stbl.m_item_id} AND x.t_promo_id={stbl.t_promo_id} Order By x.r_promo_ref_date"; 
                var tbl = db.Database.SqlQuery<t_kbk_d>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int t_kbk_h_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"select t_kbk_d_seq, x3.r_promo_ref_no, x2.t_promo_type_bayar, t_kbk_ref_id, x2.m_item_id, x2.m_proyek_id, x2.m_cust_id, x.t_kbk_d_account_id, t_kbk_ref_table, r_promo_ref_date, r_promo_ref_no, x4.m_item_name, x3.r_promo_amt - Isnull((select SUM(xx.r_promo_pay_amt) from tb_r_promo xx Where xx.m_cust_id=x3.m_cust_id AND x3.m_promo_id=xx.m_promo_id AND xx.r_promo_type='Payment' AND xx.r_promo_status=x3.r_promo_status AND xx.r_promo_ref_id=x3.r_promo_ref_id), 0.0) r_promo_amt, x3.r_promo_amt - Isnull((select SUM(xx.r_promo_pay_amt) from tb_r_promo xx Where xx.m_cust_id=x3.m_cust_id AND x3.m_promo_id=xx.m_promo_id AND xx.r_promo_type='Payment' AND xx.r_promo_status=x3.r_promo_status AND xx.r_promo_ref_id=x3.r_promo_ref_id), 0.0) r_promo_sisa_amt, x.t_kbk_d_amt, x.t_kbk_d_note, x5.m_account_code, x5.m_account_desc from tb_t_kbk_d x Inner Join tb_t_kbk_h x1 ON x1.t_kbk_h_id=x.t_kbk_h_id Inner Join tb_t_promo x2 ON x2.t_promo_id=x1.t_promo_id AND x2.t_spr_id=x1.t_spr_id AND x2.m_promo_id=x1.m_promo_id Inner Join tb_r_promo x3 ON x3.t_promo_id=x2.t_promo_id AND x3.m_promo_id=x2.m_promo_id AND x2.t_spr_id=x3.t_spr_id Inner Join tb_m_item x4 ON x4.m_item_id=x3.m_promo_id Inner join tb_m_account x5 ON x5.m_account_id=x.t_kbk_d_account_id where x.t_kbk_h_id={t_kbk_h_id} AND x3.r_promo_type!='Payment' ORDER BY x.t_kbk_d_seq";
                var tbl = db.Database.SqlQuery<t_kbk_d>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }
          
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_t_kbk_h tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_kbk_h();
                tbl.t_kbk_uid = Guid.NewGuid();
                tbl.t_kbk_date = GetServerTime();
                tbl.created_at = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.t_kbk_h_status = "In Process";
                tbl.m_company_id = db.tb_m_company.FirstOrDefault().m_company_id;
                tbl.t_promo_id = 0;tbl.m_cust_id = 0;tbl.m_proyek_id = 0;
                tbl.m_item_id = 0;
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_kbk_h.Find(id);
            }

            if (tbl == null) return HttpNotFound();
            ViewBag.action = action; 

            var sVar = "KAS_SISTEM";
            if (action == "Update Data") { if (tbl.t_kbk_type_bayar == "KK") { sVar = "KAS_SISTEM"; } else { sVar = "BANK_SISTEM"; } }
            var t_kbk_account_id = new SelectList(GetCOAInterface(new List<string> { sVar }), "m_account_id", "m_account_name", tbl.t_kbk_account_id);
            ViewBag.t_kbk_account_id = t_kbk_account_id;

            //Field
            ViewBag.m_item_name = db.tb_m_item.FirstOrDefault(w => w.m_item_id == tbl.m_item_id)?.m_item_name ?? "";
            ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(w => w.m_proyek_id == tbl.m_proyek_id)?.m_proyek_name ?? "";
            ViewBag.m_cust_name = db.tb_m_cust.FirstOrDefault(w => w.m_cust_id == (int)tbl.m_cust_id)?.m_cust_name ?? "";
            ViewBag.m_promo_name = db.Database.SqlQuery<string>($"select x1.m_item_name from tb_t_promo x inner join tb_m_item x1 ON x1.m_item_id=x.m_promo_id where x.t_promo_id={(int)tbl.t_promo_id}")?.FirstOrDefault() ?? "";
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_kbk_h tbl, List<t_kbk_d> dtl, List<m_account> dtcoa, string action)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); } 
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) { return RedirectToAction("NotAuthorize", "Profile"); }
            var msg = ""; var result = "failed"; var hdrid = ""; var sReturnNo = ""; var sReturnState = ""; var servertime = GetServerTime();             
            if (tbl.t_kbk_date < GetCutOffdate()) { msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_kbk_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />"; } 
            if (tbl.t_kbk_account_id == 0) msg += "Silahkan Pilih Akun!<br/>";
            if (string.IsNullOrEmpty(tbl.t_kbk_no)) { tbl.t_kbk_no = ""; }
            if (string.IsNullOrEmpty(tbl.t_kbk_ref_no)) { tbl.t_kbk_ref_no = ""; }
            if (string.IsNullOrEmpty(tbl.t_kbk_h_note)) { tbl.t_kbk_h_note = ""; }
            if (dtl != null)
            {
                foreach (var item in dtl)
                {
                    if (item.t_kbk_d_account_id == 0) { msg += $"{item.m_item_name} : Akun Tidak Tersedia!<br/>"; }
                    if (item.t_kbk_d_amt == 0) { msg += $"{item.m_item_name} : Jumlah Tidak boleh 0!<br/>"; }
                    if (tbl.t_kbk_h_status == "Post")
                    {
                        var sisa_piutang = db.tb_r_promo.Where(x => x.r_promo_ref_id == item.t_kbk_ref_id && x.r_promo_ref_table == item.t_kbk_ref_table && x.r_promo_status == "Post").Sum(y => y.r_promo_amt - y.r_promo_pay_amt);
                        if (sisa_piutang <= 0) { msg += $"Keterangan {item.m_item_name} sudah tidak ada sisa promo, silahkan pilih angsuran yg lain!<br/>"; }
                    }    
                }
            }

            if (tbl.t_kbk_h_status == "Post")
            {
                tbl.t_kbk_no = GenerateTransNo($"{tbl.t_kbk_type_bayar}", "r_kb_no", "tb_r_kb", tbl.m_company_id, tbl.t_kbk_date.ToString("MM/dd/yyyy")); 
                tbl.posted_at = servertime;
                tbl.posted_by = Session["UserID"].ToString();
            }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            db.tb_t_kbk_h.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            //cek data yg dihapus
                            var d_id = dtl.Select(s => s.t_kbk_d_id).ToList();
                            var dt_d = db.tb_t_kbk_d.Where(a => a.t_kbk_h_id == tbl.t_kbk_h_id && !d_id.Contains(a.t_kbk_d_id));
                            if (dt_d != null && dt_d.Count() > 0) db.tb_t_kbk_d.RemoveRange(dt_d);
                        }
                        db.SaveChanges();

                        if (tbl.t_kbk_h_status == "Post") { db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_kbk_uid, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.updated_by, tbl.updated_at)); } 

                        tb_t_kbk_d tbldtl;
                        if (dtl != null)
                        {
                            foreach (var item in dtl)
                            {
                                var cek_data = db.tb_t_kbk_d.Where(a => a.t_kbk_d_id == item.t_kbk_d_id);
                                tbldtl = (tb_t_kbk_d)MappingTable(new tb_t_kbk_d(), item);
                                if (cek_data != null && cek_data.Count() > 0) { tbldtl = db.tb_t_kbk_d.FirstOrDefault(a => a.t_kbk_d_id == item.t_kbk_d_id); } 
                                tbldtl.t_kbk_h_id = tbl.t_kbk_h_id;
                                tbldtl.t_kbk_ref_table = item.t_kbk_ref_table ?? "";
                                tbldtl.t_kbk_d_note = item.t_kbk_d_note ?? "";
                                tbldtl.created_by = tbl.updated_by;
                                tbldtl.created_at = tbl.updated_at;

                                if (cek_data != null && cek_data.Count() > 0) { db.Entry(tbldtl).State = EntityState.Modified; }
                                else { db.tb_t_kbk_d.Add(tbldtl); }
                                db.SaveChanges();

                                if (tbl.t_kbk_h_status == "Post")
                                {
                                    var t_kbk_d_note = item.t_kbk_d_note ?? db.tb_m_item.FirstOrDefault(i => i.m_item_id == item.t_kbk_ref_id && i.m_item_type == "Promo")?.m_item_name;
                                    db.tb_r_kb.Add(Insert_R_KB(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "tb_t_kbk_d", tbldtl.t_kbk_d_id, tbl.t_kbk_type_bayar, tbl.t_kbk_no, tbl.t_kbk_date, tbl.t_kbk_account_id, item.t_kbk_d_account_id, 0m, tbldtl.t_kbk_d_amt, t_kbk_d_note, tbl.created_by, tbl.created_at, 0, 0, tbl.t_kbk_h_id, tbl.t_kbk_type)); db.SaveChanges();

                                    db.tb_r_promo.Add(Insert_R_Promo(tbl.m_company_id, (int)tbl.t_promo_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, tbl.t_spr_id, tbl.t_spr_awal_id, (int)tbl.m_promo_id, "Payment", tbldtl.t_kbk_ref_table, tbldtl.t_kbk_ref_id, item.r_promo_ref_no, item.r_promo_ref_date, tbldtl.t_kbk_d_account_id, 0m, tbl.t_kbk_date, tbl.t_kbk_h_status, tbl.created_by, tbl.created_at, tbldtl.t_kbk_d_note, tbl.t_kbk_account_id, "tb_t_kbk_d", tbldtl.t_kbk_d_id, tbl.t_kbk_no, tbldtl.t_kbk_d_amt)); db.SaveChanges();
                                }
                            }
                        } 

                        if (tbl.t_kbk_h_status == "Post")
                        {
                            if (dtcoa.Any())
                            {
                                int seq = 1;
                                foreach (var item in dtcoa)
                                {
                                    db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, (int)tbl.t_promo_id, seq++, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.t_kbk_no, tbl.t_kbk_date, item.m_account_id, item.m_account_dbcr, (item.m_account_dbcr == "D" ? item.m_account_debet_amt : item.m_account_credit_amt), item.m_account_note, Session["UserID"].ToString(), GetServerTime(), tbl.m_cust_id)); 
                                    db.SaveChanges();
                                }
                            } 
                        }

                        db.SaveChanges(); objTrans.Commit(); hdrid = tbl.t_kbk_h_id.ToString();
                        if (tbl.t_kbk_h_status == "Post") { sReturnNo = tbl.t_kbk_no; sReturnState = "terposting"; }
                        else { sReturnNo = "draft " + tbl.t_kbk_h_id.ToString(); sReturnState = "tersimpan"; }
                        msg = $"Data {sReturnState} dengan No. {sReturnNo}<br />"; result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback(); var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += $"Entity of type {eve.Entry.Entity.GetType().Name} in state {eve.Entry.State} has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors) { err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />"; } 
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_t_kbk_h tbl = db.tb_t_kbk_h.Find(id);
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data tidak ditemukan!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.tb_t_kbk_d.Where(a => a.t_kbk_h_id == tbl.t_kbk_h_id);
                        db.tb_t_kbk_d.RemoveRange(trndtl); db.SaveChanges();

                        db.tb_t_kbk_h.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(string ids, string stype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            string rptname = "RptPrintKB";
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("userID", Session["UserID"].ToString());

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"),$"{rptname}.rpt"));
            sSql = $"SELECT * FROM tb_v_print_kb WHERE r_kb_h_id IN ({ids}) AND stype_trans='{stype}' ORDER BY r_kb_h_id";
            var dt = new ClassConnection().GetDataTable(sSql, "datatable");
            report.SetDataSource(dt);
            if (rptparam.Count > 0)
                foreach (var item in rptparam)
                    report.SetParameterValue(item.Key, item.Value);

            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", $"inline; filename={rptname}.pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}