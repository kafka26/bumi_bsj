﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class RevisiBayarSHMController : Controller
    {
        // GET: RevisiBayarSHM
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        public RevisiBayarSHMController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        #region Revisi Bayar SHM
        private void InitDDLAndField(tb_r_piutang tbl, string action)
        {
            //DDL
            var getCompany = db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList();
            if (action == "New Data")
                getCompany = getCompany.Where(m => m.flag == "ACTIVE").ToList();
            ViewBag.m_company_id = new SelectList(getCompany, "valuefield", "textfield", tbl.m_company_id);

            //Field
            ViewBag.m_item_name = db.tb_m_item.FirstOrDefault(w => w.m_item_id == tbl.m_item_id)?.m_item_name ?? "";
            ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(w => w.m_proyek_id == tbl.m_proyek_id)?.m_proyek_name ?? "";
            ViewBag.m_cust_name = db.tb_m_cust.FirstOrDefault(w => w.m_cust_id == tbl.m_cust_id)?.m_cust_name ?? "";

            sSql = $"Select * From tb_r_piutang Where m_item_id={tbl.m_item_id} AND r_piutang_ref_table IN ( 'tb_t_angsuran_shm_d' , 'tb_t_spr_shm') AND r_piutang_status='Post'"; 
            var r_piutang = db.Database.SqlQuery<tb_r_piutang>(sSql).ToList();
            var t_spr = db.tb_t_spr.FirstOrDefault(s => s.m_item_id == tbl.m_item_id);
            ViewBag.t_spr_no = t_spr.t_spr_no;

            ViewBag.r_piutang_amt = (t_spr.t_shm_type_bayar == "Tunai" ? r_piutang.Where(r => r.r_piutang_type == "SHM" && r.r_piutang_status == "Post").FirstOrDefault().r_piutang_amt : r_piutang.Where(r => r.r_piutang_type == "SHM").Sum(r => r.r_piutang_amt)); 

            ViewBag.r_piutang_pay_amt = r_piutang.Where(r => r.r_piutang_type == "PAYMENT").Sum(r=> r.r_piutang_pay_amt);

            ViewBag.r_piutang_amt_sisa = (t_spr.t_shm_type_bayar == "Tunai" ? r_piutang.Where(r => r.r_piutang_type == "SHM" && r.r_piutang_status == "Post").FirstOrDefault().r_piutang_amt : r_piutang.Where(r => r.r_piutang_type == "SHM").Sum(r => r.r_piutang_amt)) - r_piutang.Where(r => r.r_piutang_type == "PAYMENT").Sum(r => r.r_piutang_pay_amt);
        }

        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_r_piutang", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE isapptrans=0 AND isposttrans=0 ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }

            sSql = "Select * From (Select r.m_item_id, r.r_piutang_ref_no, r.m_item_name, r.m_cust_name, r.m_proyek_name, r.t_spr_date, r.t_spr_price, r.t_utj_amt, r.t_dp_amt, r.t_piutang_amt, r.t_kt_amt, r.t_shm_amt, r.t_spr_status From tb_v_kartu_kontrol r Where r_piutang_ref_table IN ('tb_t_spr_shm', 'tb_t_angsuran_shm_d') Group By r.m_item_id, r.r_piutang_ref_no, r.m_item_name, r.m_cust_name, r.m_proyek_name, r.t_spr_date, r.t_spr_price, r.t_utj_amt, r.t_dp_amt, r.t_piutang_amt, r.t_kt_amt, r.t_shm_amt, r.t_spr_status) r ";
            var sFixedFilter = "";
            var sOrder_by = " r.m_item_id";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_r_piutang tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_r_piutang();
                tbl.created_at = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_r_piutang.Where(r => r.m_item_id == id).FirstOrDefault();
            }

            if (tbl == null) return HttpNotFound();
            ViewBag.action = action;
            InitDDLAndField(tbl, action);
            return View(tbl);
        }

        public class r_piutang_d : tb_r_piutang
        {
            public string m_item_name { get; set; }
            public string m_cust_name { get; set; }
            public string m_proyek_name { get; set; }
        }

        [HttpPost]
        public ActionResult FillDetailData(int m_item_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select 0 r_piutang_seq, r.r_piutang_id, r.r_piutang_ref_id, r.r_piutang_ref_no, r.m_item_name, r.m_cust_name, r.m_proyek_name, r.t_spr_date, r.t_spr_price, r.t_utj_amt, r.t_dp_amt, r.t_piutang_amt, r.t_kt_amt, r.t_shm_amt, r.t_spr_status, r.r_piutang_note, r.r_piutang_ref_due_date, r.r_piutang_amt, r.r_piutang_pay_amt, r.r_piutang_disc_amt, r.r_piutang_pay_ref_date, r.r_piutang_pay_ref_table, r.m_item_id, r.r_piutang_pay_ref_id, r.r_piutang_pay_ref_no From tb_v_kartu_kontrol r Where r.m_item_id={m_item_id} AND r_piutang_status='Post' AND r.r_piutang_ref_table IN ('tb_t_spr_shm','tb_t_angsuran_shm_d') AND r.r_piutang_type='PAYMENT' Order By m_item_name, r.r_piutang_ref_id, r.r_piutang_pay_ref_date";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "m_supp"));
                //var tbl = db.Database.SqlQuery<r_piutang_d>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = sSql + e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost, ActionName("HapusData")]
        [ValidateAntiForgeryToken]
        public ActionResult HapusData(tb_r_piutang tbl, List<r_piutang_d> dtDtl)
        {
            var msg = "";
            var result = "failed";
            var hdrid = new Guid();
            if (dtDtl == null || dtDtl.Count <= 0) msg += "Please fill detail Process!<br />";

            if (string.IsNullOrEmpty(msg))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (dtDtl != null && dtDtl.Count() > 0)
                        {
                            foreach (var item in dtDtl)
                            {
                                if (item.r_piutang_pay_ref_id > 0)
                                {
                                    var t_kbm_h_id = db.tb_t_kbm_d.Where(k => k.t_kbm_d_id == item.r_piutang_pay_ref_id).FirstOrDefault().t_kbm_h_id;
                                    var t_kbm_h = db.tb_t_kbm_h.Where(a => a.t_kbm_h_id == t_kbm_h_id && a.m_item_id == tbl.m_item_id).FirstOrDefault();
                                    t_kbm_h.updated_at = GetServerTime();
                                    t_kbm_h.updated_by = Session["UserID"].ToString();
                                    t_kbm_h.t_kbm_h_status = "Force Closed";
                                    db.Entry(t_kbm_h).State = EntityState.Modified;
                                    var r_kb = db.tb_r_kb.Where(r => r.r_kb_ref_id == item.r_piutang_pay_ref_id && r.m_item_id == tbl.m_item_id && r.r_kb_ref_table == "tb_t_kbm_d").FirstOrDefault();
                                    db.tb_r_kb.Remove(r_kb);
                                    var r_gl = db.tb_r_gl.Where(r => r.r_gl_ref_id == t_kbm_h_id && r.r_gl_ref_table == "tb_t_kbm_h");
                                    db.tb_r_gl.RemoveRange(r_gl);
                                }

                                var r_piutang = db.tb_r_piutang.FirstOrDefault(r => r.r_piutang_id == item.r_piutang_id && r.m_item_id == tbl.m_item_id);
                                r_piutang.r_piutang_status = "Force Closed";
                                db.Entry(r_piutang).State = EntityState.Modified;
                            }
                        }
                        db.SaveChanges();
                        objTrans.Commit();
                        result = "success";
                        dtDtl = null;
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid, dtDtl }, JsonRequestBehavior.AllowGet);
        }


        #endregion

    }
}