﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class LaporanHarianController : Controller
    {
        // GET: LaporanHarian
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class ReportFilter
        {    
            public string periodstart { get; set; }
            public string periodend { get; set; } 
        }

        public ActionResult Report()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.periodstart = GetServerTime();
            ViewBag.periodend = GetServerTime();
            return View();
        }
         
        [HttpPost]
        public ActionResult PrintReport(ReportFilter param, string reporttype)
        {
            var rptfile = ""; var rptname = "";
            rptfile = $"RptHarian";
            rptname = $"LAPORAN_HARIAN_TGL_{param.periodstart}";
            rptfile = rptfile.Replace("PDF", "");
            sSql = $"SELECT * FROM dbo.GetReportHarianMasuk('{param.periodstart} 00:00:00', '{param.periodend} 23:59:59') r Order By m_proyek_name, r_kb_date";
            DataTable data = new ClassConnection().GetDataTable(sSql, "data");
            sSql = $"SELECT * FROM dbo.GetReportHarianKeluar('{param.periodstart}') r order by m_proyek_name";
            DataTable data2 = new ClassConnection().GetDataTable(sSql, "data");
            sSql = $"SELECT * FROM dbo.GetReportHarianSum('{param.periodstart} 00:00:00', '{param.periodend} 23:59:59') r ";
            DataTable data3 = new ClassConnection().GetDataTable(sSql, "data");

            var irequest = new ReportRequest_New()
            {
                rptDataSource = new List<DataTable> { data, data2, data3 },
                rptFile = rptfile,
                rptPaperSizeEnum = 0,
                rptPaperOrientationEnum = 0,
                rptExportType = reporttype,
                rptParam = new Dictionary<string, string>()
            };
            irequest.rptParam.Add("Periode", param.periodstart + " - " + param.periodend); 
            irequest.rptParam.Add("UserID", Session["UserID"].ToString());
            var rpt_id = GenerateReport_New(irequest);
            return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet);
        }
    }
}