﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using MIS_MAGNA_MVC.Controllers;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class InterfaceController : Controller
    {
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public InterfaceController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(tb_m_interface_h tbl, string action)
        {
        }

        public class m_interface_d : tb_m_interface_d
        {
            public string m_interface_d_type { get; set; }
        }

        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            InitAdvFilterIndex(modfil, "tb_m_interface_h", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( select m_interface_h_id, m_interface_uid, m_interface_var, m_interface_name, m_interface_status, m_interface_note, created_by, created_at, updated_by, updated_at from tb_m_interface_h ) AS t";

            var sFixedFilter = "";
            var sOrder_by = " t.m_interface_h_id ASC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res)
            {
                // simple remapping adding extra info to found dataset
                result.Add(s);
            };

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_m_interface_h tbl;

            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_m_interface_h();
                tbl.m_interface_uid = Guid.NewGuid();
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_m_interface_h.Find(id);
            }

            if (tbl == null) return HttpNotFound();

            ViewBag.action = action;
            InitDDL(tbl, action);
            return View(tbl);
        }

        [HttpPost]
        public ActionResult GetDataDetails()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT 0 m_interface_d_seq , a.m_account_id, a.m_account_code m_interface_d_code, a.m_account_desc m_interface_d_name, '' m_interface_d_note, m_account_type m_interface_d_type FROM tb_m_account a WHERE active_flag='ACTIVE' ORDER BY m_account_code";
                var tbl = db.Database.SqlQuery<m_interface_d>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int m_interface_h_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select d.m_interface_d_seq, d.m_account_id, d.m_interface_d_code, d.m_interface_d_name, m_account_type m_interface_d_type from tb_m_interface_d d Inner Join tb_m_account a ON d.m_account_id = a.m_account_id Where d.m_interface_h_id = {m_interface_h_id} Order By d.m_interface_d_seq";
                var tbl = db.Database.SqlQuery<m_interface_d>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_m_interface_h tbl, List<m_interface_d> dtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile"); 

            var msg = ""; var result = "failed"; var hdrid = "";
            var servertime = GetServerTime();

            if (dtl != null)
            {
                foreach (var item in dtl)
                {
                    if (string.IsNullOrEmpty(item.m_interface_d_code)) msg += "Silahkan isi Kode!<br/>";
                    if (string.IsNullOrEmpty(item.m_interface_d_name)) msg += "Silahkan isi Nama!<br/>";
                }
            }

            if (msg == "")
            {
                tbl.m_interface_var = tbl.m_interface_name;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Update Data")
                        {
                            tbl.updated_by = Session["UserID"].ToString();
                            tbl.updated_at = servertime;
                            db.Entry(tbl).State = EntityState.Modified;
                        }

                        var tbldtl = new List<tb_m_interface_d>();
                        if (dtl != null)
                        {
                            foreach (var item in dtl)
                            {
                                var cek_data = db.tb_m_interface_d.Where(a => a.m_interface_d_id == item.m_interface_d_id);
                                var new_dtl = (tb_m_interface_d)MappingTable(new tb_m_interface_d(), item);
                                if (cek_data != null && cek_data.Count() > 0)
                                {
                                    new_dtl = db.tb_m_interface_d.FirstOrDefault(a => a.m_interface_d_id == item.m_interface_d_id);
                                }
                                new_dtl.m_interface_h_id = tbl.m_interface_h_id;
                                new_dtl.m_interface_d_code = item.m_interface_d_code ?? "";
                                new_dtl.m_interface_d_name = item.m_interface_d_name ?? "";
                                new_dtl.created_by = tbl.updated_by;
                                new_dtl.created_at = tbl.updated_at;

                                if (cek_data != null && cek_data.Count() > 0) db.Entry(new_dtl).State = EntityState.Modified;
                                else tbldtl.Add(new_dtl);
                            }
                        }
                        if (tbldtl != null && tbldtl.Count() > 0) db.tb_m_interface_d.AddRange(tbldtl);
                        //cek data yg dihapus
                        var d_id = dtl.Select(s => s.m_interface_d_id).ToList();
                        var dt_d = db.tb_m_interface_d.Where(a => a.m_interface_h_id == tbl.m_interface_h_id && !d_id.Contains(a.m_interface_d_id));
                        if (dt_d != null && dt_d.Count() > 0) db.tb_m_interface_d.RemoveRange(dt_d);
                        db.SaveChanges();

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.m_interface_h_id.ToString();
                        msg = "Data Already Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_m_interface_h tbl = db.tb_m_interface_h.Find(id);
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data tidak ditemukan!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.tb_m_interface_d.Where(a => a.m_interface_h_id == tbl.m_interface_h_id);
                        db.tb_m_interface_d.RemoveRange(trndtl); db.SaveChanges();

                        db.tb_m_interface_h.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
    }
}