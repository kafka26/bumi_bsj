﻿using CrystalDecisions.CrystalReports.Engine;
using MIS_MAGNA_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using System.Runtime.InteropServices.ComTypes;
using System.Data.Entity.Migrations.Sql;

namespace MIS_MAGNA_MVC.Controllers
{
    public class SPRRevisiController : Controller
    {
        // GET: SPRRevisi
        private QL_MIS_MAGNAEntities db; private string sSql = "";
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]; 
        private static readonly int dp_account_id = GetCOAInterface(new List<string> { "PIUTANG DP" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int kt_account_id = GetCOAInterface(new List<string> { "PIUTANG KT" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int shm_account_id = GetCOAInterface(new List<string> { "PIUTANG SHM" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int kpr_account_id = GetCOAInterface(new List<string> { "PIUTANG KPR" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int jual_account_id = GetCOAInterface(new List<string> { "HUTANG PENJUALAN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int hutang_refund_id = GetCOAInterface(new List<string> { "HUTANG REFUND" }).FirstOrDefault()?.m_account_id ?? 0;
        #region SPR Revisi
        public class dt_gl
        {
            public decimal r_gl_amt { get; set; }
            public int m_account_id { get; set; }
            public string r_gl_db_cr { get; set; }
        }

        public SPRRevisiController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            var isCollapse = "True";
            InitAdvFilterIndex(modfil, "tb_t_spr", false);
            ViewBag.isCollapse = isCollapse;
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult IndexGantiUnit(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.Title = "SPR Ganti Unit / Ganti Harga"; 
            var isCollapse = "True";
            InitFilterIndex(modfil, "tb_t_spr", false);
            ViewBag.isCollapse = isCollapse;
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( SELECT h.t_spr_uid, h.t_spr_id, h.m_company_id, m_company_name, t_spr_date, h.t_spr_no, p.m_proyek_name, c.m_cust_name, i.m_item_name, t_spr_note, t_spr_status, t_spr_type_trans FROM tb_t_spr h INNER JOIN tb_m_company cmp ON cmp.m_company_id=h.m_company_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = h.m_proyek_id INNER JOIN tb_m_cust c ON c.m_cust_id = h.m_cust_id INNER JOIN tb_m_item i ON i.m_item_id = h.m_item_id Where t_spr_type_trans NOT IN ('Baru', 'Ganti Harga', 'Ganti Unit') ) AS t";
            var sFixedFilter = "";
            var sOrder_by = " t.t_spr_date DESC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        private void InitFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTableGantiUnit(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( SELECT h.t_spr_uid, h.t_spr_id, h.m_company_id, m_company_name, t_spr_date, h.t_spr_no, p.m_proyek_name, c.m_cust_name, i.m_item_name, t_spr_note, t_spr_status, t_spr_type_trans FROM tb_t_spr h INNER JOIN tb_m_company cmp ON cmp.m_company_id=h.m_company_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = h.m_proyek_id INNER JOIN tb_m_cust c ON c.m_cust_id = h.m_cust_id INNER JOIN tb_m_item i ON i.m_item_id = h.m_item_id Where t_spr_type_trans IN ('Ganti Unit', 'Ganti Harga') ) AS t";
            var sFixedFilter = "";
            var sOrder_by = " t.t_spr_date DESC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public class tb_marketing : tb_m_marketing
        {
            public int seq { get; set; }
        }
       
        private void InitDDL(tb_t_spr tbl, string action)
        {
            var getCompany = db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList();
            if (action == "New Data") getCompany = getCompany.Where(m => m.flag == "ACTIVE").ToList();
            ViewBag.m_company_id = new SelectList(getCompany, "valuefield", "textfield", tbl.m_company_id);

            var t_dp_account_id = new SelectList(GetCOAInterface(new List<string> { "PIUTANG DP" }), "m_account_id", "m_account_name", tbl.t_dp_account_id);
            ViewBag.t_dp_account_id = t_dp_account_id;
            var t_kt_account_id = new SelectList(GetCOAInterface(new List<string> { "PIUTANG KT" }), "m_account_id", "m_account_name", tbl.t_kt_account_id);
            ViewBag.t_kt_account_id = t_kt_account_id;
            var t_shm_account_id = new SelectList(GetCOAInterface(new List<string> { "PIUTANG SHM" }), "m_account_id", "m_account_name", tbl.t_shm_account_id);
            ViewBag.t_shm_account_id = t_shm_account_id;

            ViewBag.m_marketing_id = new SelectList(db.Database.SqlQuery<tb_marketing>($"Select * From (select m_marketing_id, m_marketing_name, 1 seq from tb_m_marketing Union ALL select 0 m_marketing_id, 'Lain-Lain' m_marketing_name, 0 seq) m Order By seq").ToList(), "m_marketing_id", "m_marketing_name", tbl.m_marketing_id);
        }

        private void FillAdditionalField(tb_t_spr tbl, string action)
        {
            ViewBag.m_cust_name = (tbl.m_cust_id != 0 ? db.tb_m_cust.FirstOrDefault(x => x.m_cust_id == tbl.m_cust_id).m_cust_name : "");
            ViewBag.m_item_name = (tbl.m_item_id != 0 ? db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id).m_item_name : "");
            ViewBag.m_proyek_name = (tbl.m_proyek_id != 0 ? db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == tbl.m_proyek_id).m_proyek_name : "");
            ViewBag.t_utj_no = (tbl.t_utj_id != 0 ? db.tb_t_utj.FirstOrDefault(x => x.t_utj_id == tbl.t_utj_id).t_utj_no : ""); 
            if (tbl.t_refund_id> 0)
            {
                var dtRefund = db.tb_t_refund.Where(r=> r.t_refund_id==tbl.t_refund_id).ToList();
                ViewBag.Biaya_ganti_unit = dtRefund.FirstOrDefault()?.t_refund_cost_amt ?? 0m;
                ViewBag.Biaya_IJB = dtRefund.FirstOrDefault()?.t_refund_cost_other_amt ?? 0m;
                ViewBag.Biaya_lain = dtRefund.FirstOrDefault()?.t_refund_cost_other1_amt ?? 0m;
                ViewBag.t_utj_pay_amt = dtRefund.FirstOrDefault()?.t_refund_utj_amt ?? 0m;
            }
        }

        public class t_refund : tb_t_refund
        {
            public int r_piutang_seq { get; set; }
            public int t_utj_id { get; set; } = 0;
            public string t_spr_no { get; set; }
            public string t_spr_no_new { get; set; }
            public string m_cust_name { get; set; }
            public string m_item_name { get; set; }
            public string m_proyek_name { get; set; }
            public string t_utj_no { get; set; }
            public decimal amt_bayar { get; set; }
            public decimal Biaya_ganti_unit { get; set; }
            public decimal Biaya_IJB { get; set; }
            public decimal Biaya_lain { get; set; }
            public decimal t_dp_amt { get; set; }
            public decimal t_dp_amt2 { get; set; }
            public decimal t_piutang_amt { get; set; }
            public decimal t_piutang_amt2 { get; set; }
            public decimal t_piutang_amt_old { get; set; }
            public decimal t_kt_amt { get; set; }
            public decimal t_kt_amt2 { get; set; }
            public decimal t_kt_amt_old { get; set; }
            public decimal t_shm_amt { get; set; }
            public decimal t_shm_amt2 { get; set; }
            public decimal t_spr_price { get; set; }
            public decimal t_spr_total_amt { get; set; }
            public decimal t_dp_amt_old { get; set; }
            
        }

        [HttpPost]
        public ActionResult GetDataSPR(string stype)
        {
            JsonResult js = null; var sList = new List<string>() { "Ganti Unit", "" };
            try
            {
                sSql = $"SELECT * from tb_v_spr_revisi r Where 1=1 AND r.t_refund_id NOT IN ( SELECT ISNULL(t_refund_id,0) FROM tb_t_spr WHERE ISNULL(t_refund_id, 0)> 0 ) ";
                if (stype == "Ganti Unit") { sSql += $" AND t_refund_type IN ('Ganti Unit', 'Ganti Harga') AND t_spr_type_trans NOT IN ('Ganti Unit', 'Ganti Harga')"; }                    
                else { sSql += $" AND t_refund_type NOT IN ('Ganti Unit', 'Ganti Harga')"; }
                var tbl = db.Database.SqlQuery<t_refund>(sSql).ToList();
                if (tbl.Count > 0)
                {
                    foreach (var item in tbl)
                    {
                        String[] strlist = item.t_spr_no.Split(new string[]{"-R"}, 1, StringSplitOptions.RemoveEmptyEntries);
                        string sNo = $"{strlist[0].ToString().Trim()}-R";
                        sSql = $"SELECT ISNULL(MAX(CAST(REPLACE(t_spr_no, '{sNo}', '') AS INTEGER)) + 1, 1) AS IDNEW FROM tb_t_spr WHERE t_spr_no LIKE '{sNo}%'"; 
                        item.t_spr_no_new = sNo + GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 0);
                        item.t_utj_no = db.tb_t_utj.FirstOrDefault(x => x.t_utj_id == item.t_utj_id)?.t_utj_no ?? "";
                        if (item.t_refund_type == "Ganti Tenor DP")
                        { 
                            item.t_dp_amt = item.t_piutang_amt;
                            item.t_dp_amt2 = item.t_refund_pay_amt_dp;
                        }
                        else if (item.t_refund_type == "Ganti Tenor KPR")
                        { 
                            item.t_piutang_amt = item.t_piutang_amt;
                            item.t_piutang_amt2 = item.t_refund_pay_amt_kpr;
                        }
                        else if (item.t_refund_type == "Ganti Tenor KT")
                        { 
                            item.t_kt_amt = item.t_piutang_amt;
                            item.t_kt_amt2 = item.t_refund_pay_amt_kt;
                        }
                        else if (item.t_refund_type == "Ganti Tenor SHM")
                        {
                            item.t_shm_amt = item.t_piutang_amt;
                            item.t_shm_amt2 = item.t_refund_pay_amt_shm;
                        }
                        else if (sList.Contains(item.t_refund_type))
                        { 
                            //item.t_spr_price = item.t_spr_price;
                            //item.t_spr_total_amt = item.t_spr_price;
                            item.t_dp_amt_old = item.t_dp_amt;
                            item.t_dp_amt2 = item.t_refund_pay_amt_dp;
                            item.t_dp_amt = (decimal)item.t_refund_sisa_amt_dp;

                            item.t_kt_amt_old = item.t_kt_amt;
                            item.t_kt_amt2 = item.t_refund_pay_amt_kt;
                            item.t_kt_amt = item.t_refund_sisa_amt_kt; 

                            item.t_piutang_amt_old = item.t_piutang_amt;
                            item.t_piutang_amt2 = item.t_refund_pay_amt_kpr;

                            decimal t_refund_total_cost_amt = item.t_refund_cost_amt + item.Biaya_ganti_unit + item.Biaya_IJB + item.Biaya_lain;
                            decimal t_refund_total_amt = item.t_refund_utj_amt + item.t_refund_pay_amt_dp + item.t_refund_pay_amt_kt + item.t_refund_pay_amt_kpr;

                            item.t_piutang_amt = (item.t_refund_price_new > 0m ? item.t_refund_price_new : item.t_refund_price) - t_refund_total_amt - t_refund_total_cost_amt;
                            item.t_refund_pay_amt = item.t_refund_total_amt;
                        }
                    }
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = sSql + e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(string stype, int t_spr_awal_id, string t_spr_no_new)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select * From tb_v_spr_revisi r Where 1=1 AND t_spr_awal_id={t_spr_awal_id}";
                var tbl = db.Database.SqlQuery<t_refund>(sSql).ToList();
                if (tbl.Count > 0)
                {
                    foreach (var item in tbl)
                    {
                        item.t_spr_no_new = t_spr_no_new.ToString();
                        if (item.t_refund_type == "Ganti Tenor DP")
                        {
                            var amt = item.t_dp_amt;
                            item.t_dp_amt = amt - item.t_refund_total_amt;
                            item.t_dp_amt2 = item.t_refund_total_amt;
                        }
                        else if (item.t_refund_type == "Ganti Tenor KPR")
                        {
                            var amt = item.t_piutang_amt;
                            item.t_piutang_amt = amt - item.t_refund_total_amt;
                            item.t_piutang_amt2 = item.t_refund_total_amt;
                        }
                        else if (item.t_refund_type == "Ganti Tenor KT")
                        {
                            var amt = item.t_kt_amt;
                            item.t_kt_amt = amt - item.t_refund_total_amt;
                            item.t_kt_amt2 = item.t_refund_total_amt;
                        }
                        else if (item.t_refund_type == "Ganti Tenor SHM")
                        {
                            var amt = item.t_shm_amt;
                            item.t_shm_amt = amt - item.t_refund_total_amt;
                            item.t_shm_amt2 = item.t_refund_total_amt;
                        }
                        else if (item.t_refund_type == "Ganti Unit")
                        {
                            item.t_spr_price = item.t_spr_price;
                            item.t_spr_total_amt = item.t_spr_price;
                            item.t_dp_amt_old = item.t_dp_amt + item.t_refund_utj_amt;
                            item.t_dp_amt2 = item.t_refund_pay_amt_dp;
                            item.t_dp_amt = (decimal)item.t_refund_sisa_amt_dp;

                            item.t_kt_amt_old = item.t_kt_amt;
                            item.t_kt_amt2 = item.t_refund_pay_amt_kt;
                            item.t_kt_amt = item.t_refund_sisa_amt_kt;

                            item.t_piutang_amt_old = item.t_piutang_amt;
                            item.t_piutang_amt2 = item.t_refund_pay_amt_kpr;
                            item.t_piutang_amt = item.t_spr_price - item.t_refund_total_amt;

                            item.t_refund_pay_amt = item.t_refund_total_amt;
                        }
                    }
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = sSql + e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataItem(int m_company_id)
        {
            JsonResult js = null;
            try
            {
                var tbl = (from i in db.tb_m_item
                           join p in db.tb_m_proyek on i.m_proyek_id equals p.m_proyek_id
                           join c in db.tb_m_cluster on i.m_cluster_id equals c.m_cluster_id
                           join t in db.tb_m_type_unit on i.m_item_type_unit_id equals t.m_type_unit_id
                           where new List<string> { "Booking", "Open" }.Contains(i.m_item_flag) && i.m_company_id == m_company_id
                           select new { i.m_item_id, i.m_item_name, p.m_proyek_id, p.m_proyek_name, c.m_cluster_name, t.m_type_unit_name, i.m_item_luas_bangunan, i.m_item_luas_standart, i.m_item_luas_tanah }).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetShowCoa(t_refund param)
        {
            JsonResult js = null; string sResult = "";
            try
            {
                var sText = ""; var new_jurnal = new List<m_account>(); 
                var coa_penjualan = db.tb_m_account.Where(x => x.m_account_id == jual_account_id);
                var coa_refund = db.tb_m_account.Where(x => x.m_account_id == hutang_refund_id);

                // Piutang Hutang Penjualan
                new_jurnal.Add(new m_account { m_account_id = hutang_refund_id, m_account_code = coa_refund.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_refund.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = param.t_refund_total_amt, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" }); 

                if (param.t_dp_amt > 0m)
                {
                    // Piutang DP
                    var coa_dp = db.tb_m_account.Where(x => x.m_account_id == dp_account_id);
                    new_jurnal.Add(new m_account { m_account_id = dp_account_id, m_account_code = coa_dp.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_dp.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = param.t_dp_amt, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });
                }

                if (param.t_kt_amt > 0m) 
                {
                    // Piutang KT
                    var coa_kt = db.tb_m_account.Where(x => x.m_account_id == kt_account_id);
                    new_jurnal.Add(new m_account { m_account_id = kt_account_id, m_account_code = coa_kt.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_kt.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = param.t_kt_amt, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });
                }

                if (param.t_piutang_amt > 0m)
                {
                    // Piutang KPR
                    var coa_kpr = db.tb_m_account.Where(x => x.m_account_id == kpr_account_id);
                    new_jurnal.Add(new m_account { m_account_id = kpr_account_id, m_account_code = coa_kpr.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_kpr.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = param.t_piutang_amt, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });
                }
                 
                new_jurnal.Add(new m_account { m_account_id = jual_account_id, m_account_code = coa_penjualan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_penjualan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = param.t_spr_price, m_account_dbcr = "C", m_account_note = "" });
                var tbl = new_jurnal.ToList();

                sResult = "ganti_harga"; tbl = new_jurnal.ToList();
                sText = "Apakah anda yakin akan ganti harga unit?";
                js = Json(new { result = sResult, stext = $"{sText}", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_t_spr tbl;
            string action = "New Data";
            ViewBag.stype = "Revisi";
            if (id == null)
            {
                tbl = new tb_t_spr();
                tbl.t_spr_uid = Guid.NewGuid();
                tbl.t_spr_date = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.t_spr_status = "In Process"; 
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_spr.Find(id);
            }

            ViewBag.action = action;
            InitDDL(tbl, action);
            FillAdditionalField(tbl, action);
            return View(tbl);
        }

        public ActionResult FormGantiUnit(int? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_t_spr tbl;
            string action = "New Data";
            ViewBag.stype = "Ganti Unit";
            if (id == null)
            {
                tbl = new tb_t_spr();
                tbl.t_spr_uid = Guid.NewGuid();
                tbl.t_spr_date = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.t_spr_status = "In Process";
                tbl.t_dp_type = "Tidak";
                tbl.t_kt_type = "Tidak"; 
                //tbl.t_spr_type_trans = "Ganti Unit";
                ViewBag.t_refund_pay_amt = 0;
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_spr.Find(id);
                ViewBag.t_refund_pay_amt = (db.tb_t_refund.FirstOrDefault(r => r.t_refund_id == tbl.t_refund_id)?.t_refund_total_amt ?? 0);
            }

            ViewBag.action = action;
            InitDDL(tbl, action);
            FillAdditionalField(tbl, action);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_spr tbl, string action, List<m_account> dtDtl)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed";
            var hdrid = ""; var servertime = GetServerTime();
            int r_piutang_ref_id = db.tb_r_piutang.AsNoTracking().ToList().Max(x => x.r_piutang_id);
            var refund = db.Database.SqlQuery<t_refund>($"select * from tb_v_spr_revisi where t_refund_id={tbl.t_refund_id}").ToList();
            if (string.IsNullOrEmpty(tbl.t_utj_type)) tbl.t_utj_type = "";
            if (string.IsNullOrEmpty(tbl.t_spr_promo_dp_type)) tbl.t_spr_promo_dp_type = "";
            if (string.IsNullOrEmpty(tbl.t_spr_promo_dp_note)) tbl.t_spr_promo_dp_note = "";
            if (string.IsNullOrEmpty(tbl.t_spr_note)) tbl.t_spr_note = "";
            if (string.IsNullOrEmpty(tbl.t_shm_status)) tbl.t_shm_status = "";
            if (string.IsNullOrEmpty(tbl.t_shm_type)) tbl.t_shm_type = "";

            if (tbl.t_dp_type == "Ya" && tbl.t_dp_amt == 0) msg += $"Anda Pilih Uang Muka (Ya), Silahkan isi nominal Uang Muka <br/>";
            else if (tbl.t_dp_type == "Tidak" && tbl.t_dp_amt > 0) msg += $"Anda Pilih Uang Muka (Tidak), Silahkan isi nol nominal Uang Muka <br/> <br/>";

            if (tbl.t_kt_type == "Ya" && tbl.t_kt_amt == 0) msg += $"Anda Pilih Kelebihan Tanah (Ya), Silahkan isi nominal Kelebihan Tanah <br/>";
            else if (tbl.t_kt_type == "Tidak" && tbl.t_kt_amt > 0) msg += $"Anda Pilih Kelebihan Tanah (Tidak), Silahkan isi nol nominal Kelebihan Tanah <br/> <br/>";

            if (tbl.t_piutang_type_bayar == "-" && tbl.t_piutang_amt > 0) msg += "Silahkan pilih tipe KPR (Tunai/Angsuran)!<br/>";
            if (tbl.t_spr_date < GetCutOffdate())
                msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_spr_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />";
            var slist = new List<string> { "Ganti Unit", "Ganti Harga" }; 
            if (!slist.Contains(tbl.t_spr_type_trans))
            {
                tbl.t_dp_amt_old = 0;
                tbl.t_piutang_amt_old = 0;
                tbl.t_kt_amt_old = 0;
            }

            if (new List<string> { "Ganti Unit" }.Contains(tbl.t_spr_type_trans))
            {   
                var sCek_item = db.tb_t_refund.Where(x => x.t_refund_id == tbl.t_refund_id && x.m_cust_id==tbl.m_cust_id);
                //if (sCek_item.Count() > 0 && action == "New Data")
                //{
                //    var m_unit = db.tb_m_item.Where(c => c.m_item_id == tbl.m_item_id).FirstOrDefault()?.m_item_name;
                //    msg += $"Unit yang anda pilih masih sama dengan unit lama {m_unit.ToUpper()} <br/>";
                //}

                if (refund.Count() <=0) msg += $"Maaf, data SPR {tbl.t_spr_no} data sedang bermasalah silahkan hubungi pihak terkait..<br/>"; 
                decimal t_refund_total_cost_amt = sCek_item.FirstOrDefault()?.t_refund_cost_amt ?? 0m + sCek_item.FirstOrDefault()?.t_refund_cost_other_amt ?? 0m + sCek_item.FirstOrDefault()?.t_refund_cost_other1_amt ?? 0m; 
                decimal t_refund_pay_amt_kpr = sCek_item.FirstOrDefault()?.t_refund_pay_amt_kpr ?? 0m;
                decimal t_refund_utj_amt = sCek_item.FirstOrDefault()?.t_refund_utj_amt ?? 0m;

                decimal total_piutang = (tbl.t_dp_amt > 0m ? tbl.t_dp_amt : tbl.t_dp_amt2) + (tbl.t_kt_amt > 0m ? tbl.t_kt_amt : tbl.t_kt_amt2) + t_refund_total_cost_amt + (t_refund_pay_amt_kpr > 0m ? t_refund_pay_amt_kpr : 0m ) + 0 + tbl.t_piutang_amt;

                decimal total_spr = (tbl.t_piutang_amt2 + tbl.t_dp_amt2 + tbl.t_dp_amt + tbl.t_piutang_amt + tbl.t_kt_amt2 + tbl.t_kt_amt) - t_refund_total_cost_amt;
                if (tbl.t_spr_price != total_spr)
                {
                    msg += $"Data yang anda input adalah : <br/> - Jumlah UM (Baru) : {toMoney(tbl.t_dp_amt)} <br/> - Jumlah KT (Baru) : {toMoney(tbl.t_kt_amt)} <br/> - Jumlah Piutang (Baru) : {toMoney(tbl.t_piutang_amt)} <br/> ";

                    msg += $">> Rumus dari form ganti unit adalah: <br/> Jumlah UM (Baru) : {toMoney(tbl.t_dp_amt)} + Jumlah UM (Terbayar) : {toMoney(tbl.t_dp_amt2)} + Jumlah KT (Baru) : {toMoney(tbl.t_kt_amt)} + Jumlah KT (Terbayar) : {toMoney(tbl.t_kt_amt2)} + Jumlah Piutang (Baru) : {toMoney(tbl.t_piutang_amt)} + Jumlah Piutang (Terbayar) : {toMoney(tbl.t_piutang_amt2)} - Total Biaya Refund : {toMoney(refund.FirstOrDefault().t_refund_cost_amt)} => hasilnya {toMoney(total_spr - refund.FirstOrDefault().t_refund_cost_amt)}..!<br/>";

                    msg += $"- Harga SPR => {toMoney(tbl.t_spr_price)} <br/>";
                    msg += $"- Ada Selisih {toMoney((tbl.t_spr_price - total_spr) + refund.FirstOrDefault().t_refund_cost_amt)} dari : { toMoney(tbl.t_spr_price)} - {toMoney(total_spr + refund.FirstOrDefault().t_refund_cost_amt)} - {toMoney(refund.FirstOrDefault().t_refund_cost_amt)} = {toMoney(tbl.t_spr_price)} <br/>";
                    msg += $"- Input nominal {toMoney(tbl.t_spr_price - total_spr)} untuk di bebankan ke Jumlah UM (Baru) atau ke yang lain..!<br/>";
                }
            }            

            if (tbl.m_cust_id == 0) { msg += "Silahkan pilih customer!<br/>"; }
            if (tbl.m_item_id == 0) { msg += "Silahkan pilih unit!<br/>"; }
            if (tbl.m_marketing_id == 0 && string.IsNullOrEmpty(tbl.m_marketing_name)) { tbl.m_marketing_id = 0; tbl.m_marketing_name = ""; }                
            if (tbl.t_spr_promo_dp_type == "DISKON" && tbl.t_spr_promo_dp_persen <= 0) { msg += "Persentase promo diskon harus lebih dari 0%!<br/>"; } 
            if (tbl.t_spr_promo_dp_type == "DISKON" && tbl.t_spr_promo_dp_persen > 100) { msg += "Persentase promo diskon harus kurang dari 100%!<br/>"; } 
            if (tbl.t_spr_promo_dp_type == "LAINNYA" && string.IsNullOrEmpty(tbl.t_spr_promo_dp_note)) { msg += "Catatan promo lain tidak boleh kosong!<br/>"; }  
            if (tbl.t_dp_type=="Tidak" && tbl.t_dp_amt > 0) msg += "Silahkan pilih tipe uang muka (Ya/Tidak)..!<br/>";
            if (string.IsNullOrEmpty(tbl.t_dp_type_bayar) && tbl.t_dp_amt > 0) msg += "Silahkan pilih tipe uang muka (Tunai/Angsuran)..!<br/>";
            if (tbl.t_kt_type == "Tidak" && tbl.t_kt_amt > 0) msg += "Silahkan pilih tipe kelebihan tanah (Ya/Tidak)!<br/>";
            if (string.IsNullOrEmpty(tbl.t_kt_type_bayar) && tbl.t_kt_amt > 0) msg += "Silahkan pilih tipe kelebihan tanah (Tunai/Angsuran)!<br/>"; 
            if (tbl.t_dp_amt == 0) { tbl.t_dp_type = "Tidak"; tbl.t_dp_type_bayar = "-"; } 
            if (tbl.t_kt_amt == 0) { tbl.t_kt_type = "Tidak"; tbl.t_kt_type_bayar = "-"; } 
            if (tbl.t_spr_type_trans == "Ganti Tenor KPR" && string.IsNullOrEmpty(tbl.t_piutang_type_bayar)) 
                msg += "Tolong pilih tipe KPR (Tunai/Angsuran)!<br/>";

            if (tbl.t_spr_type_trans != "Ganti Unit")
            {
                if (tbl.t_dp_type == "Ya")
                {
                    if (tbl.t_dp_type_bayar == "Angsuran" && tbl.t_dp_total_angsuran <= 0) { msg += "Total angsuran uang muka harus lebih dari 0!<br>"; } 
                    if (tbl.t_dp_amt <= 0) { msg += "Jumlah uang muka harus lebih dari 0!<br>"; }
                }
                if (tbl.t_kt_type == "Ya")
                {
                    if (tbl.t_kt_type_bayar == "Angsuran" && tbl.t_kt_total_angsuran <= 0) { msg += "Total angsuran kelebihan tanah harus lebih dari 0!<br>"; }
                    if (tbl.t_kt_amt <= 0) { msg += "Jumlah kelebihan tanah harus lebih dari 0!<br>"; }
                }
                if (tbl.t_shm_type == "Ya")
                {
                    if (tbl.t_shm_type_bayar == "Angsuran" && tbl.t_shm_total_angsuran <= 0) { msg += "Total angsuran kelebihan tanah harus lebih dari 0!<br>"; }
                    if (tbl.t_shm_amt <= 0) { msg += "Jumlah kelebihan tanah harus lebih dari 0!<br>"; }
                }
                if (tbl.t_piutang_type_bayar == "Angsuran")
                {
                    if (tbl.t_piutang_total_angsuran <= 0) msg += "Total angsuran piutang harus lebih dari 0!<br>";
                    if (tbl.t_piutang_amt <= 0) msg += "Jumlah piutang harus lebih dari 0!<br>";
                    if (tbl.t_spr_total_amt <= 0) msg += "Total SPR harus lebih dari 0!<br>";
                }
                else if(tbl.t_spr_total_amt < 0) msg += "Total SPR tidak boleh kurang dari 0!<br>";
            }

            if (string.IsNullOrEmpty(msg))
            {
                if (tbl.t_spr_status == "Revised") tbl.t_spr_status = "In Process";
                if (tbl.t_spr_awal_id == 0) tbl.t_spr_awal_id = refund.FirstOrDefault().t_spr_awal_id;
                if (tbl.t_spr_status == "Post")
                { 
                    tbl.posted_by = Session["UserID"].ToString();
                    tbl.posted_at = servertime;
                }
                //else tbl.t_spr_no = "";
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.m_item_old_id = db.tb_t_refund.FirstOrDefault(x => x.t_refund_id == tbl.t_refund_id)?.m_item_id ?? 0; 
                        if (action == "New Data")
                        {
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            db.tb_t_spr.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = GetServerTime();
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                        }
                        db.SaveChanges(); 

                        if (tbl.t_spr_status == "Post")
                        { 
                            //Block Post Transaction
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_spr_uid, "tb_t_spr", tbl.t_spr_id, tbl.updated_by, tbl.updated_at));

                            // --> DP
                            if (tbl.t_dp_type == "Ya" && tbl.t_dp_type_bayar == "Tunai" && tbl.t_dp_amt > 0)
                            {
                                var dp = db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "DP", "tb_t_spr_dp", r_piutang_ref_id + 1, tbl.t_spr_no, tbl.t_spr_date, tbl.t_spr_date, Convert.ToDateTime("1/1/1900".ToString()), dp_account_id, tbl.t_dp_amt, 0m, "Uang Muka", tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0m, tbl.t_spr_id, tbl.t_spr_awal_id)); 
                                db.SaveChanges(); r_piutang_ref_id = dp.r_piutang_id;
                            }
                            // --> KT
                            if (tbl.t_kt_type == "Ya" && tbl.t_kt_type_bayar == "Tunai" && tbl.t_kt_amt > 0)
                            {
                                var kt = db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "KT", "tb_t_spr_kt", r_piutang_ref_id + 1, tbl.t_spr_no, tbl.t_spr_date, tbl.t_spr_date, Convert.ToDateTime("1/1/1900".ToString()), kt_account_id, tbl.t_kt_amt, 0m, "Kelebihan Tanah", tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0m, tbl.t_spr_id, tbl.t_spr_awal_id));
                                db.SaveChanges(); r_piutang_ref_id = kt.r_piutang_id;
                            }
                            // --> SHM
                            if (tbl.t_shm_type == "Ya" && tbl.t_shm_type_bayar == "Tunai" && tbl.t_shm_amt > 0)
                            {
                                var shm = db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "SHM", "tb_t_spr_shm", r_piutang_ref_id + 1, tbl.t_spr_no, tbl.t_spr_date, tbl.t_spr_date, Convert.ToDateTime("1/1/1900".ToString()), shm_account_id, tbl.t_shm_amt, 0m, "SHM", tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0m, tbl.t_spr_id, tbl.t_spr_awal_id)); db.SaveChanges(); r_piutang_ref_id = shm.r_piutang_id;
                            }
                            // --> KPR
                            if (tbl.t_piutang_type_bayar == "Tunai" && tbl.t_piutang_amt > 0)
                            {
                                var kpr = db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "KPR", "tb_t_spr", r_piutang_ref_id + 1, tbl.t_spr_no, tbl.t_spr_date, tbl.t_spr_date, Convert.ToDateTime("1/1/1900".ToString()), kpr_account_id, tbl.t_piutang_amt, 0m, "KPR Tunai", tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0m, tbl.t_spr_id, tbl.t_spr_awal_id)); db.SaveChanges();
                            }

                            if (slist.Contains(tbl.t_spr_type_trans)) { msg = PostJurnalGanti(tbl, dtDtl); } 
                            else { msg = PostJurnalTenor(tbl, refund); } 
                            if (!string.IsNullOrEmpty(msg))
                            {
                                objTrans.Rollback(); result = "failed";
                                return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        db.SaveChanges();
                        objTrans.Commit();

                        hdrid = tbl.t_spr_id.ToString(); result = "success";
                        if (tbl.t_spr_status == "Post") msg = $"data telah diposting dengan nomor transaksi {tbl.t_spr_no}";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        private string PostJurnalGanti(tb_t_spr tbl, List<m_account> dtDtl, string msg = "", int seq = 1)
        {
            try
            { 
                if (dtDtl.Any())
                {
                    foreach (var item in dtDtl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_spr", (int)tbl.t_spr_id, tbl.t_spr_no, tbl.t_spr_date, item.m_account_id, item.m_account_dbcr, (item.m_account_dbcr == "D" ? item.m_account_debet_amt : item.m_account_credit_amt), $"SPR {tbl.t_spr_type_trans}", Session["UserID"].ToString(), GetServerTime(), tbl.m_cust_id)); db.SaveChanges();
                    }
                }
                //Akhir Insert Jurnal Akunting
            }
            catch (Exception e)
            {
                msg = e.ToString();
            }
            db.SaveChanges();
            return msg;
        }

        private string PostJurnalTenor(tb_t_spr tbl, List<t_refund> refund, string msg = "")
        { 
            try
            { 
                var new_dt_gl = new List<dt_gl>(); int seq = 1; var dtr = refund.FirstOrDefault();
                var gl_note = tbl.t_spr_note == "" ? $"SPR {tbl.t_spr_type_trans.ToUpper()}" : tbl.t_spr_note; 

                if (tbl.t_spr_type_trans.Contains("Ganti Tenor DP"))
                    new_dt_gl.Add(new dt_gl { r_gl_amt = refund.FirstOrDefault()?.t_refund_price ?? 0m - refund.FirstOrDefault()?.t_refund_pay_amt_dp ?? 0m, m_account_id = dp_account_id, r_gl_db_cr = "D" }); 
                if (tbl.t_spr_type_trans.Contains("Ganti Tenor KT"))
                    new_dt_gl.Add(new dt_gl { r_gl_amt = refund.FirstOrDefault()?.t_refund_price ?? 0m - refund.FirstOrDefault()?.t_refund_pay_amt_kt ?? 0m, m_account_id = kt_account_id, r_gl_db_cr = "D" });
                if (tbl.t_spr_type_trans.Contains("Ganti Tenor KPR"))
                    new_dt_gl.Add(new dt_gl { r_gl_amt = refund.FirstOrDefault()?.t_refund_price ?? 0m - refund.FirstOrDefault()?.t_refund_pay_amt_kpr ?? 0m, m_account_id = kpr_account_id, r_gl_db_cr = "D" });

                new_dt_gl.Add(new dt_gl { r_gl_amt = refund.FirstOrDefault()?.t_refund_total_amt ?? 0m, m_account_id = hutang_refund_id, r_gl_db_cr = "D" });
                new_dt_gl.Add(new dt_gl { r_gl_amt = refund.FirstOrDefault()?.t_refund_price ?? 0m, m_account_id = jual_account_id, r_gl_db_cr = "C" });

                var amt_debet = new_dt_gl.Where(x => x.r_gl_db_cr == "D").Sum(x => x.r_gl_amt);
                var amt_credit = new_dt_gl.Where(g => g.r_gl_db_cr == "C").Sum(x => x.r_gl_amt);
                var amtpembulatan = amt_debet - amt_credit;
                if (amtpembulatan != 0)
                {
                    var account_id_pembulatan = GetCOAInterface(new List<string> { "PEMBULATAN" }).FirstOrDefault()?.m_account_id ?? 0;
                    if (amtpembulatan < 0) { new_dt_gl.Add(new dt_gl { r_gl_amt = amtpembulatan * -1, m_account_id = account_id_pembulatan, r_gl_db_cr = "D" }); }
                    else { new_dt_gl.Add(new dt_gl { r_gl_amt = amtpembulatan, m_account_id = account_id_pembulatan, r_gl_db_cr = "C" }); }
                }

                if (new_dt_gl.Any())
                    foreach (var item in new_dt_gl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_spr", tbl.t_spr_id, tbl.t_spr_no, tbl.t_spr_date, item.m_account_id, item.r_gl_db_cr, item.r_gl_amt, gl_note, tbl.created_by, tbl.created_at, tbl.m_cust_id));
                    }
            }
            catch (Exception e)
            {
                msg = e.ToString();
            }
            db.SaveChanges();
            return msg;
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string revisi)
        {

            tb_t_spr tbl = db.tb_t_spr.Find(id);
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.tb_t_spr.Remove(tbl);
                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Print Out
        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            string rptname = "PrintRevSPR";
            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), $"rpt{rptname}.rpt"));

            sSql = $"SELECT * FROM tb_v_print_spr WHERE t_spr_id IN ({ids}) ORDER BY t_spr_id";
            var dtRpt = new ClassConnection().GetDataTable(sSql, "data");

            report.SetDataSource(dtRpt);
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }
        #endregion
    }
}