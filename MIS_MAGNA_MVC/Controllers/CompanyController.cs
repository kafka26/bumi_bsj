﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace MIS_MAGNA_MVC.Controllers
{
    public class CompanyController : Controller
    {
        // GET: Company
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        #region Mster Company
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_m_company", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column()
                {
                    name = "sAddFilter",
                    data = "sAddFilter",
                    search = new Search()
                    {
                        value = "",
                        regex = ""
                    },
                    searchable = true,
                    orderable = false
                };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM (Select b.m_company_uid, b.m_company_id, b.m_company_code, b.m_company_name, b.m_company_addr, c.m_city_name, p.m_province_name, b.m_company_npwp, b.m_company_phone_1, b.m_company_phone_2, b.m_company_email, b.active_flag, b.created_by, b.created_at, b.last_edited_by, b.last_edited_at from tb_m_company b Inner Join tb_m_city c ON c.m_city_id=b.m_company_city_id Inner Join tb_m_province p ON p.m_province_id=c.m_province_id) AS t";
            var sFixedFilter = "";
            var sOrder_by = " t.m_company_code ASC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_m_company tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_m_company();
                tbl.m_company_uid = Guid.NewGuid();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_m_company.FirstOrDefault(p => p.m_company_uid == id);
            }
            if (tbl == null)
                return HttpNotFound();
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        private void InitDDL(tb_m_company tbl)
        {
            ViewBag.m_company_city_id = new SelectList(db.tb_m_city.Where(a => a.m_city_flag == "ACTIVE").ToList(), "m_city_id", "m_city_name", tbl.m_company_city_id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_m_company tbl, string action)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            var servertime = ClassFunction.GetServerTime();
            if (string.IsNullOrEmpty(tbl.m_company_name)) msg += "Tolong, Isi Nama Company";
            if (string.IsNullOrEmpty(tbl.m_company_code)) msg += "Tolong, Isi Kode Company";

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.last_edited_at = tbl.created_at;
                            tbl.last_edited_by = tbl.created_by;
                            db.tb_m_company.Add(tbl);
                        }
                        else
                        {
                            tbl.last_edited_at = servertime;
                            tbl.last_edited_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.m_company_uid.ToString();
                        msg = "Data Already Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: PurchaseReturnService/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid? m_company_uid)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_m_company tbl = db.tb_m_company.FirstOrDefault(p => p.m_company_uid == m_company_uid);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.tb_m_company.Remove(tbl);
                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}