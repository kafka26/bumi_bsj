﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using static MIS_MAGNA_MVC.GlobalFunctions;

namespace MIS_MAGNA_MVC.Controllers
{
    public class ProyekController : Controller
    {
        // GET: Proyek
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        #region Master Proyek
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile"); 
            InitAdvFilterIndex(modfil, "tb_m_proyek", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column()
                {
                    name = "sAddFilter",
                    data = "sAddFilter",
                    search = new Search()
                    {
                        value = "",
                        regex = ""
                    },
                    searchable = true,
                    orderable = false
                };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM (Select m_proyek_id, m_proyek_uid, m_proyek_code, m_proyek_name, m_proyek_addr, m_proyek_flag, created_by, created_at, last_edited_by, last_edited_at from tb_m_proyek) AS t";
            var sFixedFilter = "";
            var sOrder_by = " t.m_proyek_name DESC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);  
            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_m_proyek tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_m_proyek();
                tbl.m_proyek_uid = Guid.NewGuid();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();                
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_m_proyek.FirstOrDefault(p => p.m_proyek_uid==id);
            }
            if (tbl == null)
                return HttpNotFound();
            ViewBag.action = action;
            FieldAdd(tbl);
            return View(tbl);
        }

        public class DDLAccount
        {
            public int m_account_id { get; set; }
            public string m_account_desc { get; set; }

        }
        private void FieldAdd(tb_m_proyek tbl)
        { 
            ViewBag.m_company_id = new SelectList(db.tb_m_company.Where(a => a.active_flag == "ACTIVE").ToList(), "m_company_id", "m_company_name", tbl.m_company_id); 

            ViewBag.m_account_id = new SelectList(GetCOAInterface(new List<string> { "PIUTANG KPR" }), "m_account_id", "m_account_name", tbl.m_account_h_id);

            ViewBag.m_account_h_id = new SelectList(GetCOAInterface(new List<string> { "HUTANG PENJUALAN" }), "m_account_id", "m_account_name", tbl.m_account_h_id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_m_proyek tbl, string action)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            var servertime = GetServerTime();
            if (string.IsNullOrEmpty(tbl.m_proyek_name)) msg += "Tolong, Isi nama proyek";
            if (string.IsNullOrEmpty(tbl.m_proyek_addr)) msg += "Tolong, Isi alamat proyek";
            if (string.IsNullOrEmpty(tbl.m_proyek_code)) msg += "Tolong, Isi Kode proyek";
            if (string.IsNullOrEmpty(tbl.m_proyek_flag)) tbl.m_proyek_flag="OPEN";
            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.last_edited_at = tbl.created_at;
                            tbl.last_edited_by = tbl.created_by;
                            db.tb_m_proyek.Add(tbl); 
                        }
                        else
                        {
                            tbl.last_edited_at = servertime;
                            tbl.last_edited_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                        } 

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.m_proyek_uid.ToString();
                        msg = "Data Already Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors) 
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />"; 
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: PurchaseReturnService/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid? m_proyek_uid)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_m_proyek tbl = db.tb_m_proyek.FirstOrDefault(p => p.m_proyek_uid == m_proyek_uid);
            var servertime = GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    { 
                        db.tb_m_proyek.Remove(tbl);
                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}