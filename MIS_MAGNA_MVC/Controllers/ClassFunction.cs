﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Security.AccessControl;
using static MIS_MAGNA_MVC.GlobalClass;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Migrations.Model;

namespace MIS_MAGNA_MVC.Controllers
{
    public class ClassFunction
    {
        public class listcoa
        {
            public int m_account_id { get; set; }
            public string m_account_code { get; set; }
            public string m_account_name { get; set; }
            public string m_account_var { get; set; }
        }

        public static List<listcoa>GetCOAInterface(List<string> sVar)
        {
            QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            List<listcoa> dt_interface = new List<listcoa>();
            List<listcoa> dt_temp = new List<listcoa>();
            //var acctg_code = (from h in db.tb_m_interface_h join d in db.tb_m_interface_d on h.m_interface_h_id equals d.m_interface_h_id where sVar.Contains(h.m_interface_name) select d.m_interface_d_code).ToList();
            var coa_code = db.tb_m_interface_h.Where(a => sVar.Contains(a.m_interface_name)).Join(db.tb_m_interface_d, m => m.m_interface_h_id, d => d.m_interface_h_id, (m, d) => new { d.m_interface_d_code, d.m_account_id, d.m_interface_d_name, m.m_interface_name }).ToList();

            var parent_id = db.tb_m_account.Where(x => x.m_account_parent_id != 0).Select(a => a.m_account_parent_id).ToList();
            if (coa_code != null && coa_code.Count > 0)
            {
                foreach (var item in coa_code)
                { 
                    var dt_account = db.tb_m_account.Where(a => !parent_id.Contains(a.m_account_id) && a.active_flag == "ACTIVE" && a.m_account_code.StartsWith(item.m_interface_d_code)).ToList();
                    if (dt_account != null && coa_code.Count > 0)
                    {
                        foreach (var a in dt_account)
                        {
                            dt_temp.Add(new listcoa
                            {
                                m_account_id = a.m_account_id,
                                m_account_code = a.m_account_code,
                                m_account_name = a.m_account_desc
                            });
                        }
                    }
                }
                if (dt_temp != null && dt_temp.Count > 0)
                {
                    foreach (var a in dt_temp.GroupBy(g => new { g.m_account_id, g.m_account_code, g.m_account_name }).ToList())
                    {
                        dt_interface.Add(new listcoa
                        {
                            m_account_id = a.Key.m_account_id,
                            m_account_code = a.Key.m_account_code,
                            m_account_name = a.Key.m_account_name
                        });
                    }
                }   
            }
            return dt_interface;
        }

        public class t_titipan : tb_t_titipan
        {
            public decimal t_titipan_debet_amt { get; set; }
            public decimal t_titipan_credit_amt { get; set; }
            public decimal t_titipan_os_amt { get; set; }
        }

        public static List<t_titipan> GetDataTitipan(tb_t_titipan tbl, int t_titipan_type_min)
        {
            QL_MIS_MAGNAEntities sdb = new QL_MIS_MAGNAEntities();
            List<t_titipan> stbl = new List<t_titipan>();
            var sSql = $"select * from dbo.GetDataTitipan('{tbl.t_titipan_type}', {tbl.t_titipan_id}, {tbl.t_titipan_ref_id}, {tbl.t_spr_id}, {tbl.m_cust_id}, {tbl.m_item_id}, {t_titipan_type_min})";
            stbl = sdb.Database.SqlQuery<t_titipan>(sSql).ToList();
            return stbl;
        }

        public class m_account 
        {
            public int m_account_id { get; set; }
            public string m_account_code { get; set; }
            public string m_account_desc { get; set; }
            public string m_account_dbcr { get; set; }
            public decimal m_account_debet_amt { get; set; }
            public decimal m_account_credit_amt { get; set; }
            public string m_account_note { get; set; }
        }

        public class trngldtl
        {
            public int glseq { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public decimal acctgdebet { get; set; }
            public decimal acctgcredit { get; set; }
        }

        public static List<trngldtl> ShowCOAPostingNew(string nomor, string cmpcode, string tablename, int tableoid)
        {
            QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
            List<trngldtl> tbl = new List<trngldtl>();
            var sSql = $"SELECT * FROM (SELECT r_gl_seq glseq, a.m_account_code acctgcode, a.m_account_desc acctgdesc, r_gl_debet_amt acctgdebet, r_gl_credit_amt acctgcredit FROM tb_r_gl gl INNER JOIN tb_m_account a ON a.m_account_id=gl.r_gl_account_id WHERE r_gl_ref_table='{tablename}' AND r_gl_ref_id={tableoid}) tbl_posting ORDER BY glseq";
            tbl = db.Database.SqlQuery<trngldtl>(sSql).ToList();

            return tbl;
        }

        public static bool checkPagePermission(string PagePath, List<RoleDetail> dtAccess)
        {
            if (HttpContext.Current.Session["UserID"].ToString().ToLower() == "admin")
            {
                return false;
            }
            List<RoleDetail> newrole = dtAccess.FindAll(o => o.formaddress.ToUpper() == PagePath.ToUpper());
            return newrole.Count() <= 0;
        }

        public static bool checkPagePermissionForReport(string PagePath, List<RoleDetail> dtAccess)
        {
            if (HttpContext.Current.Session["UserID"].ToString().ToLower() == "admin")
            {
                return false;
            }
            List<RoleDetail> newrole = dtAccess.FindAll(o => o.formaddress.ToUpper() + "/" + o.formmenu.ToUpper() == PagePath.ToUpper());
            return newrole.Count() <= 0;
        }

        public static bool isSpecialAccess(string PagePath, List<RoleSpecial> dtAccess)
        {
            if (HttpContext.Current.Session["UserID"].ToString().ToLower() == "admin") 
                return true; 
            List<RoleSpecial> newrole = dtAccess.FindAll(o => o.formaddress.ToUpper() == PagePath.ToUpper());
            return newrole.Count() > 0;
        }

        public static DateTime GetServerTime()
        {
            QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
            return db.Database.SqlQuery<DateTime>("SELECT GETDATE() AS tanggal").FirstOrDefault();
        }

        public static DateTime GetCutOffdate()
        {
            QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
            return db.Database.SqlQuery<DateTime>("select top 1 neraca_date from tb_neraca order by neraca_date desc").FirstOrDefault();
        }

        public static string GetDateToPeriodAcctg(DateTime date)
        {
            return date.ToString("yyyyMM");
        }

        public static string Tchar(string svar)
        {
            return svar.Trim().Replace("'", "''");
        }

        public static String Left(string input, int length)
        {
            var result = "";
            if ((input.Length <= 0)) return result;
            if ((length > input.Length)) length = input.Length; 
            result = input.Substring(0, length);
            return result;
        }

        public static String Mid(string input, int start, int length)
        {
            var result = "";
            if (((input.Length <= 0) || (start >= input.Length))) return result;
            if ((start + length > input.Length)) 
                length = (input.Length - start); 
            result = input.Substring(start, length);
            return result;
        }

        public static String Right(string input, int length)
        {
            var result = "";
            if ((input.Length <= 0)) return result;
            if ((length > input.Length)) length = input.Length; 
            result = input.Substring((input.Length - length), length);
            return result;
        }

        public static string GetLastPeriod(string speriod)
        {
            string periodbefore = "";
            if (speriod != "")
            {
                if (speriod.Length == 6)
                {
                    var iVal_1 = Convert.ToInt32(Left(speriod, 2));
                    if (iVal_1 == 1)
                    {
                        var sVal_2 = (Convert.ToInt32(Left(speriod, 4)) - 1).ToString("0000");
                        periodbefore = sVal_2 + "12";
                    }
                    else
                    {
                        var sVal_3 = Left(speriod, 4);
                        var sVal_4 = (Convert.ToInt32(Right(speriod, 2)) - 1).ToString("00");
                        periodbefore = sVal_3 + sVal_4;
                    }
                }
            }
            return periodbefore;
        }

        public static string GenNumberString(int iNumber, int iDefaultCounter)
        {
            int iAdd = iNumber.ToString().Length - iDefaultCounter;
            if (iAdd > 0) 
                iDefaultCounter += iAdd; 
            string sFormat = "";
            for (int i = 1; i <= iDefaultCounter; i++) 
                sFormat += "0"; 
            return iNumber.ToString(sFormat);
        }

        public static DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static Boolean IsQtyRounded(decimal dQty, decimal dRound)
        {
            long iRes;
            string sResult = Convert.ToString(dQty / dRound);
            if (long.TryParse(sResult, out iRes))
                return true;
            else
                return false;
        }

        public static string toMoney(decimal dAmt) => string.Format("{0:#,0.00}", dAmt);

        public static Boolean isLengthAccepted(string sField, string sTable, decimal dValue, ref string sErrReply)
        {
            int iPrec = 0, iScale = 0;
            using (var conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnString"]))
            using (var cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "SELECT col.precision, col.scale FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name='" + sField + "' AND ta.name='" + sTable + "'";
                try
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            iPrec = reader.GetOrdinal("precision");
                            iScale = reader.GetOrdinal("scale");
                        }
                        reader.Close();
                    }
                }
                catch
                {
                    iPrec = 0; iScale = 0;
                    sErrReply = "- Can't check the result. Please check that the parameter has been assigned correctly!";
                    return false;
                }
                conn.Close();
            }
            if (iPrec > 0 && iScale > 0)
            {
                string sTextValue = "";
                for (var i = 0; i < (iPrec - iScale - 1); i++) 
                    sTextValue += "9"; 

                if (iScale > 0)
                {
                    sTextValue += ".";
                    for (var i = 0; i < iScale - 1; i++) 
                        sTextValue += "9"; 
                }
                if (dValue > Convert.ToDecimal(sTextValue))
                {
                    sErrReply = string.Format("{0:#,0.00}", Convert.ToDecimal(sTextValue));
                    return false;
                }
            }
            return true;
        }
        
        public static Boolean CheckDataExists(string sSql)
        {
            QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            Boolean cDataEx = false;
            int sTmp = 0;

            sTmp = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (sTmp > 0) 
                cDataEx = true; 
            return cDataEx;
        }

        public static Boolean CheckDataExists(Int64 iKey, string[] sColumnKeyName, string[] sTable) {
            QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            Boolean cDataEx = false;
            string sSql = "";
            int sTmp = 0;

            for (int i = 0; i < sTable.Length; i++)
            {
                sSql = "SELECT COUNT(-1) FROM " + sTable[i] + " WHERE " + sColumnKeyName[i] + "=" + iKey;
                sTmp = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                if(sTmp > 0)
                {
                    cDataEx = true;
                    break;
                }
            }
            return cDataEx;
        }

        public static Dictionary<string, string> ConvertListofStringToDictionaryofString(List<string> param)
        {
            var result = new Dictionary<string, string>();
            foreach (var item in param)
                result.Add(item, item);
            return result;
        }

        public static string SplitTransNo(string input, string prefix, out string draft)
        {
            var result = ""; draft = "";
            var ar_input = input.Split(';');
            for (int i = 0; i < ar_input.Length; i++) 
                if (ar_input[i].Contains(prefix))
                    result += "'" + ar_input[i] + "', ";
                else
                    draft += ar_input[i] + ", "; 
            if (!string.IsNullOrEmpty(result)) result = ClassFunction.Left(result, result.Length - 2);
            if (!string.IsNullOrEmpty(draft)) draft = ClassFunction.Left(draft, draft.Length - 2);
            return result;
        }

        public static List<dtFiles> getFileFromDir(UrlHelper Url, string fileloc, string fileflag, List<dtFiles> dtFiles)
        {
            //fileflag : "set to empty if delete able"
            var sdir = HttpContext.Current.Server.MapPath(fileloc);
            if (Directory.Exists(sdir))
            {
                foreach (var file in Directory.GetFiles(sdir))
                {
                    var dtfile = new dtFiles();
                    dtfile.dt_fileseq = dtFiles.Count() + 1;
                    var sname = file.Split('\\');
                    dtfile.dt_filename = sname[sname.Length - 1];
                    dtfile.dt_fileurl = Url.Content(fileloc + "/" + dtfile.dt_filename);
                    dtfile.dt_filepath = fileloc + "/" + dtfile.dt_filename;
                    //dtfile.fileflag = fileflag;
                    dtFiles.Add(dtfile);
                }
            }
            return dtFiles;
        }

        public static List<dtFiles> getFileFromDir(UrlHelper Url, string fileloc, string filectrl, string fileflag, List<dtFiles> dtFiles)
        {
            //fileflag : "set to empty if delete able"
            var sdir = HttpContext.Current.Server.MapPath(fileloc);
            if (Directory.Exists(sdir))
            {
                foreach (var file in Directory.GetFiles(sdir))
                {
                    var dtfile = new dtFiles();
                    dtfile.dt_fileseq = dtFiles.Count() + 1;
                    var sname = file.Split('\\');
                    dtfile.dt_filename = sname[sname.Length - 1];
                    dtfile.dt_fileurl = Url.Content(fileloc + "/" + dtfile.dt_filename);
                    dtfile.dt_filepath = fileloc + "/" + dtfile.dt_filename;
                    //dtfile.fileflag = fileflag;
                    dtfile.dt_filectrl = filectrl;
                    dtFiles.Add(dtfile);
                }
            }
            return dtFiles;
        }

        public static string moveUploadedFolder(string fromfolderpath, string tofolderpath, List<dtFiles> dtfiles)
        {
            var todir = HttpContext.Current.Server.MapPath(tofolderpath);
            if (Directory.Exists(todir))
            {
                foreach (var file in Directory.GetFiles(todir))
                {
                    var filename = Path.GetFileName(file);
                    var dtfile = dtfiles.Where(w => w.dt_filename == filename);
                    if (dtfiles.Where(w => w.dt_filename == filename).Count() <= 0) 
                        System.IO.File.Delete(file); //delete file if not in list 
                }
            }
            else //Create Upload Folder
            {
                DirectorySecurity securityRules = new DirectorySecurity();
                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                Directory.CreateDirectory(todir, securityRules);
            }

            if (!string.IsNullOrEmpty(fromfolderpath))
            {
                var fromdir = HttpContext.Current.Server.MapPath(fromfolderpath);//get All file from temp directory
                if (Directory.Exists(fromdir))
                {
                    foreach (var file in Directory.GetFiles(fromdir))
                    {
                        var filename = Path.GetFileName(file);
                        if (dtfiles.Where(w => w.dt_filename == filename).Count() > 0)
                        {
                            if (System.IO.File.Exists(Path.Combine(todir, filename)))//already exist todir delete first
                                System.IO.File.Delete(Path.Combine(todir, filename));

                            System.IO.File.Move(file, Path.Combine(todir, filename));//move from temporary folder
                        }
                        else 
                            System.IO.File.Delete(file); 
                    }
                    Directory.Delete(fromdir);
                }
            }
            return tofolderpath;
        }

        public static bool clearFilesTemp()
        {
            var sdir = HttpContext.Current.Server.MapPath("~/UploadedFiles/FileTemps");
            if (Directory.Exists(sdir))
            {
                foreach (var file in Directory.GetFiles(sdir))
                {
                    var updtime = File.GetLastWriteTime(file);
                    if (updtime.Date < DateTime.Now.Date) 
                        File.Delete(file); 
                }
                foreach (var dir in Directory.GetDirectories(sdir))
                {
                    var updtime = File.GetLastWriteTime(dir);
                    if (updtime.Date < DateTime.Now.Date) 
                        Directory.Delete(dir, true); 
                }
            }
            sdir = HttpContext.Current.Server.MapPath("~/Images/ImagesTemps");
            if (Directory.Exists(sdir))
            {
                foreach (var file in Directory.GetFiles(sdir))
                {
                    var updtime = File.GetLastWriteTime(file);
                    if (updtime.Date < DateTime.Now.Date) 
                        File.Delete(file); 
                }
                foreach (var dir in Directory.GetDirectories(sdir))
                {
                    var updtime = File.GetLastWriteTime(dir);
                    if (updtime.Date < DateTime.Now.Date) 
                        Directory.Delete(dir, true); 
                }
            }
            return true;
        }

        public static string moveUploadedFile(string fromfilepath, string tofolderpath, string renamefile = null)//Move from file path
        {
            if (string.IsNullOrEmpty(fromfilepath))
                return "";
            string tofilepath = "";
            var sdir = HttpContext.Current.Server.MapPath(tofolderpath);
            if (!Directory.Exists(sdir))
            {
                DirectorySecurity securityRules = new DirectorySecurity();
                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                Directory.CreateDirectory(sdir, securityRules);
            }

            var filepath = HttpContext.Current.Server.MapPath(fromfilepath);
            if (System.IO.File.Exists(filepath))
            {
                if (string.IsNullOrEmpty(renamefile)) renamefile = Path.GetFileName(filepath);
                else renamefile = renamefile + Path.GetExtension(filepath);

                var filepathnew = Path.Combine(sdir, renamefile);

                if (filepath.ToLower() != filepathnew.ToLower())
                {
                    if (System.IO.File.Exists(filepathnew)) 
                        System.IO.File.Delete(filepathnew); 
                    System.IO.File.Move(filepath, filepathnew);
                }
                tofilepath = tofolderpath + "/" + renamefile;
            }
            return tofilepath;
        }

        public static string copyUploadedFile(string fromfilepath, string tofolderpath, string renamefile = null)//Copy from file path
        {
            if (string.IsNullOrEmpty(fromfilepath))
                return "";
            string tofilepath = "";
            var sdir = HttpContext.Current.Server.MapPath(tofolderpath);
            if (!Directory.Exists(sdir))
            {
                DirectorySecurity securityRules = new DirectorySecurity();
                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                Directory.CreateDirectory(sdir, securityRules);
            }

            var filepath = HttpContext.Current.Server.MapPath(fromfilepath);
            if (System.IO.File.Exists(filepath))
            {
                if (string.IsNullOrEmpty(renamefile)) renamefile = Path.GetFileName(filepath);
                else renamefile = renamefile + Path.GetExtension(filepath);

                var filepathnew = Path.Combine(sdir, renamefile);

                if (filepath.ToLower() != filepathnew.ToLower())
                {
                    if (System.IO.File.Exists(filepathnew)) 
                        System.IO.File.Delete(filepathnew); 
                    System.IO.File.Copy(filepath, filepathnew);
                }
                tofilepath = tofolderpath + "/" + renamefile;
            }
            return tofilepath;
        }

        public static bool deleteUploadedFile(string filepath)
        {
            if (!string.IsNullOrEmpty(filepath))
            {
                var filedir = HttpContext.Current.Server.MapPath(filepath);
                if (System.IO.File.Exists(filedir))
                    System.IO.File.Delete(filedir);
            }
            return true;
        }

        public static Byte[] getImage(string sPath)
        {
            Byte[] arrImage = null;
            if (File.Exists(sPath))
            {
                Image img = Image.FromFile(sPath);
                MemoryStream ms = new MemoryStream();
                img.Save(ms, img.RawFormat);
                arrImage = ms.GetBuffer();
                ms.Close();
                ms.Dispose();
            }
            return arrImage;
        }

        public static bool SplitFilterLikeIn(string field, string value, out string filterlike, out string filterin)
        {
            filterlike = ""; filterin = "";
            if (!string.IsNullOrEmpty(value))
            {
                var data = value.Split(';');
                for (int i = 0; i < data.Length; i++)
                {
                    if (data[i] != "")
                    {
                        if (data[i].Contains("%")) { filterlike += field + " LIKE '" + data[i] + "' OR "; }
                        else { filterin += "'" + data[i] + "', "; }
                    }
                }                    
                if (filterlike != "") { filterlike = "(" + Left(filterlike, filterlike.Length - 4) + ")"; }                    
                if (filterin != "") { filterin = "(" + field + " IN (" + Left(filterin, filterin.Length - 2) + "))"; }
                    
            }
            return (filterlike != "" || filterin != "");
        }

        public static object MappingTable(object tbl_dest, object tbl_source)
        {
            foreach (var prop in tbl_dest.GetType().GetProperties())
            {
                var prop_source = tbl_source.GetType().GetProperties().Where(g => g.Name == prop.Name).FirstOrDefault();
                if (prop_source != null)
                {
                    var fieldvalue = prop_source.GetValue(tbl_source, null);
                    prop.SetValue(tbl_dest, fieldvalue);
                }
            }
            return tbl_dest;
        }
        
        public static int GetLastTransNo(string prefix, string field_no, string tablename, int m_company_id, string transdate = "", string nomor = "", QL_MIS_MAGNAEntities db = null)
        {
            var sSql = $"SELECT ISNULL(MAX(CAST(RIGHT({field_no}, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM {tablename} WHERE {field_no} LIKE '{nomor}%'";
            if (db == null) db = new QL_MIS_MAGNAEntities();
            var result = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (result == 0) result = 1;
            return result;
        }

        public static string GenerateTransNo(string prefix, string field_no, string tablename, int m_company_id, string transdate = "", QL_MIS_MAGNAEntities db = null)
        {
            var sNo =  $"{prefix}-{(string.IsNullOrEmpty(transdate) ? GetServerTime().ToString("yyyy.MM") : DateTime.Parse(transdate).ToString("yyyy.MM"))}-";
            var result = sNo + GenNumberString(GetLastTransNo(prefix, field_no, tablename, m_company_id, transdate: transdate, nomor: sNo, db: db), 4);
            return result;
        }

        public static string GenerateCodeMaster(string prefix, string field_no, string tablename)
        {
            var sSql = $"SELECT ISNULL(MAX(CAST(RIGHT({field_no}, 3) AS INTEGER)) + 1, 1) AS IDNEW FROM {tablename} WHERE {field_no} LIKE '{prefix}%'";
            var result = new QL_MIS_MAGNAEntities().Database.SqlQuery<int>(sSql).FirstOrDefault();
            var result_no= prefix + GenNumberString(result, 3);
            return result_no;
        }

        public static string GetQueryString(int t_spr_awal_id, int m_cust_id, int m_item_id, string t_kbm_type_bayar = "", int m_promo_id = 0, string sSql = "")
        {
            sSql = $@"Select r_hutang_ref_id t_refund_id, h.r_hutang_ref_no t_refund_no, h.r_hutang_ref_date t_refund_date
            , p.m_proyek_code m_proyek_name, c.m_cust_name, i.m_item_name, h.r_hutang_amt t_refund_amt
            , h.r_hutang_amt - Isnull(r.r_hutang_pay_amt, 0.0) amt_sisa, 'PAYMENT' r_piutang_ref_type
            , h.r_hutang_account_id, h.m_item_old_id from tb_r_hutang h Inner Join tb_m_cust c ON c.m_cust_id=h.m_cust_id 
            Inner Join tb_m_proyek p ON p.m_proyek_id=h.m_proyek_id Inner Join tb_m_item i ON i.m_item_id=h.m_item_id 
            Outer Apply ( Select Isnull(SUM(r.r_hutang_pay_amt), 0.0) r_hutang_pay_amt from tb_r_hutang r Where r.r_hutang_type='PAYMENT' 
            AND r.r_hutang_ref_table=h.r_hutang_ref_table AND r.r_hutang_ref_id=h.r_hutang_ref_id ) r 
            Where h.r_hutang_type='Refund' AND r_hutang_ref_table='tb_t_spr_new' AND h.r_hutang_amt - Isnull(r.r_hutang_pay_amt, 0.0) > 0.0 
            AND h.m_cust_id={m_cust_id} AND i.m_item_id={m_item_id} AND h.t_spr_awal_id={t_spr_awal_id} Order By h.r_hutang_ref_no";

            if (t_kbm_type_bayar == "REFUND")
            {
                sSql = $@"SELECT r.r_hutang_ref_id t_refund_id, r.r_hutang_ref_no t_refund_no, r_hutang_ref_date t_refund_date
                , p.m_proyek_code m_proyek_name, c.m_cust_name, i.m_item_name, r.r_hutang_account_id t_kbk_d_account_id
                , d.m_account_code, d.m_account_desc m_account_name, Isnull(r.r_hutang_amt, 0.0) t_refund_amt, 'PAYMENT' r_piutang_ref_type
                , r.r_hutang_amt - Isnull(Isnull(x.r_hutang_pay_amt, 0.0), 0.0) amt_sisa, Isnull(x.r_hutang_pay_amt, 0.0) r_hutang_pay_amt
                , r.m_item_old_id, 0.0 t_kbk_d_netto, r.r_hutang_note t_kbk_d_note, r.r_hutang_ref_table t_kbk_ref_table 
                FROM tb_r_hutang r 
                INNER JOIN tb_m_account d ON d.m_account_id = r.r_hutang_account_id 
                Inner Join tb_m_proyek p ON p.m_proyek_id=r.m_proyek_id 
                Inner Join tb_m_cust c ON c.m_cust_id=r.m_cust_id 
                Inner Join tb_m_item i ON i.m_item_id=r.m_item_id 
                Inner Join tb_t_refund xx ON xx.t_refund_id=r.r_hutang_ref_id 
                OUTER APPLY ( 
	                SELECT SUM(r_hutang_pay_amt) r_hutang_pay_amt FROM tb_r_hutang x WHERE x.r_hutang_type = 'Payment' 
	                AND x.r_hutang_ref_id = r.r_hutang_ref_id AND r.r_hutang_ref_table = x.r_hutang_ref_table 
	                AND x.m_item_id = r.m_item_id AND r.m_cust_id = x.m_cust_id AND r.m_proyek_id = x.m_proyek_id 
                ) x WHERE r.m_item_id !=0 AND r.t_spr_awal_id != 0
                AND r.r_hutang_type = 'Refund' AND r.r_hutang_ref_table='tb_t_refund' 
                AND r.r_hutang_amt - Isnull(x.r_hutang_pay_amt, 0.0) > 0 AND xx.t_refund_type_bayar='Tunai'
                AND xx.t_refund_flag=1 ORDER BY r_hutang_ref_id";
            }
            else if (t_kbm_type_bayar == "PROMO") 
            {
                sSql = $@"SELECT r.r_promo_ref_id AS t_refund_id, r.m_promo_id, ri.m_item_name AS t_refund_no
                , r_promo_ref_date AS t_refund_date, rp.m_proyek_name, rc.m_cust_name, ri.m_item_name
                , r.r_promo_account_id t_kbk_d_account_id, ra.m_account_code, ra.m_account_desc AS m_account_name
                , r.r_promo_amt AS t_refund_amt, 'Payment' AS r_piutang_ref_type, r.r_promo_amt - Isnull(x.r_promo_pay_amt, 0.0) AS amt_sisa
                , Isnull(x.r_promo_pay_amt, 0.0) AS r_hutang_pay_amt, 0 AS m_item_old_id, 0.0 AS t_kbk_d_netto, r.r_promo_note AS t_kbk_d_note
                , r.r_promo_ref_table AS t_kbk_ref_table 
                From tb_r_promo r 
                Inner Join tb_t_promo rx ON rx.t_promo_id=r.t_promo_id
                Inner Join tb_m_cust rc ON rc.m_cust_id=r.m_cust_id 
                Inner Join tb_m_item ri ON ri.m_item_id=r.m_promo_id 
                Inner Join tb_m_proyek rp ON rp.m_proyek_id=r.m_proyek_id 
                Inner Join tb_m_account ra ON ra.m_account_id=r.r_promo_account_id 
                Outer Apply (
	                select SUM(r_promo_pay_amt) r_promo_pay_amt from tb_r_promo o 
	                Where o.r_promo_type='Payment' And o.r_promo_ref_id=r.r_promo_ref_id AND o.m_cust_id=r.m_cust_id 
	                AND o.m_promo_id=r.m_promo_id AND o.t_promo_id=r.t_promo_id AND o.t_spr_id=r.t_spr_id 
                ) x Where rx.t_promo_type_bayar='Tunai' AND r.r_promo_type='Promo' ANd r.r_promo_status='Post'
                AND r.r_promo_ref_table='tb_t_promo' AND r.m_promo_id={m_promo_id} Order By r.m_promo_id";
            }
            return sSql;
        }

        private static Random _rdm = new Random();

        public static string GenerateBarCode()
        {
            var barcode = GetServerTime().ToString("yyMMdd") + GenerateRandomNo(4);
            return barcode;
        }

        public static string GenerateRandomNo(int n)
        {
            var no = "";
            for (var i = 0; i < n; i++) { no += _rdm.Next(0, 10); }
            return no;
        }

        public static List<Dictionary<string, object>> toObject(DataTable tbl)
        {
            List<Dictionary<string, object>> rowsdtl = new List<Dictionary<string, object>>();
            if (tbl.Rows.Count > 0)
            {
                foreach (DataRow dr in tbl.Rows)
                {
                    var row = new Dictionary<string, object>();
                    foreach (DataColumn col in tbl.Columns) 
                        row.Add(col.ColumnName, (dr[col] == DBNull.Value) ? "" : dr[col]); 
                    rowsdtl.Add(row);
                }
            }
            return rowsdtl;
        }

        public static IList<Dictionary<string, object>> getListDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount, string select_query, string fixed_filter = "", string default_order = "")
        {
            QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
            var take = model.length;
            var skip = model.start;

            var and_filter = "";
            if ((model.search != null) ? !string.IsNullOrEmpty(model.search.value) : false)//search in all column
            {
                var col_tosearch = model.columns.Where(w => w.searchable & w.data != "sAddFilter").Select(s => s.data).ToList();
                foreach (var s in model.search.value.Trim().Split(' '))
                {
                    var or_filter = "";
                    foreach (var a in col_tosearch)
                    {
                        if (or_filter != "") or_filter += " OR ";
                        or_filter += a + " LIKE '%" + s.tChar() + "%'";
                    }
                    if (or_filter != "") 
                        and_filter += " AND (" + or_filter + ")"; 
                }
            }
            foreach (var obj in model.columns.Where(w => w.searchable & w.search != null).ToList())//search by column
            {
                if (!string.IsNullOrEmpty(obj.search.value))
                {
                    if (obj.search.value.ToLower().StartsWith("between") | obj.search.value.ToLower().StartsWith("in(") | obj.search.value.StartsWith("=") | obj.search.value.StartsWith("<>")) 
                        and_filter += " AND " + obj.data + " " + obj.search.value; 
                    else if (obj.data == "sAddFilter") 
                        and_filter += obj.search.value; 
                    else
                    {
                        var n_filter = "";
                        foreach (var s in obj.search.value.Split(' '))
                        {
                            if (n_filter != "") n_filter += " OR ";
                            n_filter += obj.data + " LIKE '" + obj.search.value + "'";
                        }
                        if (n_filter != "") 
                            and_filter += " AND (" + n_filter + ")"; 
                    }
                }
            }

            string orderBy = "";
            if (model.order != null)
            {
                foreach (var o in model.order)
                {
                    if (!string.IsNullOrEmpty(orderBy)) orderBy += ", ";
                    orderBy += model.columns[o.column].data + " " + o.dir.ToLower();
                }
            }

            // search the dbase taking into consideration table sorting and paging
            //var result = GetDataFromDbase(and_filter, take, skip, orderBy, out filteredResultsCount, out totalResultsCount);
            if (String.IsNullOrEmpty(orderBy))
            {
                if (string.IsNullOrEmpty(default_order))
                    default_order = model.columns.Select(s => s.data).First() + " DESC";
                orderBy = default_order;
            }

            var result_query = select_query + " WHERE 1=1 " + and_filter + fixed_filter + " ORDER BY " + orderBy + " OFFSET " + skip + " ROWS FETCH NEXT " + take + " ROWS ONLY";
            var result = toObject(new ClassConnection().GetDataTable(result_query, "result")); // SQL server 2016

            var rs_count = new ClassConnection().GetDataTable(select_query + " WHERE 1=1 " + and_filter + fixed_filter + " ", "result2");
            var rs_count2 = new ClassConnection().GetDataTable(select_query + " WHERE 1=1 " + fixed_filter + " ", "result3");
            // now just get the count of items (without the skip and take) - eg how many could be returned with filtering
            filteredResultsCount = rs_count.Rows.Count;
            totalResultsCount = rs_count2.Rows.Count;
            if (result == null) 
                return new List<Dictionary<string, object>>(); 
            return result;
        }

        public static decimal GetTaxValue(DateTime trn_date)
        {
            var result = 10M;
            if (trn_date.ToString("MM/dd/yyyy").ToDateTime() >= new DateTime(2022, 4, 1)) result = 11M;
            return result;
        }

        public static decimal Ceiling(decimal num, decimal significance)
        {
            return Math.Ceiling(num / significance) * significance;
        }

        public static decimal Floor(decimal num, decimal significance)
        {
            return Math.Floor(num / significance) * significance;
        }

        public static tb_post_trans InsertPostTrans(int m_company_id, Guid post_trans_uid, string post_trans_ref_table, int post_trans_ref_id, string created_by, DateTime created_at)
        {
            var new_data = new tb_post_trans()
            {
                post_trans_uid = post_trans_uid,
                m_company_id = m_company_id,
                post_trans_ref_table = post_trans_ref_table,
                post_trans_ref_id = post_trans_ref_id, 
                created_by = created_by,
                created_at = created_at
            };
            return new_data;
        }

        public static tb_t_spr Insert_t_spr(Guid t_spr_guid, int m_company_id, int m_cust_id, int m_proyek_id, int m_item_id, int m_item_old_id, string sNo, int spr_id, decimal t_spr_price_new, decimal t_utj_amt2, decimal t_dp_amt2, decimal t_dp_amt_old, decimal t_kt_amt2, decimal t_kt_amt_old, decimal t_piutang_amt2, decimal t_piutang_amt_old, int m_marketing_id, string m_marketing_name, string created_by, DateTime created_at, DateTime t_spr_date, string t_spr_type_trans = "")
        {
            var new_data = new tb_t_spr()
            {
                t_spr_uid = t_spr_guid,
                m_company_id = m_company_id,
                t_spr_no = sNo,
                t_spr_date = t_spr_date,
                m_cust_id = m_cust_id,
                m_proyek_id = m_proyek_id,
                m_item_id = m_item_id,
                m_item_old_id = m_item_old_id,
                t_spr_type_trans = t_spr_type_trans,
                t_spr_reff_id = spr_id,
                t_spr_awal_id = spr_id,
                t_spr_promo_id = 0,
                t_refund_id = 0,
                t_spr_price = t_spr_price_new,
                t_utj_type = "-",
                t_utj_id = 0,
                t_utj_amt = 0m,
                t_utj_amt2 = t_utj_amt2,
                t_dp_type = "Tidak",
                t_dp_type_bayar = "-",
                t_dp_total_angsuran = 0,
                t_dp_account_id = 0,
                t_dp_amt = 0m,
                t_dp_amt2 = 0m,
                t_dp_amt_old = t_dp_amt_old,
                t_dp_status = "-",
                t_kt_type = "Tidak",
                t_kt_type_bayar = "-",
                t_kt_total_angsuran = 0,
                t_kt_account_id = 0,
                t_kt_amt = 0m,
                t_kt_amt2 = 0m,
                t_kt_amt_old = t_kt_amt_old,
                t_kt_status = "-",
                t_shm_type = "Tidak",
                t_shm_type_bayar = "-",
                t_shm_total_angsuran = 0,
                t_shm_account_id = 0,
                t_shm_amt = 0m,
                t_shm_amt2 = 0m,
                t_shm_status = "-",
                t_piutang_type_bayar = "Tunai",
                t_piutang_total_angsuran = 1,
                t_piutang_amt = t_spr_price_new,
                t_piutang_amt2 = t_piutang_amt2,
                t_piutang_amt_old = t_piutang_amt_old,
                t_piutang_status = "Complete",
                t_spr_promo_dp_type = "-",
                t_spr_promo_dp_persen = 0m,
                t_spr_promo_dp_note = "-",
                t_spr_total_amt = t_spr_price_new,
                m_marketing_id = m_marketing_id,
                m_marketing_name = m_marketing_name,
                t_spr_status = "Post",
                t_spr_promo_status = "-",
                t_spr_note = "Ganti Unit/Harga Lebih Bayar (Lunas)",
                created_by = created_by,
                created_at = (DateTime)created_at,
                updated_by = created_by,
                updated_at = GetServerTime(),
                posted_by = created_by,
                posted_at = GetServerTime(),

            };
            return new_data;
        }

        public static tb_t_refund Insert_t_refund(Guid t_refund_uid, int m_company_id, string t_refund_no, DateTime t_refund_date, int m_cust_id, int m_proyek_id, int m_item_id, int m_item_new_id, int t_spr_awal_id, string t_refund_type, string t_refund_ref_table, int t_refund_ref_id, decimal t_refund_price_new, decimal t_spr_price, decimal t_refund_utj_amt, decimal t_refund_pay_amt, decimal t_refund_pay_amt_dp, decimal t_refund_disc_amt_dp, decimal t_refund_sisa_amt_dp, decimal t_refund_pay_amt_kpr, decimal t_refund_disc_amt_kpr, decimal t_refund_sisa_amt_kpr, decimal t_refund_pay_amt_kt, decimal t_refund_disc_amt_kt, decimal t_refund_sisa_amt_kt, decimal t_refund_total_amt, decimal t_refund_disc_amt, int t_refund_account_id, string t_refund_note, string created_by, DateTime created_at, string t_refund_status = "")
        {
            var new_data = new tb_t_refund();
            new_data.t_refund_uid = t_refund_uid;
            new_data.m_company_id = m_company_id;
            new_data.t_refund_no = t_refund_no;
            new_data.t_refund_date = t_refund_date;
            new_data.m_cust_id = m_cust_id;
            new_data.m_proyek_id = m_proyek_id;
            new_data.m_item_id = m_item_id;
            new_data.m_item_new_id = m_item_new_id;
            new_data.t_spr_awal_id = t_spr_awal_id;
            new_data.t_refund_type = t_refund_type;
            new_data.t_refund_total_angsuran = 0;
            new_data.t_refund_ref_table = t_refund_ref_table;
            new_data.t_refund_ref_id = t_refund_ref_id;
            new_data.t_refund_bank_name = "";
            new_data.t_refund_norek = "";
            new_data.t_refund_atas_nama = "";
            new_data.t_refund_price = t_spr_price;
            new_data.t_refund_price_new = t_refund_price_new;
            new_data.t_refund_utj_amt = t_refund_utj_amt;
            new_data.t_refund_pay_amt = t_refund_pay_amt;
            new_data.t_refund_pay_amt_dp = t_refund_pay_amt_dp;
            new_data.t_refund_disc_amt_dp = t_refund_disc_amt_dp;
            new_data.t_refund_sisa_amt_dp = t_refund_sisa_amt_dp;
            new_data.t_refund_pay_amt_kpr = t_refund_pay_amt_kpr;
            new_data.t_refund_disc_amt_kpr = t_refund_disc_amt_kpr;
            new_data.t_refund_sisa_amt_kpr = t_refund_sisa_amt_kpr;
            new_data.t_refund_pay_amt_kt = t_refund_pay_amt_kt;
            new_data.t_refund_disc_amt_kt = t_refund_disc_amt_kt;
            new_data.t_refund_sisa_amt_kt = t_refund_sisa_amt_kt;
            new_data.t_refund_pay_amt_shm = 0m;
            new_data.t_refund_disc_amt_shm = 0m;
            new_data.t_refund_sisa_amt_shm = 0m;
            new_data.t_refund_pay_ppn_amt = 0m;
            new_data.t_refund_pay_bphtb_amt = 0m;
            new_data.t_refund_pay_balik_nama_amt = 0m;
            new_data.t_refund_pay_peningkatan_amt = 0m;
            new_data.t_refund_disc_amt = t_refund_disc_amt;
            new_data.t_refund_cost_amt = 0m;
            new_data.t_refund_cost_other_amt = 0m;
            new_data.t_refund_cost_other1_amt = 0m;
            new_data.t_refund_total_amt = t_refund_total_amt;
            new_data.t_refund_type_bayar = "Tunai";
            new_data.t_refund_account_id = t_refund_account_id;
            new_data.t_refund_cost_account_id = 0;
            new_data.t_refund_cost1_account_id = 0;
            new_data.t_refund_status = t_refund_status;
            new_data.t_refund_note = t_refund_note;
            new_data.created_by = created_by;
            new_data.created_at = created_at;
            new_data.updated_by = created_by;
            new_data.updated_at = created_at;
            new_data.posted_by = created_by;
            new_data.posted_at = created_at;
            return new_data;
        }

        public static tb_r_piutang Insert_R_Piutang(int m_company_id, int m_cust_id, int m_proyek_id, int m_item_id, string r_piutang_type, string r_piutang_ref_table, int r_piutang_ref_id, string r_piutang_ref_no, DateTime r_piutang_ref_date, DateTime r_piutang_ref_due_date, DateTime r_piutang_pay_ref_date, int r_piutang_account_id, decimal r_piutang_amt, decimal r_piutang_pay_amt, string r_piutang_note, string created_by, DateTime created_at, string r_piutang_pay_ref_table, int r_piutang_pay_ref_id, string r_piutang_pay_ref_no, int r_piutang_pay_account_id, decimal r_piutang_disc_amt = 0, int t_spr_id = 0, int t_spr_awal_id = 0, string r_piutang_ref_type = "", string r_piutang_trans_type = "", int r_piutang_trans_id = 0)
        {
            var new_data = new tb_r_piutang()
            {
                m_company_id = m_company_id,
                m_cust_id = m_cust_id,
                m_proyek_id = m_proyek_id,
                m_item_id = m_item_id,
                t_spr_id = t_spr_id,
                t_spr_awal_id = t_spr_awal_id,
                r_piutang_trans_id = r_piutang_trans_id,
                r_piutang_type = r_piutang_type,
                r_piutang_ref_type= r_piutang_ref_type,
                r_piutang_trans_type= r_piutang_trans_type,
                r_piutang_ref_table = r_piutang_ref_table,
                r_piutang_ref_id = r_piutang_ref_id,
                r_piutang_ref_no = r_piutang_ref_no,
                r_piutang_ref_date = (DateTime)r_piutang_ref_date,
                r_piutang_ref_due_date = (DateTime)r_piutang_ref_due_date,
                r_piutang_pay_ref_date = (DateTime)r_piutang_pay_ref_date,
                r_piutang_account_id = r_piutang_account_id,
                r_piutang_amt = r_piutang_amt,
                r_piutang_pay_ref_table = r_piutang_pay_ref_table,
                r_piutang_pay_ref_id = r_piutang_pay_ref_id,
                r_piutang_pay_ref_no = r_piutang_pay_ref_no,
                r_piutang_pay_account_id = r_piutang_pay_account_id,
                r_piutang_pay_amt = r_piutang_pay_amt,
                r_piutang_disc_amt = r_piutang_disc_amt,
                r_piutang_status = "Post",
                r_piutang_note = r_piutang_note,
                created_by = created_by,
                created_at = (DateTime)created_at
            };
            return new_data;
        }

        public static tb_r_promo Insert_R_Promo(int m_company_id, int t_promo_id, int m_cust_id, int m_proyek_id, int m_item_id, int t_spr_id, int t_spr_awal_id, int m_promo_id, string r_promo_type, string r_promo_ref_table, int r_promo_ref_id, string r_promo_ref_no, DateTime r_promo_ref_date, int r_promo_account_id, Decimal r_promo_amt, DateTime r_promo_pay_ref_date, string r_promo_status, string created_by, DateTime created_at, string r_promo_note = "", int r_promo_pay_account_id = 0, string r_promo_pay_ref_table = "", int r_promo_pay_ref_id = 0, string r_promo_pay_ref_no = "", Decimal r_promo_pay_amt = 0m)
        {
            var new_data = new tb_r_promo()
            {
                m_company_id = m_company_id,
                t_promo_id = t_promo_id,
                m_cust_id = m_cust_id,
                m_proyek_id = m_proyek_id,
                m_item_id = m_item_id,
                t_spr_id = t_spr_id,
                t_spr_awal_id = t_spr_awal_id,
                m_promo_id = m_promo_id,
                r_promo_type = r_promo_type,
                r_promo_ref_table = r_promo_ref_table,
                r_promo_ref_id = r_promo_ref_id,
                r_promo_ref_no = r_promo_ref_no,
                r_promo_ref_date = r_promo_ref_date,
                r_promo_account_id = r_promo_account_id,
                r_promo_amt = r_promo_amt,
                r_promo_pay_ref_table = r_promo_pay_ref_table,
                r_promo_pay_ref_id = r_promo_pay_ref_id,
                r_promo_pay_ref_no = r_promo_pay_ref_no,
                r_promo_pay_ref_date = r_promo_pay_ref_date,
                r_promo_pay_account_id = r_promo_pay_account_id,
                r_promo_pay_amt = r_promo_pay_amt,
                r_promo_status = r_promo_status,
                r_promo_note = r_promo_note,
                created_by = created_by,
                created_at = created_at,
            };
            return new_data;
        }

        public static tb_r_hutang Insert_R_Hutang(int m_company_id, int m_cust_id, int m_proyek_id, int m_item_id, string r_hutang_type, string r_hutang_ref_table, int r_hutang_ref_id, string r_hutang_ref_no, DateTime r_hutang_ref_date, DateTime r_hutang_ref_due_date, DateTime r_hutang_pay_ref_date, int r_hutang_account_id, decimal r_hutang_amt, decimal r_hutang_pay_amt, string r_hutang_note, string created_by, DateTime created_at, string r_hutang_pay_ref_table, int r_hutang_pay_ref_id, string r_hutang_pay_ref_no, int r_hutang_pay_account_id, int t_spr_id = 0, int t_spr_awal_id = 0, int m_item_old_id = 0)
        {
            var new_data = new tb_r_hutang()
            {
                m_company_id = m_company_id,
                m_cust_id = m_cust_id,
                m_proyek_id = m_proyek_id,
                m_item_id = m_item_id,
                m_item_old_id = m_item_old_id,
                t_spr_id = t_spr_id,
                t_spr_awal_id = t_spr_awal_id,
                r_hutang_type = r_hutang_type,
                r_hutang_ref_table = r_hutang_ref_table,
                r_hutang_ref_id = r_hutang_ref_id,
                r_hutang_ref_no = r_hutang_ref_no,
                r_hutang_ref_date = (DateTime)r_hutang_ref_date,
                r_hutang_ref_due_date = (DateTime)r_hutang_ref_due_date,
                r_hutang_pay_ref_date = (DateTime)r_hutang_pay_ref_date,
                r_hutang_account_id = r_hutang_account_id,
                r_hutang_amt = r_hutang_amt,
                r_hutang_pay_ref_table = r_hutang_pay_ref_table,
                r_hutang_pay_ref_id = r_hutang_pay_ref_id,
                r_hutang_pay_ref_no = r_hutang_pay_ref_no,
                r_hutang_pay_account_id = r_hutang_pay_account_id,
                r_hutang_pay_amt = r_hutang_pay_amt,
                r_hutang_note = r_hutang_note,
                created_by = created_by,
                created_at = (DateTime)created_at
            };
            return new_data;
        }

        public static tb_r_kb Insert_R_KB(int m_company_id, int m_cust_id, int m_proyek_id, int m_item_id, string r_kb_ref_table, int r_kb_ref_id, string r_kb_type, string r_kb_no, DateTime r_kb_date, int r_kb_account_id, int r_kb_d_account_id, decimal r_kb_debet_amt, decimal r_kb_credit_amt, string r_kb_note, string created_by, DateTime created_at, int t_spr_id = 0, int t_spr_awal_id = 0, int r_kb_h_ref_id = 0, string r_kb_ref_type = "")
        {
            var new_data = new tb_r_kb()
            {
                m_company_id = m_company_id,
                m_cust_id = m_cust_id,
                m_proyek_id = m_proyek_id,
                m_item_id = m_item_id,
                t_spr_id = t_spr_id,
                t_spr_awal_id = t_spr_awal_id,
                r_kb_ref_table = r_kb_ref_table,
                r_kb_ref_id = r_kb_ref_id,
                r_kb_h_ref_id= r_kb_h_ref_id,
                r_kb_no = r_kb_no,
                r_kb_type = r_kb_type,
                r_kb_ref_type= r_kb_ref_type,
                r_kb_date = r_kb_date,
                r_kb_account_id = r_kb_account_id,
                r_kb_d_account_id = r_kb_d_account_id,
                r_kb_debet_amt = r_kb_debet_amt,
                r_kb_credit_amt = r_kb_credit_amt,
                r_kb_note = r_kb_note,
                created_by = created_by,
                created_at = created_at
            };
            return new_data;
        }

        public static tb_r_gl Insert_R_GL(int m_company_id, int m_proyek_id, int m_item_id, int r_gl_seq, string r_gl_ref_table, int r_gl_ref_id, string r_gl_ref_no, DateTime r_gl_date, int r_gl_account_id, string r_gl_type, decimal r_gl_amt, string r_gl_note, string created_by, DateTime created_at, int m_cust_id = 0 )
        {
            var new_data = new tb_r_gl()
            {
                m_company_id = m_company_id,
                m_proyek_id = m_proyek_id,
                m_item_id = m_item_id,
                m_cust_id = m_cust_id,
                r_gl_seq = r_gl_seq,
                r_gl_ref_table = r_gl_ref_table,
                r_gl_ref_id = r_gl_ref_id,
                r_gl_ref_no = r_gl_ref_no,
                r_gl_date = r_gl_date,
                r_gl_account_id = r_gl_account_id,
                r_gl_debet_amt = (r_gl_type == "D" ? r_gl_amt : 0),
                r_gl_credit_amt = (r_gl_type == "C" ? r_gl_amt : 0),
                r_gl_note = r_gl_note,
                created_by = created_by,
                created_at = created_at
            };
            return new_data;
        }

        public static List<ReportModels.ModelAccount> GetAccountDataToList(QL_MIS_MAGNAEntities db, string action, string all_text = "", int all_value = 0, List<string> sVar = null)
        {
            var result = new List<ReportModels.ModelAccount>();
            var account = (from h in db.tb_m_interface_h join d in db.tb_m_interface_d on h.m_interface_h_id equals d.m_interface_h_id select new { d.m_interface_d_id, d.m_interface_d_code, d.m_interface_d_name, h.m_interface_name, h.m_interface_status }).AsQueryable();
            if (action == "New Data") account = account.Where(d => d.m_interface_status == "ACTIVE");
            if (sVar != null) account = account.Where(d => sVar.Contains(d.m_interface_name));
            account = account.OrderBy(d => d.m_interface_d_name);
            if (!string.IsNullOrEmpty(all_text)) result.Add(new ReportModels.ModelAccount { m_account_id = all_value, m_account_code = all_text, m_account_name = all_text });
            foreach (var item in account)
                result.Add(new ReportModels.ModelAccount { m_account_id = item.m_interface_d_id, m_account_code = item.m_interface_d_code, m_account_name = item.m_interface_d_name });
            return result;
        }

        public static List<SelectListItem> GetDefaultDDL(string text, List<SelectListItem> data) => new List<SelectListItem> { new SelectListItem { Value = "0", Text = $"-- Pilih {text} --" } }.Union(data).ToList();

        public static List<SelectListItem> GetTahun(int plus = 5)
        {
            var result = new List<SelectListItem>();
            int sTahun = DateTime.Now.Year - plus;
            for (int i = sTahun; i < sTahun + plus; i++) { result.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }                
            for (int i = DateTime.Now.Year; i < DateTime.Now.Year + plus; i++) { result.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
            return result;
        }

        public static List<SelectListItem> GetBulan()
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem { Text = "Januari", Value = "01" },
                new SelectListItem { Text = "Februari", Value = "02" },
                new SelectListItem { Text = "Maret", Value = "03" },
                new SelectListItem { Text = "April", Value = "04" },
                new SelectListItem { Text = "Mei", Value = "05" },
                new SelectListItem { Text = "Juni", Value = "06" },
                new SelectListItem { Text = "Juli", Value = "07" },
                new SelectListItem { Text = "Agustus", Value = "08" },
                new SelectListItem { Text = "September", Value = "09" },
                new SelectListItem { Text = "Oktober", Value = "10" },
                new SelectListItem { Text = "November", Value = "11" },
                new SelectListItem { Text = "Desember", Value = "12" },
            };
            return result;
        }

        public static JsonResult getJsResult(string sQuery)
        {
            string result = string.Empty; var tbl = toObject(new ClassConnection().GetDataTable(sQuery, "tbl"));
            return new JsonResult() { Data = new { result, tbl, sQuery, error = "" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue, };
        }
    } 

    public static class StringExtensions
    {
        public static string Right(this string str, int length)
        {
            if (length < 0 | string.IsNullOrEmpty(str)) 
                str = ""; 
            else if (!string.IsNullOrEmpty(str) & str.Length > length) 
                str = str.Substring(str.Length - length, length); 
            return str;
        }
        public static string Left(this string str, int length)
        {
            if (length < 0 | string.IsNullOrEmpty(str)) 
                str = ""; 
            else if (!string.IsNullOrEmpty(str) & str.Length > length) 
                str = str.Substring(0, length); 
            return str;
        }
        public static decimal tDecimal(this string str)
        {
            decimal num = 0;
            decimal.TryParse(str.Replace(",", ""), out num);
            return num;
        }
        public static int tInt(this string str)
        {
            int num = 0;
            int.TryParse(str.Replace(",", ""), out num);
            return num;
        }
        public static string tChar(this string str)
        {
            return (str ?? "").Replace("'", "''");
        }
         
    }
     
}