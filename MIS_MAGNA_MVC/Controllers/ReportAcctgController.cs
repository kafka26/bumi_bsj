﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class ReportAcctgController : Controller
    {
        // GET: ReportAcctg
        private QL_MIS_MAGNAEntities db;
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        public ReportAcctgController() { db = new QL_MIS_MAGNAEntities(); db.Database.CommandTimeout = 0; }

        public class sFilter
        {
            public string ddltype { get; set; }
            public int ddlproyek { get; set; }
            public string periodstart { get; set; }
            public string periodend { get; set; }
            public string filterakun { get; set; }
            public string filterno { get; set; }
        }

        #region Laporan Accounting
        public ActionResult ReportAcctg()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/ReportAcctg", (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var proyek = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "ALL" } }.Union(db.tb_m_proyek.Where(x => x.m_proyek_flag == "OPEN").Select(x => new SelectListItem() { Value = x.m_proyek_id.ToString(), Text = x.m_proyek_name })).ToList();
            ViewBag.ddlproyek = new SelectList(proyek, "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult GetModalData(sFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                if (modaltype.ToUpper() == "AKUN")
                    sSql = $"select 0 seq, m_account_code Kode, m_account_desc from tb_m_account Where m_account_id IN (select r_gl_account_id from tb_r_gl) Order By m_account_code";
                else if (modaltype.ToUpper() == "NO")
                    sSql = $"SELECT Distinct 0 seq, r_kb_no [No. Transaksi], Format(r_kb_date, 'dd/MM/yyyy') Tanggal FROM dbo.GetBukuBesar('{param.periodstart} 00:00:00', '{param.periodend} 23:59:59') r Where 1=1 AND r_kb_no!='SALDO' Order By Format(r_kb_date, 'dd/MM/yyyy')";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblModal" + modaltype);
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq") { item = (i++).ToString(); } 
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName)) { tblcols.Add(col.ColumnName); } 
                        }
                        tblrows.Add(row);
                    }
                }
                else { result = "Data Not Found."; }
                    
            }
            catch (Exception e) { result = e.ToString() + sSql; }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ViewReport(sFilter param, string reporttype)
        {
            var rptfile = ""; var rptname = "";
            var Periode = $"{param.periodstart.ToDateTime().ToString("dd/MM/yyyy") }-{param.periodend.ToDateTime().ToString("dd/MM/yyyy")}";
            rptfile = $"RptAcctg{param.ddltype + reporttype}";
            rptname = $"LAPORAN_{param.ddltype}_{Periode}";

            rptfile = rptfile.Replace("PDF", "");
            var sField = ""; var sGroup = ""; var sOrderby = "";
            if (param.ddltype == "GJ")
            {
                sField = "r_gl_reff_type, r.r_gl_id, r.r_gl_seq, r.r_gl_date, r.r_gl_ref_no, r.r_gl_note, r.m_account_id, r.m_account_code, r.m_account_name, r.r_gl_awal_amt, r.r_gl_debet_amt, r.r_gl_credit_amt, r.m_proyek_id, r.m_proyek_name, Isnull(m_item_name, '') m_item_name, m_cust_name";
                sOrderby = " Order By m_proyek_id, r_gl_date, r_gl_id, r_gl_seq";
            }
            else if (param.ddltype == "TB")
            {
                sField = "m_account_id, r.m_account_code, m_account_name, Isnull(SUM(r_gl_awal_amt), 0.0) r_gl_awal_amt, Isnull(Sum(r_gl_debet_amt), 0.0) r_gl_debet_amt, Isnull(Sum(r_gl_credit_amt), 0.0) r_gl_credit_amt, 0 m_proyek_id, '' m_proyek_name, Isnull(SUM(r_gl_awal_amt), 0.0) + Isnull(Sum(r_gl_debet_amt), 0.0) - Isnull(Sum(r_gl_credit_amt), 0.0) r_gl_balance_amt";
                sGroup = " Group By m_account_id, m_account_code, m_account_name";
                sOrderby = " Order By m_account_code";
            }
            else if (param.ddltype == "GL")
            {
                sField = "r.r_gl_seq, r.r_gl_date, m_account_id, r.m_account_code, m_account_name, Isnull(r_gl_awal_amt, 0.0) r_gl_awal_amt, Isnull(r_gl_debet_amt, 0.0) r_gl_debet_amt, Isnull(r_gl_credit_amt, 0.0) r_gl_credit_amt, m_proyek_id, m_proyek_name, Isnull(r_gl_awal_amt, 0.0) + Isnull(r_gl_debet_amt, 0.0) - Isnull(r_gl_credit_amt, 0.0) r_gl_balance_amt, r.r_gl_ref_no, r.r_gl_note, Isnull(m_item_name, '') m_item_name, m_cust_name, reff_account_code, reff_account_desc, r_gl_ref_type";
                sOrderby = " Order By m_account_code, r.r_gl_date, r_gl_seq";
            }

            sSql = $"SELECT {sField} FROM dbo.GetReportGL('{param.periodstart} 00:00:00', '{param.periodend} 23:59:59') r Where 1=1 ";
            sSql += param.ddltype == "GJ" ? "AND r.r_gl_reff_type = 'Transaksi'" : "";
            if (!string.IsNullOrEmpty(param.filterakun)) sSql += $" AND r.m_account_code IN ('{param.filterakun.Replace(";", "', '")}')";
            if (param.ddlproyek != 0) sSql += $" AND r.m_proyek_id = {param.ddlproyek}";
            if (param.ddltype == "GJ" && !string.IsNullOrEmpty(param.filterno)) sSql += $" AND r.r_gl_ref_no IN ('{param.filterno.Replace(";", "', '")}')";
            sSql += $"{sGroup}{sOrderby}"; 

            if (param.ddltype == "BUKUBESAR") {
                sSql = $"SELECT * FROM dbo.GetBukuBesar('{param.periodstart} 00:00:00', '{param.periodend} 23:59:59') r Where 1=1";
                if (!string.IsNullOrEmpty(param.filterno)) sSql += $" AND r.r_kb_no IN ('{param.filterno.Replace(";", "', '")}')";
                if (!string.IsNullOrEmpty(param.filterakun)) sSql += $" AND r.m_account_code IN ('{param.filterakun.Replace(";", "', '")}')";
                sSql += " Order By m_account_code, r_kb_id, r_kb_date, seq";                
            }

            var dt_rpt = new ClassConnection().GetDataTable(sSql, $"data");
            var req = new PrintOutRequest
            {
                rpt_src = new DataTable(),
                rpt_name = rptfile,
                file_name = rptname,
                rpt_param = new Dictionary<string, string>()
            };
            req.rpt_src = dt_rpt;
            req.rpt_param.Add("Periode", Periode);
            req.rpt_param.Add("UserID", Session["UserID"].ToString());
            return setPrintOut(req); 
        }
        #endregion
    }
}