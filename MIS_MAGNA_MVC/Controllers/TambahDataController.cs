﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class TambahDataController : Controller
    {
        // GET: TambahData
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        public TambahDataController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        #region Revisi Kartu kontrol
        public class sFilter
        {
            public int m_item_id { get; set; }
            public int m_cust_id { get; set; }
            public int t_spr_id { get; set; }
            public int t_total_angsuran { get; set; }
            public decimal r_pay_amt { get; set; }
            public decimal t_spr_amt { get; set; }
            public decimal t_per_bln_angsuran { get; set; }
            public decimal t_amount_kartu { get; set; }
            public decimal t_spr_price { get; set; }
            public decimal t_spr_nominal { get; set; }
            public string t_tanggal_angsuran { get; set; }
            public string r_piutang_ref_table { get; set; }
            public string r_piutang_type_revisi { get; set; }
            public string t_type_bayar { get; set; }
            public string ddl_type_bayar { get; set; }
        }

        public class r_piutang_d : tb_r_piutang
        {
            public int r_piutang_seq { get; set; }
            public string m_item_name { get; set; }
            public string m_cust_name { get; set; }
            public string m_proyek_name { get; set; }
        }

        private void InitDDLAndField(tb_t_spr tbl, string action)
        {
            //DDL
            var getCompany = db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList();
            if (action == "New Data")
                getCompany = getCompany.Where(m => m.flag == "ACTIVE").ToList();
            ViewBag.m_company_id = new SelectList(getCompany, "valuefield", "textfield", tbl.m_company_id);

            //Field
            ViewBag.m_item_name = db.tb_m_item.FirstOrDefault(w => w.m_item_id == tbl.m_item_id)?.m_item_name ?? "";
            ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(w => w.m_proyek_id == tbl.m_proyek_id)?.m_proyek_name ?? "";
            ViewBag.m_cust_name = db.tb_m_cust.FirstOrDefault(w => w.m_cust_id == tbl.m_cust_id)?.m_cust_name ?? "";
            ViewBag.t_spr_id = tbl.t_spr_id;
            ViewBag.t_spr_no = tbl.t_spr_no;
            ViewBag.t_spr_price = tbl.t_spr_price;

            sSql = $"Select * From tb_r_piutang Where m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND r_piutang_status='Post' AND r_piutang_ref_table !='tb_t_titipan'";
            var r_piutang = db.Database.SqlQuery<tb_r_piutang>(sSql).ToList(); 
            
            ViewBag.r_piutang_amt = r_piutang.Where(r => r.r_piutang_type != "PAYMENT").Sum(r => Math.Round(r.r_piutang_amt));
            ViewBag.r_piutang_pay_amt = r_piutang.Where(r => r.r_piutang_type == "PAYMENT").Sum(r => Math.Round(r.r_piutang_pay_amt));
            ViewBag.r_piutang_amt_sisa = r_piutang.Where(r => r.r_piutang_type != "PAYMENT").Sum(r => Math.Round(r.r_piutang_amt)) - r_piutang.Where(r => r.r_piutang_type == "PAYMENT").Sum(r => Math.Round(r.r_piutang_pay_amt));

            var t_piutang_amt = r_piutang.Where(r => r.r_piutang_type == "KPR").Sum(r => Math.Round(r.r_piutang_amt));
            var t_dp_amt = r_piutang.Where(r => r.r_piutang_type == "DP").Sum(r => Math.Round(r.r_piutang_amt));
            var t_kt_amt = r_piutang.Where(r => r.r_piutang_type == "KT").Sum(r => Math.Round(r.r_piutang_amt));
            var t_shm_amt = r_piutang.Where(r => r.r_piutang_type == "SHM").Sum(r => Math.Round(r.r_piutang_amt));

            tbl.t_piutang_amt = t_piutang_amt;
            tbl.t_dp_amt = t_dp_amt;
            tbl.t_kt_amt = t_kt_amt;
            tbl.t_shm_amt = t_shm_amt;
            tbl.t_spr_total_amt = tbl.t_spr_price - tbl.t_utj_amt - t_dp_amt - t_kt_amt + t_shm_amt;
            db.Entry(tbl).State = EntityState.Modified;
            db.SaveChanges();
        }

        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_r_piutang", false); 
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE isapptrans=0 AND isposttrans=0 ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }

            sSql = "Select * From ( Select r.t_spr_id, r.m_item_id, r.t_spr_no, i.m_item_name, c.m_cust_name, p.m_proyek_name, r.t_spr_date, r.t_spr_price, r.t_utj_amt, r.t_dp_amt, r.t_piutang_amt, r.t_shm_type, r.t_kt_type, r.t_kt_amt, r.t_shm_amt, r.t_spr_status From tb_t_spr r Inner Join tb_m_item i ON i.m_item_id=r.m_item_id Inner join tb_m_cust c ON c.m_cust_id=r.m_cust_id Inner Join tb_m_proyek p ON p.m_proyek_id=r.m_proyek_id Where r.t_spr_status='Post' ) r ";
            var sFixedFilter = "";
            var sOrder_by = " r.t_spr_id";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile"); 
            tb_t_spr tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_spr();
                tbl.created_at = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_spr.Where(r => r.t_spr_id == id).FirstOrDefault();
                ViewBag.t_tanggal_angsuran = GetServerTime().ToString("MM/dd/yyyy");
                ViewBag.t_total_angsuran = tbl.t_shm_total_angsuran;
                ViewBag.t_amount_kartu = 0m;
                ViewBag.t_spr_amt = 0m;
                ViewBag.t_per_bln_angsuran = 0m;
            } 
             
            if (tbl == null) return HttpNotFound();
            ViewBag.action = action;
            InitDDLAndField(tbl, action);
            return View(tbl);
        }

        [HttpPost]
        public ActionResult GetDDLType(string t_type_bayar)
        {   
            JsonResult js = null;
            try
            {
                var tbl = new SelectList(new Dictionary<string, string>() {["None"] = "-- Pilih Type Revisi --",["TANGGAL"] = "Revisi Tanggal",["ANGSURAN"] = "Revisi Angsuran",["NOMINAL"] = "Revisi Nominal",["TAMBAH"] = "Tambah Data" }, "Key", "Value").ToList(); 
                if (t_type_bayar == "Tunai")
                    tbl = new SelectList(new Dictionary<string, string>() {["None"] = "-- Pilih Type Revisi --",["TANGGAL"] = "Revisi Tanggal",["NOMINAL"] = "Revisi Nominal",["TAMBAH"] = "Tambah Data" }, "Key", "Value").ToList(); 
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }  

        [HttpPost]
        public ActionResult GetDataKartu(sFilter param)
        {
            JsonResult js = null;
            try
            {
                var spr = db.tb_t_spr.FirstOrDefault(s=> s.m_item_id==param.m_item_id);
                int t_total_angsuran = 0; var t_type_bayar = ""; decimal t_amount_kartu = 0;
                
                if (param.r_piutang_ref_table == "DP")
                {
                    t_total_angsuran = spr.t_dp_total_angsuran;
                    t_type_bayar = spr.t_dp_type_bayar;
                    t_amount_kartu = spr.t_dp_amt + spr.t_dp_amt2;
                    sSql += " AND r_piutang_ref_table IN ('tb_t_spr_dp', 'tb_t_angsuran_dp_d')";
                }
                else if (param.r_piutang_ref_table == "KT")
                {
                    t_total_angsuran = spr.t_kt_total_angsuran;
                    t_type_bayar = spr.t_kt_type_bayar;
                    t_amount_kartu = spr.t_kt_amt + spr.t_kt_amt2;
                    sSql += " AND r_piutang_ref_table IN ('tb_t_spr_kt', 'tb_t_angsuran_kt_d')";
                }
                else if (param.r_piutang_ref_table == "KPR")
                {
                    t_total_angsuran = spr.t_piutang_total_angsuran;
                    t_type_bayar = spr.t_piutang_type_bayar;
                    t_amount_kartu = spr.t_piutang_amt;
                    sSql += " AND r_piutang_ref_table IN ('tb_t_spr_kpr', 'tb_t_angsuran_kpr_d')";
                }                    
                else if (param.r_piutang_ref_table == "SHM")
                {
                    t_total_angsuran = spr.t_shm_total_angsuran;
                    t_type_bayar = spr.t_shm_type_bayar;
                    t_amount_kartu = spr.t_shm_amt + spr.t_shm_amt2;
                    sSql += " AND r_piutang_ref_table IN ('tb_t_spr_shm', 'tb_t_angsuran_shm_d')";
                }

                sSql = $"Select * From tb_r_piutang Where m_item_id={param.m_item_id} AND r_piutang_status='Post'";
                var r_piutang = db.Database.SqlQuery<tb_r_piutang>(sSql).ToList();
                
                sSql = $"Select Top 1 r.r_piutang_ref_due_date, r_piutang_type, m_item_id, {t_total_angsuran} t_total_angsuran, '{t_type_bayar}' t_type_bayar, {t_amount_kartu} t_amount_kartu, {r_piutang.Sum(r=> r.r_piutang_pay_amt)} r_pay_amt From tb_r_piutang r Where r.m_item_id = {param.m_item_id} AND r_piutang_type IN ('{param.r_piutang_ref_table }')";                 
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tb_v_kartu_kontrol"));
                if (tbl.Count > 0)
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
                //else
                //    js = Json(new { result = "Data tidak ditemukan" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                js = Json(new { result = sSql + e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }
        
        [HttpPost]
        public ActionResult FillDetailData(sFilter param)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select 0 r_piutang_seq, r.m_item_id, r.r_piutang_type, r.r_piutang_id, r.r_piutang_ref_id, r.r_piutang_ref_no, r.m_item_name, r.m_cust_name, r.m_proyek_name, r.t_spr_date, r.t_spr_price, r.t_utj_amt, r.t_dp_amt, r.t_piutang_amt, r.t_kt_amt, r.t_shm_amt, r.t_spr_status, r.r_piutang_note, r.r_piutang_ref_due_date, r.r_piutang_amt, Isnull((Select SUM(k.r_piutang_pay_amt) from tb_r_piutang k Where k.r_piutang_ref_id = r.r_piutang_ref_id AND k.r_piutang_type = 'PAYMENT' AND k.r_piutang_ref_table = r.r_piutang_ref_table AND r.m_item_id = k.m_item_id And k.r_piutang_status='Post'), 0.0) r_piutang_pay_amt, Isnull((Select SUM(k.r_piutang_disc_amt) from tb_r_piutang k Where k.r_piutang_ref_id = r.r_piutang_ref_id AND k.r_piutang_type = 'PAYMENT' AND k.r_piutang_ref_table = r.r_piutang_ref_table AND r.m_item_id = k.m_item_id And k.r_piutang_status='Post'), 0.0) r_piutang_disc_amt, Cast(Isnull((Select MAX(k.r_piutang_pay_ref_date) from tb_r_piutang k Where k.r_piutang_ref_id = r.r_piutang_ref_id AND k.r_piutang_type = 'PAYMENT' AND k.r_piutang_ref_table = r.r_piutang_ref_table AND r.m_item_id = k.m_item_id And k.r_piutang_status='Post'), '1/1/1900') as datetime) r_piutang_pay_ref_date From tb_v_kartu_kontrol r Where r_piutang_type!= 'PAYMENT' AND r.m_item_id = {param.m_item_id}";
                if (param.r_piutang_ref_table == "UTJ")
                    sSql += " AND r_piutang_ref_table IN ('tb_t_spr_utj', 'tb_t_utj')";
                else if (param.r_piutang_ref_table == "DP")
                    sSql += " AND r_piutang_ref_table IN ('tb_t_spr_dp', 'tb_t_angsuran_dp_d')";
                else if (param.r_piutang_ref_table == "KT")
                    sSql += " AND r_piutang_ref_table IN ('tb_t_spr_kt', 'tb_t_angsuran_kt_d')";
                else if (param.r_piutang_ref_table == "KPR")
                    sSql += " AND r_piutang_ref_table IN ('tb_t_spr_kpr', 'tb_t_angsuran_kpr_d')";
                else if (param.r_piutang_ref_table == "SHM")
                    sSql += " AND r_piutang_ref_table IN ('tb_t_spr_shm', 'tb_t_angsuran_shm_d')";
                else
                    sSql += $" AND r_piutang_ref_table IN ('{param.r_piutang_ref_table}')";

                sSql += " Group By r.r_piutang_id, r.r_piutang_ref_id, r.r_piutang_ref_no, r.m_item_name, r.m_cust_name, r.m_proyek_name, r.t_spr_date, r.t_spr_price, r.t_utj_amt, r.t_dp_amt, r.t_piutang_amt, r.t_kt_amt, r.t_shm_amt, r.t_spr_status, r.r_piutang_note, r.r_piutang_ref_due_date, r.r_piutang_amt, r.m_item_id, r.r_piutang_type, r.r_piutang_ref_table Order By m_item_name, r.r_piutang_ref_id";
               var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tb_v_kartu_kontrol"));
               if (tbl != null || tbl.Count > 0) 
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                } 
            }
            catch (Exception e)
            {
                js = Json(new { result = sSql + e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(sFilter param, List<r_piutang_d> data)
        {
            var result = "";
            List<r_piutang_d> tbl = new List<r_piutang_d>();
            int maxloop = param.t_total_angsuran;
            decimal dValue = Math.Round(param.t_per_bln_angsuran, 0);
            decimal amtTotal = Math.Round(param.t_amount_kartu, 0);
            decimal amtGen = 0;
            var r_piutang_note = "";

            //var tbl = new List<r_piutang_d>();
            //data = data.Where(r => r.r_piutang_type == param.r_piutang_ref_table && r.m_item_id == param.m_item_id).OrderBy(r => r.r_piutang_pay_ref_id).ToList();
            try
            {
                for (var i = 0; i < maxloop; i++)
                {
                    var r_piutang_seq = i + 1;
                    if (param.r_piutang_ref_table == "DP")
                        r_piutang_note = $"Uang Muka {r_piutang_seq}";
                    else if (param.r_piutang_ref_table == "KT")
                        r_piutang_note = $"Kelebihan Tanah {r_piutang_seq}";
                    else if (param.r_piutang_ref_table == "KPR")
                        r_piutang_note = $"Cicilan {r_piutang_seq}";
                    else if (param.r_piutang_ref_table == "SHM")
                        r_piutang_note = $"SHM {r_piutang_seq}";
                    tbl.Add(new r_piutang_d()
                    {
                        r_piutang_seq = i + 1,
                        r_piutang_note = r_piutang_note,
                        r_piutang_ref_due_date = DateTime.Parse(param.t_tanggal_angsuran).AddMonths(i),
                        r_piutang_ref_date = DateTime.Parse(param.t_tanggal_angsuran).AddMonths(i),
                        r_piutang_amt = (i == (maxloop - 1) ? (amtTotal - amtGen) : dValue),
                        r_piutang_pay_amt = 0,
                        r_piutang_pay_ref_id = 0,
                        r_piutang_disc_amt = 0,                       
                        r_piutang_status = "Post",
                        r_piutang_pay_account_id = 0,
                        r_piutang_account_id = 0,
                        r_piutang_ref_table=param.r_piutang_ref_table,
                    });
                    amtGen += dValue;
                }

            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            JsonResult js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        
        [HttpPost, ActionName("RevisiData")]
        [ValidateAntiForgeryToken]
        public ActionResult HapusData(List<r_piutang_d> dtDtl, sFilter param, DateTime t_tanggal_angsuran)
        {          
            var msg = ""; var result = "failed";  var hdrid = new Guid(); var genNumber = "";
            var spr = db.tb_t_spr.FirstOrDefault(s => s.t_spr_id == param.t_spr_id && s.m_item_id == param.m_item_id);
            if (param.r_piutang_type_revisi == "None") msg += "Pilih Type Revisi dulu!<br />";
            if (dtDtl == null || dtDtl.Count <= 0) msg += "Revisi Kartu Kontrol Detail belum ada!<br />";
            if (param.t_type_bayar=="Angsuran" && param.t_total_angsuran <= 0)
                msg += $"Jika Tipe Bayar {param.t_type_bayar} total angsuran harus lebih dari nol..!<br />"; 
            
            if (param.r_piutang_type_revisi == "NOMINAL")
                foreach (var item in dtDtl)
                    if (item.r_piutang_pay_amt > 0)
                        msg += $"Tolong untuk hapus data pembayaran {item.r_piutang_note} dulu..!<br />"; 

            sSql = $"Select * From tb_r_piutang Where m_item_id={param.m_item_id} AND r_piutang_status='Post' AND r_piutang_type='PAYMENT'";
            if (param.r_piutang_ref_table == "DP")
            {
                genNumber = GenerateTransNo($"{param.r_piutang_ref_table}", "t_angsuran_dp_no", "tb_t_angsuran_dp_h", spr.m_company_id, t_tanggal_angsuran.ToString("MM/dd/yyyy"));
                sSql += " AND r_piutang_ref_table IN ('tb_t_spr_dp', 'tb_t_angsuran_dp_d')";
            }                
            else if (param.r_piutang_ref_table == "KT")
            {
                genNumber = GenerateTransNo($"{param.r_piutang_ref_table}", "t_angsuran_kt_no", "tb_t_angsuran_kt_h", spr.m_company_id, t_tanggal_angsuran.ToString("MM/dd/yyyy"));
                sSql += " AND r_piutang_ref_table IN ('tb_t_spr_kt', 'tb_t_angsuran_kt_d')";
            }                
            else if (param.r_piutang_ref_table == "KPR")
            {
                genNumber = GenerateTransNo($"{param.r_piutang_ref_table}", "t_angsuran_kpr_no", "tb_t_angsuran_kpr_h", spr.m_company_id, t_tanggal_angsuran.ToString("MM/dd/yyyy"));
                sSql += " AND r_piutang_ref_table IN ('tb_t_spr_kpr', 'tb_t_angsuran_kpr_d')";
            }                
            else if (param.r_piutang_ref_table == "SHM")
            {
                genNumber = GenerateTransNo($"{param.r_piutang_ref_table}", "t_angsuran_shm_no", "tb_t_angsuran_shm_h", spr.m_company_id, t_tanggal_angsuran.ToString("MM/dd/yyyy"));
                sSql += " AND r_piutang_ref_table IN ('tb_t_spr_shm', 'tb_t_angsuran_shm_d')";
            }
            if (param.r_piutang_type_revisi == "ANGSURAN")
            {
                var cek_bayar = db.Database.SqlQuery<tb_r_piutang>(sSql).ToList();
                if (cek_bayar.Sum(r => r.r_piutang_pay_amt) > 0) msg += $"Tolong untuk hapus data pembayaran {spr.t_spr_no} dulu..!<br />";
            }

            if (string.IsNullOrEmpty(msg))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (param.r_piutang_type_revisi == "TANGGAL")
                        {
                            if (dtDtl != null && dtDtl.Count() > 0)
                            {
                                if (param.t_type_bayar == "Angsuran")
                                {
                                    if (param.r_piutang_ref_table == "DP")
                                    {
                                        var dp_h = db.tb_t_angsuran_dp_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                        foreach (var item in dtDtl)
                                        {
                                            var dp_d = db.tb_t_angsuran_dp_d.FirstOrDefault(d => d.t_angsuran_dp_h_id == dp_h.t_angsuran_dp_h_id && d.t_angsuran_dp_d_id == item.r_piutang_ref_id);
                                            dp_d.t_angsuran_dp_due_date = item.r_piutang_ref_due_date;
                                            db.Entry(dp_d).State = EntityState.Modified;
                                        }

                                    }
                                    else if (param.r_piutang_ref_table == "KT")
                                    {
                                        var kt_h = db.tb_t_angsuran_kt_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                        foreach (var item in dtDtl)
                                        {
                                            var kt_d = db.tb_t_angsuran_kt_d.FirstOrDefault(d => d.t_angsuran_kt_h_id == kt_h.t_angsuran_kt_h_id && d.t_angsuran_kt_d_id == item.r_piutang_ref_id);
                                            kt_d.t_angsuran_kt_due_date = item.r_piutang_ref_due_date;
                                            db.Entry(kt_d).State = EntityState.Modified;
                                        }
                                    }
                                    else if (param.r_piutang_ref_table == "KPR")
                                    {
                                        var kpr_h = db.tb_t_angsuran_kpr_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                        foreach (var item in dtDtl)
                                        {
                                            var kpr_d = db.tb_t_angsuran_kpr_d.FirstOrDefault(d => d.t_angsuran_kpr_h_id == kpr_h.t_angsuran_kpr_h_id && d.t_angsuran_kpr_d_id == item.r_piutang_ref_id);
                                            kpr_d.t_angsuran_kpr_due_date = item.r_piutang_ref_due_date;
                                            db.Entry(kpr_d).State = EntityState.Modified;
                                        }
                                    }
                                    else if (param.r_piutang_ref_table == "SHM")
                                    {
                                        var shm_h = db.tb_t_angsuran_shm_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                        foreach (var item in dtDtl)
                                        {
                                            var shm_d = db.tb_t_angsuran_shm_d.FirstOrDefault(d => d.t_angsuran_shm_h_id == shm_h.t_angsuran_shm_h_id && d.t_angsuran_shm_d_id == item.r_piutang_ref_id);
                                            shm_d.t_angsuran_shm_due_date = item.r_piutang_ref_due_date;
                                            db.Entry(shm_d).State = EntityState.Modified;
                                        }
                                    }
                                }
                                //== Update Table tb_r_piutang ==
                                tb_r_piutang rPiutang = db.tb_r_piutang.FirstOrDefault(r => r.m_item_id == param.m_item_id && r.r_piutang_type == param.r_piutang_ref_table);
                                foreach (var item in dtDtl)
                                {
                                    rPiutang = db.tb_r_piutang.FirstOrDefault(r => r.m_item_id == param.m_item_id && r.r_piutang_id == item.r_piutang_id && r.r_piutang_ref_id == item.r_piutang_ref_id && r.r_piutang_type == param.r_piutang_ref_table);
                                    rPiutang.r_piutang_ref_date = item.r_piutang_ref_due_date;
                                    rPiutang.r_piutang_ref_due_date = item.r_piutang_ref_due_date;
                                    db.Entry(rPiutang).State = EntityState.Modified;
                                }
                            }
                        }
                        else if (param.r_piutang_type_revisi == "NOMINAL")
                        {
                            if (dtDtl != null && dtDtl.Count() > 0)
                            {
                                if (param.t_type_bayar == "Angsuran")
                                {
                                    if (param.r_piutang_ref_table == "DP")
                                    {
                                        var dp_h = db.tb_t_angsuran_dp_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                        foreach (var item in dtDtl)
                                        {
                                            var dp_d = db.tb_t_angsuran_dp_d.FirstOrDefault(d => d.t_angsuran_dp_h_id == dp_h.t_angsuran_dp_h_id && d.t_angsuran_dp_d_id == item.r_piutang_ref_id);
                                            dp_d.t_angsuran_dp_d_amt = item.r_piutang_amt;
                                            db.Entry(dp_d).State = EntityState.Modified;
                                        }
                                    }
                                    else if (param.r_piutang_ref_table == "KT")
                                    {
                                        var kt_h = db.tb_t_angsuran_kt_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                        foreach (var item in dtDtl)
                                        {
                                            var kt_d = db.tb_t_angsuran_kt_d.FirstOrDefault(d => d.t_angsuran_kt_h_id == kt_h.t_angsuran_kt_h_id && d.t_angsuran_kt_d_id == item.r_piutang_ref_id);
                                            kt_d.t_angsuran_kt_d_amt = item.r_piutang_amt;
                                            db.Entry(kt_d).State = EntityState.Modified;
                                        }
                                    }
                                    else if (param.r_piutang_ref_table == "KPR")
                                    {
                                        var kpr_h = db.tb_t_angsuran_kpr_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                        foreach (var item in dtDtl)
                                        {
                                            var kpr_d = db.tb_t_angsuran_kpr_d.FirstOrDefault(d => d.t_angsuran_kpr_h_id == kpr_h.t_angsuran_kpr_h_id && d.t_angsuran_kpr_d_id == item.r_piutang_ref_id);
                                            kpr_d.t_angsuran_kpr_d_amt = item.r_piutang_amt;
                                            db.Entry(kpr_d).State = EntityState.Modified;
                                        }
                                    }
                                    else if (param.r_piutang_ref_table == "SHM")
                                    {
                                        var shm_h = db.tb_t_angsuran_shm_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                        foreach (var item in dtDtl)
                                        {
                                            var shm_d = db.tb_t_angsuran_shm_d.FirstOrDefault(d => d.t_angsuran_shm_h_id == shm_h.t_angsuran_shm_h_id && d.t_angsuran_shm_d_id == item.r_piutang_ref_id);
                                            shm_d.t_angsuran_shm_d_amt = item.r_piutang_amt;
                                            db.Entry(shm_d).State = EntityState.Modified;
                                        }
                                    }
                                }

                                //== Update Table tb_r_piutang ==
                                tb_r_piutang rPiutang = db.tb_r_piutang.FirstOrDefault(r => r.m_item_id == param.m_item_id && r.r_piutang_type == param.r_piutang_ref_table);
                                foreach (var item in dtDtl)
                                {
                                    rPiutang = db.tb_r_piutang.FirstOrDefault(r => r.m_item_id == param.m_item_id && r.r_piutang_id == item.r_piutang_id && r.r_piutang_ref_id == item.r_piutang_ref_id && r.r_piutang_type == param.r_piutang_ref_table);
                                    rPiutang.r_piutang_amt = item.r_piutang_amt;
                                    db.Entry(rPiutang).State = EntityState.Modified;
                                }

                                // == Update Table SPR
                                if (param.r_piutang_ref_table == "DP") spr.t_dp_amt = dtDtl.Sum(r => r.r_piutang_amt);
                                if (param.r_piutang_ref_table == "KT") spr.t_kt_amt = dtDtl.Sum(r => r.r_piutang_amt);
                                if (param.r_piutang_ref_table == "KPR") spr.t_piutang_amt = dtDtl.Sum(r => r.r_piutang_amt);
                                if (param.r_piutang_ref_table == "SHM") spr.t_shm_amt = dtDtl.Sum(r => r.r_piutang_amt);
                                db.Entry(spr).State = EntityState.Modified;
                            }
                        }
                        else if (param.r_piutang_type_revisi == "ANGSURAN" && param.t_type_bayar == "Angsuran")
                        {
                            if (dtDtl != null && dtDtl.Count() > 0)
                            {
                                if (param.r_piutang_ref_table == "DP")
                                {
                                    tb_t_angsuran_dp_h dp_h = db.tb_t_angsuran_dp_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                    var dtdtl = db.tb_t_angsuran_dp_d.Where(a => a.t_angsuran_dp_h_id == dp_h.t_angsuran_dp_h_id);
                                    db.tb_t_angsuran_dp_d.RemoveRange(dtdtl);
                                    db.SaveChanges();

                                    db.tb_t_angsuran_dp_h.Remove(dp_h);
                                    db.SaveChanges();

                                    var tbl_piutang = db.tb_r_piutang.Where(r => r.m_item_id == param.m_item_id && r.r_piutang_type == param.r_piutang_ref_table);
                                    db.tb_r_piutang.RemoveRange(tbl_piutang);

                                    //insert tbl angsuran
                                    dp_h = new tb_t_angsuran_dp_h();
                                    dp_h.t_angsuran_dp_uid = Guid.NewGuid();
                                    dp_h.t_spr_awal_id = spr.t_spr_awal_id;
                                    dp_h.m_cust_id = spr.m_cust_id;
                                    dp_h.m_item_id = spr.m_item_id;
                                    dp_h.m_company_id = spr.m_company_id;
                                    dp_h.m_proyek_id = spr.m_proyek_id;
                                    dp_h.t_angsuran_dp_date = t_tanggal_angsuran;
                                    dp_h.t_angsuran_dp_no = genNumber;
                                    dp_h.t_angsuran_dp_h_status = "Post";
                                    dp_h.t_angsuran_dp_h_note = "Revisi Kartu";
                                    dp_h.created_by = Session["UserID"].ToString();
                                    dp_h.created_at = GetServerTime();
                                    dp_h.updated_at = GetServerTime();
                                    dp_h.updated_by = Session["UserID"].ToString();
                                    dp_h.posted_at = GetServerTime();
                                    dp_h.posted_by = Session["UserID"].ToString();
                                    db.tb_t_angsuran_dp_h.Add(dp_h);
                                    db.SaveChanges();

                                    tb_t_angsuran_dp_d tbldtl;
                                    for (int i = 0; i < dtDtl.Count(); i++)
                                    {
                                        tbldtl = (tb_t_angsuran_dp_d)MappingTable(new tb_t_angsuran_dp_d(), dtDtl[i]);
                                        tbldtl.t_angsuran_dp_d_seq = i + 1;
                                        tbldtl.t_angsuran_dp_due_date = dtDtl[i].r_piutang_ref_due_date;
                                        tbldtl.t_angsuran_dp_d_amt = dtDtl[i].r_piutang_amt;
                                        tbldtl.t_angsuran_dp_d_amt_accum = 0;
                                        tbldtl.t_angsuran_dp_d_status = "Post";
                                        tbldtl.t_angsuran_dp_h_id = dp_h.t_angsuran_dp_h_id;
                                        tbldtl.t_angsuran_dp_d_note = dtDtl[i].r_piutang_note ?? "";
                                        tbldtl.created_by = dp_h.created_by;
                                        tbldtl.created_at = GetServerTime();
                                        db.tb_t_angsuran_dp_d.Add(tbldtl);
                                    }
                                    db.SaveChanges();

                                    db.tb_post_trans.Add(InsertPostTrans(dp_h.m_company_id, dp_h.t_angsuran_dp_uid, "tb_t_angsuran_dp_h", dp_h.t_angsuran_dp_h_id, dp_h.updated_by, dp_h.updated_at));

                                    var dtldata = db.tb_t_angsuran_dp_d.Where(d => d.t_angsuran_dp_h_id == dp_h.t_angsuran_dp_h_id).ToList();
                                    foreach (var item in dtldata)
                                    {
                                        db.tb_r_piutang.Add(Insert_R_Piutang(spr.m_company_id, spr.m_cust_id, spr.m_proyek_id, spr.m_item_id, "DP", "tb_t_angsuran_dp_d", item.t_angsuran_dp_d_id, spr.t_spr_no, dp_h.t_angsuran_dp_date, item.t_angsuran_dp_due_date, Convert.ToDateTime("1/1/1900".ToString()), spr.t_dp_account_id, item.t_angsuran_dp_d_amt, 0m, item.t_angsuran_dp_d_note ?? "", dp_h.posted_by, (DateTime)dp_h.posted_at, "", 0, "", 0, 0, spr.t_spr_id, spr.t_spr_awal_id));
                                    }
                                    db.SaveChanges();

                                    spr.t_dp_amt = dtldata.Sum(r => r.t_angsuran_dp_d_amt);
                                    spr.t_dp_total_angsuran = dtldata.Count;
                                    db.Entry(spr).State = EntityState.Modified;
                                }
                                else if (param.r_piutang_ref_table == "KT")
                                {
                                    tb_t_angsuran_kt_h kt_h = db.tb_t_angsuran_kt_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                    var dtdtl = db.tb_t_angsuran_kt_d.Where(a => a.t_angsuran_kt_h_id == kt_h.t_angsuran_kt_h_id);
                                    db.tb_t_angsuran_kt_d.RemoveRange(dtdtl);
                                    db.SaveChanges();

                                    db.tb_t_angsuran_kt_h.Remove(kt_h);
                                    db.SaveChanges();

                                    var tbl_piutang = db.tb_r_piutang.Where(r => r.m_item_id == param.m_item_id && r.r_piutang_type == param.r_piutang_ref_table);
                                    db.tb_r_piutang.RemoveRange(tbl_piutang);

                                    //insert tbl angsuran
                                    kt_h = new tb_t_angsuran_kt_h();
                                    kt_h.t_angsuran_kt_uid = Guid.NewGuid();
                                    kt_h.t_spr_awal_id = spr.t_spr_awal_id;
                                    kt_h.m_cust_id = spr.m_cust_id;
                                    kt_h.m_item_id = spr.m_item_id;
                                    kt_h.m_company_id = spr.m_company_id;
                                    kt_h.m_proyek_id = spr.m_proyek_id;
                                    kt_h.t_angsuran_kt_date = t_tanggal_angsuran;
                                    kt_h.t_angsuran_kt_no = genNumber;
                                    kt_h.t_angsuran_kt_h_status = "Post";
                                    kt_h.t_angsuran_kt_h_note = "Revisi Kartu";
                                    kt_h.created_by = Session["UserID"].ToString();
                                    kt_h.created_at = GetServerTime();
                                    kt_h.updated_at = GetServerTime();
                                    kt_h.updated_by = Session["UserID"].ToString();
                                    kt_h.posted_at = GetServerTime();
                                    kt_h.posted_by = Session["UserID"].ToString();
                                    db.tb_t_angsuran_kt_h.Add(kt_h);
                                    db.SaveChanges();

                                    tb_t_angsuran_kt_d tbldtl;
                                    for (int i = 0; i < dtDtl.Count(); i++)
                                    {
                                        tbldtl = (tb_t_angsuran_kt_d)MappingTable(new tb_t_angsuran_kt_d(), dtDtl[i]);
                                        tbldtl.t_angsuran_kt_d_seq = i + 1;
                                        tbldtl.t_angsuran_kt_due_date = dtDtl[i].r_piutang_ref_due_date;
                                        tbldtl.t_angsuran_kt_d_amt = dtDtl[i].r_piutang_amt;
                                        tbldtl.t_angsuran_kt_d_amt_accum = 0;
                                        tbldtl.t_angsuran_kt_d_status = "Post";
                                        tbldtl.t_angsuran_kt_h_id = kt_h.t_angsuran_kt_h_id;
                                        tbldtl.t_angsuran_kt_d_note = dtDtl[i].r_piutang_note ?? "";
                                        tbldtl.created_by = kt_h.created_by;
                                        tbldtl.created_at = GetServerTime();
                                        db.tb_t_angsuran_kt_d.Add(tbldtl);
                                    }
                                    db.SaveChanges();

                                    db.tb_post_trans.Add(InsertPostTrans(kt_h.m_company_id, kt_h.t_angsuran_kt_uid, "tb_t_angsuran_kt_h", kt_h.t_angsuran_kt_h_id, kt_h.updated_by, kt_h.updated_at));

                                    var dtldata = db.tb_t_angsuran_kt_d.Where(d => d.t_angsuran_kt_h_id == kt_h.t_angsuran_kt_h_id).ToList();
                                    foreach (var item in dtldata)
                                    {
                                        db.tb_r_piutang.Add(Insert_R_Piutang(spr.m_company_id, spr.m_cust_id, spr.m_proyek_id, spr.m_item_id, "KT", "tb_t_angsuran_kt_d", item.t_angsuran_kt_d_id, spr.t_spr_no, kt_h.t_angsuran_kt_date, item.t_angsuran_kt_due_date, Convert.ToDateTime("1/1/1900".ToString()), spr.t_kt_account_id, item.t_angsuran_kt_d_amt, 0m, item.t_angsuran_kt_d_note ?? "", kt_h.posted_by, (DateTime)kt_h.posted_at, "", 0, "", 0, 0, spr.t_spr_id, spr.t_spr_awal_id));
                                    }
                                    db.SaveChanges();

                                    spr.t_kt_amt = dtldata.Sum(r => r.t_angsuran_kt_d_amt);
                                    spr.t_kt_total_angsuran = dtldata.Count;
                                    db.Entry(spr).State = EntityState.Modified;
                                }
                                else if (param.r_piutang_ref_table == "KPR")
                                {
                                    tb_t_angsuran_kpr_h kpr_h = db.tb_t_angsuran_kpr_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                    var dtdtl = db.tb_t_angsuran_kpr_d.Where(a => a.t_angsuran_kpr_h_id == kpr_h.t_angsuran_kpr_h_id);
                                    db.tb_t_angsuran_kpr_d.RemoveRange(dtdtl);
                                    db.SaveChanges();

                                    db.tb_t_angsuran_kpr_h.Remove(kpr_h);
                                    db.SaveChanges();

                                    var tbl_piutang = db.tb_r_piutang.Where(r => r.m_item_id == param.m_item_id && r.r_piutang_type == param.r_piutang_ref_table);
                                    db.tb_r_piutang.RemoveRange(tbl_piutang);

                                    //insert tbl angsuran
                                    kpr_h = new tb_t_angsuran_kpr_h();
                                    kpr_h.t_angsuran_kpr_uid = Guid.NewGuid();
                                    kpr_h.t_spr_awal_id = spr.t_spr_awal_id;
                                    kpr_h.m_cust_id = spr.m_cust_id;
                                    kpr_h.m_item_id = spr.m_item_id;
                                    kpr_h.m_company_id = spr.m_company_id;
                                    kpr_h.m_proyek_id = spr.m_proyek_id;
                                    kpr_h.t_angsuran_kpr_date = t_tanggal_angsuran;
                                    kpr_h.t_angsuran_kpr_no = genNumber;
                                    kpr_h.t_angsuran_kpr_h_status = "Post";
                                    kpr_h.t_angsuran_kpr_h_note = "Revisi Kartu";
                                    kpr_h.created_by = Session["UserID"].ToString();
                                    kpr_h.created_at = GetServerTime();
                                    kpr_h.updated_at = GetServerTime();
                                    kpr_h.updated_by = Session["UserID"].ToString();
                                    kpr_h.posted_at = GetServerTime();
                                    kpr_h.posted_by = Session["UserID"].ToString();
                                    db.tb_t_angsuran_kpr_h.Add(kpr_h);
                                    db.SaveChanges();

                                    tb_t_angsuran_kpr_d tbldtl;
                                    for (int i = 0; i < dtDtl.Count(); i++)
                                    {
                                        tbldtl = (tb_t_angsuran_kpr_d)MappingTable(new tb_t_angsuran_kpr_d(), dtDtl[i]);
                                        tbldtl.t_angsuran_kpr_d_seq = i + 1;
                                        tbldtl.t_angsuran_kpr_due_date = dtDtl[i].r_piutang_ref_due_date;
                                        tbldtl.t_angsuran_kpr_d_amt = dtDtl[i].r_piutang_amt;
                                        tbldtl.t_angsuran_kpr_d_amt_accum = 0;
                                        tbldtl.t_angsuran_kpr_d_status = "Post";
                                        tbldtl.t_angsuran_kpr_h_id = kpr_h.t_angsuran_kpr_h_id;
                                        tbldtl.t_angsuran_kpr_d_note = dtDtl[i].r_piutang_note ?? "";
                                        tbldtl.created_by = kpr_h.created_by;
                                        tbldtl.created_at = GetServerTime();
                                        db.tb_t_angsuran_kpr_d.Add(tbldtl);
                                    }
                                    db.SaveChanges();

                                    db.tb_post_trans.Add(InsertPostTrans(kpr_h.m_company_id, kpr_h.t_angsuran_kpr_uid, "tb_t_angsuran_kpr_h", kpr_h.t_angsuran_kpr_h_id, kpr_h.updated_by, kpr_h.updated_at));

                                    var dtldata = db.tb_t_angsuran_kpr_d.Where(d => d.t_angsuran_kpr_h_id == kpr_h.t_angsuran_kpr_h_id).ToList();
                                    foreach (var item in dtldata)
                                    {
                                        db.tb_r_piutang.Add(Insert_R_Piutang(spr.m_company_id, spr.m_cust_id, spr.m_proyek_id, spr.m_item_id, "KPR", "tb_t_angsuran_kpr_d", item.t_angsuran_kpr_d_id, spr.t_spr_no, kpr_h.t_angsuran_kpr_date, item.t_angsuran_kpr_due_date, Convert.ToDateTime("1/1/1900".ToString()), db.tb_m_proyek.FirstOrDefault(p => p.m_proyek_id == spr.m_proyek_id).m_account_id, item.t_angsuran_kpr_d_amt, 0m, item.t_angsuran_kpr_d_note ?? "", kpr_h.posted_by, (DateTime)kpr_h.posted_at, "", 0, "", 0, 0, spr.t_spr_id, spr.t_spr_awal_id));
                                    }
                                    db.SaveChanges();

                                    spr.t_piutang_amt = dtldata.Sum(r => r.t_angsuran_kpr_d_amt);
                                    spr.t_piutang_total_angsuran = dtldata.Count;
                                    db.Entry(spr).State = EntityState.Modified;
                                }
                                else if (param.r_piutang_ref_table == "SHM")
                                {
                                    tb_t_angsuran_shm_h shm_h = db.tb_t_angsuran_shm_h.FirstOrDefault(a => a.m_item_id == param.m_item_id);
                                    var dtdtl = db.tb_t_angsuran_shm_d.Where(a => a.t_angsuran_shm_h_id == shm_h.t_angsuran_shm_h_id);
                                    db.tb_t_angsuran_shm_d.RemoveRange(dtdtl);
                                    db.SaveChanges();

                                    db.tb_t_angsuran_shm_h.Remove(shm_h);
                                    db.SaveChanges();

                                    var tbl_piutang = db.tb_r_piutang.Where(r => r.m_item_id == param.m_item_id && r.r_piutang_type == param.r_piutang_ref_table);
                                    db.tb_r_piutang.RemoveRange(tbl_piutang);

                                    //insert tbl angsuran
                                    shm_h = new tb_t_angsuran_shm_h();
                                    shm_h.t_angsuran_shm_uid = Guid.NewGuid();
                                    shm_h.t_spr_awal_id = spr.t_spr_awal_id;
                                    shm_h.m_cust_id = spr.m_cust_id;
                                    shm_h.m_item_id = spr.m_item_id;
                                    shm_h.m_company_id = spr.m_company_id;
                                    shm_h.m_proyek_id = spr.m_proyek_id;
                                    shm_h.t_angsuran_shm_date = t_tanggal_angsuran;
                                    shm_h.t_angsuran_shm_no = genNumber;
                                    shm_h.t_angsuran_shm_h_status = "Post";
                                    shm_h.t_angsuran_shm_h_note = "Revisi Kartu";
                                    shm_h.created_by = Session["UserID"].ToString();
                                    shm_h.created_at = GetServerTime();
                                    shm_h.updated_at = GetServerTime();
                                    shm_h.updated_by = Session["UserID"].ToString();
                                    shm_h.posted_at = GetServerTime();
                                    shm_h.posted_by = Session["UserID"].ToString();
                                    db.tb_t_angsuran_shm_h.Add(shm_h);
                                    db.SaveChanges();

                                    tb_t_angsuran_shm_d tbldtl;
                                    for (int i = 0; i < dtDtl.Count(); i++)
                                    {
                                        tbldtl = (tb_t_angsuran_shm_d)MappingTable(new tb_t_angsuran_shm_d(), dtDtl[i]);
                                        tbldtl.t_angsuran_shm_d_seq = i + 1;
                                        tbldtl.t_angsuran_shm_due_date = dtDtl[i].r_piutang_ref_due_date;
                                        tbldtl.t_angsuran_shm_d_amt = dtDtl[i].r_piutang_amt;
                                        tbldtl.t_angsuran_shm_d_amt_accum = 0;
                                        tbldtl.t_angsuran_shm_d_status = "Post";
                                        tbldtl.t_angsuran_shm_h_id = shm_h.t_angsuran_shm_h_id;
                                        tbldtl.t_angsuran_shm_d_note = dtDtl[i].r_piutang_note ?? "";
                                        tbldtl.created_by = shm_h.created_by;
                                        tbldtl.created_at = GetServerTime();
                                        db.tb_t_angsuran_shm_d.Add(tbldtl);
                                    }
                                    db.SaveChanges();

                                    db.tb_post_trans.Add(InsertPostTrans(shm_h.m_company_id, shm_h.t_angsuran_shm_uid, "tb_t_angsuran_shm_h", shm_h.t_angsuran_shm_h_id, shm_h.updated_by, shm_h.updated_at));

                                    var dtldata = db.tb_t_angsuran_shm_d.Where(d => d.t_angsuran_shm_h_id == shm_h.t_angsuran_shm_h_id).ToList();
                                    foreach (var item in dtldata)
                                    {
                                        db.tb_r_piutang.Add(Insert_R_Piutang(spr.m_company_id, spr.m_cust_id, spr.m_proyek_id, spr.m_item_id, "SHM", "tb_t_angsuran_shm_d", item.t_angsuran_shm_d_id, spr.t_spr_no, shm_h.t_angsuran_shm_date, item.t_angsuran_shm_due_date, Convert.ToDateTime("1/1/1900".ToString()), spr.t_shm_account_id, item.t_angsuran_shm_d_amt, 0m, item.t_angsuran_shm_d_note ?? "", shm_h.posted_by, (DateTime)shm_h.posted_at, "", 0, "", 0, 0, spr.t_spr_id, spr.t_spr_awal_id));
                                    }
                                    db.SaveChanges();
                                    spr.t_shm_amt = dtldata.Sum(r => r.t_angsuran_shm_d_amt);
                                    spr.t_shm_total_angsuran = dtldata.Count;
                                    db.Entry(spr).State = EntityState.Modified;
                                }

                            }
                        } 
                        spr.t_spr_price = param.t_spr_price;
                        spr.t_spr_note = "Revisi Kartu Kontrol";
                        spr.t_spr_total_amt = param.t_spr_price - spr.t_utj_amt - spr.t_dp_amt - spr.t_kt_amt + spr.t_shm_amt;
                        db.Entry(spr).State = EntityState.Modified;

                        db.SaveChanges();
                        objTrans.Commit();
                        result = "success";
                        dtDtl = null;
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid, dtDtl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("TambahData")]
        [ValidateAntiForgeryToken]
        public ActionResult TambahData(List<r_piutang_d> dtDtl, sFilter param, DateTime t_tanggal_angsuran)
        {
            var msg = ""; var result = "failed"; var hdrid = new Guid();
            var genNumber = ""; var tb_t_spr = ""; var m_accoun_id = 0;
            var spr = db.tb_t_spr.FirstOrDefault(s => s.t_spr_id == param.t_spr_id);
            if(param.ddl_type_bayar == "None") msg += "Pilih Type Bayar Revisi dulu!<br />";
            if (param.r_piutang_type_revisi == "None") msg += "Pilih Type Revisi dulu!<br />";
            if (param.ddl_type_bayar == "Angsuran")
                if (dtDtl == null || dtDtl.Count <= 0) msg += "Revisi Kartu Kontrol Detail belum ada!<br />";

            if (param.ddl_type_bayar == "Angsuran" && param.t_total_angsuran <= 0)
                msg += $"Jika Tipe Bayar {param.t_type_bayar} total angsuran harus lebih dari nol..!<br />"; 

            sSql = $"Select * From tb_r_piutang Where m_item_id={param.m_item_id} AND r_piutang_status='Post' AND r_piutang_type='{param.r_piutang_ref_table}'";
            var cek_bayar = db.Database.SqlQuery<tb_r_piutang>(sSql).ToList();
            //if (cek_bayar.Count > 0) msg += $"Data Kartu {param.r_piutang_ref_table} Nomer SPR : {spr.t_spr_no} sudah ada..!<br />";

            if (param.r_piutang_ref_table == "DP")
            {
                genNumber = GenerateTransNo($"{param.r_piutang_ref_table}", "t_angsuran_dp_no", "tb_t_angsuran_dp_h", spr.m_company_id, t_tanggal_angsuran.ToString("MM/dd/yyyy"));
                m_accoun_id = GetCOAInterface(new List<string> { "PIUTANG DP" }).FirstOrDefault()?.m_account_id ?? 0;
            }
            else if (param.r_piutang_ref_table == "KT")
            {
                genNumber = GenerateTransNo($"{param.r_piutang_ref_table}", "t_angsuran_kt_no", "tb_t_angsuran_kt_h", spr.m_company_id, t_tanggal_angsuran.ToString("MM/dd/yyyy"));
                m_accoun_id = GetCOAInterface(new List<string> { "PIUTANG KT" }).FirstOrDefault()?.m_account_id ?? 0;
            }
            else if (param.r_piutang_ref_table == "KPR")
            { 
                genNumber = GenerateTransNo($"{param.r_piutang_ref_table}", "t_angsuran_kpr_no", "tb_t_angsuran_kpr_h", spr.m_company_id, t_tanggal_angsuran.ToString("MM/dd/yyyy"));
                m_accoun_id = GetCOAInterface(new List<string> { "PIUTANG KPR" }).FirstOrDefault()?.m_account_id ?? 0;
            }
            else if (param.r_piutang_ref_table == "SHM")
            {
                genNumber = GenerateTransNo($"{param.r_piutang_ref_table}", "t_angsuran_shm_no", "tb_t_angsuran_shm_h", spr.m_company_id, t_tanggal_angsuran.ToString("MM/dd/yyyy"));
                m_accoun_id = GetCOAInterface(new List<string> { "PIUTANG SHM" }).FirstOrDefault()?.m_account_id ?? 0;
            }

            if (string.IsNullOrEmpty(msg))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (param.ddl_type_bayar=="Angsuran")
                        {
                            if (param.r_piutang_ref_table == "DP")
                            {
                                tb_t_angsuran_dp_h dp_h;
                                //insert tbl angsuran
                                dp_h = new tb_t_angsuran_dp_h();
                                dp_h.t_angsuran_dp_uid = Guid.NewGuid();
                                dp_h.t_spr_id = spr.t_spr_id;
                                dp_h.t_spr_awal_id = spr.t_spr_awal_id;
                                dp_h.m_cust_id = spr.m_cust_id;
                                dp_h.m_item_id = spr.m_item_id;
                                dp_h.m_company_id = spr.m_company_id;
                                dp_h.m_proyek_id = spr.m_proyek_id;
                                dp_h.t_angsuran_dp_date = t_tanggal_angsuran;
                                dp_h.t_angsuran_dp_no = genNumber;
                                dp_h.t_angsuran_dp_h_status = "Post";
                                dp_h.t_angsuran_dp_h_note = "Tambah Kartu";
                                dp_h.created_by = Session["UserID"].ToString();
                                dp_h.created_at = GetServerTime();
                                dp_h.updated_at = GetServerTime();
                                dp_h.updated_by = Session["UserID"].ToString();
                                dp_h.posted_at = GetServerTime();
                                dp_h.posted_by = Session["UserID"].ToString();
                                db.tb_t_angsuran_dp_h.Add(dp_h);
                                db.SaveChanges();

                                tb_t_angsuran_dp_d tbldtl;
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    tbldtl = (tb_t_angsuran_dp_d)MappingTable(new tb_t_angsuran_dp_d(), dtDtl[i]);
                                    tbldtl.t_angsuran_dp_d_seq = i + 1;
                                    tbldtl.t_angsuran_dp_due_date = dtDtl[i].r_piutang_ref_due_date;
                                    tbldtl.t_angsuran_dp_d_amt = dtDtl[i].r_piutang_amt;
                                    tbldtl.t_angsuran_dp_d_amt_accum = 0;
                                    tbldtl.t_angsuran_dp_d_status = "Post";
                                    tbldtl.t_angsuran_dp_h_id = dp_h.t_angsuran_dp_h_id;
                                    tbldtl.t_angsuran_dp_d_note = dtDtl[i].r_piutang_note ?? "";
                                    tbldtl.created_by = dp_h.created_by;
                                    tbldtl.created_at = GetServerTime();
                                    db.tb_t_angsuran_dp_d.Add(tbldtl);
                                }
                                db.SaveChanges();

                                db.tb_post_trans.Add(InsertPostTrans(dp_h.m_company_id, dp_h.t_angsuran_dp_uid, "tb_t_angsuran_dp_h", dp_h.t_angsuran_dp_h_id, dp_h.updated_by, dp_h.updated_at));

                                var dtldata = db.tb_t_angsuran_dp_d.Where(d => d.t_angsuran_dp_h_id == dp_h.t_angsuran_dp_h_id).ToList();
                                foreach (var item in dtldata)
                                {
                                    db.tb_r_piutang.Add(Insert_R_Piutang(spr.m_company_id, spr.m_cust_id, spr.m_proyek_id, spr.m_item_id, "DP", "tb_t_angsuran_dp_d", item.t_angsuran_dp_d_id, spr.t_spr_no, dp_h.t_angsuran_dp_date, item.t_angsuran_dp_due_date, Convert.ToDateTime("1/1/1900".ToString()), m_accoun_id, item.t_angsuran_dp_d_amt, 0m, item.t_angsuran_dp_d_note ?? "", dp_h.posted_by, (DateTime)dp_h.posted_at, "", 0, "", 0, 0, spr.t_spr_id, spr.t_spr_awal_id));
                                }
                                db.SaveChanges();

                                spr.t_spr_price = param.t_spr_price + dtldata.Sum(r => r.t_angsuran_dp_d_amt);
                                spr.t_dp_amt = dtldata.Sum(r => r.t_angsuran_dp_d_amt);
                                spr.t_dp_type = "Ya";
                                spr.t_dp_type_bayar = param.ddl_type_bayar;
                                spr.t_dp_status = "Post";
                                spr.t_dp_account_id = m_accoun_id;
                                spr.t_dp_total_angsuran = dtldata.Count;
                                db.Entry(spr).State = EntityState.Modified;
                            }
                            else if (param.r_piutang_ref_table == "KT")
                            {
                                tb_t_angsuran_kt_h kt_h;
                                //insert tbl angsuran
                                kt_h = new tb_t_angsuran_kt_h();
                                kt_h.t_angsuran_kt_uid = Guid.NewGuid();
                                kt_h.t_spr_id = spr.t_spr_id;
                                kt_h.t_spr_awal_id = spr.t_spr_awal_id;
                                kt_h.m_cust_id = spr.m_cust_id;
                                kt_h.m_item_id = spr.m_item_id;
                                kt_h.m_company_id = spr.m_company_id;
                                kt_h.m_proyek_id = spr.m_proyek_id;
                                kt_h.t_angsuran_kt_date = t_tanggal_angsuran;
                                kt_h.t_angsuran_kt_no = genNumber;
                                kt_h.t_angsuran_kt_h_status = "Post";
                                kt_h.t_angsuran_kt_h_note = "Tambah Kartu";
                                kt_h.created_by = Session["UserID"].ToString();
                                kt_h.created_at = GetServerTime();
                                kt_h.updated_at = GetServerTime();
                                kt_h.updated_by = Session["UserID"].ToString();
                                kt_h.posted_at = GetServerTime();
                                kt_h.posted_by = Session["UserID"].ToString();
                                db.tb_t_angsuran_kt_h.Add(kt_h);
                                db.SaveChanges();

                                tb_t_angsuran_kt_d tbldtl;
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    tbldtl = (tb_t_angsuran_kt_d)MappingTable(new tb_t_angsuran_kt_d(), dtDtl[i]);
                                    tbldtl.t_angsuran_kt_d_seq = i + 1;
                                    tbldtl.t_angsuran_kt_due_date = dtDtl[i].r_piutang_ref_due_date;
                                    tbldtl.t_angsuran_kt_d_amt = dtDtl[i].r_piutang_amt;
                                    tbldtl.t_angsuran_kt_d_amt_accum = 0;
                                    tbldtl.t_angsuran_kt_d_status = "Post";
                                    tbldtl.t_angsuran_kt_h_id = kt_h.t_angsuran_kt_h_id;
                                    tbldtl.t_angsuran_kt_d_note = dtDtl[i].r_piutang_note ?? "";
                                    tbldtl.created_by = kt_h.created_by;
                                    tbldtl.created_at = GetServerTime();
                                    db.tb_t_angsuran_kt_d.Add(tbldtl);
                                }
                                db.SaveChanges();

                                db.tb_post_trans.Add(InsertPostTrans(kt_h.m_company_id, kt_h.t_angsuran_kt_uid, "tb_t_angsuran_kt_h", kt_h.t_angsuran_kt_h_id, kt_h.updated_by, kt_h.updated_at));

                                var dtldata = db.tb_t_angsuran_kt_d.Where(d => d.t_angsuran_kt_h_id == kt_h.t_angsuran_kt_h_id).ToList();
                                foreach (var item in dtldata)
                                {
                                    db.tb_r_piutang.Add(Insert_R_Piutang(spr.m_company_id, spr.m_cust_id, spr.m_proyek_id, spr.m_item_id, "KT", "tb_t_angsuran_kt_d", item.t_angsuran_kt_d_id, spr.t_spr_no, kt_h.t_angsuran_kt_date, item.t_angsuran_kt_due_date, Convert.ToDateTime("1/1/1900".ToString()), m_accoun_id, item.t_angsuran_kt_d_amt, 0m, item.t_angsuran_kt_d_note ?? "", kt_h.posted_by, (DateTime)kt_h.posted_at, "", 0, "", 0, 0, spr.t_spr_id, spr.t_spr_awal_id));
                                }
                                db.SaveChanges();

                                spr.t_spr_price = param.t_spr_price + dtldata.Sum(r => r.t_angsuran_kt_d_amt);
                                spr.t_kt_amt = dtldata.Sum(r => r.t_angsuran_kt_d_amt);
                                spr.t_kt_type = "Ya";
                                spr.t_kt_type_bayar = param.ddl_type_bayar;
                                spr.t_kt_status = "Post";
                                spr.t_kt_account_id = m_accoun_id;
                                spr.t_kt_total_angsuran = dtldata.Count;
                                db.Entry(spr).State = EntityState.Modified;
                            }
                            else if (param.r_piutang_ref_table == "KPR")
                            {
                                tb_t_angsuran_kpr_h kpr_h;
                                //insert tbl angsuran
                                kpr_h = new tb_t_angsuran_kpr_h();
                                kpr_h.t_angsuran_kpr_uid = Guid.NewGuid();
                                kpr_h.t_spr_id = spr.t_spr_id;
                                kpr_h.t_spr_awal_id = spr.t_spr_awal_id;
                                kpr_h.m_cust_id = spr.m_cust_id;
                                kpr_h.m_item_id = spr.m_item_id;
                                kpr_h.m_company_id = spr.m_company_id;
                                kpr_h.m_proyek_id = spr.m_proyek_id;
                                kpr_h.t_angsuran_kpr_date = t_tanggal_angsuran;
                                kpr_h.t_angsuran_kpr_no = genNumber;
                                kpr_h.t_angsuran_kpr_h_status = "Post";
                                kpr_h.t_angsuran_kpr_h_note = "Tambah Kartu";
                                kpr_h.created_by = Session["UserID"].ToString();
                                kpr_h.created_at = GetServerTime();
                                kpr_h.updated_at = GetServerTime();
                                kpr_h.updated_by = Session["UserID"].ToString();
                                kpr_h.posted_at = GetServerTime();
                                kpr_h.posted_by = Session["UserID"].ToString();
                                db.tb_t_angsuran_kpr_h.Add(kpr_h);
                                db.SaveChanges();

                                tb_t_angsuran_kpr_d tbldtl;
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    tbldtl = (tb_t_angsuran_kpr_d)MappingTable(new tb_t_angsuran_kpr_d(), dtDtl[i]);
                                    tbldtl.t_angsuran_kpr_d_seq = i + 1;
                                    tbldtl.t_angsuran_kpr_due_date = dtDtl[i].r_piutang_ref_due_date;
                                    tbldtl.t_angsuran_kpr_d_amt = dtDtl[i].r_piutang_amt;
                                    tbldtl.t_angsuran_kpr_d_amt_accum = 0;
                                    tbldtl.t_angsuran_kpr_d_status = "Post";
                                    tbldtl.t_angsuran_kpr_h_id = kpr_h.t_angsuran_kpr_h_id;
                                    tbldtl.t_angsuran_kpr_d_note = dtDtl[i].r_piutang_note ?? "";
                                    tbldtl.created_by = kpr_h.created_by;
                                    tbldtl.created_at = GetServerTime();
                                    db.tb_t_angsuran_kpr_d.Add(tbldtl);
                                }
                                db.SaveChanges();

                                db.tb_post_trans.Add(InsertPostTrans(kpr_h.m_company_id, kpr_h.t_angsuran_kpr_uid, "tb_t_angsuran_kpr_h", kpr_h.t_angsuran_kpr_h_id, kpr_h.updated_by, kpr_h.updated_at));

                                var dtldata = db.tb_t_angsuran_kpr_d.Where(d => d.t_angsuran_kpr_h_id == kpr_h.t_angsuran_kpr_h_id).ToList();
                                foreach (var item in dtldata)
                                {
                                    db.tb_r_piutang.Add(Insert_R_Piutang(spr.m_company_id, spr.m_cust_id, spr.m_proyek_id, spr.m_item_id, "KPR", "tb_t_angsuran_kpr_d", item.t_angsuran_kpr_d_id, spr.t_spr_no, kpr_h.t_angsuran_kpr_date, item.t_angsuran_kpr_due_date, Convert.ToDateTime("1/1/1900".ToString()), db.tb_m_proyek.FirstOrDefault(p => p.m_proyek_id == spr.m_proyek_id).m_account_id, item.t_angsuran_kpr_d_amt, 0m, item.t_angsuran_kpr_d_note ?? "", kpr_h.posted_by, (DateTime)kpr_h.posted_at, "", 0, "", 0, 0, spr.t_spr_id, spr.t_spr_awal_id));
                                }
                                db.SaveChanges();

                                spr.t_spr_price = param.t_spr_price + dtldata.Sum(r => r.t_angsuran_kpr_d_amt);
                                spr.t_piutang_amt = dtldata.Sum(r => r.t_angsuran_kpr_d_amt);
                                spr.t_piutang_type_bayar = param.ddl_type_bayar;
                                spr.t_piutang_status = "Post";
                                spr.t_piutang_total_angsuran = dtldata.Count;
                                db.Entry(spr).State = EntityState.Modified;
                            }
                            else if (param.r_piutang_ref_table == "SHM")
                            {
                                tb_t_angsuran_shm_h shm_h;
                                //insert tbl angsuran
                                shm_h = new tb_t_angsuran_shm_h();
                                shm_h.t_angsuran_shm_uid = Guid.NewGuid();
                                shm_h.t_spr_awal_id = spr.t_spr_awal_id;
                                shm_h.t_spr_id = spr.t_spr_id;
                                shm_h.m_cust_id = spr.m_cust_id;
                                shm_h.m_item_id = spr.m_item_id;
                                shm_h.m_company_id = spr.m_company_id;
                                shm_h.m_proyek_id = spr.m_proyek_id;
                                shm_h.t_angsuran_shm_date = t_tanggal_angsuran;
                                shm_h.t_angsuran_shm_no = genNumber;
                                shm_h.t_angsuran_shm_h_status = "Post";
                                shm_h.t_angsuran_shm_h_note = "Tambah Kartu";
                                shm_h.created_by = Session["UserID"].ToString();
                                shm_h.created_at = GetServerTime();
                                shm_h.updated_at = GetServerTime();
                                shm_h.updated_by = Session["UserID"].ToString();
                                shm_h.posted_at = GetServerTime();
                                shm_h.posted_by = Session["UserID"].ToString();
                                db.tb_t_angsuran_shm_h.Add(shm_h);
                                db.SaveChanges();

                                tb_t_angsuran_shm_d tbldtl;
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    tbldtl = (tb_t_angsuran_shm_d)MappingTable(new tb_t_angsuran_shm_d(), dtDtl[i]);
                                    tbldtl.t_angsuran_shm_d_seq = i + 1;
                                    tbldtl.t_angsuran_shm_due_date = dtDtl[i].r_piutang_ref_due_date;
                                    tbldtl.t_angsuran_shm_d_amt = dtDtl[i].r_piutang_amt;
                                    tbldtl.t_angsuran_shm_d_amt_accum = 0;
                                    tbldtl.t_angsuran_shm_d_status = "Post";
                                    tbldtl.t_angsuran_shm_h_id = shm_h.t_angsuran_shm_h_id;
                                    tbldtl.t_angsuran_shm_d_note = dtDtl[i].r_piutang_note ?? "";
                                    tbldtl.created_by = shm_h.created_by;
                                    tbldtl.created_at = GetServerTime();
                                    db.tb_t_angsuran_shm_d.Add(tbldtl);
                                }
                                db.SaveChanges();

                                db.tb_post_trans.Add(InsertPostTrans(shm_h.m_company_id, shm_h.t_angsuran_shm_uid, "tb_t_angsuran_shm_h", shm_h.t_angsuran_shm_h_id, shm_h.updated_by, shm_h.updated_at));

                                var dtldata = db.tb_t_angsuran_shm_d.Where(d => d.t_angsuran_shm_h_id == shm_h.t_angsuran_shm_h_id).ToList();
                                foreach (var item in dtldata)
                                {
                                    db.tb_r_piutang.Add(Insert_R_Piutang(spr.m_company_id, spr.m_cust_id, spr.m_proyek_id, spr.m_item_id, "SHM", "tb_t_angsuran_shm_d", item.t_angsuran_shm_d_id, spr.t_spr_no, shm_h.t_angsuran_shm_date, item.t_angsuran_shm_due_date, Convert.ToDateTime("1/1/1900".ToString()), m_accoun_id, item.t_angsuran_shm_d_amt, 0m, item.t_angsuran_shm_d_note ?? "", shm_h.posted_by, (DateTime)shm_h.posted_at, "", 0, "", 0, 0, spr.t_spr_id, spr.t_spr_awal_id));
                                }
                                db.SaveChanges();

                                //spr.t_spr_price = param.t_spr_price + dtldata.Sum(r => r.t_angsuran_shm_d_amt);
                                spr.t_shm_amt = dtldata.Sum(r => r.t_angsuran_shm_d_amt);
                                spr.t_shm_type = "Ya";
                                spr.t_shm_type_bayar = param.ddl_type_bayar;
                                spr.t_shm_status = "Post";
                                spr.t_shm_account_id = m_accoun_id;
                                spr.t_shm_total_angsuran = dtldata.Count;
                                db.Entry(spr).State = EntityState.Modified;
                            }
                        }
                        else
                        {
                            if (param.r_piutang_ref_table == "DP")
                            {
                                tb_t_spr = "tb_t_spr_dp";
                                spr.t_spr_price = param.t_spr_price + param.t_amount_kartu;
                                spr.t_dp_account_id = m_accoun_id;
                                spr.t_dp_amt = param.t_amount_kartu;
                                spr.t_dp_type = "Ya";
                                spr.t_dp_type_bayar = param.ddl_type_bayar;
                                spr.t_dp_status = "Post";
                                spr.t_dp_total_angsuran = 0;
                            }
                            else if (param.r_piutang_ref_table == "KT")
                            {
                                tb_t_spr = "tb_t_spr_kt";
                                spr.t_spr_price = param.t_spr_price + param.t_amount_kartu;
                                spr.t_kt_account_id = m_accoun_id;
                                spr.t_kt_amt = param.t_amount_kartu;
                                spr.t_kt_type = "Ya";
                                spr.t_kt_type_bayar = param.ddl_type_bayar;
                                spr.t_kt_status = "Post";
                                spr.t_kt_total_angsuran = 0;
                            }
                            else if (param.r_piutang_ref_table == "KPR")
                            {
                                tb_t_spr = "tb_t_spr";
                                spr.t_spr_price = param.t_spr_price + param.t_amount_kartu;
                                spr.t_piutang_amt = param.t_amount_kartu;
                                spr.t_piutang_type_bayar = param.ddl_type_bayar;
                                spr.t_piutang_status = "Post";
                                spr.t_piutang_total_angsuran = 0;
                            }
                            else if (param.r_piutang_ref_table == "SHM") {
                                tb_t_spr = "tb_t_spr_shm";
                                spr.t_shm_account_id = m_accoun_id;
                                spr.t_shm_amt = param.t_amount_kartu;
                                spr.t_shm_type = "Ya";
                                spr.t_shm_type_bayar = param.ddl_type_bayar;
                                spr.t_shm_status = "Post";
                                spr.t_shm_total_angsuran = 0;
                            } 
                            db.Entry(spr).State = EntityState.Modified;

                            db.tb_r_piutang.Add(Insert_R_Piutang(spr.m_company_id, spr.m_cust_id, spr.m_proyek_id, spr.m_item_id, param.r_piutang_ref_table, tb_t_spr, spr.t_spr_id, spr.t_spr_no, spr.t_spr_date, spr.t_spr_date, Convert.ToDateTime("1/1/1900".ToString()), m_accoun_id, param.t_amount_kartu, 0m, param.r_piutang_ref_table + " "+ param.ddl_type_bayar, spr.posted_by, (DateTime)spr.posted_at, "", 0, "", 0, 0, spr.t_spr_id, spr.t_spr_awal_id));
                            db.SaveChanges();
                        }

                        spr.t_spr_note = $"Tambah Data {param.r_piutang_type_revisi}";
                        spr.t_spr_total_amt = param.t_spr_price - spr.t_utj_amt - spr.t_dp_amt - spr.t_kt_amt + spr.t_shm_amt;
                        db.Entry(spr).State = EntityState.Modified;

                        msg = PostJurnal(spr, param);
                        if (!string.IsNullOrEmpty(msg))
                        {
                            objTrans.Rollback(); result = "failed";
                            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        result = "success";
                        dtDtl = null;
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid, dtDtl }, JsonRequestBehavior.AllowGet);
        }

        public class dt_gl
        {
            public decimal r_gl_amt { get; set; }
            public int m_account_id { get; set; }
            public string r_gl_db_cr { get; set; }
        }

        private string PostJurnal(tb_t_spr tbl, sFilter param)
        {
            var msg = "";
            try
            {
                // INSERT AUTO JURNAL
                var seq = 0; 
                var new_dt_gl = new List<dt_gl>(); var ref_table = new string[] { "" };
                var gl_note = $"PENGAJUAN (NO: {tbl.t_spr_no} | CUSTOMER: {db.tb_m_cust.FirstOrDefault(x => x.m_cust_id == tbl.m_cust_id)?.m_cust_name ?? ""}) - NOTE: {tbl.t_spr_note}";

                var account_id_jual = GetCOAInterface(new List<string> { "HUTANG PENJUALAN" }).FirstOrDefault()?.m_account_id ?? 0;
                var piutang_account_id = 0;
                if(param.r_piutang_ref_table=="KT")
                    piutang_account_id = GetCOAInterface(new List<string> { "PIUTANG KT" }).FirstOrDefault()?.m_account_id ?? 0;   
                else
                    piutang_account_id = GetCOAInterface(new List<string> { "PIUTANG SHM" }).FirstOrDefault()?.m_account_id ?? 0;

                new_dt_gl.Add(new dt_gl { r_gl_amt = param.t_amount_kartu, m_account_id = piutang_account_id, r_gl_db_cr = "D" });
                new_dt_gl.Add(new dt_gl { r_gl_amt = param.t_amount_kartu, m_account_id = account_id_jual, r_gl_db_cr = "C" });

                if (new_dt_gl != null && new_dt_gl.Count() > 0)
                {
                    foreach (var item in new_dt_gl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_spr", tbl.t_spr_id, tbl.t_spr_no, GetServerTime(), item.m_account_id, item.r_gl_db_cr, item.r_gl_amt, gl_note, tbl.created_by, GetServerTime(), tbl.m_cust_id));
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.ToString();
            }
            db.SaveChanges();
            return msg;
        }
        #endregion
    }
}