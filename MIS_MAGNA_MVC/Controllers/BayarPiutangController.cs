﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq; 
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models; 
using System.IO; 
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.Controllers.ClassFunction; 
namespace MIS_MAGNA_MVC.Controllers
{
    public class BayarPiutangController : Controller
    {
        // GET: BayarPiutang
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public BayarPiutangController() { db = new QL_MIS_MAGNAEntities(); db.Database.CommandTimeout = 0; }

        #region Bayar Piutang
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.periodstart = GetServerTime().ToString("01/dd/yyyy");
            ViewBag.periodend = GetServerTime().ToString("MM/dd/yyyy");
            ViewBag.Title = "Pembayaran Angsuran";
            return View();
        }
         
        [HttpPost]
        public ActionResult GetDataIndex(string periodstart, string periodend)
        { 
            sSql = $@"SELECT * FROM ( SELECT h.t_kbm_uid, h.t_kbm_h_id, h.m_cust_id, h.m_item_id, h.m_proyek_id, h.t_kbm_ref_type, t_kbm_no, h.t_kbm_date, ISNULL((SELECT m_cust_name FROM tb_m_cust c WHERE c.m_cust_id=h.m_cust_id), '') m_cust_name, ISNULL((SELECT m_item_name FROM tb_m_item i WHERE i.m_item_id=h.m_item_id),'') m_unit_name, ISNULL((SELECT m_account_desc FROM tb_m_account i WHERE i.m_account_id=h.t_kbm_account_id),'') m_account_name, t_kbm_ref_no, t_kbm_h_note, t_kbm_h_status, t_kbm_h_amt, created_at FROM tb_t_kbm_h h WHERE t_kbm_h_status != ('Force Closed') AND t_kbm_h_id IN (select t_kbm_h_id from tb_t_kbm_d d where t_kbm_ref_table NOT IN ('', 'tb_t_titipan', 'tb_m_interface_d') group by t_kbm_h_id) ) AS t Order By t_kbm_h_id DESC"; 
            return getJsResult(sSql);
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            ViewBag.Title = "Pembayaran Angsuran";
            tb_t_kbm_h tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_kbm_h();
                tbl.m_company_id = db.tb_m_company.FirstOrDefault()?.m_company_id ?? 0;
                tbl.t_kbm_uid = Guid.NewGuid();
                tbl.t_kbm_date = GetServerTime();
                tbl.created_at = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.t_kbm_h_status = "In Process";
                tbl.t_kbm_type = "Proyek";
                tbl.t_kbm_ref_no = "";
                tbl.t_kbm_h_note = "";
                tbl.t_refund_id = 0;
                tbl.m_promo_id = 0;
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_kbm_h.Find(id);
            }

            if (tbl == null) return HttpNotFound();
            //ViewBag.myTime = GetServerTime().ToString("hh:mm:ss");
            ViewBag.flag_tunai = "";
            ViewBag.action = action;
            InitDDLAndField(tbl, action);
            return View(tbl);
        }

        private void InitDDLAndField(tb_t_kbm_h tbl, string action)
        {
            //DDL
            var getCompany = db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList();
            if (action == "New Data") { getCompany = getCompany.Where(m => m.flag == "ACTIVE").ToList(); } 
            ViewBag.m_company_id = new SelectList(getCompany, "valuefield", "textfield", tbl.m_company_id); 
            var sVar = "KAS_SISTEM";
            if (action == "Update Data")
            {
                if (tbl.t_kbm_type_bayar == "KM") { sVar = "KAS_SISTEM"; } 
                else if (tbl.t_kbm_type_bayar == "BM") { sVar = "BANK_SISTEM"; } 
                else if (tbl.t_kbm_type_bayar == "DP") { sVar = "HUTANG LAIN"; }
                else if (tbl.t_kbm_type_bayar == "REFUND") { sVar = "HUTANG REFUND"; }
                else if (tbl.t_kbm_type_bayar == "PROMO") { sVar = tbl.t_kbm_type_bayar; }
            }
            var t_kbm_account_id = new SelectList(GetCOAInterface(new List<string> { sVar }), "m_account_id", "m_account_name", tbl.t_kbm_account_id);
            ViewBag.t_kbm_account_id = t_kbm_account_id; 
            //Field
            ViewBag.m_item_name = db.tb_m_item.FirstOrDefault(w => w.m_item_id == tbl.m_item_id)?.m_item_name ?? "";
            ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(w => w.m_proyek_id == tbl.m_proyek_id)?.m_proyek_name ?? "";
            ViewBag.m_cust_name = db.tb_m_cust.FirstOrDefault(w => w.m_cust_id == tbl.m_cust_id)?.m_cust_name ?? "";

            var r_piutang_amt = (tbl.m_cust_id != 0 ? db.Database.SqlQuery<decimal>($"SELECT ISNULL(ROUND(SUM(r_piutang_amt), 0), 0.0) FROM tb_r_piutang WHERE m_cust_id = {tbl.m_cust_id} AND m_item_id = {tbl.m_item_id} AND r_piutang_status='Post' AND r_piutang_type IN ('{tbl.t_kbm_ref_type}')").FirstOrDefault() : 0);
            ViewBag.r_piutang_amt = r_piutang_amt;

            var r_piutang_pay_amt = (tbl.m_cust_id != 0 ? db.Database.SqlQuery<decimal>($"SELECT ISNULL(SUM(r_piutang_pay_amt), 0.0) FROM tb_r_piutang b WHERE m_cust_id = {tbl.m_cust_id} AND m_item_id = {tbl.m_item_id} AND r_piutang_status='Post' AND r_piutang_type = 'PAYMENT' AND r_piutang_ref_table IN (select r_piutang_ref_table from tb_r_piutang a where a.r_piutang_type IN ('{tbl.t_kbm_ref_type}') AND a.m_cust_id=b.m_cust_id AND a.m_item_id=b.m_item_id Group BY r_piutang_ref_table) AND r_piutang_pay_ref_date <= '{tbl.t_kbm_date}'").FirstOrDefault() : 0);
            if (tbl.t_kbm_type_bayar == "REFUND") {
                ViewBag.t_refund_no = db.tb_t_refund.Where(x => x.t_refund_id == tbl.t_refund_id).FirstOrDefault()?.t_refund_no ?? "";
            }
            else if (tbl.t_kbm_type_bayar == "PROMO") {
                ViewBag.t_refund_no = db.Database.SqlQuery<string>($"select xx.m_item_name from tb_t_promo x Inner Join tb_m_item xx ON xx.m_item_id=x.m_promo_id where x.m_promo_id={tbl.m_promo_id} AND x.t_promo_id={tbl.t_refund_id}").ToList()?.FirstOrDefault() ?? "";
            }
            ViewBag.r_piutang_pay_amt = r_piutang_pay_amt;
            ViewBag.r_piutang_amt_sisa = r_piutang_amt - r_piutang_pay_amt;
        }

        #endregion

        #region Get Data
        [HttpPost]
        public ActionResult GetUnitData(string t_kbm_no, string t_kbm_ref_type, string t_kbm_type_bayar = "")
        {
            sSql = $"Select * from dbo.GetDataPiutang('{t_kbm_ref_type}', '{t_kbm_no}') r WHERE (Round(r.total_piutang, 0) - Round(r.r_piutang_pay_amt, 0)) > 0 ORDER BY m_cust_name";
            if (t_kbm_type_bayar == "PROMO")
            {
                sSql = $@"Select x.t_spr_id, x.t_spr_awal_id, x.t_promo_id, x.m_promo_id
                    , x.m_cust_id, m_cust_name, x.m_proyek_id, m_proyek_name, x.m_item_id, m_item_name
                    , x.t_promo_total_amt total_piutang, 0.0 total_bayar, xx.r_promo_amt r_piutang_amt
                    , xx.r_promo_account_id t_kbm_account_id, Isnull(xx.r_promo_pay_amt, 0.0) r_piutang_pay_amt
                    , xx.r_promo_amt-Isnull(xx.r_promo_pay_amt, 0.0) r_piutang_amt_sisa from tb_t_promo x 
                    Inner Join tb_m_item x1 ON x1.m_item_id=x.m_item_id Inner Join tb_m_cust x2 ON x2.m_cust_id=x.m_cust_id 
                    Inner Join tb_m_proyek x3 ON x.m_proyek_id=x3.m_proyek_id 
                    Cross Apply ( Select r.r_promo_amt, r.r_promo_account_id, Isnull((select SUM(rr.r_promo_pay_amt)
                        from tb_r_promo rr Where rr.r_promo_ref_id=r.r_promo_ref_id AND rr.m_promo_id=r.m_promo_id 
                        AND rr.t_promo_id=r.t_promo_id AND rr.r_promo_type='Payment' AND rr.r_promo_pay_ref_no!='{t_kbm_no}' ), 0.0) r_promo_pay_amt 
                        from tb_r_promo r Where r.m_promo_id=x.m_promo_id AND r.t_promo_id=x.t_promo_id 
                        AND r.r_promo_type='Promo' AND r.r_promo_status='Post' Group BY r.t_promo_id
                        , r.m_promo_id, r.r_promo_amt, r.r_promo_ref_id, r.r_promo_account_id ) xx 
                    Where x.t_promo_status='Post' AND x.t_promo_type_bayar='Tunai'";
            }
            return getJsResult(sSql);
        }

        [HttpPost]
        public ActionResult GetRefundData(int t_spr_awal_id, int m_cust_id, int m_item_id, string t_kbm_type_bayar = "", int m_promo_id = 0)
        {
            sSql = GetQueryString(t_spr_awal_id, m_cust_id, m_item_id, t_kbm_type_bayar, m_promo_id);
            return getJsResult(sSql);
        }

        public class t_kbm_d : tb_t_kbm_d
        {
            public string m_account_code { get; set; }
            public string m_account_name { get; set; }
            public DateTime r_piutang_ref_date { get; set; }
            public DateTime r_piutang_ref_due_date { get; set; }
            public string t_spr_no { get; set; }
            public decimal r_piutang_amt { get; set; }
            public string flag_tunai { get; set; }
        } 

        [HttpPost]
        public ActionResult GetDataDetails(string t_kbm_ref_type, int m_item_id, int t_kbm_h_id, int m_cust_id, int t_spr_id)
        {
            sSql = $"SELECT * FROM dbo.GetDtlBayar('{t_kbm_ref_type}', {t_kbm_h_id}, {db.tb_m_company.FirstOrDefault()?.m_company_id ?? 0}, {m_cust_id}, {m_item_id}, {t_spr_id}) Order By r_piutang_ref_id ";
            return getJsResult(sSql); 
        }

        [HttpPost]
        public ActionResult GetCoaDebet(string t_kbm_type_bayar, string action, string flag_tunai, string sVar = "")
        {
            JsonResult js = null;
            try
            { 
                if (t_kbm_type_bayar == "KM") { sVar = "KAS_SISTEM"; } 
                else if (t_kbm_type_bayar == "BM") { sVar = "BANK_SISTEM"; } 
                else if (t_kbm_type_bayar == "DP") { sVar = "HUTANG LAIN"; }
                else if (t_kbm_type_bayar == "REFUND") { sVar = "HUTANG REFUND"; }
                else if (t_kbm_type_bayar == "PROMO") { sVar = "PROMO"; }
                var tbl = GetCOAInterface(new List<string> { sVar });
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int t_kbm_h_id, int m_cust_id)
        {
            sSql = $"Select * from tb_v_Details_kbm WHERE t_kbm_h_id = {t_kbm_h_id} AND m_cust_id={m_cust_id} Order By t_kbm_d_seq";
            return getJsResult(sSql); 
        }
        #endregion

        #region Save data
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_kbm_h tbl, List<t_kbm_d> dtl, string action)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = ""; var t_kbm_h_note = "";
            var m_account_id = GetCOAInterface(new List<string> { "POTONGAN KPR" }).ToList().FirstOrDefault().m_account_id;
            var sReturnNo = ""; var sReturnState = ""; decimal t_kbm_disc_amt = dtl.Sum(xx => xx.t_kbm_disc_amt); 
            if (tbl.t_kbm_date < GetCutOffdate()) { msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_kbm_date.ToString("dd/MM/yyyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />"; }
                
            if (tbl.m_item_id == 0) { msg += "Silahkan Pilih Unit!<br/>"; }
            if (tbl.t_kbm_account_id == 0) msg += "Silahkan Pilih Akun!<br/>";
            if (string.IsNullOrEmpty(tbl.t_kbm_no)) { tbl.t_kbm_no = ""; }
            if (string.IsNullOrEmpty(tbl.t_kbm_ref_no)) { tbl.t_kbm_ref_no = ""; }
            if (tbl.t_kbm_type_bayar == "PROMO") {
                t_kbm_h_note = $"{db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_promo_id).m_item_name} ({db.tb_t_promo.FirstOrDefault(x => x.t_promo_id == tbl.t_refund_id).t_promo_type_bayar})";
            }
            if (dtl != null)
            {
                foreach (var item in dtl)
                {
                    if (item.t_kbm_d_account_id == 0) { msg += "Akun Tidak Tersedia!<br/>"; }
                    if (item.t_kbm_d_amt == 0) { msg += "Jumlah Tidah boleh 0!<br/>"; }
                    if (tbl.t_kbm_h_status == "Post")
                    {
                        var sisa_piutang = db.tb_r_piutang.Where(x => x.r_piutang_ref_id == item.t_kbm_ref_id && x.r_piutang_ref_table == item.t_kbm_ref_table && x.r_piutang_status == "Post").Sum(y => y.r_piutang_amt - y.r_piutang_pay_amt);
                        if (sisa_piutang <= 0) { msg += $"Keterangan {item.t_kbm_d_note} sudah tidak ada sisa tagihan, silahkan pilih angsuran yg lain!<br/>"; } 
                    }
                }
            }

            if (tbl.t_kbm_h_status == "Post")
            {
                tbl.t_kbm_no = GenerateTransNo($"{tbl.t_kbm_type_bayar}", "r_kb_no", "tb_r_kb", tbl.m_company_id, tbl.t_kbm_date.ToString("MM/dd/yyyy"));
                tbl.posted_at = tbl.created_at;
                tbl.posted_by = Session["UserID"].ToString();
                if (tbl.t_kbm_type_bayar == "DP") { tbl.t_kbm_no = GenerateTransNo($"{tbl.t_kbm_type_bayar}", "t_kbm_no", "tb_t_kbm_h", tbl.m_company_id, tbl.t_kbm_date.ToString("MM/dd/yyyy")); } else if (tbl.t_kbm_type_bayar == "REFUND") { tbl.t_kbm_no = GenerateTransNo($"REF", "t_kbm_no", "tb_t_kbm_h", tbl.m_company_id, tbl.t_kbm_date.ToString("MM/dd/yyyy")); }                    
            }

            if (tbl.t_kbm_type_bayar == "DP" && tbl.t_refund_id == 0) { msg += "Silakan pilih Pakai No. Titipan..<br />"; }
            if (tbl.t_kbm_type_bayar == "REFUND" && tbl.t_refund_id == 0) { msg += "Silakan pilih Pakai No. Titipan..<br />"; }
            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.t_kbm_date = DateTime.Parse(tbl.t_kbm_date.ToString("MM/dd/yyyy") + " " + GetServerTime().ToString("hh:mm:ss"));
                        if (action == "New Data")
                        {
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            tbl.t_kbm_h_note = t_kbm_h_note;
                            tbl.t_kbm_h_amt = dtl.Sum(a => a.t_kbm_d_amt);
                            db.tb_t_kbm_h.Add(tbl);
                        }
                        else
                        {
                            tbl.t_kbm_h_note = t_kbm_h_note;
                            tbl.updated_at = GetServerTime();
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            //cek data yg dihapus
                            var d_id = dtl.Select(s => s.t_kbm_d_id).ToList();
                            var dt_d = db.tb_t_kbm_d.Where(a => a.t_kbm_h_id == tbl.t_kbm_h_id && !d_id.Contains(a.t_kbm_d_id));
                            if (dt_d != null && dt_d.Count() > 0) { db.tb_t_kbm_d.RemoveRange(dt_d); }
                        }
                        db.SaveChanges();
                        
                        if (tbl.t_kbm_h_status == "Post") { db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_kbm_uid, "tb_t_kbm_h", tbl.t_kbm_h_id, tbl.updated_by, tbl.updated_at)); } 

                        tb_t_kbm_d tbldtl;
                        if (dtl != null)
                        {
                            foreach (var item in dtl)
                            {
                                var cek_data = db.tb_t_kbm_d.Where(a => a.t_kbm_d_id == item.t_kbm_d_id);
                                tbldtl = (tb_t_kbm_d)MappingTable(new tb_t_kbm_d(), item);
                                if (cek_data != null && cek_data.Count() > 0) { tbldtl = db.tb_t_kbm_d.FirstOrDefault(a => a.t_kbm_d_id == item.t_kbm_d_id); }
                                tbldtl.t_kbm_h_id = tbl.t_kbm_h_id;
                                tbldtl.t_kbm_d_note = item.t_kbm_d_note ?? "";
                                tbldtl.created_by = tbl.updated_by;
                                tbldtl.created_at = tbl.created_at;
                                tbldtl.t_kbm_d_netto = item.t_kbm_d_amt - item.t_kbm_disc_amt;
                                if (cek_data != null && cek_data.Count() > 0) { db.Entry(tbldtl).State = EntityState.Modified; }
                                else { db.tb_t_kbm_d.Add(tbldtl); }
                                db.SaveChanges();

                                if (tbl.t_kbm_h_status == "Post")
                                {
                                    var t_kbm_ref_type = tbl.t_kbm_ref_type;
                                    int r_piutang_trans_id = db.tb_r_piutang.FirstOrDefault(x => x.r_piutang_type == tbl.t_kbm_ref_type && x.r_piutang_ref_id == item.t_kbm_ref_id)?.r_piutang_trans_id ?? 0;
                                    if (tbl.t_kbm_ref_type == "PB") { t_kbm_ref_type = "Peningkatan"; } 
                                    else if (tbl.t_kbm_ref_type == "JB") { t_kbm_ref_type = "Peningkatan"; }
                                    db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "PAYMENT", item.t_kbm_ref_table, item.t_kbm_ref_id, item.t_spr_no, item.r_piutang_ref_date, item.r_piutang_ref_due_date, tbl.t_kbm_date, item.t_kbm_d_account_id, 0m, item.t_kbm_d_amt, item.t_kbm_d_note ?? "", tbl.created_by, tbl.created_at, "tb_t_kbm_d", tbldtl.t_kbm_d_id, tbl.t_kbm_no, tbl.t_kbm_account_id, 0m, tbl.t_spr_id, tbl.t_spr_awal_id, r_piutang_ref_type: t_kbm_ref_type.ToUpper(), r_piutang_trans_type: t_kbm_ref_type.ToUpper(), r_piutang_trans_id: r_piutang_trans_id));

                                    if (item.t_kbm_disc_amt > 0m)
                                    {
                                        db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "PAYMENT", item.t_kbm_ref_table, item.t_kbm_ref_id, item.t_spr_no, item.r_piutang_ref_date, item.r_piutang_ref_due_date, tbl.t_kbm_date, item.t_kbm_d_account_id, 0m, item.t_kbm_d_amt, $"Diskon {t_kbm_ref_type}", tbl.created_by, GetServerTime(), "tb_t_kbm_d", tbldtl.t_kbm_d_id, tbl.t_kbm_no, m_account_id, item.t_kbm_disc_amt, tbl.t_spr_id, tbl.t_spr_awal_id, r_piutang_ref_type: "DISKON", r_piutang_trans_type: t_kbm_ref_type.ToUpper(), r_piutang_trans_id: r_piutang_trans_id));
                                    }
                                    // Jika ada diskon
                                    if(tbl.t_kbm_h_amt - t_kbm_disc_amt > 0m)
                                    {
                                        var lis_type_bayar = new List<string>() { "DP", "REFUND", "PROMO" };
                                        if (!lis_type_bayar.Contains(tbl.t_kbm_type_bayar))
                                        {
                                            db.tb_r_kb.Add(Insert_R_KB(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "tb_t_kbm_d", tbldtl.t_kbm_d_id, tbl.t_kbm_type_bayar, tbl.t_kbm_no, tbl.t_kbm_date, tbl.t_kbm_account_id, item.t_kbm_d_account_id, item.t_kbm_d_amt - item.t_kbm_disc_amt, 0m, item.t_kbm_d_note ?? "", tbl.created_by, tbl.created_at, tbl.t_spr_id, tbl.t_spr_awal_id, tbl.t_kbm_h_id, tbl.t_kbm_type));
                                        }

                                        if (tbl.t_kbm_type_bayar == "DP")
                                        {
                                            var dt_ref = db.tb_r_hutang.AsNoTracking().FirstOrDefault(x => x.r_hutang_ref_id == (int)tbl.t_refund_id);
                                            db.tb_r_hutang.Add(Insert_R_Hutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "PAYMENT", dt_ref.r_hutang_ref_table, (int)tbl.t_refund_id, dt_ref?.r_hutang_ref_no ?? "", dt_ref?.r_hutang_ref_date ?? item.r_piutang_ref_date, dt_ref?.r_hutang_ref_date ?? item.r_piutang_ref_date, tbl.t_kbm_date, dt_ref?.r_hutang_account_id ?? 0, 0m, tbldtl.t_kbm_d_amt, tbldtl.t_kbm_d_note, tbl.created_by, tbl.created_at, "tb_t_kbm_d", tbldtl.t_kbm_d_id, tbl.t_kbm_no, item.t_kbm_d_account_id, tbl.t_spr_id, tbl.t_spr_awal_id, m_item_old_id: dt_ref?.m_item_old_id ?? 0));
                                        }

                                        if (tbl.t_kbm_type_bayar == "REFUND")
                                        {
                                            var dt_ref = db.tb_r_hutang.AsNoTracking().FirstOrDefault(x => x.r_hutang_ref_id == (int)tbl.t_refund_id && x.r_hutang_ref_table == "tb_t_refund");
                                            db.tb_r_hutang.Add(Insert_R_Hutang(dt_ref.m_company_id, dt_ref.m_cust_id, dt_ref.m_proyek_id, dt_ref.m_item_id, "PAYMENT", dt_ref.r_hutang_ref_table, dt_ref.r_hutang_ref_id, dt_ref.r_hutang_ref_no, dt_ref.r_hutang_ref_date, dt_ref.r_hutang_ref_date, tbl.t_kbm_date, tbldtl.t_kbm_d_account_id, 0m, tbldtl.t_kbm_d_amt, tbldtl.t_kbm_d_note, tbl.created_by, tbl.created_at, "tb_t_kbm_d", tbldtl.t_kbm_d_id, tbl.t_kbm_no, tbldtl.t_kbm_d_account_id, dt_ref.t_spr_id, dt_ref.t_spr_awal_id, m_item_old_id: dt_ref?.m_item_old_id ?? 0)); 
                                        }
                                    }
                                    db.SaveChanges();
                                }
                            } 
                           
                            // INSERT AUTO JURNAL 
                            if (tbl.t_kbm_h_status == "Post")
                            {
                                // Insert Kartu Promo 
                                var dt_promo = db.tb_r_promo.AsNoTracking().FirstOrDefault(x => x.t_promo_id == tbl.t_refund_id && x.m_promo_id == tbl.m_promo_id);
                                if (tbl.t_kbm_type_bayar == "PROMO")
                                {
                                    db.tb_r_promo.Add(Insert_R_Promo(tbl.m_company_id, (int)tbl.t_refund_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, tbl.t_spr_id, tbl.t_spr_awal_id, (int)tbl.m_promo_id, "Payment", dt_promo.r_promo_ref_table, (int)tbl.t_refund_id, dt_promo?.r_promo_ref_no ?? "", db.tb_r_promo.FirstOrDefault(x => x.m_promo_id == tbl.m_promo_id && x.t_promo_id == tbl.t_refund_id).r_promo_ref_date, tbl.t_kbm_account_id, 0m, tbl.t_kbm_date, tbl.t_kbm_h_status, tbl.created_by, tbl.created_at, t_kbm_h_note, dtl.FirstOrDefault().t_kbm_d_account_id, "tb_t_kbm_h", tbl.t_kbm_h_id, tbl.t_kbm_no, tbl.t_kbm_h_amt)); db.SaveChanges();
                                }

                                var seq = 1; var tbl_note = $"Pembayaran Angsuran {tbl.t_kbm_ref_type}"; t_kbm_disc_amt = dtl.Sum(x => x.t_kbm_disc_amt);
                                foreach(var items in dtl) { t_kbm_disc_amt += items.t_kbm_disc_amt; }
                                if (tbl.t_kbm_h_amt - t_kbm_disc_amt > 0m)
                                {
                                    db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbm_h", tbl.t_kbm_h_id, tbl.t_kbm_no, tbl.t_kbm_date, tbl.t_kbm_account_id, "D", tbl.t_kbm_h_amt - t_kbm_disc_amt, tbl_note ?? "", tbl.created_by, tbl.created_at, tbl.m_cust_id));
                                }

                                foreach (var item in dtl)
                                {
                                    if (item.t_kbm_disc_amt > 0)
                                    { 
                                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbm_h", tbl.t_kbm_h_id, tbl.t_kbm_no, tbl.t_kbm_date, m_account_id, "D", item.t_kbm_disc_amt, item.t_kbm_d_note ?? "", tbl.created_by, tbl.created_at, tbl.m_cust_id));
                                    }

                                    db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbm_h", tbl.t_kbm_h_id, tbl.t_kbm_no, tbl.t_kbm_date, item.t_kbm_d_account_id, "C", item.t_kbm_d_amt, item.t_kbm_d_note ?? "", tbl.created_by, tbl.created_at, tbl.m_cust_id));
                                }
                                db.SaveChanges();
                            }
                        }

                        db.SaveChanges(); objTrans.Commit(); hdrid = tbl.t_kbm_h_id.ToString();
                        if (tbl.t_kbm_h_status == "Post") { sReturnNo = tbl.t_kbm_no; sReturnState = "terposting"; }
                        else { sReturnNo = "draft " + tbl.t_kbm_h_id.ToString(); sReturnState = "tersimpan"; }
                        msg = $"Data {sReturnState} dengan No. {sReturnNo}<br />"; result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += $"Entity of type {eve.Entry.Entity.GetType().Name} in state {eve.Entry.State} has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors) { err += $"- Property: {ve.PropertyName}, Error: {ve.ErrorMessage}<br />"; } 
                        }
                        msg = err;
                    }
                    catch (Exception ex) { objTrans.Rollback(); msg = ex.ToString(); }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); } 
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) { return RedirectToAction("NotAuthorize", "Profile"); }                
            tb_t_kbm_h tbl = db.tb_t_kbm_h.Find(id);
            string result = "success"; string msg = "";
            if (tbl == null) { result = "failed"; msg = "Data tidak ditemukan!"; } 
            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.tb_t_kbm_d.Where(a => a.t_kbm_h_id == tbl.t_kbm_h_id);
                        db.tb_t_kbm_d.RemoveRange(trndtl); db.SaveChanges();
                        db.tb_t_kbm_h.Remove(tbl); db.SaveChanges(); objTrans.Commit();
                    }
                    catch (Exception ex) { objTrans.Rollback(); result = "failed"; msg = ex.ToString(); }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Print Out
        public class r_piutang
        {
            public decimal r_piutang_pay_amt { get; set; }
            public decimal r_piutang_sisa_amt { get; set; }
            public decimal r_piutang_pay_amt_kpr { get; set; }
            public decimal r_piutang_pay_amt_dp { get; set; }
            public decimal r_piutang_pay_amt_kt { get; set; }
            public decimal t_refund_pay_amt_dp { get; set; }
            public decimal t_refund_pay_amt_kpr { get; set; }
            public decimal t_refund_pay_amt_kt { get; set; }           
            public string t_kbm_ref_type { get; set; }
            public int r_piutang_ref_id { get; set; }
            public int t_kbm_d_id { get; set; }
        } 

        public ActionResult PrintReport(int ids, string format = "")
        {
            string rptname = $"PrintKBM_KPR"; tb_t_kbm_h tbl = db.tb_t_kbm_h.Find(ids);
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermission(ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) { return RedirectToAction("NotAuthorize", "Profile"); }
            if (format == "SHM") { rptname = $"PrintKBM"; }
            if ((new List<string>() { "PB", "JB" }).Contains(format)) { rptname = $"PrintPM"; }
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("userID", Session["UserID"].ToString());
            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), $"rpt{rptname}.rpt")); 

            if (format == "SHM")
            {
                sSql = $@"SELECT t_kbm_h_id, m_cust_name, m_cust_phone1, t_kbm_date
                , t_kbm_no, t_kbm_type_bayar, t_kbm_h_amt, m_unit_name, m_account_name
                , t_kbm_ref_no, t_kbm_h_note, t_kbm_h_status, t_kbm_d_seq, t_kbm_d_id, t_kbm_ref_id
                , t_spr_no, r_piutang_ref_date, r_piutang_ref_due_date, r_piutang_amt, t_kbm_d_amt
                , t_kbm_disc_amt, t_kbm_d_netto, t_kbm_d_note, 0 r_piutang_pay_dp_amt, 0 r_piutang_pay_kt_amt
                , r_piutang_pay_shm_amt r_piutang_pay_kpr_amt, r_piutang_sisa_kpr_amt, 0 t_utj_amt
                , t_kt_amt, r_piutang_shm_amt t_spr_price, r_piutang_type, r_piutang_pay_shm_amt, r_piutang_shm_amt 
                from tb_v_print_kbm where t_kbm_h_id IN ({ids}) AND r_piutang_type='SHM' ORDER BY t_kbm_h_id";
            }
            else if ((new List<string>() { "PB", "JB" }).Contains(format))
            {
                sSql = $@"SELECT *, 0.0 r_piutang_bayar, 0.0 r_piutang_os FROM tb_v_print_kbm_PM 
                where t_kbm_h_id IN ({ids}) ORDER BY t_kbm_h_id, t_kbm_d_seq Asc";
            }
            else if ((new List<string>() { "KPR", "KT", "DP" }).Contains(format))
            {
                sSql = $@"SELECT *,0.0 r_piutang_bayar, 0.0 r_piutang_os, 0.0 r_piutang_pay_amt_kpr
                , 0.0 r_piutang_pay_amt_dp, 0.0 r_piutang_pay_amt_kt from tb_v_print_kbm_kpr 
                where t_kbm_h_id IN ({ids}) ORDER BY t_kbm_h_id, t_kbm_d_seq Asc"; 
            }  
            var dt = new ClassConnection().GetDataTable(sSql, "datatable");
            report.SetDataSource(dt);
            if (rptparam.Count > 0) { foreach (var item in rptparam) { report.SetParameterValue(item.Key, item.Value); } }
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing) { if (disposing) { db.Dispose(); } base.Dispose(disposing); }
        #endregion
    }
}