﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class PembayaranRefundUnitController : Controller
    {
        // GET: PembayaranRefundUnit
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        public PembayaranRefundUnitController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }
        #region Pembayaran Refund Unit
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            InitAdvFilterIndex(modfil, "tb_t_kbk_h", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE isapptrans=0 AND isposttrans=0 ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( SELECT h.t_kbk_uid, h.t_kbk_h_id, t_kbk_no, h.t_kbk_date, ISNULL((SELECT m_item_name FROM tb_m_item i WHERE i.m_item_id=h.m_item_id),'') m_unit_name, ISNULL((SELECT m_account_desc FROM tb_m_account i WHERE i.m_account_id=h.t_kbk_account_id),'') m_account_name, t_kbk_ref_no, t_kbk_h_note, t_kbk_h_status, ISNULL((SELECT i.m_cust_name FROM tb_m_cust i WHERE i.m_cust_id=h.m_cust_id), '') m_cust_name, h.created_at, h.t_kbk_h_amt FROM tb_t_kbk_h h Where t_kbk_type='Refund' ) AS t";
            var sFixedFilter = "";
            var sOrder_by = " t.t_kbk_h_id DESC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_t_kbk_h tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_kbk_h();
                tbl.t_kbk_uid = Guid.NewGuid();
                tbl.t_kbk_date = GetServerTime();
                tbl.created_at = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.t_kbk_h_status = "In Process";
                tbl.t_refund_id = 0;
                tbl.m_promo_id = 0;
                tbl.m_promo_id = 0;
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_kbk_h.Find(id);
            }

            if (tbl == null) return HttpNotFound(); 
            ViewBag.action = action;
            InitDDLAndField(tbl, action);
            return View(tbl);
        }

        private void InitDDLAndField(tb_t_kbk_h tbl, string action)
        {
            //DDL
            var getCompany = db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList();
            if (action == "New Data")
                getCompany = getCompany.Where(m => m.flag == "ACTIVE").ToList();
            ViewBag.m_company_id = new SelectList(getCompany, "valuefield", "textfield", tbl.m_company_id);

            var sVar = "KAS_SISTEM";
            if (action == "Update Data")
            {
                if (tbl.t_kbk_type_bayar == "KK")
                    sVar = "KAS_SISTEM";
                else
                    sVar = "BANK_SISTEM";
            }
            var t_kbk_account_id = new SelectList(GetCOAInterface(new List<string> { sVar }), "m_account_id", "m_account_name", tbl.t_kbk_account_id);
            ViewBag.t_kbk_account_id = t_kbk_account_id;

            //Field
            ViewBag.m_item_name = db.tb_m_item.FirstOrDefault(w => w.m_item_id == tbl.m_item_id)?.m_item_name ?? "";
            ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(w => w.m_proyek_id == tbl.m_proyek_id)?.m_proyek_name ?? "";
            ViewBag.m_cust_name = db.tb_m_cust.FirstOrDefault(w => w.m_cust_id == tbl.m_cust_id)?.m_cust_name ?? "";
        }

        [HttpPost]
        public ActionResult InitDDLAccount(string t_kbk_type_bayar, string action)
        {
            JsonResult js = null;
            try
            {
                var sVar = "";
                if (t_kbk_type_bayar == "KK") sVar = "KAS_SISTEM";
                else sVar = "BANK_SISTEM";

                var tbl = GetCOAInterface(new List<string> { sVar });
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                } 
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetUnitData()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT rp.t_spr_awal_id, rp.t_spr_id, rf.t_refund_id, rp.r_hutang_ref_no, rp.m_cust_id, m_cust_name, rp.m_proyek_id, m_proyek_name, rp.m_item_id, m_item_name, SUM(rp.r_hutang_amt) r_hutang_amt, Isnull(d.r_hutang_pay_amt, 0.0) r_hutang_pay_amt, SUM(rp.r_hutang_amt) - Isnull(d.r_hutang_pay_amt, 0.0) r_hutang_sisa_amt FROM tb_r_hutang rp INNER JOIN tb_m_cust c ON c.m_cust_id = rp.m_cust_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = rp.m_proyek_id INNER JOIN tb_m_item i ON i.m_item_id = rp.m_item_id INNER JOIN tb_t_refund rf ON rf.t_refund_no=rp.r_hutang_ref_no AND rf.t_refund_type='Batal SPR' OUTER APPLY ( Select SUM(r_hutang_pay_amt) r_hutang_pay_amt From tb_r_hutang x Where x.r_hutang_type='Payment' AND rp.m_item_id=x.m_item_id AND rp.m_cust_id=x.m_cust_id Group By x.m_item_id, x.m_cust_id) d WHERE rp.r_hutang_ref_table IN ('tb_t_refund', 'tb_t_angsuran_refund_d', 'tb_t_refund_sa') AND rp.r_hutang_type='Refund' GROUP BY rp.t_spr_awal_id, rp.t_spr_id, rf.t_refund_id, rp.r_hutang_ref_no, rp.m_cust_id, m_cust_name, rp.m_proyek_id, m_proyek_name, rp.m_item_id, m_item_name, Isnull(d.r_hutang_pay_amt, 0.0) HAVING SUM(rp.r_hutang_amt) - Isnull(d.r_hutang_pay_amt, 0.0) > 0 UNION ALL SELECT rp.t_spr_awal_id, rp.t_spr_id, rf.t_refund_id, rp.r_hutang_ref_no, rp.m_cust_id, m_cust_name, rp.m_proyek_id, m_proyek_name, rp.m_item_id, m_item_name, SUM(rp.r_hutang_amt) r_hutang_amt, Isnull(d.r_hutang_pay_amt, 0.0) r_hutang_pay_amt, SUM(rp.r_hutang_amt) - Isnull(d.r_hutang_pay_amt, 0.0) r_hutang_sisa_amt FROM tb_r_hutang rp INNER JOIN tb_m_cust c ON c.m_cust_id = rp.m_cust_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = rp.m_proyek_id INNER JOIN tb_m_item i ON i.m_item_id = rp.m_item_id INNER JOIN tb_t_refund rf ON rf.t_refund_id=rp.r_hutang_ref_id OUTER APPLY ( Select SUM(r_hutang_pay_amt) r_hutang_pay_amt From tb_r_hutang x Where x.r_hutang_type='Payment' AND rp.m_item_id=x.m_item_id AND rp.m_cust_id=x.m_cust_id Group By x.m_item_id, x.m_cust_id ) d WHERE rp.r_hutang_ref_table IN ('tb_t_spr_new') AND rp.r_hutang_type='Refund' AND rf.t_refund_type IN ('Ganti Unit', 'Ganti Harga') AND rf.t_refund_ref_table IN ('tb_t_spr_new','tb_t_spr') GROUP BY rp.t_spr_awal_id, rf.t_refund_id, rp.t_spr_id, rp.r_hutang_ref_no, rp.m_cust_id, m_cust_name, rp.m_proyek_id, m_proyek_name, rp.m_item_id, m_item_name, Isnull(d.r_hutang_pay_amt, 0.0) HAVING SUM(rp.r_hutang_amt) - Isnull(d.r_hutang_pay_amt, 0.0) > 0"; 
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class t_kbk_d : tb_t_kbk_d
        {
            public int m_item_old_id { get; set; }
            public string m_account_code { get; set; }
            public string m_account_name { get; set; }
            public DateTime r_hutang_ref_date { get; set; }
            public string r_hutang_ref_no { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(int m_item_id, int t_spr_awal_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT 0 t_kbk_d_seq, r.r_hutang_ref_id t_kbk_ref_id, r.m_item_old_id, r.r_hutang_ref_no, r_hutang_ref_date, r.r_hutang_account_id t_kbk_d_account_id, d.m_account_code, d.m_account_desc m_account_name, Isnull(x.r_hutang_pay_amt, 0.0) r_hutang_pay_amt, r.r_hutang_amt - Isnull(Isnull(x.r_hutang_pay_amt, 0.0), 0.0) t_kbk_d_amt, Isnull(x.r_hutang_pay_amt, 0.0) r_hutang_pay_amt, 0.0 t_kbk_d_netto, r.r_hutang_note t_kbk_d_note, r.r_hutang_ref_table t_kbk_ref_table FROM tb_r_hutang r INNER JOIN tb_m_account d ON d.m_account_id = r.r_hutang_account_id OUTER APPLY ( SELECT SUM(r_hutang_pay_amt) r_hutang_pay_amt FROM tb_r_hutang x WHERE x.r_hutang_type = 'Payment' AND x.r_hutang_ref_id = r.r_hutang_ref_id AND r.r_hutang_ref_table = x.r_hutang_ref_table AND x.m_item_id = r.m_item_id AND r.m_cust_id = x.m_cust_id AND r.m_proyek_id = x.m_proyek_id ) x WHERE r.m_item_id = {m_item_id} AND r.t_spr_awal_id = {t_spr_awal_id} AND r.r_hutang_type = 'Refund' AND r.r_hutang_amt - Isnull(x.r_hutang_pay_amt, 0.0) > 0 ORDER BY r_hutang_ref_id"; 
                var tbl = db.Database.SqlQuery<t_kbk_d>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int t_kbk_h_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT d.t_kbk_d_seq, r.r_hutang_ref_no, d.t_kbk_ref_table, r.r_hutang_ref_date, d.t_kbk_ref_id, d.t_kbk_d_account_id, id.m_account_code, id.m_account_desc m_account_name, t_kbk_d_amt, t_kbk_d_amt t_kbk_d_netto, t_kbk_d_note, r.m_item_old_id FROM tb_t_kbk_d d INNER JOIN tb_m_account id ON id.m_account_id = d.t_kbk_d_account_id INNER JOIN tb_r_hutang r ON r.r_hutang_ref_id = d.t_kbk_ref_id AND d.t_kbk_ref_table=r.r_hutang_ref_table AND r.r_hutang_pay_ref_id=0 Where d.t_kbk_ref_table IN ('tb_t_refund', 'tb_t_angsuran_refund_d', 'tb_t_spr_new') AND t_kbk_h_id = {t_kbk_h_id} ORDER BY d.t_kbk_d_seq";
                var tbl = db.Database.SqlQuery<t_kbk_d>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_kbk_h tbl, List<t_kbk_d> dtl, string action)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";
            var servertime = GetServerTime();

            if (tbl.m_item_id == 0) msg += "Silahkan Pilih Unit!<br />";
            if (tbl.t_kbk_account_id == 0) msg += "Silahkan Pilih Akun!<br/>"; 
            if (tbl.t_kbk_date < GetCutOffdate())
                msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_kbk_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />";
            if (string.IsNullOrEmpty(tbl.t_kbk_no)) tbl.t_kbk_no = "";
            if (string.IsNullOrEmpty(tbl.t_kbk_ref_no)) tbl.t_kbk_ref_no = "";
            if (string.IsNullOrEmpty(tbl.t_kbk_h_note)) tbl.t_kbk_h_note = "";

            if (dtl != null)
            {
                foreach (var item in dtl)
                {
                    if (item.t_kbk_d_account_id == 0) msg += "Akun Tidak Tersedia!<br/>";
                    if (item.t_kbk_d_amt == 0) msg += "Jumlah Tidah boleh 0!<br/>";
                    else if (tbl.t_kbk_h_status == "Post")
                    {
                        var sisa_piutang = db.tb_r_hutang.Where(x => x.r_hutang_ref_id == item.t_kbk_ref_id && x.r_hutang_ref_table == item.t_kbk_ref_table).Sum(y => y.r_hutang_amt - y.r_hutang_pay_amt);
                        if (sisa_piutang <= 0) msg += $"keterangan {item.t_kbk_d_note} sudah tidak ada sisa tagihan, silahkan pilih angsuran yg lain!<br/>";
                    }
                }
            }

            if (tbl.t_kbk_h_status == "Post")
            {
                tbl.t_kbk_no = GenerateTransNo($"{tbl.t_kbk_type_bayar}", "r_kb_no", "tb_r_kb", tbl.m_company_id, tbl.t_kbk_date.ToString("MM/dd/yyyy"));
                tbl.posted_at = servertime;
                tbl.posted_by = Session["UserID"].ToString();
            }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            db.tb_t_kbk_h.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            //cek data yg dihapus
                            var d_id = dtl.Select(s => s.t_kbk_d_id).ToList();
                            var dt_d = db.tb_t_kbk_d.Where(a => a.t_kbk_h_id == tbl.t_kbk_h_id && !d_id.Contains(a.t_kbk_d_id));
                            if (dt_d != null && dt_d.Count() > 0) 
                                db.tb_t_kbk_d.RemoveRange(dt_d);
                        }
                        db.SaveChanges();

                        if (tbl.t_kbk_h_status == "Post")
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_kbk_uid, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.updated_by, tbl.updated_at));

                        tb_t_kbk_d tbldtl;
                        if (dtl != null)
                        {
                            foreach (var item in dtl)
                            {
                                var cek_data = db.tb_t_kbk_d.Where(a => a.t_kbk_d_id == item.t_kbk_d_id);
                                tbldtl = (tb_t_kbk_d)MappingTable(new tb_t_kbk_d(), item);
                                if (cek_data != null && cek_data.Count() > 0) tbldtl = db.tb_t_kbk_d.FirstOrDefault(a => a.t_kbk_d_id == item.t_kbk_d_id);
                                tbldtl.t_kbk_ref_table = item.t_kbk_ref_table ?? "";
                                tbldtl.t_kbk_h_id = tbl.t_kbk_h_id;
                                tbldtl.t_kbk_d_note = item.t_kbk_d_note ?? "";
                                tbldtl.created_by = tbl.updated_by;
                                tbldtl.created_at = tbl.updated_at;
                                if (cek_data != null && cek_data.Count() > 0) 
                                    db.Entry(tbldtl).State = EntityState.Modified;
                                else
                                    db.tb_t_kbk_d.Add(tbldtl);
                                db.SaveChanges();

                                if (tbl.t_kbk_h_status == "Post")
                                {
                                    //if (tbl.t_kbk_date >= GetCutOffdate())
                                    //{
                                    db.tb_r_kb.Add(Insert_R_KB(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "tb_t_kbk_d", tbldtl.t_kbk_d_id, tbl.t_kbk_type_bayar, tbl.t_kbk_no, tbl.t_kbk_date, tbl.t_kbk_account_id, item.t_kbk_d_account_id, 0m, item.t_kbk_d_amt, item.t_kbk_d_note ?? "", tbl.created_by, tbl.created_at, tbl.t_spr_id, tbl.t_spr_awal_id, tbl.t_kbk_h_id, tbl.t_kbk_type));
                                    //    db.SaveChanges();
                                    //}    

                                    db.tb_r_hutang.Add(Insert_R_Hutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "PAYMENT", tbldtl.t_kbk_ref_table, tbldtl.t_kbk_ref_id, tbl.t_kbk_no, item.r_hutang_ref_date, item.r_hutang_ref_date, tbl.t_kbk_date, tbldtl.t_kbk_d_account_id, 0m, tbldtl.t_kbk_d_amt, tbldtl.t_kbk_d_note, tbl.created_by, tbl.created_at, "tb_t_kbk_d", tbldtl.t_kbk_d_id, tbl.t_kbk_no, tbl.t_kbk_account_id, tbl.t_spr_id, tbl.t_spr_awal_id, item.m_item_old_id));
                                }
                            }

                            if (tbl.t_kbk_h_status == "Post")
                            {
                                var seq = 1;
                                var tbl_note = "Pembayaran Refund";
                                foreach (var item in dtl)
                                {
                                    db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.t_kbk_no, tbl.t_kbk_date, item.t_kbk_d_account_id, "D", item.t_kbk_d_amt, item.t_kbk_d_note ?? "", tbl.created_by, tbl.created_at, tbl.m_cust_id));
                                }
                                db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.t_kbk_no, tbl.t_kbk_date, tbl.t_kbk_account_id, "C", tbl.t_kbk_h_amt, tbl_note ?? "", tbl.created_by, tbl.created_at, tbl.m_cust_id));
                                db.SaveChanges();
                            }
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.t_kbk_h_id.ToString();
                        if (tbl.t_kbk_h_status == "Post")
                        {
                            sReturnNo = tbl.t_kbk_no;
                            sReturnState = "terposting";
                        }
                        else
                        {
                            sReturnNo = "draft " + tbl.t_kbk_h_id.ToString();
                            sReturnState = "tersimpan";
                        }
                        msg = "Data " + sReturnState + " dengan No. " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_t_kbk_h tbl = db.tb_t_kbk_h.Find(id);
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data tidak ditemukan!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.tb_t_kbk_d.Where(a => a.t_kbk_h_id == tbl.t_kbk_h_id);
                        db.tb_t_kbk_d.RemoveRange(trndtl); db.SaveChanges();

                        db.tb_t_kbk_h.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            string rptname = "PrintKBK";
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("userID", Session["UserID"].ToString());

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rpt" + rptname + ".rpt"));
            sSql = $"SELECT * FROM tb_v_print_bayar_refund WHERE t_kbk_h_id IN ({ids}) ORDER BY t_kbk_h_id";
            var dt = new ClassConnection().GetDataTable(sSql, "datatable");
            report.SetDataSource(dt);
            if (rptparam.Count > 0)
                foreach (var item in rptparam)
                    report.SetParameterValue(item.Key, item.Value);

            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
 }