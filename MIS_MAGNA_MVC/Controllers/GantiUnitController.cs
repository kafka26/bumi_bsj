﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations.Sql;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Antlr.Runtime.Tree;
using CrystalDecisions.CrystalReports.ViewerObjectModel;
using MIS_MAGNA_MVC.Models;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class GantiUnitController : Controller
    {
        // GET: GantiUnit
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private static readonly int dp_account_id = GetCOAInterface(new List<string> { "PIUTANG DP" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int kt_account_id = GetCOAInterface(new List<string> { "PIUTANG KT" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int shm_account_id = GetCOAInterface(new List<string> { "PIUTANG SHM" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int kpr_account_id = GetCOAInterface(new List<string> { "PIUTANG KPR" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int jual_account_id = GetCOAInterface(new List<string> { "HUTANG PENJUALAN" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int hutang_refund_id = GetCOAInterface(new List<string> { "HUTANG REFUND" }).FirstOrDefault()?.m_account_id ?? 0;
        private static readonly int hutang_lain_id = GetCOAInterface(new List<string> { "HUTANG LAIN" }).FirstOrDefault()?.m_account_id ?? 0;

        public class t_spr : tb_t_spr
        {
            public string action { get; set; }
            public decimal r_piutang_amt_utj { get; set; }
            public decimal r_piutang_pay_amt_utj { get; set; }
            public decimal r_piutang_amt_dp { get; set; }
            public decimal r_piutang_pay_amt_dp { get; set; }
            public decimal r_piutang_amt_kt { get; set; }
            public decimal r_piutang_pay_amt_kt { get; set; }
            public decimal r_piutang_amt_kpr { get; set; }
            public decimal r_piutang_pay_amt_kpr { get; set; }
            public decimal r_piutang_amt { get; set; }
            public decimal r_piutang_pay_amt { get; set; }
            public decimal r_piutang_sisa_amt { get; set; }
            public decimal t_spr_price_new { get; set; }
            public decimal r_piutang_disc_amt { get; set; }
            public decimal r_piutang_amt_total { get; set; }
            public decimal m_item_luas_standart { get; set; }
            public decimal m_item_luas_bangunan { get; set; }
            public decimal m_item_lebih_tanah { get; set; }
            public decimal m_item_luas_tanah { get; set; } 
            public string sResult { get; set; }
            public string m_item_old_name { get; set; }
            public string m_item_name { get; set; }
            public string m_item_flag { get; set; }
            public string m_cust_name { get; set; }
            public string m_proyek_code { get; set; }
            public int m_cluster_id { get; set; }
            public int m_item_type_unit_id { get; set; } 
            public int m_item_new_id { get; set; }
            public int m_proyek_new_id { get; set; }
            public DateTime spr_date_new { get; set; }
           
        }

        public GantiUnitController()
        {
            db = new QL_MIS_MAGNAEntities(); db.Database.CommandTimeout = 0;
        }

        #region Ganti Unit Lunas
        public ActionResult Index()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile"); 
            return View();
        }

        [HttpPost]
        public ActionResult GetDataSPR()
        {
            JsonResult js = null;
            try
            {
                sSql = $"select * from tb_v_spr_rev_unit where 1=1 /*AND t_spr_price-r_piutang_pay_amt Between 0 AND 500*/ order by m_proyek_id, m_item_name";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl.Count > 0)
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        } 

        private string GetNoSPR(tb_t_spr tbl)
        {
            String[] strlist = tbl.t_spr_no.Split(new string[] { "-R" }, 1, StringSplitOptions.RemoveEmptyEntries);
            string sNo = strlist[0].ToString().Trim() + "-R";
            sSql = "SELECT ISNULL(MAX(CAST(REPLACE(t_spr_no, '" + sNo + "', '') AS INTEGER)) + 1, 1) AS IDNEW FROM tb_t_spr WHERE t_spr_no LIKE '" + sNo + "%'";
            string sCounter = GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 0);
            sNo = sNo + sCounter;
            return sNo;
        }  
     
        //AND r.t_spr_id={t_spr_id}
        public List<t_spr> GetListPiutang(tb_t_spr tbl, string ref_type, int t_spr_id = 0)
        {
            sSql = $"SELECT Isnull(SUM(r.r_piutang_amt), 0.0) r_piutang_amt, Isnull(d.r_piutang_pay_amt, 0.0) r_piutang_pay_amt, Isnull(SUM(r.r_piutang_amt), 0.0) - Isnull(d.r_piutang_pay_amt, 0.0) r_piutang_sisa_amt, Isnull(x.r_piutang_disc_amt, 0.0) r_piutang_disc_amt FROM tb_r_piutang r OUTER APPLY ( SELECT Isnull(SUM(r_piutang_pay_amt), 0.0) r_piutang_pay_amt, 0.0 r_piutang_disc_amt FROM tb_r_piutang d WHERE d.r_piutang_status IN ('Post','Cancel','Complete') AND r.m_proyek_id = d.m_proyek_id AND d.r_piutang_type = 'Payment' AND d.m_item_id = r.m_item_id AND r.m_cust_id = d.m_cust_id AND d.r_piutang_ref_table = r.r_piutang_ref_table AND d.r_piutang_ref_type!='DISKON' GROUP BY m_item_id, m_cust_id, m_proyek_id, d.r_piutang_ref_table ) d OUTER APPLY ( SELECT 0.0 r_piutang_pay_amt, Isnull(SUM(r_piutang_disc_amt), 0.0) r_piutang_disc_amt FROM tb_r_piutang d WHERE d.r_piutang_status IN ('Post', 'Cancel', 'Complete') AND r.m_proyek_id = d.m_proyek_id AND d.r_piutang_type = 'Payment' AND d.m_item_id = r.m_item_id AND r.m_cust_id = d.m_cust_id AND d.r_piutang_ref_table = r.r_piutang_ref_table AND d.r_piutang_ref_type!='DISKON' GROUP BY m_item_id, m_cust_id, m_proyek_id, d.r_piutang_ref_table ) x WHERE r_piutang_status IN ('Post','Cancel','Complete') AND r_piutang_type!='Payment' AND r.r_piutang_type='{ref_type}' AND m_item_id={tbl.m_item_id} AND m_cust_id={tbl.m_cust_id} AND m_proyek_id={tbl.m_proyek_id} {(ref_type != "UTJ" ? $" AND r.t_spr_id={t_spr_id}" : "")} GROUP BY m_item_id, m_cust_id, m_proyek_id, x.r_piutang_disc_amt, r.r_piutang_ref_table, d.r_piutang_pay_amt";
            var sList = db.Database.SqlQuery<t_spr>(sSql).ToList();  
            return sList;
        }

        public ActionResult Form(int item_id = 0, int t_spr_id = 0)
        {
            ViewBag.Title = "Ganti Unit / Harga SPR";
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            var tbl = db.Database.SqlQuery<t_spr>($"select * from tb_v_spr_rev_unit Where t_spr_id={t_spr_id} AND m_item_id={item_id}").ToList().FirstOrDefault();

            tbl.m_item_old_id = tbl.m_item_id;
            ViewBag.m_item_id = tbl.m_item_id;
            ViewBag.t_spr_price_new = tbl.t_spr_price; 
            ViewBag.m_item_old_name = tbl.m_item_name ?? "";
            ViewBag.m_item_new_id = "0";
            ViewBag.m_proyek_new_id="0";
            ViewBag.m_item_name = "";
            ViewBag.spr_date_new = GetServerTime().ToString("MM/dd/yyyy");
            ViewBag.m_cust_name = tbl.m_cust_name ?? "";
            ViewBag.m_proyek_code = tbl.m_proyek_code ?? "";

            ViewBag.r_piutang_amt_utj = GetListPiutang(tbl, "UTJ", 0).FirstOrDefault()?.r_piutang_amt ?? 0m;
            ViewBag.r_piutang_pay_amt_utj = GetListPiutang(tbl, "UTJ", 0).FirstOrDefault()?.r_piutang_pay_amt ?? 0m;

            ViewBag.r_piutang_amt_dp = GetListPiutang(tbl, "DP", tbl.t_spr_id).FirstOrDefault()?.r_piutang_amt ?? 0m;
            ViewBag.r_piutang_pay_amt_dp = GetListPiutang(tbl, "DP", tbl.t_spr_id).FirstOrDefault()?.r_piutang_pay_amt ?? 0m;

            ViewBag.r_piutang_amt_kpr = GetListPiutang(tbl, "KPR", tbl.t_spr_id).FirstOrDefault()?.r_piutang_amt ?? 0m;
            ViewBag.r_piutang_pay_amt_kpr = GetListPiutang(tbl, "KPR", tbl.t_spr_id).FirstOrDefault()?.r_piutang_pay_amt ?? 0m;

            ViewBag.r_piutang_amt_kt = GetListPiutang(tbl, "KT", tbl.t_spr_id).FirstOrDefault()?.r_piutang_amt ?? 0m;
            ViewBag.r_piutang_pay_amt_kt = GetListPiutang(tbl, "KT", tbl.t_spr_id).FirstOrDefault()?.r_piutang_pay_amt ?? 0m;

            ViewBag.r_piutang_amt = tbl?.r_piutang_amt ?? 0m;
            ViewBag.r_piutang_pay_amt = tbl?.r_piutang_pay_amt ?? 0m;
            ViewBag.r_piutang_disc_amt = tbl?.r_piutang_disc_amt ?? 0m;
            ViewBag.r_piutang_amt_total = tbl?.r_piutang_pay_amt - tbl?.r_piutang_disc_amt;
            ViewBag.r_piutang_sisa_amt = tbl.t_spr_price - tbl?.r_piutang_pay_amt ?? 0m;
            ViewBag.action = "Update"; InitDDL(tbl); 
            ViewBag.t_spr_guid = Guid.NewGuid();
            ViewBag.sResult = "";
            return View(tbl);
        }

        private void InitDDL(t_spr tbl)
        { 
            ViewBag.m_proyek_new_id = new SelectList(db.tb_m_proyek.Where(a => a.m_proyek_flag == "OPEN").ToList(), "m_proyek_id", "m_proyek_name", tbl.m_proyek_id);
            ViewBag.m_item_type_unit_id = new SelectList(db.tb_m_type_unit.Where(a => a.m_type_unit_flag == "ACTIVE").ToList(), "m_type_unit_id", "m_type_unit_name", tbl.m_item_type_unit_id);
            ViewBag.m_cluster_id = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleField>(GetCluster(tbl.m_proyek_id)).ToList(), "ifield", "sfield", tbl.m_cluster_id);
            ViewBag.m_item_luas_standart = tbl.m_item_luas_standart;
            ViewBag.m_item_luas_bangunan = tbl.m_item_luas_bangunan;
            ViewBag.m_item_lebih_tanah = tbl.m_item_lebih_tanah; 
            ViewBag.m_item_luas_tanah = tbl.m_item_luas_tanah;
        }

        [HttpPost]
        public ActionResult GetShowCoa(t_spr param)
        {
            JsonResult js = null; string sResult = "";
            try
            {
                var sText = ""; var new_jurnal = new List<m_account>();
                decimal m_account_debet_amt = 0m; decimal m_account_credit_amt = 0m;
                var coa_penjualan = db.tb_m_account.Where(x => x.m_account_id == jual_account_id);
                var coa_refund = db.tb_m_account.Where(x => x.m_account_id == hutang_refund_id);

                // Piutang Hutang Penjualan
                new_jurnal.Add(new m_account { m_account_id = jual_account_id, m_account_code = coa_penjualan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_penjualan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = param.t_spr_price, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });

                if(param.r_piutang_amt_utj - param.r_piutang_pay_amt_utj > 0)
                {
                    // Piutang UTJ
                    var coa_utj = db.tb_m_account.Where(x => x.m_account_id == dp_account_id);
                    new_jurnal.Add(new m_account { m_account_id = dp_account_id, m_account_code = coa_refund.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_refund.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = param.r_piutang_amt_utj - param.r_piutang_pay_amt_utj, m_account_dbcr = "C", m_account_note = "" }); ;
                }

                if(param.r_piutang_amt_dp - param.r_piutang_pay_amt_dp > 0)
                {
                    // Piutang DP
                    var coa_dp = db.tb_m_account.Where(x => x.m_account_id == dp_account_id);
                    new_jurnal.Add(new m_account { m_account_id = dp_account_id, m_account_code = coa_dp.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_dp.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = param.r_piutang_amt_dp - param.r_piutang_pay_amt_dp, m_account_dbcr = "C", m_account_note = "" });
                }

                if (param.r_piutang_amt_kt - param.r_piutang_pay_amt_kt > 0)
                {
                    // Piutang KT
                    var coa_kt = db.tb_m_account.Where(x => x.m_account_id == kt_account_id);
                    new_jurnal.Add(new m_account { m_account_id = kt_account_id, m_account_code = coa_kt.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_kt.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = param.r_piutang_amt_kt - param.r_piutang_pay_amt_kt, m_account_dbcr = "C", m_account_note = "" });
                }

                if (param.r_piutang_amt_kpr - param.r_piutang_pay_amt_kpr > 0)
                {
                    // Piutang KPR
                    var coa_kpr = db.tb_m_account.Where(x => x.m_account_id == kpr_account_id);
                    new_jurnal.Add(new m_account { m_account_id = kpr_account_id, m_account_code = coa_kpr.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_kpr.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = param.r_piutang_amt_kpr - param.r_piutang_pay_amt_kpr, m_account_dbcr = "C", m_account_note = "" });
                }

                // Hutang Refund
                new_jurnal.Add(new m_account { m_account_id = hutang_refund_id, m_account_code = coa_refund.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_refund.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = param.r_piutang_pay_amt, m_account_dbcr = "C", m_account_note = "" });

                if (param.t_spr_price_new - param.r_piutang_pay_amt <= 0)
                {
                    // Hutang Refund
                    new_jurnal.Add(new m_account { m_account_id = hutang_refund_id, m_account_code = coa_refund.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_refund.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = param.r_piutang_pay_amt, m_account_credit_amt = 0m, m_account_dbcr = "D", m_account_note = "" });
                    if (param.t_spr_price_new - param.r_piutang_pay_amt < 0)
                    {
                        m_account_debet_amt = 0m;
                        m_account_credit_amt = (param.t_spr_price_new - param.r_piutang_pay_amt) * -1; 
                    }
                    else
                    {
                        m_account_debet_amt = (param.t_spr_price_new - param.r_piutang_pay_amt);
                        m_account_credit_amt = 0m; 
                    }
                    // hutang Penjualan
                    new_jurnal.Add(new m_account { m_account_id = jual_account_id, m_account_code = coa_penjualan.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_penjualan.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = param.t_spr_price_new, m_account_dbcr = "C", m_account_note = "" });

                    // Lebih Bayar 
                    if (param.t_spr_price_new - param.r_piutang_pay_amt != 0m)
                    {
                        var coa_lain = db.tb_m_account.Where(x => x.m_account_id == hutang_lain_id);
                        new_jurnal.Add(new m_account { m_account_id = hutang_lain_id, m_account_code = coa_lain.FirstOrDefault()?.m_account_code ?? "", m_account_desc = coa_lain.FirstOrDefault()?.m_account_desc ?? "", m_account_debet_amt = 0m, m_account_credit_amt = (param.t_spr_price_new - param.r_piutang_pay_amt) < 0 ? (param.t_spr_price_new - param.r_piutang_pay_amt) * -1 : (param.t_spr_price_new - param.r_piutang_pay_amt), m_account_dbcr = "C", m_account_note = "" });
                    } 
                }

                var tbl = new_jurnal.ToList();
                if (param.m_item_id == param.m_item_new_id && param.t_spr_price == param.t_spr_price_new)
                {
                    sText = $"<p>Anda tidak melakukan perubahan data apapun</p>";
                    sText += $"<pre>Unit lama {param.m_item_old_name = db.tb_m_item.FirstOrDefault(x => x.m_item_id == param.m_item_old_id)?.m_item_name ?? ""} = Unit Baru {db.tb_m_item.FirstOrDefault(x => x.m_item_id == param.m_item_new_id)?.m_item_name ?? ""}</pre>";
                    sText += $"<pre>Harga Lama = {toMoney(param.t_spr_price)} = Harga baru {toMoney(param.t_spr_price_new)}</pre>";
                    sText += $"<pre>pastikan anda melakukan perubahan data yang sesuai..</pre>"; sResult = "no";
                    js = Json(new { result = $"{sResult}", stext = $"{sText}" }, JsonRequestBehavior.AllowGet);
                }    
                else
                {
                    if (param.m_item_id != param.m_item_new_id && param.t_spr_price == param.t_spr_price_new) {
                        sResult = "ganti_unit_harga_sama"; sText = "Apakah anda yakin akan melakukan perubahan ganti unit, harga tetap?";
                        js = Json(new { result = sResult, stext = $"{sText}", tbl }, JsonRequestBehavior.AllowGet);
                    }
                    else if (param.t_spr_price != param.t_spr_price_new)
                    {
                        sResult = "ganti_harga"; tbl = new_jurnal.ToList();
                        sText = "Setelah klik tombol Update data, silakan lanjutkan data ini ke menu SPR GANTI HARGA / UNIT pada modul PENGAJUAN <br>, Apakah anda yakin akan melakukan perubahan (ganti harga / unit)?";
                        js = Json(new { result = sResult, stext = $"{sText}", tbl }, JsonRequestBehavior.AllowGet);
                    }
                }
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        private string GetCluster(int m_proyek_id)
        {
            return $"select m_cluster_id ifield, m_cluster_name sfield from tb_m_cluster Where m_proyek_id={m_proyek_id} Order By m_cluster_name";
        }

        [HttpPost]
        public ActionResult GetDataCluster(int m_proyek_id)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.Database.SqlQuery<ReportModels.DDLDoubleField>(GetCluster(m_proyek_id)).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataItem(t_spr param)
        {
            JsonResult js = null;
            try
            { 
                sSql = $"select i.m_item_id, i.m_item_name, p.m_proyek_id, p.m_proyek_code, c.m_cluster_name, t.m_type_unit_name, i.m_item_luas_bangunan, i.m_item_luas_standart, i.m_item_luas_tanah, i.m_item_flag from tb_m_item i Inner Join tb_m_proyek p ON p.m_proyek_id=i.m_proyek_id Inner Join tb_m_cluster c ON c.m_cluster_id=i.m_cluster_id Inner Join tb_m_type_unit t ON t.m_type_unit_id=i.m_item_type_unit_id Where 1=1 {(param.m_item_name != "" ? $" AND m_item_name = '{param.m_item_name}'" : "")} Order By p.m_proyek_id, i.m_item_name"; 
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0) {
                    var dtunit = db.tb_m_item.Where(x=>x.m_item_name==param.m_item_name).FirstOrDefault(); 
                    var sName = dtunit?.m_item_name ?? ""; var sFlag = dtunit?.m_item_flag ?? "";
                    js = Json(new {
                        result = $"Unit {sName} yang anda cari {(sFlag !="" ? $"sudah {sFlag}" : "tidak ada")}, Silakan Input Unit Baru"
                    }, JsonRequestBehavior.AllowGet);
                } 
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        private string sValidate(t_spr param, tb_t_spr tbl, List<tb_m_item> sItemNew, string msg = "")
        {
            var sCek = sItemNew.Where(x => x.m_item_name == param.m_item_name && x.m_proyek_id == param.m_proyek_new_id && x.m_cluster_id == param.m_cluster_id).ToList();
            if (sCek.Count() > 0 && param.action == "New")
            {
                var mPro = db.tb_m_proyek.Where(x => x.m_proyek_id == param.m_proyek_new_id)?.FirstOrDefault().m_proyek_name ?? "";
                var mCluster = db.tb_m_cluster.Where(x => x.m_cluster_id == param.m_cluster_id)?.FirstOrDefault().m_cluster_name ?? "";
                msg += $"Maaf, Nama Unit <strong>{param.m_item_name}</strong> dengan proyek <strong> {mPro} </strong> dan cluster <strong>{mCluster}</strong> sudah ada, ganti unit dengan nama yang lain!<br />";
            }

            if (tbl.m_item_id != param.m_item_new_id && (sItemNew.FirstOrDefault()?.m_item_flag ?? "") == "Terjual")
                msg = $"<strong>Unit baru {sItemNew.FirstOrDefault().m_item_name} sudah {sItemNew.FirstOrDefault().m_item_flag}, Silakan pilih unit yang lain..!!</strong></br>";
            if (param.m_item_new_id == 0) msg += "<strong>Maaf, silakan pilih unit dulu..!</strong><br />";
            return msg;
        }
         
        [HttpPost, ActionName("GantiUnit")]
        [ValidateAntiForgeryToken]
        public ActionResult GantiUnit(t_spr param, Guid t_spr_guid, List<m_account> dtDtl)
        {
            var tbl = db.tb_t_spr.Find(param.t_spr_id); string result = "success"; string msg = "";
            var sItemOld = db.tb_m_item.AsNoTracking().Where(x => x.m_item_id == tbl.m_item_id);
            var sItemNew = db.tb_m_item.AsNoTracking().Where(x => x.m_item_id == param.m_item_new_id); 
            msg += sValidate(param, tbl, sItemNew.ToList(), ""); 
            var r_piutang = db.tb_r_piutang.Where(x => x.m_item_id == tbl.m_item_id && x.m_proyek_id == tbl.m_proyek_id && x.m_cust_id == tbl.m_cust_id);
             
            if (string.IsNullOrEmpty(msg))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {  
                        if (r_piutang.Count() > 0)
                        {
                            foreach (var item2 in r_piutang)
                            {
                                item2.r_piutang_status = "Cancel";
                                db.Entry(item2).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            
                            msg = PostJurnal(tbl, param, t_spr_guid, dtDtl);
                            if (!string.IsNullOrEmpty(msg))
                            {
                                objTrans.Rollback(); result = "failed";
                                return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        msg = UpdUnit(param, t_spr_guid, sItemOld.Union(sItemNew).ToList(), tbl, msg);
                        if (!string.IsNullOrEmpty(msg))
                        {
                            objTrans.Rollback(); result = "failed";
                            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                        }
                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            else result = "failed";
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("ReplaceUnit")]
        [ValidateAntiForgeryToken]
        public ActionResult ReplaceUnit(t_spr param, Guid t_spr_guid, List<m_account> dtDtl) {
            var tbl = db.tb_t_spr.Find(param.t_spr_id); 
            string result = "success"; string msg = "";
            var sItemOld = db.tb_m_item.AsNoTracking().Where(x => x.m_item_id == tbl.m_item_id);
            var sItemNew = db.tb_m_item.AsNoTracking().Where(x => x.m_item_id == param.m_item_new_id);
            msg += sValidate(param, tbl, sItemNew.ToList(), "");  
            var utj = db.tb_t_utj.Where(x => x.m_item_id == tbl.m_item_id && x.m_cust_id == tbl.m_cust_id && x.m_proyek_id == tbl.m_proyek_id);
            var r_piutang = db.tb_r_piutang.Where(x => x.m_item_id == tbl.m_item_id && x.m_proyek_id == tbl.m_proyek_id && x.m_cust_id == tbl.m_cust_id);
            var rkb = db.tb_r_kb.Where(x => x.m_item_id == tbl.m_item_id && x.m_proyek_id == tbl.m_proyek_id && x.r_kb_ref_table == "tb_t_kbm_d" && x.m_cust_id == tbl.m_cust_id);
            var kbm = db.tb_t_kbm_h.Where(x => x.m_item_id == tbl.m_item_id && x.m_proyek_id == tbl.m_proyek_id && x.m_cust_id == tbl.m_cust_id);
            var rgl = db.tb_r_gl.Where(x => x.m_item_id == tbl.m_item_id && x.m_proyek_id == tbl.m_proyek_id && x.m_cust_id == tbl.m_cust_id); 
            
            if (string.IsNullOrEmpty(msg))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (utj.Count() > 0)
                        {
                            foreach (var dUtj in utj)
                            {
                                dUtj.m_item_id = param.m_item_new_id;
                                dUtj.m_proyek_id = param.m_proyek_new_id;
                                db.Entry(dUtj).State = EntityState.Modified; 
                            }
                            db.SaveChanges();
                        }

                        if (r_piutang.Count() > 0)
                        {
                            foreach (var rPiutang in r_piutang)
                            {
                                rPiutang.m_item_id = param.m_item_new_id;
                                rPiutang.m_proyek_id = param.m_proyek_new_id;
                                db.Entry(rPiutang).State = EntityState.Modified; 
                            }
                            db.SaveChanges();
                        }

                        if (rkb.Count() > 0)
                        {
                            foreach (var r_kb in rkb)
                            {
                                r_kb.m_item_id = param.m_item_new_id;
                                r_kb.m_proyek_id = param.m_proyek_new_id;
                                db.Entry(r_kb).State = EntityState.Modified;
                            } 
                            db.SaveChanges();
                        }

                        if (kbm.Count() > 0)
                        {
                            foreach (var item4 in kbm)
                            {
                                item4.m_item_id = param.m_item_new_id;
                                item4.m_proyek_id = param.m_proyek_new_id;
                                db.Entry(item4).State = EntityState.Modified;
                            }
                            db.SaveChanges();
                        }

                        if (rgl.Count() > 0)
                        {
                            foreach (var item4 in rgl)
                            {
                                item4.m_item_id = param.m_item_new_id;
                                item4.m_proyek_id = param.m_proyek_new_id;
                                db.Entry(item4).State = EntityState.Modified;
                            }
                            db.SaveChanges();
                        }

                        msg = UpdUnit(param, t_spr_guid, sItemOld.Union(sItemNew).ToList(), tbl, msg);
                        if (!string.IsNullOrEmpty(msg))
                        {
                            objTrans.Rollback(); result = "failed";
                            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                        }
                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            else result = "failed";
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        private string PostJurnal(tb_t_spr tbl, t_spr param, Guid t_spr_guid, List<m_account> dtDtl, string msg = "")
        {
            try
            {
                var NoSPR = GetNoSPR(tbl); int seq = 1;
                var No_Ref = GenerateTransNo($"REF", "t_refund_no", "tb_t_refund", tbl.m_company_id, param.spr_date_new.ToString("MM/dd/yyyy"));
                var t_refund_type = "Ganti Harga"; tb_t_refund dtRef;

                if (param.t_spr_price_new != param.r_piutang_pay_amt) { t_refund_type = "Ganti Harga"; }
                else if (tbl.m_item_id != param.m_item_new_id) { t_refund_type = "Ganti Unit"; }
                //Insert SPR Revisi yang sudah Lunas
                if (param.t_spr_price_new - param.r_piutang_pay_amt <= 0)
                {                    
                    var stbl = db.tb_t_spr.Add(Insert_t_spr(t_spr_guid, tbl.m_company_id, tbl.m_cust_id, param.m_proyek_new_id, param.m_item_new_id, param.m_item_id, NoSPR, tbl.t_spr_id, param.t_spr_price_new, 0m, param.r_piutang_pay_amt_dp - param.r_piutang_amt_dp, tbl.t_dp_amt, param.r_piutang_amt_kt - param.r_piutang_pay_amt_kt, tbl.t_kt_amt, param.r_piutang_amt_kpr - param.r_piutang_pay_amt_kpr, tbl.t_piutang_amt, tbl.m_marketing_id, tbl.m_marketing_name, Session["UserID"].ToString(), GetServerTime(), param.spr_date_new, "Ganti Harga"));
                    db.SaveChanges();

                    db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, stbl.m_proyek_id, stbl.m_item_id, "KPR", "tb_t_spr", stbl.t_spr_id, NoSPR, stbl.t_spr_date, stbl.t_spr_date, Convert.ToDateTime("1/1/1900".ToString()), db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == stbl.m_proyek_id).m_account_id, param.t_spr_price_new, 0m, "KPR Tunai", Session["UserID"].ToString(), GetServerTime(), "", 0, "", 0, 0, stbl.t_spr_id, stbl.t_spr_awal_id));
                    db.SaveChanges();

                    db.tb_r_piutang.Add(Insert_R_Piutang(stbl.m_company_id, stbl.m_cust_id, stbl.m_proyek_id, stbl.m_item_id, "PAYMENT", "tb_t_spr", stbl.t_spr_id, NoSPR, stbl.t_spr_date, stbl.t_spr_date, stbl.t_spr_date, db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == stbl.m_proyek_id).m_account_id, 0m, stbl.t_spr_price, "KPR Tunai", Session["UserID"].ToString(), GetServerTime(), "tb_t_kbm_d", stbl.t_spr_id, NoSPR, 0, 0m, stbl.t_spr_id, stbl.t_spr_awal_id));
                    db.SaveChanges();

                    t_refund_type += "Lebih Bayar";
                    dtRef = db.tb_t_refund.Add(Insert_t_refund(stbl.t_spr_uid, stbl.m_company_id, No_Ref, tbl.t_spr_date, stbl.m_cust_id, stbl.m_proyek_id, stbl.m_item_id, param.m_item_new_id, stbl.t_spr_awal_id, t_refund_type, "tb_t_spr", stbl.t_spr_id, param.t_spr_price_new, stbl.t_spr_price, stbl.t_utj_amt, param.r_piutang_pay_amt, param.r_piutang_pay_amt_dp, GetListPiutang(stbl, "DP", stbl.t_spr_id).FirstOrDefault()?.r_piutang_disc_amt ?? 0m, param.r_piutang_amt_dp - param.r_piutang_pay_amt_dp, param.r_piutang_pay_amt_kpr, GetListPiutang(stbl, "KPR", stbl.t_spr_id).FirstOrDefault()?.r_piutang_disc_amt ?? 0m, param.r_piutang_amt_kpr - param.r_piutang_pay_amt_kpr, param.r_piutang_pay_amt_kt, GetListPiutang(stbl, "KT", stbl.t_spr_id).FirstOrDefault()?.r_piutang_disc_amt ?? 0m, param.r_piutang_amt_kt - param.r_piutang_pay_amt_kt, (param.t_spr_price_new - param.r_piutang_pay_amt <= 0 ? param.r_piutang_sisa_amt * -1 : param.r_piutang_sisa_amt), param.r_piutang_disc_amt, hutang_refund_id, $"SPR {t_refund_type}", Session["UserID"].ToString(), stbl.t_spr_date, t_refund_status:"Complete")); db.SaveChanges();

                    db.tb_r_hutang.Add(Insert_R_Hutang(dtRef.m_company_id, dtRef.m_cust_id, dtRef.m_proyek_id, dtRef.m_item_new_id, "Refund", "tb_t_spr_new", dtRef.t_refund_id, dtRef.t_refund_no, dtRef.t_refund_date, dtRef.t_refund_date, Convert.ToDateTime("1/1/1900".ToString()), hutang_lain_id, param.r_piutang_sisa_amt * -1, 0m, $"{t_refund_type}", Session["UserID"].ToString(), GetServerTime(), "", 0, "", 0, t_spr_id: stbl.t_spr_id, t_spr_awal_id: param.t_spr_awal_id, m_item_old_id: param.m_item_id)); db.SaveChanges(); 
                }
                else
                {   
                    dtRef = db.tb_t_refund.Add(Insert_t_refund(t_spr_guid, tbl.m_company_id, No_Ref, param.spr_date_new, tbl.m_cust_id, tbl.m_proyek_id, param.m_item_id, param.m_item_new_id, tbl.t_spr_awal_id, t_refund_type, "tb_t_spr", tbl.t_spr_id, param.t_spr_price_new, tbl.t_spr_price, tbl.t_utj_amt, param.r_piutang_pay_amt, param.r_piutang_pay_amt_dp, GetListPiutang(tbl, "DP", tbl.t_spr_id).FirstOrDefault()?.r_piutang_disc_amt ?? 0m, param.r_piutang_amt_dp - param.r_piutang_pay_amt_dp, param.r_piutang_pay_amt_kpr, GetListPiutang(tbl, "KPR", tbl.t_spr_id).FirstOrDefault()?.r_piutang_disc_amt ?? 0m, param.r_piutang_amt_kpr - param.r_piutang_pay_amt_kpr, param.r_piutang_pay_amt_kt, GetListPiutang(tbl, "KT", tbl.t_spr_id).FirstOrDefault()?.r_piutang_disc_amt ?? 0m, param.r_piutang_amt_kt - param.r_piutang_pay_amt_kt, param.r_piutang_pay_amt, param.r_piutang_disc_amt, hutang_refund_id, $"SPR {t_refund_type}", Session["UserID"].ToString(), param.spr_date_new, "Post")); db.SaveChanges(); 
                }
                // Akhir Insert SPR Revisi yang sudah Lunas

                if (dtDtl.Any())
                {
                    if (param.t_spr_price_new - param.r_piutang_pay_amt <= 0)
                    {
                        foreach (var item in dtDtl)
                        {
                            db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, param.m_proyek_new_id, param.m_item_new_id, seq++, "tb_t_spr_new", dtRef.t_refund_id, dtRef.t_refund_no, dtRef.t_refund_date, item.m_account_id, item.m_account_dbcr, (item.m_account_dbcr == "D" ? item.m_account_debet_amt : item.m_account_credit_amt), $"SPR {t_refund_type}", Session["UserID"].ToString(), GetServerTime(), param.m_cust_id)); db.SaveChanges();
                        }
                    }
                    else
                    {
                        foreach (var item in dtDtl)
                        {
                            db.tb_r_gl.Add(Insert_R_GL(dtRef.m_company_id, dtRef.m_proyek_id, dtRef.m_item_id, seq++, "tb_t_spr_new", dtRef.t_refund_id, dtRef.t_refund_no, dtRef.t_refund_date, item.m_account_id, item.m_account_dbcr, (item.m_account_dbcr == "D" ? item.m_account_debet_amt : item.m_account_credit_amt), $"SPR {t_refund_type}", Session["UserID"].ToString(), GetServerTime(), tbl.m_cust_id)); 
                            db.SaveChanges();
                        }
                    }
                }
                //Akhir Insert Jurnal Akunting
            }
            catch (Exception e)
            {
                msg = e.ToString();
            }
            db.SaveChanges();
            return msg;
        }

        private string UpdUnit(t_spr param, Guid t_spr_guid, List<tb_m_item> sItem, tb_t_spr tbl, string msg = "")
        {
            var sItemOld = sItem.Where(x => x.m_item_id == tbl.m_item_id).FirstOrDefault();
            var sItemNew = sItem.Where(x => x.m_item_id == param.m_item_new_id);
            var tblUnit = new tb_m_item();
            try {
                sItemOld.m_item_flag = "Open";
                db.Entry(sItemOld).State = EntityState.Modified;
                db.SaveChanges();

                if (sItemNew.Count() > 0 && param.action == "Update")
                {
                    var dItem = sItemNew.FirstOrDefault();
                    dItem.m_item_type_unit_id = param.m_item_type_unit_id;
                    dItem.m_item_luas_bangunan = param.m_item_luas_bangunan;
                    dItem.m_item_luas_tanah = param.m_item_luas_tanah;
                    dItem.m_item_luas_standart = param.m_item_luas_standart;
                    dItem.m_item_lebih_tanah = param.m_item_lebih_tanah;
                    dItem.m_item_flag = "Terjual";
                    db.Entry(dItem).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tblUnit.m_company_id = db.tb_m_company.FirstOrDefault(a => a.active_flag == "ACTIVE").m_company_id;
                    tblUnit.m_item_uid = t_spr_guid;
                    tblUnit.m_item_name = param.m_item_name;
                    tblUnit.m_proyek_id = param.m_proyek_new_id;
                    tblUnit.m_item_type = "Unit";
                    tblUnit.m_cluster_id = param.m_cluster_id;
                    tblUnit.m_item_type_unit_id = param.m_item_type_unit_id;
                    tblUnit.m_item_luas_tanah = param.m_item_luas_tanah;
                    tblUnit.m_item_luas_bangunan = param.m_item_luas_bangunan;
                    tblUnit.m_item_luas_standart = param.m_item_luas_standart;
                    tblUnit.m_item_lebih_tanah = param.m_item_lebih_tanah;
                    tblUnit.m_item_flag = "Terjual";
                    tblUnit.created_at = GetServerTime();
                    tblUnit.created_by = Session["UserID"].ToString();
                    tblUnit.last_edited_at = GetServerTime();
                    tblUnit.last_edited_by = Session["UserID"].ToString();
                    db.tb_m_item.Add(tblUnit);
                    db.SaveChanges();

                    param.m_item_new_id = tblUnit.m_item_id;
                    param.m_item_id = tblUnit.m_item_id;
                    param.m_proyek_id = tblUnit.m_proyek_id;
                }
                if (param.sResult == "ganti_unit_harga_sama")
                {
                    tbl.m_item_id = param.m_item_new_id;
                    tbl.m_item_old_id = sItemOld.m_item_id;
                    tbl.m_proyek_id = param.m_proyek_new_id;
                    tbl.t_spr_status = "Post";
                    db.Entry(tbl).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tbl.t_spr_status = "Cancel";
                    tbl.m_item_old_id = param.m_item_id;
                    db.Entry(tbl).State = EntityState.Modified;
                    db.SaveChanges();
                }
                
            }
            catch (Exception e)
            {
                msg = e.ToString();
            }
            db.SaveChanges();
            return msg; 
        }
        
        #endregion

      
    }
}
