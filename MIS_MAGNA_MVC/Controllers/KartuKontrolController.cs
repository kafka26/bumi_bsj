﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using System.Xml;

namespace MIS_MAGNA_MVC.Controllers
{
    public class KartuKontrolController : Controller
    {
        // GET: KartuKontrol
        private QL_MIS_MAGNAEntities db;
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public KartuKontrolController() { db = new QL_MIS_MAGNAEntities(); db.Database.CommandTimeout = 0; }

        public class sFilter
        {
            public string ddltype { get; set; }
            public List<string> ddlstatus { get; set; }
            public int ddlproyek { get; set; }
            public string periodstart { get; set; }
            public string periodend { get; set; }
            public string filterspr { get; set; }
            public string ddlfilter { get; set; }
        }

        #region Kartu Kontrol

        public ActionResult KartuKontrol()
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/KartuKontrol", (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            var proyek = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "ALL" } }.Union(db.tb_m_proyek.Where(x => x.m_proyek_flag == "OPEN").Select(x => new SelectListItem() { Value = x.m_proyek_id.ToString(), Text = x.m_proyek_name })).ToList();
            ViewBag.ddlproyek = new SelectList(proyek, "Value", "Text");
            var dt_status = new List<string> { "Post", "Cancel" }.ToList();
            ViewBag.ddlstatus = new SelectList(dt_status, dt_status);
            return View();
        }

        [HttpPost]
        public ActionResult GetModalData(sFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                if (modaltype.ToUpper() == "SPR")
                    sSql = $"SELECT 0 seq, m_item_name [Unit], t_spr_no [No. SPR], FORMAT(t_spr_date, 'MM/dd/yyyy') [Tgl. SPR], m_cust_code [Kode], m_cust_name [Customer] FROM tb_v_kartu_detail r WHERE 1=1";
                if (param.ddlproyek != 0) sSql += $" AND m_proyek_id = {param.ddlproyek}";
                if (param.ddlstatus != null && param.ddlstatus.Count() > 0)
                {
                    var ddlstatus = string.Join("','", param.ddlstatus);
                    sSql += $" AND t_spr_status IN ('{ddlstatus}')";
                }
                sSql += $" Group By m_cust_code, m_item_name, t_spr_no, t_spr_date, m_cust_code, m_cust_name ORDER BY r.m_cust_code";
                DataTable tbl = new ClassConnection().GetDataTable(sSql, $"tblModal{modaltype}");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq") { item = (i++).ToString(); } 
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName)) { tblcols.Add(col.ColumnName); } 
                        }
                        tblrows.Add(row);
                    }
                }
                else { result = "Data Not Found."; } 
            }
            catch (Exception e)
            {
                result = e.ToString() + sSql;
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private Decimal sQuery(string s_type, int spr_id, int cust_id, int item_id, int proyek_id, int spr_awal, int t_refund_id = 0, Decimal dAmount = 0m)
        {
            var list_ref_tbl=new List<string>() { "tb_t_angsuran_shm_d", "tb_t_spr_shm", "tb_t_titipan", "tb_t_peningkatan_h_jb", "tb_t_peningkatan_h_pb", "tb_t_peningkatan_d_jb", "tb_t_peningkatan_d_pb" };
            dAmount = db.tb_r_piutang.Where(x => x.t_spr_id == spr_id && x.r_piutang_type == s_type && x.m_cust_id == cust_id && x.m_item_id == item_id && x.m_proyek_id == proyek_id && !list_ref_tbl.Contains(x.r_piutang_ref_table) && x.r_piutang_ref_type != "DISKON" && x.r_piutang_status == "Post").ToList().Sum(x => x.r_piutang_amt);

            if (s_type == "UTJ") {
                dAmount = db.tb_r_piutang.Where(x => x.t_spr_awal_id == spr_awal && x.r_piutang_type == s_type && x.m_cust_id == cust_id && x.m_item_id == item_id && x.m_proyek_id == proyek_id && !list_ref_tbl.Contains(x.r_piutang_ref_table) && x.r_piutang_ref_type != "DISKON").ToList().Sum(x => x.r_piutang_amt); 
            }

            if (s_type == "amt_sisa") { sSql = $"SELECT Isnull(r_piutang_amt_sisa, 0.0) FROM tb_v_termin_kartu_rekap x WHERE x.m_item_id = {item_id} AND x.m_cust_id = {cust_id} AND x.m_proyek_id = {proyek_id} AND x.t_spr_id = {spr_id}"; dAmount = db.Database.SqlQuery<Decimal>(sSql)?.FirstOrDefault() ?? 0m; }

            if (s_type == "refund") { 
                sSql = $"select Isnull(x.t_refund_utj_amt+x.t_refund_pay_amt_dp+x.t_refund_pay_amt_kpr+x.t_refund_pay_amt_kt, 0.0) t_refund_total_amt from tb_t_refund x where x.t_refund_id={t_refund_id}"; dAmount = db.Database.SqlQuery<Decimal>(sSql)?.FirstOrDefault() ?? 0m;
            }

            if (s_type == "piutang_amt") {
                dAmount = db.tb_r_piutang.Where(x => x.t_spr_id == spr_id && x.r_piutang_type != "PAYMENT" && x.m_cust_id == cust_id && x.m_item_id == item_id && x.m_proyek_id == proyek_id && !list_ref_tbl.Contains(x.r_piutang_ref_table) && x.r_piutang_ref_type != "DISKON" && x.r_piutang_status == "Post").ToList().Sum(x => x.r_piutang_amt);
            }

            if (s_type == "pay_amt") {
                dAmount = db.tb_r_piutang.Where(x => x.t_spr_id == spr_id && x.r_piutang_type == "PAYMENT" && x.m_cust_id == cust_id && x.m_item_id == item_id && x.m_proyek_id == proyek_id && !list_ref_tbl.Contains(x.r_piutang_ref_table) && x.r_piutang_ref_type != "DISKON" && x.r_piutang_status == "Post").ToList().Sum(x => x.r_piutang_pay_amt);
            }

            return dAmount;
        }

        private DataTable sData(DataTable dt_rpt)
        {
            for (int i = 0; i < dt_rpt.Rows.Count; i++)
            { 
                string r_piutang_status = "MACET";
                Decimal r_piutang_amt_sisa = dt_rpt.Rows[i]["r_piutang_amt_sisa"].ToString().ToDecimal();
                Decimal r_piutang_pay_amt = dt_rpt.Rows[i]["r_piutang_pay_amt"].ToString().ToDecimal();
                Decimal r_piutang_amt = dt_rpt.Rows[i]["r_piutang_amt"].ToString().ToDecimal();  
                int status_late = dt_rpt.Rows[i]["status_late"].ToString().ToInteger();

                if (status_late <= 3 && Math.Round(r_piutang_amt - r_piutang_pay_amt, 0) > 0m && Math.Round(r_piutang_amt_sisa, 0) <= 1000) { r_piutang_status = "LANCAR"; }
                else if (status_late > 3 && status_late < 6 && Math.Round(r_piutang_amt - r_piutang_pay_amt, 0) > 0m) { r_piutang_status = "KURANG LANCAR"; }
                else if (status_late > 6 && Math.Round(r_piutang_amt - r_piutang_pay_amt, 0) > 0m && Math.Round(r_piutang_amt_sisa, 0) > 1000) { r_piutang_status = "MACET"; }
                else if (Math.Round(r_piutang_amt - r_piutang_pay_amt, 0) <= 0m) { r_piutang_status = "LUNAS"; }
                if(r_piutang_status == "LUNAS") { dt_rpt.Rows[i]["r_piutang_amt_sisa"] = 0m; } 
                dt_rpt.Rows[i]["r_piutang_status"] = r_piutang_status;
            } 
            return dt_rpt;
        }

        [HttpPost]
        public ActionResult ViewReport(sFilter param, string reporttype)
        {
            var rptfile = ""; var rptname = "";
            var Periode = $"{param.periodstart.ToDateTime().ToString("dd/MM/yyyy")}-{param.periodend.ToDateTime().ToString("dd/MM/yyyy")}";
            rptfile = $"RptKartuKontrol{param.ddltype}{reporttype}";
            rptname = $"KARTU_KONTROL{param.ddltype}_{Periode}";
            rptfile = rptfile.Replace("PDF", "");
            var sGroupBy = ""; var sOrderBy = "";
            if (param.ddltype == "DETAIL")
            {
                sSql = $"select *, Case When r_piutang_termin <= 0 AND amt_sisa > 0 then amt_sisa Else 0 End amt_sisa_piutang, Isnull((Select SUM(r_piutang_disc_amt) from tb_r_piutang d Where d.m_item_id=r.m_item_id AND r.m_cust_id=d.m_cust_id AND r.m_proyek_id=d.m_proyek_id AND d.r_piutang_type='PAYMENT' AND d.r_piutang_ref_type='DISKON' AND d.t_spr_id=r.t_spr_id), 0.0) total_disc from ( Select *, Cast(format(r.r_piutang_amt - Isnull((Select sum((r_piutang_pay_amt)) from tb_r_piutang x where r.r_piutang_ref_id = x.r_piutang_ref_id AND x.r_piutang_status = r.r_piutang_status AND r.r_piutang_ref_table = x.r_piutang_ref_table AND r.t_spr_id = x.t_spr_id AND r.m_item_id = x.m_item_id), 0.0), '0') as decimal) amt_sisa FROM tb_v_kartu_detail r ) r Where 1 = 1";
                sOrderBy = " Order By r.m_cust_id, r.t_spr_id, r.m_item_id, seq, r_piutang_ref_id, r_piutang_ref_date, r.created_at";
            }
            else if (param.ddltype == "SUMMARY")
            {
                sSql = $"SELECT * FROM tb_v_report_kartu_sum r Where 1=1";
                sOrderBy = " Order by seq, r_piutang_ref_id, r_piutang_pay_date ";
            }
            else if (param.ddltype == "REKAP")
            {
                sSql = $"select * from tb_v_report_kartu_rekap r where 1=1";
                sOrderBy = " Order By r.m_proyek_id, m_cust_name, m_item_name";
            }

            if (param.ddlproyek != 0) { sSql += $" AND r.m_proyek_id = {param.ddlproyek}"; }
            if (!string.IsNullOrEmpty(param.filterspr)) { sSql += $" AND {param.ddlfilter} IN ('{param.filterspr.Replace(";", "', '")}')"; }

            if (param.ddlstatus != null && param.ddlstatus.Count() > 0)
            {
                var ddlstatus = string.Join("','", param.ddlstatus);
                sSql += $" AND t_spr_status IN ('{ddlstatus}')";
            }
            else sSql += $" AND t_spr_status IN ('Post', 'Complete', 'Cancel')";
            sSql += $" AND t_spr_date >= Cast('{param.periodstart} 0:0:0' as Datetime) AND t_spr_date <= Cast('{param.periodend} 23:59:59' as datetime) {sGroupBy + sOrderBy}";

            var dt_rpt = new ClassConnection().GetDataTable(sSql, $"data");
            if (param.ddltype == "REKAP") { dt_rpt = sData(dt_rpt); } 

            var req = new PrintOutRequest
            {
                rpt_src = new DataTable(),
                rpt_name = rptfile,
                file_name = rptname,
                rpt_param = new Dictionary<string, string>()
            };
            req.rpt_src = dt_rpt;
            req.rpt_param.Add("Periode", Periode);
            req.rpt_param.Add("UserID", Session["UserID"].ToString());
            req.rpt_param.Add("sTitle", $"KARTU KONTROL {param.ddltype.ToUpper()}");
            return setPrintOut(req); 
        }

        #endregion

        #region Kartu Kontrol SHM
        public ActionResult KartuSHM()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/KartuSHM", (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            var proyek = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "ALL" } }.Union(db.tb_m_proyek.Where(x => x.m_proyek_flag == "OPEN").Select(x => new SelectListItem() { Value = x.m_proyek_id.ToString(), Text = x.m_proyek_name })).ToList();
            ViewBag.ddlproyek = new SelectList(proyek, "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult GetModalDataSHM(sFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                if (modaltype.ToUpper() == "SPR")
                {
                    sSql = $"SELECT 0 seq, m_item_name [Unit], t_spr_no [No. SPR], FORMAT(t_spr_date, 'MM/dd/yyyy') [Tgl. SPR], m_proyek_name [Proyek], m_cust_name [Customer], m_cust_addr [Alamat] FROM tb_t_spr s Inner Join tb_m_cust b ON b.m_cust_id=s.m_cust_id Inner Join tb_m_item i ON i.m_item_id=s.m_item_id Inner Join tb_m_proyek p ON p.m_proyek_id=i.m_proyek_id Inner Join tb_m_type_unit t ON t.m_type_unit_id=i.m_item_type_unit_id WHERE s.t_shm_type='Ya'";
                }                    
                if (param.ddlproyek != 0) { sSql += $" AND m_proyek_id = {param.ddlproyek}"; }
                sSql += $" ORDER BY m_proyek_name, m_item_name, m_cust_name";
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblModal" + modaltype);
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq") { item = (i++).ToString(); } row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName)) { tblcols.Add(col.ColumnName); } 
                        }
                        tblrows.Add(row);
                    }
                }
                else { result = "Data Not Found."; } 
            }
            catch (Exception e) { result = e.ToString() + sSql; }
            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ViewReportSHM(sFilter param, string reporttype)
        {
            var rptfile = ""; var rptname = "";
            var Periode = $"{param.periodstart.ToDateTime().ToString("dd/MM/yyyy")}-{param.periodend.ToDateTime().ToString("dd/MM/yyyy")}";
            rptfile = $"RptKartuSHM{param.ddltype}{reporttype}";
            rptname = $"KartuSHM{param.ddltype}_{Periode}";
            rptfile = rptfile.Replace("PDF", "");

            var sField = ""; var sGroupBy = ""; var sOrderBy = "";
            if (param.ddltype == "DETAIL")
            {
                sField = "seq, m_cust_id, r_piutang_ref_id, m_cust_code, m_cust_name, r_piutang_type, m_cust_addr, m_cust_couple_phone, t_spr_no, t_spr_date, t_spr_id, m_proyek_id, m_proyek_name, m_proyek_addr, m_cluster_name, m_item_id, m_item_name, r_piutang_ref_date, r_piutang_ref_due_date, t_spr_price, t_utj_amt, t_dp_amt, t_dp_total_angsuran, t_kt_total_angsuran, r_piutang_amt, r_piutang_note, t_piutang_amt, r_piutang_pay_amt, t_piutang_total_angsuran, r_piutang_pay_ref_date, type_report, Piutang_bulan, t_dp_type_bayar, t_kt_type_bayar, t_piutang_type_bayar, r_piutang_pay_date, r_piutang_ref_table, trans_type, r_piutang_ref_pay_amt, r_piutang_disc_amt, AmtSisa, r.created_at, r.urutan";
                sOrderBy = " ORDER BY r.m_proyek_id, r.m_cust_id, r.m_item_id, seq, r.r_piutang_ref_id, r.urutan, r.r_piutang_pay_date";
                sSql = $"SELECT {sField} FROM tb_v_piutang_shm r Where 1=1 AND t_spr_date >= Cast('{param.periodstart} 0:0:0' as datetime) AND t_spr_date <= Cast('{param.periodend} 23:59:59' as datetime)";
            }
            else if (param.ddltype == "SUMMARY")
            {
                sOrderBy = " Order by seq, r_piutang_ref_id, r_piutang_pay_date ";
                sSql = $"SELECT * FROM tb_v_piutang_shm_sum r Where 1=1 AND t_spr_date >= Cast('{param.periodstart} 0:0:0' as datetime) AND t_spr_date <= Cast('{param.periodend} 23:59:59' as datetime)";
            }

            if (param.ddlproyek != 0) sSql += $" AND r.m_proyek_id = {param.ddlproyek}";
            if (!string.IsNullOrEmpty(param.filterspr)) sSql += $" AND r.m_item_name IN ('{param.filterspr.Replace(";", "', '")}')";
            sSql += sGroupBy + sOrderBy;
             
            var dt_rpt = new ClassConnection().GetDataTable(sSql, $"data");
            var req = new PrintOutRequest
            {
                rpt_src = new DataTable(),
                rpt_name = rptfile,
                file_name = rptname,
                rpt_param = new Dictionary<string, string>()
            };
            req.rpt_src = dt_rpt;
            req.rpt_param.Add("Periode", Periode);
            req.rpt_param.Add("UserID", Session["UserID"].ToString()); 
            return setPrintOut(req); 
        }
        #endregion

        #region Kartu Refund
        public ActionResult KartuRefund()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/KartuRefund", (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            var proyek = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "ALL" } }.Union(db.tb_m_proyek.Where(x => x.m_proyek_flag == "OPEN").Select(x => new SelectListItem() { Value = x.m_proyek_id.ToString(), Text = x.m_proyek_name })).ToList();
            ViewBag.ddlproyek = new SelectList(proyek, "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult GetModalRefund(sFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                if (modaltype.ToUpper() == "SPR")
                    sSql = $"Select 0 seq, i.m_item_name Unit, r.t_refund_no [No. Refund], Format(r.t_refund_date, 'dd/MM/yyyy') Tanggal, c.m_cust_name [Customer], m_proyek_name [Proyek] from tb_t_refund r Inner Join tb_m_item i ON i.m_item_id=r.m_item_id Inner Join tb_m_type_unit t ON t.m_type_unit_id=i.m_item_type_unit_id Inner Join tb_m_cust c ON c.m_cust_id = r.m_cust_id Inner Join tb_m_proyek mp ON mp.m_proyek_id=r.m_proyek_id Where t_refund_type='Batal SPR'";
                if (param.ddlproyek != 0) sSql += $" AND m_proyek_id = {param.ddlproyek}";
                sSql += $" ORDER BY m_proyek_name, m_item_name, m_cust_name";
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblModal" + modaltype);
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString() + sSql;
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public string getquery(string sSql = "")
        {
            sSql = $@"( select p.m_proyek_code, p.m_proyek_name, c.m_cust_name, i.m_item_name, t.m_type_unit_name
            , isnull(spr.t_spr_no, r.t_refund_no) t_spr_no, isnull(spr.t_spr_date, r.t_refund_date) t_spr_date
            , isnull(spr.t_utj_date, '1/1/1900') t_utj_date, r.t_refund_no, r.t_refund_date
            , r.t_refund_pay_amt, r.t_refund_cost_amt, r.t_refund_cost_other_amt
            , r.t_refund_pay_amt - (r.t_refund_cost_amt + r.t_refund_cost_other_amt + r.t_refund_disc_amt) t_refund_amt
            , r.t_refund_total_amt, (r.t_refund_cost_amt + r.t_refund_cost_other_amt + r.t_refund_disc_amt) t_refund_total_cost_amt
            , r.t_refund_note, r.m_proyek_id, r.m_cust_id, r.m_item_id, r.t_spr_awal_id, Isnull(spr.t_spr_id, r.t_spr_awal_id) t_spr_id
            , Isnull(rr.r_hutang_amt, 0.0) r_hutang_amt, Isnull(rr.r_hutang_pay_amt, 0.0) r_hutang_pay_amt
            , Isnull(rr.r_hutang_amt, 0.0)-Isnull(rr.r_hutang_pay_amt, 0.0) t_refund_sisa_amt 
            from tb_t_refund r
            inner join tb_m_proyek p on p.m_proyek_id=r.m_proyek_id
            inner join tb_m_cust c on c.m_cust_id=r.m_cust_id
            inner join tb_m_item i on i.m_item_id=r.m_item_id
            inner join tb_m_type_unit t on t.m_type_unit_id=i.m_item_type_unit_id
            outer apply (
	            select spr.t_spr_id, spr.t_spr_no, spr.t_spr_date 
	            , (select xx.t_utj_date from tb_t_utj xx where xx.m_cust_id=spr.m_cust_id and xx.m_item_id=spr.m_item_id) t_utj_date
	            from tb_t_spr spr where spr.t_spr_id=r.t_refund_ref_id
            ) spr
            outer apply ( 
	            select sum(rr.r_hutang_amt) r_hutang_amt, sum(rr.r_hutang_pay_amt) r_hutang_pay_amt
	            from tb_r_hutang rr where rr.m_cust_id=r.m_cust_id and r.m_item_id=rr.m_item_id
            ) rr where r.t_refund_type='Batal SPR' and r.t_refund_ref_table in ('tb_t_spr', 'tb_t_spr_sa') )";
            return sSql;
        }

        [HttpPost]
        public ActionResult ViewReportRefund(sFilter param, string reporttype)
        {
            var rptfile = ""; var rptname = "";
            var Periode = $"{param.periodstart.ToDateTime().ToString("dd/MM/yyyy")}-{param.periodend.ToDateTime().ToString("dd/MM/yyyy")}";
            rptfile = $"RptKartuRefund{param.ddltype}{reporttype}";
            rptname = $"KartuRefund{param.ddltype}_{Periode}";
            rptfile = rptfile.Replace("PDF", "");
            var sField = "*"; var sGroupBy = ""; var sOrderBy = "";

            if (param.ddltype == "DETAIL") { sOrderBy = "Order By m_proyek_id, m_item_id, r_hutang_ref_id, r_hutang_pay_date"; }
            else if (param.ddltype == "REKAP") { sOrderBy = " Order by m_proyek_id, t_refund_date, m_cust_name, t_utj_date"; }
            else if (param.ddltype == "BULAN")
            {
                sField += ", Cast(Isnull(( select Count(*) from tb_r_hutang h Where h.m_item_id=r.m_item_id AND h.r_hutang_ref_id=r.r_hutang_ref_id Group by h.r_hutang_ref_id HAVING SUM(r_hutang_amt) - SUM(r_hutang_pay_amt)=0 ), 0.0) as char(5)) +' Of '+ Cast(Isnull(( select Count(*) from tb_r_hutang h Where h.m_item_id=r.m_item_id AND h.r_hutang_type!='Payment' ), 0.0) as char(5)) Keterangan";
                sOrderBy = " Order by m_proyek_id, m_cust_name, t_utj_date";
            }
            else if (param.ddltype == "SUMMARY")
            {
                sField = "r_hutang_ref_id, m_proyek_code, m_proyek_name, m_cust_name, m_item_name, m_type_unit_name, t_spr_no, t_spr_date, t_utj_date, t_refund_no, t_refund_date, t_refund_pay_amt, t_refund_cost_amt, t_refund_cost_other_amt, t_refund_amt, t_refund_total_amt, t_refund_total_cost_amt, t_refund_note, m_proyek_id, m_cust_id, m_item_id, t_spr_awal_id, t_spr_id, r_hutang_ref_due_date, r_hutang_amt, SUM(r_hutang_pay_amt) r_hutang_pay_amt, r_hutang_total_pay_amt, SUM(r_hutang_total_amt) r_hutang_total_amt, Max(r_hutang_pay_ref_date) r_hutang_pay_ref_date, Max(r_hutang_note) r_hutang_note";
                sGroupBy = " Group By m_proyek_code, m_proyek_name, m_cust_name, m_item_name, m_type_unit_name, t_spr_no, t_spr_date, t_utj_date, t_refund_no, t_refund_date, t_refund_pay_amt, t_refund_cost_amt, t_refund_cost_other_amt, t_refund_amt, t_refund_total_amt, t_refund_total_cost_amt, t_refund_note, m_proyek_id, m_cust_id, m_item_id, t_spr_awal_id, t_spr_id, r_hutang_amt, r_hutang_total_pay_amt, r_hutang_ref_due_date, r_hutang_ref_id";
                sOrderBy = " Order By m_proyek_id, m_item_id, r_hutang_ref_id, MAX(r_hutang_pay_date)";
            }

            sSql = $"SELECT {sField} from {(param.ddltype == "REKAP" ? $"{getquery()}" : "tb_v_kartu_refund")} r Where 1=1";

            if (param.ddltype == "BULAN")
                sSql += $" AND r_hutang_ref_date >= Cast('{param.periodstart} 0:0:0' as datetime) AND r_hutang_ref_date <= Cast('{param.periodend} 23:59:59' as datetime)";
            else if (param.ddltype == "SUMMARY")
                sSql += $" AND t_refund_date >= Cast('{param.periodstart} 0:0:0' as datetime) AND t_refund_date <= Cast('{param.periodend} 23:59:59' as datetime)";
            else if (param.ddltype == "DETAIL")
                sSql += $" AND t_refund_date >= Cast('{param.periodstart} 0:0:0' as datetime) AND t_refund_date <= Cast('{param.periodend} 23:59:59' as datetime)";

            if (param.ddlproyek != 0) sSql += $" AND r.m_proyek_id = {param.ddlproyek}";
            if (!string.IsNullOrEmpty(param.filterspr)) sSql += $" AND r.m_item_name IN ('{param.filterspr.Replace(";", "', '")}')";
            sSql += sGroupBy + sOrderBy;

            var PaperSize = 9;
            var irequest = new ReportRequest()
            {
                rptQuery = sSql,
                rptFile = rptfile,
                rptPaperSizeEnum = PaperSize,
                rptPaperOrientationEnum = 1,
                rptExportType = reporttype,
                rptParam = new Dictionary<string, string>()
            };
            irequest.rptParam.Add("Periode", Periode);
            irequest.rptParam.Add("UserID", Session["UserID"].ToString());
            var rpt_id = GenerateReport(irequest);
            return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Kartu Promo
        public ActionResult KartuPromo()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/KartuRefund", (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            var proyek = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "ALL" } }.Union(db.tb_m_proyek.Where(x => x.m_proyek_flag == "OPEN").Select(x => new SelectListItem() { Value = x.m_proyek_id.ToString(), Text = x.m_proyek_name })).ToList();
            ViewBag.ddlproyek = new SelectList(proyek, "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult GetModalPromo(sFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                if (modaltype.ToUpper() == "SPR")
                    sSql = $"Select 0 seq, m_promo_name [Jenis Promo], m_cust_name Customer, t_spr_no [No. SPR], m_item_name Unit from tb_v_kartu_promo Where 1=1";
                if (param.ddlproyek != 0) sSql += $" AND m_proyek_id = {param.ddlproyek}";
                sSql += $"  Group By m_cust_name, t_spr_no, m_item_name, m_promo_name Order By m_cust_name";
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblModal" + modaltype);
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq") { item = (i++).ToString(); } row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName)) { tblcols.Add(col.ColumnName); } 
                        }
                        tblrows.Add(row);
                    }
                }
                else { result = "Data Not Found."; }
                    
            }
            catch (Exception e)
            {
                result = e.ToString() + sSql;
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ViewReportPromo(sFilter param, string reporttype)
        {
            var rptfile = ""; var rptname = "";
            var Periode = $"{param.periodstart.ToDateTime().ToString("dd/MM/yyyy")}-{param.periodend.ToDateTime().ToString("dd/MM/yyyy")}";
            rptfile = $"RptKartuPromo{param.ddltype}{reporttype}";
            rptname = $"KartuPromo{param.ddltype}_{Periode}";
            rptfile = rptfile.Replace("PDF", "");
             
            sSql = $"select r_promo_id, t_spr_id, m_proyek_id, m_cust_id, m_promo_id, r_hutang_ref_id, m_proyek_name, m_cust_name, m_item_name, t_promo_type_bayar, t_spr_no, t_spr_date, r_hutang_note, t_promo_amt, r_hutang_amt, r_hutang_ref_date, r_hutang_pay_amt, r_promo_pay_ref_date, r_hutang_pay_date, r_promo_total_pay_amt, r_promo_total_sisa_amt, r_hutang_sisa_amt, m_promo_name from tb_v_kartu_promo r Where 1=1 AND t_spr_date >= Cast('{param.periodstart} 0:0:0' as datetime) AND t_spr_date <= Cast('{param.periodend} 23:59:59' as datetime)";

            if (param.ddlproyek != 0) { sSql += $" AND r.m_proyek_id = {param.ddlproyek}"; }
            if (!string.IsNullOrEmpty(param.filterspr)) { sSql += $" AND r.m_promo_name IN ('{param.filterspr.Replace(";", "', '")}')"; }
            sSql += $" Order By r_hutang_ref_id, r_hutang_ref_date, r_promo_pay_ref_date";
            var dt_rpt = new ClassConnection().GetDataTable(sSql, $"data");
            var req = new PrintOutRequest
            {
                rpt_src = new DataTable(),
                rpt_name = rptfile,
                file_name = rptname,
                rpt_param = new Dictionary<string, string>()
            };
            req.rpt_src = dt_rpt;
            req.rpt_param.Add("Periode", Periode);
            req.rpt_param.Add("UserID", Session["UserID"].ToString());
            return setPrintOut(req); 
        }
        #endregion
    }
}