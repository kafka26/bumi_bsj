﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq; 
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models; 
using System.IO; 
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using static MIS_MAGNA_MVC.GlobalFunctions;

namespace MIS_MAGNA_MVC.Controllers
{
    public class UangTandaJadiController : Controller
    {
        // GET: UangTandaJadi
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public class dt_gl
        {
            public decimal r_gl_amt { get; set; }
            public int m_account_id { get; set; }
            public string r_gl_db_cr { get; set; }
        }

        #region Uang Tanda Jadi
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_t_utj", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column()
                {
                    name = "sAddFilter",
                    data = "sAddFilter",
                    search = new Search(){ value = "", regex = "" },
                    searchable = true,
                    orderable = false
                };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( Select utj.t_utj_id, utj.t_utj_uid, utj.t_utj_no, utj.t_utj_date, utj.t_utj_due_date, mc.m_cust_name, mp.m_proyek_name, i.m_item_name, i.m_item_luas_tanah, i.m_item_luas_bangunan, t.m_type_unit_name, mp.m_proyek_addr, em.m_marketing_name, utj.t_utj_type_bayar, utj.t_utj_amt, utj.t_utj_status, utj.t_utj_note, utj.created_by, utj.created_at, utj.updated_by, utj.updated_at, utj.posted_by, utj.posted_at from tb_t_utj utj Inner Join tb_m_cust mc ON mc.m_cust_id=utj.m_cust_id Inner Join tb_m_proyek mp ON mp.m_proyek_id=utj.m_proyek_id Left Join tb_m_marketing em ON em.m_marketing_id=utj.m_marketing_id Inner Join tb_m_item i ON i.m_item_id=utj.m_item_id Inner Join tb_m_type_unit t ON t.m_type_unit_id=i.m_item_type_unit_id ) AS t";
            var sFixedFilter = "";
            var sOrder_by = " t.t_utj_id DESC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            tb_t_utj tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_utj();
                tbl.t_utj_uid = Guid.NewGuid();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.t_utj_date = GetServerTime();
                tbl.t_utj_due_date = GetServerTime();
                tbl.t_utj_status = "In Process";
                tbl.m_sales_id = 0;
                tbl.t_utj_no = GenerateTransNo($"UTJ", "t_utj_no", "tb_t_utj", 0, tbl.t_utj_date.ToString("MM/dd/yyyy"));
                tbl.m_manager_id = 0;
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_utj.FirstOrDefault(p => p.t_utj_uid == id);
            }
            if (tbl == null) return HttpNotFound();
            ViewBag.action = action;
            FieldAdd(tbl, action);
            return View(tbl);
        }

        [HttpPost]
        public ActionResult GetDataCust()
        {
            return GetJsonResult($"Select mc.m_cust_id, m_cust_code, m_cust_name, m_cust_addr From tb_m_cust mc Where mc.m_cust_flag='ACTIVE' AND mc.m_cust_id NOT IN (Select m_cust_id from tb_t_utj) Order By m_cust_code");
        }

        [HttpPost]
        public ActionResult GetDataItem()
        {
            return GetJsonResult($"Select i.m_item_id, i.m_item_name, un.m_type_unit_id, un.m_type_unit_name, mp.m_proyek_id, mp.m_proyek_name, i.m_item_luas_standart, i.m_item_lebih_tanah, i.m_item_luas_tanah, i.m_item_luas_bangunan, cl.m_cluster_name From tb_m_item i Inner Join tb_m_proyek mp ON mp.m_proyek_id=i.m_proyek_id Inner Join tb_m_type_unit un ON un.m_type_unit_id=i.m_item_type_unit_id Inner Join tb_m_cluster cl ON cl.m_cluster_id=i.m_cluster_id Where i.m_item_flag='Open'");
        }

        public class tb_marketing : tb_m_marketing
        {
            public int seq { get; set; } 
        }

        [HttpPost]
        public ActionResult GetDataSales()
        {
            return GetJsonResult($"Select em.m_marketing_id, em.m_marketing_name, em.m_marketing_gender From tb_m_marketing em Where em.m_marketing_flag='ACTIVE'");
        }

        private void FieldAdd(tb_t_utj tbl, string action)
        {
            if(tbl.m_cust_id != 0)
            {
                ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(p => p.m_proyek_id == tbl.m_proyek_id).m_proyek_name;
                ViewBag.m_item_name = db.tb_m_item.FirstOrDefault(p => p.m_item_id == tbl.m_item_id).m_item_name;
                ViewBag.m_cust_name = db.tb_m_cust.FirstOrDefault(p => p.m_cust_id == tbl.m_cust_id).m_cust_name; 
            }

            sSql = $"Select * From (select m_marketing_id, m_marketing_name, 1 seq from tb_m_marketing Union ALL select 0 m_marketing_id, 'Lain-Lain' m_marketing_name, 0 seq) m Order By seq";
            ViewBag.m_marketing_id = new SelectList(db.Database.SqlQuery<tb_marketing>(sSql).ToList(), "m_marketing_id", "m_marketing_name", tbl.m_marketing_id);

            ViewBag.m_company_id = new SelectList(db.tb_m_company.Where(a => a.active_flag == "ACTIVE").ToList(), "m_company_id", "m_company_name", tbl.m_company_id);
             
            ViewBag.t_utj_account_id = new SelectList(GetCOAInterface(new List<string> { "PIUTANG DP" }), "m_account_id", "m_account_name", tbl.t_utj_account_id);
        }

        public ActionResult GenerateNo(DateTime t_utj_date)
        {
            var sJson = GenerateTransNo($"UTJ", "t_utj_no", "tb_t_utj", 0, t_utj_date.ToString("MM/dd/yyyy"));
            return Json(sJson);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_utj tbl, string action)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            var msg = ""; var result = "failed"; var hdrid = "";
            var servertime = GetServerTime();

            if (tbl.t_utj_date < GetCutOffdate())
                msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_utj_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />";

            if (tbl.m_cust_id == 0) msg += "Tolong, Pilih konsumen..!<br />";
            if (tbl.m_item_id == 0) msg += "Tolong, Pilih Produk/Unit..!<br />";
            if (tbl.m_marketing_id == 0 && string.IsNullOrEmpty(tbl.m_marketing_name)) msg += "Tolong, Pilih Marketing..!<br />";
            if (tbl.t_utj_amt == 0) msg += "Tolong, Isi Nominal UTJ..!<br />";
            if (msg == "")
            {
                if (tbl.t_utj_status == "Post")
                { 
                    tbl.t_utj_no = GenerateTransNo($"UTJ", "t_utj_no", "tb_t_utj", 0, tbl.t_utj_date.ToString("MM/dd/yyyy"));
                    tbl.posted_by = Session["UserID"].ToString();
                    tbl.posted_at = servertime;
                    var tb_item = db.tb_m_item.FirstOrDefault(i => i.m_item_id == tbl.m_item_id);
                    tb_item.m_item_flag = "Booking";
                    db.Entry(tb_item).State = EntityState.Modified;
                }
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (string.IsNullOrEmpty(tbl.t_utj_note)) tbl.t_utj_note = "";
                        if (action == "New Data")
                        {
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.tb_t_utj.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                        }
                        db.SaveChanges();

                        if (tbl.t_utj_status == "Post")
                        {
                            //Block Post Transaction
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_utj_uid, "tb_t_utj", tbl.t_utj_id, tbl.updated_by, tbl.updated_at));

                            db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "UTJ", "tb_t_utj", tbl.t_utj_id, tbl.t_utj_no, tbl.t_utj_date, tbl.t_utj_due_date, Convert.ToDateTime("1/1/1900".ToString()), tbl.t_utj_account_id, tbl.t_utj_amt, 0m, "Booking Fee", tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0));

                            // --> Insert Jurnal
                            msg = PostJurnal(tbl);
                            if (!string.IsNullOrEmpty(msg))
                            {
                                objTrans.Rollback(); result = "failed";
                                return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.t_utj_uid.ToString();
                        msg = "Data Already Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        private string PostJurnal(tb_t_utj tbl) {
            var msg = "";
            try
            {
                // INSERT AUTO JURNAL
                var seq = 1;
                var new_dt_gl = new List<dt_gl>();
                var jual_account_id = GetCOAInterface(new List<string> { "HUTANG PENJUALAN" }).FirstOrDefault()?.m_account_id ?? 0;
                var gl_note = tbl.t_utj_note == "" ? $" PENERIMAAN UTJ" : tbl.t_utj_note;
                // Insert Jurnal Akun HUTANG PENJUALAN  
                new_dt_gl.Add(new dt_gl { r_gl_amt = tbl.t_utj_amt, m_account_id = tbl.t_utj_account_id, r_gl_db_cr = "D" });
                new_dt_gl.Add(new dt_gl { r_gl_amt = tbl.t_utj_amt, m_account_id = jual_account_id, r_gl_db_cr = "C" });

                if (new_dt_gl != null && new_dt_gl.Count() > 0)
                {
                    foreach (var item in new_dt_gl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_utj", tbl.t_utj_id, tbl.t_utj_no, tbl.t_utj_date, item.m_account_id, item.r_gl_db_cr, item.r_gl_amt, gl_note, tbl.created_by, tbl.created_at, tbl.m_cust_id));
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.ToString();
            }
            db.SaveChanges();
            return msg;
        }
        // POST: PurchaseReturnService/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid? t_utj_uid)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_t_utj tbl = db.tb_t_utj.FirstOrDefault(p => p.t_utj_uid == t_utj_uid);
            var servertime = GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.tb_t_utj.Remove(tbl);
                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Print Out
        public ActionResult PrintReport(int ids)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            string rptname = "PrintUtj"; 
            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rpt" + rptname + ".rpt"));
            sSql = $"SELECT * FROM tb_v_print_utj WHERE t_utj_id IN ({ids}) ORDER BY t_utj_id";
            var dt = new ClassConnection().GetDataTable(sSql, "datatalble");
            report.SetDataSource(dt);

            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }
         
        #endregion
    }
}