﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using static MIS_MAGNA_MVC.GlobalFunctions;

namespace MIS_MAGNA_MVC.Controllers
{
    public class RefundController : Controller
    {
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_t_angsuran_refund_h", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column()
                {
                    name = "sAddFilter",
                    data = "sAddFilter",
                    search = new Search() { value = "", regex = "" },
                    searchable = true,
                    orderable = false
                };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( Select h.t_angsuran_refund_h_id, h.t_angsuran_refund_type, h.t_angsuran_refund_uid, h.t_angsuran_refund_no, h.t_angsuran_refund_date, h.t_spr_id, spr.t_spr_no, r.t_refund_total_amt, r.t_refund_total_angsuran, r.t_refund_type_bayar, r.t_refund_type, h.m_cust_id, c.m_cust_name, h.m_proyek_id, mp.m_proyek_name, mp.m_proyek_addr, h.m_item_id, i.m_item_name, tu.m_type_unit_name, i.m_item_luas_tanah, i.m_item_luas_bangunan, h.t_angsuran_refund_h_status, h.t_angsuran_refund_h_note, h.created_by, h.created_at, h.updated_by, h.updated_at, h.posted_by, h.posted_at from tb_t_angsuran_refund_h h INNER JOIN tb_t_refund r ON r.t_refund_ref_id = h.t_spr_id Left Join tb_t_spr spr ON spr.t_spr_id=h.t_spr_id Inner Join tb_m_cust c ON c.m_cust_id=h.m_cust_id Inner Join tb_m_proyek mp ON mp.m_proyek_id=h.m_proyek_id Inner Join tb_m_item i ON i.m_item_id=h.m_item_id Inner Join tb_m_type_unit tu ON tu.m_type_unit_id=i.m_item_type_unit_id Where r.t_refund_ref_table!='tb_t_spr_sa' AND h.t_angsuran_refund_type='Refund' UNION ALL Select h.t_angsuran_refund_h_id, h.t_angsuran_refund_type, h.t_angsuran_refund_uid, h.t_angsuran_refund_no, h.t_angsuran_refund_date, h.t_spr_id, '' t_spr_no, r.t_refund_total_amt, r.t_refund_total_angsuran, r.t_refund_type_bayar, r.t_refund_type, h.m_cust_id, c.m_cust_name, h.m_proyek_id, mp.m_proyek_name, mp.m_proyek_addr, h.m_item_id, i.m_item_name, tu.m_type_unit_name, i.m_item_luas_tanah, i.m_item_luas_bangunan, h.t_angsuran_refund_h_status, h.t_angsuran_refund_h_note, h.created_by, h.created_at, h.updated_by, h.updated_at, h.posted_by, h.posted_at from tb_t_angsuran_refund_h h INNER JOIN tb_t_refund r ON r.t_refund_id = h.t_refund_id Inner Join tb_m_cust c ON c.m_cust_id=h.m_cust_id Inner Join tb_m_proyek mp ON mp.m_proyek_id=h.m_proyek_id Inner Join tb_m_item i ON i.m_item_id=h.m_item_id Inner Join tb_m_type_unit tu ON tu.m_type_unit_id=i.m_item_type_unit_id Where r.t_refund_ref_table='tb_t_spr_sa' AND h.t_angsuran_refund_type='Refund' UNION ALL Select h.t_angsuran_refund_h_id, h.t_angsuran_refund_type, h.t_angsuran_refund_uid, h.t_angsuran_refund_no, h.t_angsuran_refund_date, h.t_spr_id, spr.t_spr_no, r.t_promo_total_amt t_refund_total_amt, r.t_promo_total_angsuran t_refund_total_angsuran, r.t_promo_type_bayar t_refund_type_bayar, 'Promo'  t_refund_type, h.m_cust_id, c.m_cust_name, h.m_proyek_id, mp.m_proyek_name, mp.m_proyek_addr, h.m_item_id, i.m_item_name, tu.m_type_unit_name, i.m_item_luas_tanah, i.m_item_luas_bangunan, h.t_angsuran_refund_h_status, h.t_angsuran_refund_h_note, h.created_by, h.created_at, h.updated_by, h.updated_at, h.posted_by, h.posted_at from tb_t_angsuran_refund_h h INNER JOIN tb_t_promo r ON r.t_promo_id = h.t_promo_id Left Join tb_t_spr spr ON spr.t_spr_id=h.t_spr_id Inner Join tb_m_cust c ON c.m_cust_id=h.m_cust_id Inner Join tb_m_proyek mp ON mp.m_proyek_id=h.m_proyek_id Inner Join tb_m_item i ON i.m_item_id=h.m_item_id Inner Join tb_m_type_unit tu ON tu.m_type_unit_id=i.m_item_type_unit_id Where h.t_angsuran_refund_type='Promo' ) AS t"; 

            // action inside a standard controller
            int filteredResultsCount; int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, "", " t.t_angsuran_refund_no ASC"); 
            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) { result.Add(s); }
            return Json(new { draw = model.draw, recordsTotal = totalResultsCount, recordsFiltered = filteredResultsCount, data = result });
        }

        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            tb_t_angsuran_refund_h tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_angsuran_refund_h();
                tbl.t_spr_id = 0;
                tbl.t_angsuran_refund_uid = Guid.NewGuid();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.t_angsuran_refund_date = GetServerTime();
                tbl.t_angsuran_refund_h_status = "In Process";
                tbl.t_angsuran_refund_no = GenerateTransNo($"GREF", "t_angsuran_refund_no", "tb_t_angsuran_refund_h", db.tb_m_company.FirstOrDefault().m_company_id, tbl.t_angsuran_refund_date.ToString("MM/dd/yyyy"));
                tbl.m_company_id = db.tb_m_company.FirstOrDefault(a => a.active_flag == "ACTIVE").m_company_id;
                tbl.t_promo_id = 0;
                tbl.m_promo_id = 0;
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_angsuran_refund_h.FirstOrDefault(p => p.t_angsuran_refund_uid == id);
            }
            if (tbl == null) return HttpNotFound();
            ViewBag.action = action;
            ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(p => p.m_proyek_id == tbl.m_proyek_id)?.m_proyek_name ?? string.Empty;
            var mItem = db.tb_m_item.FirstOrDefault(p => p.m_item_id == tbl.m_item_id);
            if (tbl.t_angsuran_refund_type == "Refund") {
                var dt_ref = db.tb_t_refund.Where(p => p.t_refund_id == tbl.t_refund_id);
                ViewBag.t_refund_type_bayar = dt_ref.FirstOrDefault()?.t_refund_type_bayar ?? string.Empty;
                ViewBag.t_refund_total_angsuran = dt_ref.FirstOrDefault()?.t_refund_total_angsuran ?? 0;
                ViewBag.t_refund_total_amt = dt_ref.FirstOrDefault()?.t_refund_total_amt ?? 0m;
            }
            else if (tbl.t_angsuran_refund_type == "Promo")
            {
                mItem = db.tb_m_item.FirstOrDefault(p => p.m_item_id == tbl.m_item_id);
                var dt_ref = db.tb_t_promo.Where(p => p.t_promo_id == tbl.t_promo_id);
                ViewBag.t_refund_type_bayar = dt_ref.FirstOrDefault()?.t_promo_type_bayar ?? string.Empty;
                ViewBag.t_refund_total_angsuran = dt_ref.FirstOrDefault()?.t_promo_total_angsuran ?? 0;
                ViewBag.t_refund_total_amt = dt_ref.FirstOrDefault()?.t_promo_total_amt ?? 0m;
            }

            ViewBag.m_item_name = mItem?.m_item_name ?? string.Empty;
            var m_item_type_unit_id = mItem?.m_item_type_unit_id ?? 0;
            ViewBag.m_type_unit_name = db.tb_m_type_unit.Where(t => t.m_type_unit_id == m_item_type_unit_id).FirstOrDefault()?.m_type_unit_name ?? string.Empty; 
            ViewBag.m_cust_name = db.tb_m_cust.Where(p => p.m_cust_id == tbl.m_cust_id).FirstOrDefault()?.m_cust_name ?? string.Empty;  
            ViewBag.t_spr_no = db.tb_t_spr.Where(p => p.t_spr_id == tbl.t_spr_id).FirstOrDefault()?.t_spr_no ?? string.Empty; 
            ViewBag.t_angsuran_amt = db.tb_t_angsuran_refund_d.Where(w => w.t_angsuran_refund_h_id == tbl.t_angsuran_refund_h_id).FirstOrDefault()?.t_angsuran_refund_d_amt ?? 0m;
            return View(tbl);
        }

        public ActionResult GenerateNo(sParam param, string sPrefix = "")
        {
            if (param.t_angsuran_refund_type == "Refund") { sPrefix = "GREF"; } else if (param.t_angsuran_refund_type == "Promo") { sPrefix = "GPR"; }
            var sJson = GenerateTransNo(sPrefix, "t_angsuran_refund_no", "tb_t_angsuran_refund_h", db.tb_m_company.FirstOrDefault().m_company_id, param.t_angsuran_refund_date.ToString("MM/dd/yyyy"));
            return Json(sJson);
        }
        [HttpPost]
        public ActionResult GetDataSPR(sParam param)
        {
            if (param.t_angsuran_refund_type == "Refund")
            {
                sSql = $"Select r.t_refund_id, spr.t_spr_id, spr.t_spr_awal_id, spr.t_spr_no, Format(spr.t_spr_date, 'MM/dd/yyyy') t_spr_date, c.m_cust_id, c.m_cust_name, mp.m_proyek_id, mp.m_proyek_name, i.m_item_id, i.m_item_name, tu.m_type_unit_name, r.t_refund_type_bayar, r.t_refund_total_angsuran, r.t_refund_total_amt From tb_t_refund r INNER JOIN tb_t_spr spr ON spr.t_spr_id = r.t_refund_ref_id Inner Join tb_m_cust c ON c.m_cust_id=spr.m_cust_id Inner Join tb_m_proyek mp ON mp.m_proyek_id=spr.m_proyek_id Inner Join tb_m_item i ON i.m_item_id=spr.m_item_id Inner Join tb_m_type_unit tu ON tu.m_type_unit_id=i.m_item_type_unit_id Where r.t_refund_type_bayar = '{param.t_refund_type_bayar}' AND r.t_refund_status='Post' AND t_refund_ref_table='tb_t_spr' UNION ALL Select r.t_refund_id, r.t_refund_id t_spr_id, r.t_refund_id t_spr_awal_id, r.t_refund_no t_spr_no, Format(r.t_refund_date, 'MM/dd/yyyy') t_spr_date, c.m_cust_id, c.m_cust_name, mp.m_proyek_id, mp.m_proyek_name, i.m_item_id, i.m_item_name, tu.m_type_unit_name, r.t_refund_type_bayar, r.t_refund_total_angsuran, r.t_refund_total_amt From tb_t_refund r Inner Join tb_m_cust c ON c.m_cust_id=r.m_cust_id Inner Join tb_m_proyek mp ON mp.m_proyek_id=r.m_proyek_id Inner Join tb_m_item i ON i.m_item_id=r.m_item_id Inner Join tb_m_type_unit tu ON tu.m_type_unit_id=i.m_item_type_unit_id Where r.t_refund_type_bayar = '{param.t_refund_type_bayar}' AND r.t_refund_status='Post' AND t_refund_ref_table='tb_t_spr_sa' ";
            }
            else if (param.t_angsuran_refund_type == "Promo") 
            {
                sSql = $"select 0 t_refund_id, t_promo_id, m_promo_id, x.t_spr_id, x.t_spr_awal_id, t_spr_no, t_spr_date, x.m_cust_id, x2.m_cust_name, x.m_proyek_id, x4.m_proyek_name, x.m_item_id, x1.m_item_name, '' m_type_unit_name, x.t_promo_type_bayar t_refund_type_bayar, x.t_promo_total_angsuran t_refund_total_angsuran, x.t_promo_total_amt t_refund_total_amt, x.t_promo_status From tb_t_promo x Inner Join tb_m_item x1 ON x1.m_item_id=x.m_promo_id Inner Join tb_m_cust x2 ON x2.m_cust_id=x.m_cust_id Inner Join tb_t_spr x3 ON x3.t_spr_id=x.t_spr_id Inner Join tb_m_proyek x4 ON x4.m_proyek_id=x.m_proyek_id Where x.t_promo_type_bayar='{param.t_refund_type_bayar}' AND x.t_promo_status=''";
            }
            return GetJsonResult(sSql);
        }

        public class t_angsuran_refund_d : tb_t_angsuran_refund_d { }

        [HttpPost]
        public ActionResult GetDataDetails(sParam param)
        {
            List<t_angsuran_refund_d> tbl = new List<t_angsuran_refund_d>(); var result = ""; 
            try
            {
                int maxloop = param.t_refund_total_angsuran;
                decimal dValue = Math.Round(param.t_angsuran_amt, 0);
                decimal amtTotal = Math.Round(param.t_refund_amt, 0);
                decimal amtGen = 0;
                string sStatus = "In Process";

                for (var i = 0; i < maxloop; i++)
                {
                    var seq = i + 1;
                    tbl.Add(new t_angsuran_refund_d()
                    {
                        t_angsuran_refund_d_seq = i + 1,
                        t_angsuran_refund_due_date = param.t_angsuran_refund_date.AddMonths(i),
                        t_angsuran_refund_d_amt_accum = 0,
                        t_angsuran_refund_d_amt = (i == (maxloop - 1) ? (amtTotal - amtGen) : dValue),
                        t_angsuran_refund_d_note = $"{param.t_angsuran_refund_type} {seq}",
                        t_angsuran_refund_d_status = sStatus
                    });
                    amtGen += dValue;
                }

            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            JsonResult js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int t_angsuran_refund_h_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select t_angsuran_refund_d_seq, t_angsuran_refund_due_date, t_angsuran_refund_d_amt, t_angsuran_refund_d_amt_accum, t_angsuran_refund_d_status, Isnull(t_angsuran_refund_d_note, '') t_angsuran_refund_d_note, created_by, created_at from tb_t_angsuran_refund_d Where t_angsuran_refund_h_id={t_angsuran_refund_h_id} Order by t_angsuran_refund_d_seq";
                var tbl = db.Database.SqlQuery<t_angsuran_refund_d>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString(), sSql }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }
         
        public class sParam
        {
            public int t_spr_id { get; set; }
            public int t_refund_total_angsuran { get; set; }
            public DateTime t_angsuran_refund_date { get; set; } 
            public Decimal t_angsuran_amt { get; set; }
            public Decimal t_refund_amt { get; set; }
            public string t_refund_type_bayar { get; set; }
            public string t_angsuran_refund_type { get; set; }
        }
        // POST: poitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_angsuran_refund_h tbl, List<t_angsuran_refund_d> dtDtl, string action, sParam param)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = ""; var servertime = GetServerTime();
            var refund_amt = 0m;
            if (tbl.t_spr_id == 0) msg += "Silahkan Pilih No. Surat Pesanan Rumah ! <br />";
            if (dtDtl == null) msg += "Silahkan Isi Data Detail ! <br />";
            if (dtDtl != null && dtDtl.Count > 0)
            {
                foreach (var item in dtDtl) refund_amt += item.t_angsuran_refund_d_amt;
                if (param.t_refund_amt != refund_amt)
                    msg += $"Nominal Total Angsuran {string.Format("{0:#,0.00}", Convert.ToDecimal(refund_amt))} harus sama dengan Nominal Refund {string.Format("{0:#,0.00}", Convert.ToDecimal(param.t_refund_amt))} <br />";
            }

            if (tbl.t_angsuran_refund_h_status == "Post")
            {
                if (tbl.t_angsuran_refund_type == "Refund") { param.t_angsuran_refund_type = "GREF"; }
                else if (tbl.t_angsuran_refund_type == "Promo") { param.t_angsuran_refund_type = "GPR"; }
                    tbl.t_angsuran_refund_no = GenerateTransNo(param.t_angsuran_refund_type, "t_angsuran_refund_no", "tb_t_angsuran_refund_h", tbl.m_company_id, tbl.t_angsuran_refund_date.ToString("MM/dd/yyyy"));
                tbl.posted_at = servertime;
                tbl.posted_by = Session["UserID"].ToString();
            } 

            if (string.IsNullOrEmpty(tbl.t_angsuran_refund_h_note)) { tbl.t_angsuran_refund_h_note = ""; }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.created_at = servertime;
                            tbl.created_by = Session["UserID"].ToString();
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.tb_t_angsuran_refund_h.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            var trndtl = db.tb_t_angsuran_refund_d.Where(a => a.t_angsuran_refund_h_id == tbl.t_angsuran_refund_h_id);
                            db.tb_t_angsuran_refund_d.RemoveRange(trndtl);
                        }
                        db.SaveChanges();

                        if (tbl.t_angsuran_refund_type == "Promo")
                        {
                            var tPromo = db.tb_t_promo.FirstOrDefault(x => x.t_promo_id == tbl.t_promo_id && x.m_promo_id == tbl.m_promo_id);
                            tPromo.t_promo_status = "Complete";
                            db.Entry(tPromo).State = EntityState.Modified;
                        }
                        else if (tbl.t_angsuran_refund_type == "Refund")
                        {
                            var dt_ref = db.tb_t_refund.FirstOrDefault(s => s.t_refund_id == tbl.t_refund_id);
                            dt_ref.t_refund_status = "Complete";
                            db.Entry(dt_ref).State = EntityState.Modified;
                        }
                        db.SaveChanges();

                        tb_t_angsuran_refund_d tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = (tb_t_angsuran_refund_d)MappingTable(new tb_t_angsuran_refund_d(), dtDtl[i]);
                            tbldtl.t_angsuran_refund_d_seq = i + 1;
                            tbldtl.t_angsuran_refund_h_id = tbl.t_angsuran_refund_h_id;
                            tbldtl.t_angsuran_refund_d_note = dtDtl[i].t_angsuran_refund_d_note ??  string.Empty;
                            tbldtl.created_by = tbl.created_by;
                            tbldtl.created_at = tbl.created_at;
                            db.tb_t_angsuran_refund_d.Add(tbldtl);
                        }
                        db.SaveChanges();

                        if (tbl.t_angsuran_refund_h_status == "Post")
                        {
                            //Block Post Transaction
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_angsuran_refund_uid, "tb_t_angsuran_refund_h", tbl.t_angsuran_refund_h_id, tbl.updated_by, tbl.updated_at));

                            var dtldata = db.tb_t_angsuran_refund_d.AsNoTracking().Where(d => d.t_angsuran_refund_h_id == tbl.t_angsuran_refund_h_id).ToList(); 
                            foreach (var item in dtldata)
                            {    
                                if (tbl.t_angsuran_refund_type == "Refund")
                                {
                                    var dt_ref = db.tb_t_refund.FirstOrDefault(s => s.t_refund_id == tbl.t_refund_id);
                                    db.tb_r_hutang.Add(Insert_R_Hutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "Refund", "tb_t_angsuran_refund_d", item.t_angsuran_refund_d_id, dt_ref.t_refund_no, item.t_angsuran_refund_due_date, item.t_angsuran_refund_due_date, Convert.ToDateTime("1/1/1900".ToString()), dt_ref.t_refund_account_id, item.t_angsuran_refund_d_amt, 0m, item.t_angsuran_refund_d_note ?? string.Empty, tbl.created_by, tbl.created_at, "", 0, "", 0, t_spr_id: dt_ref.t_refund_ref_id, t_spr_awal_id: tbl.t_spr_awal_id));
                                }
                                else
                                {
                                    var tPromo = db.tb_t_promo.FirstOrDefault(x => x.t_promo_id == tbl.t_promo_id && x.m_promo_id == tbl.m_promo_id);
                                    db.tb_r_promo.Add(Insert_R_Promo(tbl.m_company_id, tbl.t_promo_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, tbl.t_spr_id, tbl.t_spr_awal_id, tbl.m_promo_id, "Promo", "tb_t_angsuran_promo_d", item.t_angsuran_refund_d_id, tPromo.t_promo_no, item.t_angsuran_refund_due_date, db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_promo_id)?.m_account_id ?? 0, item.t_angsuran_refund_d_amt, new DateTime(1900, 1, 1), tbl.t_angsuran_refund_h_status, item.created_by, item.created_at, item.t_angsuran_refund_d_note ?? "Promo Tunai"));
                                }
                            } 
                        }

                        db.SaveChanges(); objTrans.Commit();
                        hdrid = tbl.t_angsuran_refund_h_id.ToString();
                        if (tbl.t_angsuran_refund_h_status == "Post") { sReturnNo = tbl.t_angsuran_refund_no; sReturnState = "terposting"; }
                        else { sReturnNo = $"draft {tbl.t_angsuran_refund_h_id.ToString()}"; sReturnState = "tersimpan"; }
                        msg = $"Data {sReturnState} dengan No. {sReturnNo}<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback(); var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += $"Entity of type {eve.Entry.Entity.GetType().Name} in state {eve.Entry.State} has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors) { err += $"- Property: {ve.PropertyName}, Error: {ve.ErrorMessage} <br />"; } 
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: poitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_t_angsuran_refund_h tbl = db.tb_t_angsuran_refund_h.FirstOrDefault(d => d.t_angsuran_refund_uid == id);
            var servertime = GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var dt_ref = db.tb_t_refund.FirstOrDefault(s => s.t_refund_id == tbl.t_refund_id);
                        dt_ref.t_refund_status = "Post";
                        db.Entry(dt_ref).State = EntityState.Modified;

                        var trndtl = db.tb_t_angsuran_refund_d.Where(a => a.t_angsuran_refund_h_id == tbl.t_angsuran_refund_h_id);
                        db.tb_t_angsuran_refund_d.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.tb_t_angsuran_refund_h.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
    }
}