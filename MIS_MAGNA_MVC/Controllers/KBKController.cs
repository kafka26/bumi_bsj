﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq; 
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models; 
using System.IO; 
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class KBKController : Controller
    {
        // GET: KBK
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public KBKController() { db = new QL_MIS_MAGNAEntities(); db.Database.CommandTimeout = 0; }
         
        [HttpPost]
        public ActionResult InitDDLAccount(string t_kbk_type_bayar, string action)
        {
            JsonResult js = null;
            try
            {
                var sVar = "";
                if (t_kbk_type_bayar == "KK") { sVar = "KAS_SISTEM"; } else { sVar = "BANK_SISTEM"; } 
                var tbl = GetCOAInterface(new List<string> { sVar });
                if (tbl == null || tbl.Count <= 0) { js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet); } 
                else { js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet); js.MaxJsonLength = Int32.MaxValue; }
            }
            catch (Exception e) { js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet); }
            return js;
        }

        [HttpPost]
        public ActionResult GetUnitData(int m_company_id)
        {
            JsonResult js = null;
            try
            { 
                sSql = $"SELECT DISTINCT rp.m_cust_id, m_cust_name, rp.m_proyek_id, m_proyek_name, rp.m_item_id, m_item_name FROM tb_r_piutang rp INNER JOIN tb_m_cust c ON c.m_cust_id = rp.m_cust_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = rp.m_proyek_id INNER JOIN tb_m_item i ON i.m_item_id = rp.m_item_id WHERE rp.r_piutang_status IN ('Post', 'Complete') GROUP BY rp.m_company_id, rp.m_cust_id, m_cust_name, rp.m_proyek_id, m_proyek_name, rp.m_item_id, m_item_name, r_piutang_amt, r_piutang_note, r_piutang_ref_table, r_piutang_ref_id, r_piutang_ref_no HAVING (r_piutang_amt - SUM(r_piutang_pay_amt)) > 0";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0) { js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet); } 
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class t_kbk_d : tb_t_kbk_d
        {
            public string m_account_code { get; set; }
            public string m_account_name { get; set; }
            public string t_kbk_d_no { get; set; }
            public string t_kbk_d_type_bayar { get; set; }
        }
         
        [HttpPost]
        public ActionResult GetDataDetails(int m_company_id, string t_kbk_type, int t_kbk_account_id)
        {
            JsonResult js = null;
            try
            {
                List<string> interface_name = new List<string> { "PENGELUARAN" };
                var coa_temp = GetCOAInterface(interface_name);
                if (t_kbk_type == "Pindah Buku") {
                    interface_name = new List<string> { "KAS_SISTEM", "BANK_SISTEM" };
                    coa_temp = GetCOAInterface(interface_name).Where(x => x.m_account_id != t_kbk_account_id).ToList();
                }

                List<t_kbk_d> tbl = new List<t_kbk_d>();
                if (coa_temp != null && coa_temp.Count > 0)
                {
                    foreach (var item in coa_temp)
                    {
                        tbl.Add(new t_kbk_d {
                            t_kbk_d_seq = 0,
                            t_kbk_ref_table = "",
                            t_kbk_ref_id = 0,
                            t_kbk_d_account_id = item.m_account_id,
                            m_account_code = item.m_account_code,
                            m_account_name = item.m_account_name,
                            t_kbk_d_amt = 0,
                            t_kbk_d_note = "",
                        });
                    }
                } 
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e) { js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet); }
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT d.t_kbk_d_seq, d.t_kbk_ref_table, d.t_kbk_ref_id, d.t_kbk_d_account_id, id.m_account_code, id.m_account_desc m_account_name,  t_kbk_d_amt, t_kbk_d_note FROM tb_t_kbk_d d INNER JOIN tb_m_account id ON id.m_account_id = d.t_kbk_d_account_id WHERE t_kbk_h_id = {id} ORDER BY d.t_kbk_d_seq";
                var tbl = db.Database.SqlQuery<t_kbk_d>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_t_kbk_h", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;
            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001") { modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1); } 
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001") { modfil.filterperiodto = GetServerTime(); } 
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE isapptrans=0 AND isposttrans=0 ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( SELECT h.t_kbk_uid, h.t_kbk_h_id, t_kbk_no, h.t_kbk_date, ISNULL((SELECT m_item_name FROM tb_m_item i WHERE i.m_item_id=h.m_item_id),'') m_unit_name, ISNULL((SELECT m_account_desc FROM tb_m_account i WHERE i.m_account_id=h.t_kbk_account_id),'') m_account_name, t_kbk_ref_no, t_kbk_h_note, t_kbk_h_status, h.t_kbk_h_amt FROM tb_t_kbk_h h Where t_kbk_type NOT IN ('Promo', 'Refund') ) AS t";

            var sFixedFilter = "";
            var sOrder_by = " t.t_kbk_h_id DESC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_t_kbk_h tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_kbk_h();
                tbl.t_kbk_uid = Guid.NewGuid();
                tbl.t_kbk_date = GetServerTime();
                tbl.created_at = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.t_kbk_h_status = "In Process";
                tbl.t_refund_id = 0;
                tbl.m_promo_id = 0;
                tbl.m_promo_id = 0;
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_kbk_h.Find(id);
            }

            if (tbl == null) { return HttpNotFound(); }
            ViewBag.action = action;
            //DDL
            var getCompany = db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList();
            if (action == "New Data") { getCompany = getCompany.Where(m => m.flag == "ACTIVE").ToList(); }
            ViewBag.m_company_id = new SelectList(getCompany, "valuefield", "textfield", tbl.m_company_id);

            var sVar = "KAS_SISTEM";
            if (action == "Update Data") if (tbl.t_kbk_type_bayar == "KK") { sVar = "KAS_SISTEM"; } else { sVar = "BANK_SISTEM"; }
            var t_kbk_account_id = new SelectList(GetCOAInterface(new List<string> { sVar }), "m_account_id", "m_account_name", tbl.t_kbk_account_id);
            ViewBag.t_kbk_account_id = t_kbk_account_id;

            //Field
            ViewBag.m_item_name = db.tb_m_item.FirstOrDefault(w => w.m_item_id == tbl.m_item_id)?.m_item_name ?? "";
            ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(w => w.m_proyek_id == tbl.m_proyek_id)?.m_proyek_name ?? "";
            ViewBag.m_cust_name = db.tb_m_cust.FirstOrDefault(w => w.m_cust_id == tbl.m_cust_id)?.m_cust_name ?? "";
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_kbk_h tbl, List<t_kbk_d> dtl, string action)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";
            var servertime = GetServerTime();

            if (tbl.t_kbk_date < GetCutOffdate()) { msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_kbk_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />"; }                
            if (tbl.t_kbk_account_id == 0) { msg += "Silahkan Pilih Akun!<br/>"; }
            if (string.IsNullOrEmpty(tbl.t_kbk_no)) { tbl.t_kbk_no = ""; }
            if (string.IsNullOrEmpty(tbl.t_kbk_ref_no)) { tbl.t_kbk_ref_no = ""; }
            if (string.IsNullOrEmpty(tbl.t_kbk_h_note)) { tbl.t_kbk_h_note = ""; }

            if (dtl != null)
            {
                foreach (var item in dtl)
                {
                    if (item.t_kbk_d_account_id == 0) { msg += "Akun Tidak Tersedia!<br/>"; }
                    if (item.t_kbk_d_amt == 0) { msg += "Jumlah Tidak boleh 0!<br/>"; }                   
                }
            }

            if (tbl.t_kbk_h_status == "Post")
            {//BANK_SISTEM
                tbl.t_kbk_no = GenerateTransNo($"{tbl.t_kbk_type_bayar}", "r_kb_no", "tb_r_kb", tbl.m_company_id, tbl.t_kbk_date.ToString("MM/dd/yyyy"));
                tbl.posted_at = servertime;
                tbl.posted_by = Session["UserID"].ToString();
                if (dtl.Count > 0 && tbl.t_kbk_type == "Pindah Buku") {
                    List<string> interface_name = new List<string>();
                    var seq_kas = 0; var seq_bank = 0; var sNo ="";

                    foreach (var item in dtl)
                    {
                        interface_name = new List<string> { "KAS_SISTEM" };
                        var coa_temp = GetCOAInterface(interface_name).Where(x=> x.m_account_id==item.t_kbk_d_account_id).Count();
                        if(coa_temp > 0)
                        {
                            sNo = $"KM-{tbl.t_kbk_date.ToString("yyyy.MM")}-";
                            if (seq_kas == 0) { seq_kas = GetLastTransNo("KM", "r_kb_no", "tb_r_kb", tbl.m_company_id, transdate: tbl.t_kbk_date.ToString("MM/dd/yyyy"), nomor: sNo, db: db); } else { seq_kas++; } 
                            item.t_kbk_d_no = sNo + GenNumberString(seq_kas, 4);
                            item.t_kbk_d_type_bayar = "KM";
                        }
                        else
                        {
                            sNo = $"BM-{tbl.t_kbk_date.ToString("yyyy.MM")}-";
                            if (seq_bank == 0) { seq_bank = GetLastTransNo("BM", "r_kb_no", "tb_r_kb", tbl.m_company_id, transdate: tbl.t_kbk_date.ToString("MM/dd/yyyy"), nomor: sNo, db: db); } else { seq_bank++; }
                            item.t_kbk_d_no = sNo + GenNumberString(seq_bank, 4);
                            item.t_kbk_d_type_bayar = "BM";
                        }
                    }
                }

            }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            db.tb_t_kbk_h.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            //cek data yg dihapus
                            var d_id = dtl.Select(s => s.t_kbk_d_id).ToList();
                            var dt_d = db.tb_t_kbk_d.Where(a => a.t_kbk_h_id == tbl.t_kbk_h_id && !d_id.Contains(a.t_kbk_d_id));
                            if (dt_d != null && dt_d.Count() > 0) db.tb_t_kbk_d.RemoveRange(dt_d);
                        }
                        db.SaveChanges();

                        if (tbl.t_kbk_h_status == "Post"){ db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_kbk_uid, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.updated_by, tbl.updated_at)); } tb_t_kbk_d tbldtl;
                        if (dtl != null)
                        {
                            foreach (var item in dtl)
                            {
                                var cek_data = db.tb_t_kbk_d.Where(a => a.t_kbk_d_id == item.t_kbk_d_id);
                                tbldtl = (tb_t_kbk_d)MappingTable(new tb_t_kbk_d(), item);
                                if (cek_data != null && cek_data.Count() > 0) { tbldtl = db.tb_t_kbk_d.FirstOrDefault(a => a.t_kbk_d_id == item.t_kbk_d_id); } 
                                tbldtl.t_kbk_h_id = tbl.t_kbk_h_id;
                                tbldtl.t_kbk_ref_table = item.t_kbk_ref_table ?? "";
                                tbldtl.t_kbk_d_note = item.t_kbk_d_note ?? "";
                                tbldtl.created_by = tbl.updated_by;
                                tbldtl.created_at = tbl.updated_at; 
                                if (cek_data != null && cek_data.Count() > 0) { db.Entry(tbldtl).State = EntityState.Modified; }
                                else { db.tb_t_kbk_d.Add(tbldtl); }
                                db.SaveChanges();

                                if (tbl.t_kbk_h_status == "Post")
                                {
                                    db.tb_r_kb.Add(Insert_R_KB(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "tb_t_kbk_d", tbldtl.t_kbk_d_id, tbl.t_kbk_type_bayar, tbl.t_kbk_no, tbl.t_kbk_date, tbl.t_kbk_account_id, item.t_kbk_d_account_id, 0m, item.t_kbk_d_amt, item.t_kbk_d_note ?? "", tbl.created_by, tbl.created_at, 0, 0, tbl.t_kbk_h_id, tbl.t_kbk_type));
                                    db.SaveChanges();
                                    if (tbl.t_kbk_type == "Pindah Buku")
                                    {
                                        db.tb_r_kb.Add(Insert_R_KB(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "tb_t_kbk_d", tbldtl.t_kbk_d_id, item.t_kbk_d_type_bayar, item.t_kbk_d_no, tbl.t_kbk_date, item.t_kbk_d_account_id, tbl.t_kbk_account_id, item.t_kbk_d_amt, 0m, item.t_kbk_d_note ?? "", tbl.created_by, tbl.created_at, 0, 0, tbl.t_kbk_h_id, tbl.t_kbk_type));
                                    }
                                    db.SaveChanges();
                                }
                            }
                        }

                        if (tbl.t_kbk_h_status == "Post")
                        {
                            // --> Insert Jurnal
                            msg = PostJurnal(tbl, dtl);
                            if (!string.IsNullOrEmpty(msg)) { objTrans.Rollback(); result = "failed"; return Json(new { result, msg }, JsonRequestBehavior.AllowGet); }
                        }

                        db.SaveChanges(); objTrans.Commit();
                        hdrid = tbl.t_kbk_h_id.ToString();

                        if (tbl.t_kbk_h_status == "Post") { sReturnNo = tbl.t_kbk_no; sReturnState = "terposting"; }
                        else { sReturnNo = "draft " + tbl.t_kbk_h_id.ToString(); sReturnState = "tersimpan"; }
                        msg = "Data " + sReturnState + " dengan No. " + sReturnNo + "<br />"; result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback(); var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors) { err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />"; }

                        }
                        msg = err;
                    }
                    catch (Exception ex) { objTrans.Rollback(); msg = ex.ToString(); }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        private string PostJurnal(tb_t_kbk_h tbl, List<t_kbk_d> dtl)
        {
            var msg = "";
            try
            { 
                var seq = 1;
                var tbl_note = $"Pengeluaran {tbl.t_kbk_type}";
                if (tbl.t_kbk_type == "Pindah Buku")
                {
                    db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.t_kbk_no, tbl.t_kbk_date, 32, "D", dtl.Sum(x => x.t_kbk_d_amt), tbl_note, tbl.created_by, tbl.created_at, tbl.m_cust_id));

                    db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.t_kbk_no, tbl.t_kbk_date, tbl.t_kbk_account_id, "C", dtl.Sum(x => x.t_kbk_d_amt), tbl_note, tbl.created_by, tbl.created_at, tbl.m_cust_id));

                    foreach (var item in dtl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.t_kbk_no, tbl.t_kbk_date, item.t_kbk_d_account_id, "D", item.t_kbk_d_amt, item.t_kbk_d_note ?? "", tbl.created_by, tbl.created_at, tbl.m_cust_id));

                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.t_kbk_no, tbl.t_kbk_date, 32, "C", item.t_kbk_d_amt, item.t_kbk_d_note ?? "", tbl.created_by, tbl.created_at, tbl.m_cust_id));
                    }
                }
                else
                {
                    foreach (var item in dtl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, db.tb_m_account.Where(a=> a.m_account_group=="PROYEK" && a.m_account_id == item.t_kbk_d_account_id).Count() > 0 ? tbl.m_proyek_id : 0, tbl.m_item_id, seq++, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.t_kbk_no, tbl.t_kbk_date, item.t_kbk_d_account_id, "D", item.t_kbk_d_amt, item.t_kbk_d_note ?? "", tbl.created_by, tbl.created_at));
                    }
                    db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, db.tb_m_account.Where(a => a.m_account_group == "PROYEK" && a.m_account_id == tbl.t_kbk_account_id).Count() > 0 ? tbl.m_proyek_id : 0, tbl.m_item_id, seq++, "tb_t_kbk_h", tbl.t_kbk_h_id, tbl.t_kbk_no, tbl.t_kbk_date, tbl.t_kbk_account_id, "C", tbl.t_kbk_h_amt, tbl_note ?? "", tbl.created_by, tbl.created_at));
                } 
            }
            catch (Exception e) { msg = e.ToString(); }
            return msg;
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); } 
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            tb_t_kbk_h tbl = db.tb_t_kbk_h.Find(id);
            string result = "success"; string msg = "";
            if (tbl == null) { result = "failed"; msg = "Data tidak ditemukan!"; }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.tb_t_kbk_d.Where(a => a.t_kbk_h_id == tbl.t_kbk_h_id);
                        db.tb_t_kbk_d.RemoveRange(trndtl); db.SaveChanges(); 
                        db.tb_t_kbk_h.Remove(tbl); db.SaveChanges(); objTrans.Commit();
                    }
                    catch (Exception ex) { objTrans.Rollback(); result = "failed"; msg = ex.ToString(); }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(string ids, string stype)
        {
            if (Session["UserID"] == null) { return RedirectToAction("Login", "Profile"); }                
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) { return RedirectToAction("NotAuthorize", "Profile"); } 

            string rptname = "RptPrintKB";
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("userID", Session["UserID"].ToString());

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"),$"{rptname}.rpt"));
            sSql = $"SELECT * FROM tb_v_print_kb WHERE r_kb_h_id IN ({ids}) AND stype_trans='{stype}' ORDER BY r_kb_h_id";
            var dt = new ClassConnection().GetDataTable(sSql, "datatable");
            report.SetDataSource(dt);
            if (rptparam.Count > 0) { foreach (var item in rptparam) { report.SetParameterValue(item.Key, item.Value); } }   
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", $"inline; filename={rptname}.pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing) { if (disposing) { db.Dispose(); } base.Dispose(disposing); }
    }
}