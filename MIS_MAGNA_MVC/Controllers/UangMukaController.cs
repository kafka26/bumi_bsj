﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using static MIS_MAGNA_MVC.GlobalFunctions;

namespace MIS_MAGNA_MVC.Controllers
{
    public class UangMukaController : Controller
    {
        // GET: UangMuka 
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        #region Uang Muka
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_t_angsuran_dp_h", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column()
                {
                    name = "sAddFilter",
                    data = "sAddFilter",
                    search = new Search() { value = "", regex = "" },
                    searchable = true,
                    orderable = false
                };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( Select dp.t_angsuran_dp_h_id, dp.t_angsuran_dp_uid, dp.t_angsuran_dp_no, dp.t_angsuran_dp_date, dp.t_spr_id, spr.t_spr_no, spr.t_dp_amt, spr.t_dp_total_angsuran, spr.t_dp_type_bayar, spr.t_dp_type, dp.m_cust_id, c.m_cust_name, dp.m_proyek_id, mp.m_proyek_name, mp.m_proyek_addr, dp.m_item_id, i.m_item_name, tu.m_type_unit_name, i.m_item_luas_tanah, i.m_item_luas_bangunan, dp.t_angsuran_dp_h_status, dp.t_angsuran_dp_h_note, dp.created_by, dp.created_at, dp.updated_by, dp.updated_at, dp.posted_by, dp.posted_at from tb_t_angsuran_dp_h dp Inner Join tb_t_spr spr ON spr.t_spr_id=dp.t_spr_id Inner Join tb_m_cust c ON c.m_cust_id=dp.m_cust_id Inner Join tb_m_proyek mp ON mp.m_proyek_id=dp.m_proyek_id Inner Join tb_m_item i ON i.m_item_id=dp.m_item_id Inner Join tb_m_type_unit tu ON tu.m_type_unit_id=i.m_item_type_unit_id ) AS t";
            var sFixedFilter = "";
            var sOrder_by = " t.t_angsuran_dp_no ASC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            tb_t_angsuran_dp_h tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_angsuran_dp_h();
                tbl.t_angsuran_dp_uid = Guid.NewGuid();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.t_angsuran_dp_date = GetServerTime();
                tbl.t_angsuran_dp_h_status = "In Process";
                tbl.t_angsuran_dp_no = GenerateTransNo($"DP", "t_angsuran_dp_no", "tb_t_angsuran_dp_h", db.tb_m_company.FirstOrDefault().m_company_id, tbl.t_angsuran_dp_date.ToString("MM/dd/yyyy"));
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_angsuran_dp_h.FirstOrDefault(p => p.t_angsuran_dp_uid == id);
            }
            if (tbl == null) return HttpNotFound();
            ViewBag.action = action;
            ViewBag.t_angsuran_seq = "1";
            FieldAdd(tbl);
            return View(tbl);
        }

        private void FieldAdd(tb_t_angsuran_dp_h tbl)
        {
            if (tbl.t_spr_id != 0)
            {
                ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(p => p.m_proyek_id == tbl.m_proyek_id).m_proyek_name;
                var mItem = db.tb_m_item.FirstOrDefault(p => p.m_item_id == tbl.m_item_id);
                ViewBag.m_item_name = mItem.m_item_name;
                ViewBag.m_type_unit_name = db.tb_m_type_unit.FirstOrDefault(t => t.m_type_unit_id == mItem.m_item_type_unit_id).m_type_unit_name;

                ViewBag.m_cust_name = db.tb_m_cust.FirstOrDefault(p => p.m_cust_id == tbl.m_cust_id).m_cust_name;
                var tspr = db.tb_t_spr.FirstOrDefault(p => p.t_spr_id == tbl.t_spr_id);
                ViewBag.t_spr_no = tspr.t_spr_no;
                ViewBag.t_dp_type_bayar = tspr.t_dp_type_bayar;
                ViewBag.t_dp_total_angsuran = tspr.t_dp_total_angsuran;
                ViewBag.t_angsuran_amt = db.tb_t_angsuran_dp_d.FirstOrDefault(w => w.t_angsuran_dp_h_id == tbl.t_angsuran_dp_h_id).t_angsuran_dp_d_amt;
                ViewBag.t_dp_amt = tspr.t_dp_amt;

                ViewBag.r_piutang_note = "0";
                if (tspr.t_spr_type_trans == "Ganti Tenor Uang Muka")
                {
                    ViewBag.r_piutang_note = db.tb_r_piutang.Where(r => r.m_item_id == tbl.m_item_id && r.r_piutang_status == "Post" && r.r_piutang_type == "DP").Select(r => r.r_piutang_note.Replace("Uang Muka", "")).Max();
                }
            }
            ViewBag.m_company_id = new SelectList(db.tb_m_company.Where(a => a.active_flag == "ACTIVE").ToList(), "m_company_id", "m_company_name", tbl.m_company_id);
        }

        public ActionResult GenerateNo(DateTime t_angsuran_dp_date, int m_company_id)
        {
            var sJson = GenerateTransNo($"DP", "t_angsuran_dp_no", "tb_t_angsuran_dp_h", m_company_id, t_angsuran_dp_date.ToString("MM/dd/yyyy"));
            return Json(sJson);
        }
        [HttpPost]
        public ActionResult GetDataSPR(string t_dp_type_bayar)
        {
            return GetJsonResult($"Select spr.t_spr_id, spr.t_spr_awal_id, spr.t_spr_no, Format(spr.t_spr_date, 'MM/dd/yyyy') t_spr_date, c.m_cust_id, c.m_cust_name, mp.m_proyek_id, mp.m_proyek_name, i.m_item_id, i.m_item_name, tu.m_type_unit_name, spr.t_dp_type_bayar, spr.t_dp_total_angsuran, spr.t_dp_amt, ROUND(spr.t_dp_amt2,0) t_dp_amt2, t_spr_type_trans, Isnull((select top 1  Cast(REPLACE(r_piutang_note, 'Uang Muka', '') AS int) + 1 From tb_r_piutang r Where r.m_item_id=spr.m_item_id AND r_piutang_ref_table='tb_t_angsuran_dp_d' And r_piutang_status='POST' AND r_piutang_type='DP' ORDER BY r_piutang_ref_id desc), 0) r_piutang_note From tb_t_spr spr Inner Join tb_m_cust c ON c.m_cust_id=spr.m_cust_id Inner Join tb_m_proyek mp ON mp.m_proyek_id=spr.m_proyek_id Inner Join tb_m_item i ON i.m_item_id=spr.m_item_id Inner Join tb_m_type_unit tu ON tu.m_type_unit_id=i.m_item_type_unit_id Where spr.t_dp_type='Ya' AND (Isnull(spr.t_dp_status, '')='' Or spr.t_dp_status is null) AND spr.t_spr_status='Post' AND spr.t_dp_type_bayar='{t_dp_type_bayar}' Order BY spr.t_spr_no");
        }

        public class t_angsuran_dp_d : tb_t_angsuran_dp_d { }

        [HttpPost]
        public ActionResult GetDataDetails(string t_angsuran_dp_date, int t_dp_total_angsuran, decimal t_dp_amt, decimal t_angsuran_amt, string t_spr_type_trans , int t_angsuran_seq)
        {
            List<t_angsuran_dp_d> tbl = new List<t_angsuran_dp_d>();
            var result = "";

            try
            {
                int maxloop = t_dp_total_angsuran;
                decimal dValue = Math.Round(t_angsuran_amt, 0);
                decimal amtTotal = Math.Round(t_dp_amt, 0);
                decimal amtGen = 0;
                string sStatus = "In Process";
                //var seq = r_piutang_note;

                for (var i = 0; i < maxloop; i++)
                {
                    //t_angsuran_seq = t_angsuran_seq;
                    tbl.Add(new t_angsuran_dp_d()
                    {
                        t_angsuran_dp_d_seq = i + 1,
                        t_angsuran_dp_due_date = DateTime.Parse(t_angsuran_dp_date).AddMonths(i),
                        t_angsuran_dp_d_amt_accum = 0,
                        t_angsuran_dp_d_amt = (i == (maxloop - 1) ? (amtTotal - amtGen) : dValue),
                        t_angsuran_dp_d_note = $"Uang Muka {t_angsuran_seq++}",
                        t_angsuran_dp_d_status = sStatus
                    });
                    //if (t_spr_type_trans == "Ganti Tenor Uang Muka") t_angsuran_seq = t_angsuran_seq + 0;
                    amtGen += dValue;
                }

            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            JsonResult js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int t_angsuran_dp_h_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select t_angsuran_dp_d_seq, t_angsuran_dp_due_date, t_angsuran_dp_d_amt, t_angsuran_dp_d_amt_accum, t_angsuran_dp_d_status, Isnull(t_angsuran_dp_d_note, '') t_angsuran_dp_d_note, created_by, created_at from tb_t_angsuran_dp_d Where t_angsuran_dp_h_id={t_angsuran_dp_h_id} Order by t_angsuran_dp_d_seq";
                var tbl = db.Database.SqlQuery<t_angsuran_dp_d>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString(), sSql }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class sParam
        {
            public string t_dp_type_bayar { get; set; }
            public decimal t_dp_amt { get; set; } 
        }
        // POST: poitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_angsuran_dp_h tbl, List<t_angsuran_dp_d> dtDtl, string action, sParam param)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = ""; var servertime = GetServerTime();
            var dp_amt = 0m;
            if (tbl.t_spr_id == 0) msg += "Silahkan Pilih No. Surat Pesanan Rumah ! <br />";  
            if (dtDtl == null) msg += "Silahkan Isi Data Detail ! <br />"; 
            if(dtDtl != null && dtDtl.Count > 0)
            {
                foreach(var item in dtDtl) dp_amt += item.t_angsuran_dp_d_amt; 
                if(param.t_dp_amt != dp_amt) 
                    msg += $"Nominal Total Angsuran {string.Format("{0:#,0.00}", Convert.ToDecimal(dp_amt))} harus sama denga Nominal DP {string.Format("{0:#,0.00}", Convert.ToDecimal(param.t_dp_amt))} <br />"; 
            }
             
            if (tbl.t_angsuran_dp_h_status == "Post")
            {
                tbl.t_angsuran_dp_no = GenerateTransNo($"DP", "t_angsuran_dp_no", "tb_t_angsuran_dp_h", tbl.m_company_id, tbl.t_angsuran_dp_date.ToString("MM/dd/yyyy"));
                tbl.posted_at = servertime;
                tbl.posted_by = Session["UserID"].ToString();
            }

            var tspr = db.tb_t_spr.FirstOrDefault(s => s.t_spr_id == tbl.t_spr_id);
            tspr.t_dp_status = "Complete";
            db.Entry(tspr).State = EntityState.Modified;
            if (string.IsNullOrEmpty(tbl.t_angsuran_dp_h_note)) tbl.t_angsuran_dp_h_note = "";

            if (msg == "")
            {                 
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.created_at = servertime;
                            tbl.created_by = Session["UserID"].ToString();
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString(); 
                            db.tb_t_angsuran_dp_h.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = servertime;
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            var trndtl = db.tb_t_angsuran_dp_d.Where(a => a.t_angsuran_dp_h_id == tbl.t_angsuran_dp_h_id);
                            db.tb_t_angsuran_dp_d.RemoveRange(trndtl);
                        }
                        db.SaveChanges();

                        tb_t_angsuran_dp_d tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = (tb_t_angsuran_dp_d)MappingTable(new tb_t_angsuran_dp_d(), dtDtl[i]);
                            tbldtl.t_angsuran_dp_d_seq = i + 1;
                            tbldtl.t_angsuran_dp_h_id = tbl.t_angsuran_dp_h_id;
                            tbldtl.t_angsuran_dp_d_note = dtDtl[i].t_angsuran_dp_d_note ?? "";
                            tbldtl.created_by = tbl.created_by;
                            tbldtl.created_at = tbl.created_at;
                            db.tb_t_angsuran_dp_d.Add(tbldtl);
                        }
                        db.SaveChanges();

                        if (tbl.t_angsuran_dp_h_status == "Post")
                        { 
                            //Block Post Transaction
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_angsuran_dp_uid, "tb_t_angsuran_dp_h", tbl.t_angsuran_dp_h_id, tbl.updated_by, tbl.updated_at));

                            var spr_shm = db.tb_t_spr.FirstOrDefault(r => r.t_spr_id == tbl.t_spr_id && r.m_item_id == tbl.m_item_id);
                            spr_shm.t_dp_status = "Complete";

                            db.Entry(spr_shm).State = EntityState.Modified;
                            var dtldata = db.tb_t_angsuran_dp_d.Where(d=> d.t_angsuran_dp_h_id==tbl.t_angsuran_dp_h_id).ToList();
                            foreach (var item in dtldata)
                            {
                                db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "DP", "tb_t_angsuran_dp_d", item.t_angsuran_dp_d_id, tspr.t_spr_no, item.t_angsuran_dp_due_date, item.t_angsuran_dp_due_date, Convert.ToDateTime("1/1/1900".ToString()), tspr.t_dp_account_id, item.t_angsuran_dp_d_amt, 0m, item.t_angsuran_dp_d_note ?? "", tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0, tbl.t_spr_id, tbl.t_spr_awal_id));
                            } 
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.t_angsuran_dp_h_id.ToString();
                        if (tbl.t_angsuran_dp_h_status == "Post")
                        {
                            sReturnNo = tbl.t_angsuran_dp_no;
                            sReturnState = "terposting";
                        }
                        else
                        {
                            sReturnNo = "draft " + tbl.t_angsuran_dp_h_id.ToString();
                            sReturnState = "tersimpan";
                        }
                        msg = "Data " + sReturnState + " dengan No. " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += $"Entity of type {eve.Entry.Entity.GetType().Name} in state {eve.Entry.State} has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                err += $"- Property: {ve.PropertyName}, Error: {ve.ErrorMessage} <br />"; 
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: poitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_t_angsuran_dp_h tbl = db.tb_t_angsuran_dp_h.FirstOrDefault(d => d.t_angsuran_dp_uid==id);
            var servertime = GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var tspr = db.tb_t_spr.FirstOrDefault(s => s.t_spr_id == tbl.t_spr_id);
                        tspr.t_dp_status = "";
                        db.Entry(tspr).State = EntityState.Modified;

                        var trndtl = db.tb_t_angsuran_dp_d.Where(a => a.t_angsuran_dp_h_id == tbl.t_angsuran_dp_h_id);
                        db.tb_t_angsuran_dp_d.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.tb_t_angsuran_dp_h.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}