﻿using MIS_MAGNA_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class SHMFormController : Controller
    {
        // GET: SHMForm
        private QL_MIS_MAGNAEntities db;
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public SHMFormController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        public class dt_gl
        {
            public decimal r_gl_amt { get; set; }
            public int m_account_id { get; set; }
            public string r_gl_db_cr { get; set; }
        }

        #region SHM Form Controller
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            var isCollapse = "True";
            InitAdvFilterIndex(modfil, "tb_t_spr", false);
            ViewBag.isCollapse = isCollapse;
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( SELECT h.t_spr_uid, h.t_spr_id, t_spr_date, h.t_spr_no, p.m_proyek_name, c.m_cust_name, i.m_item_name, t_shm_type, t_shm_type_bayar, t_shm_total_angsuran, t_shm_amt, t_spr_note, t_shm_status, updated_at FROM tb_t_spr h INNER JOIN tb_m_company cmp ON cmp.m_company_id=h.m_company_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = h.m_proyek_id INNER JOIN tb_m_cust c ON c.m_cust_id = h.m_cust_id INNER JOIN tb_m_item i ON i.m_item_id = h.m_item_id Where t_spr_type_trans IN ('Baru') AND t_spr_status='Post' ) AS t";
            var sFixedFilter = ""; 
            var sOrder_by = " t.updated_at DESC";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_t_spr tbl;
            string action = "Update Data";
            action = "Update Data";
            tbl = db.tb_t_spr.Find(id);
            ViewBag.action = action;
            tbl.t_shm_type = "Ya";
            //InitDDL(tbl, action);
            FillAdditionalField(tbl, action);
            return View(tbl);
        }

        private void FillAdditionalField(tb_t_spr tbl, string action)
        {
            var getCompany = db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList();
            if (action == "New Data") getCompany = getCompany.Where(m => m.flag == "ACTIVE").ToList();
            ViewBag.m_company_id = new SelectList(getCompany, "valuefield", "textfield", tbl.m_company_id);
            ViewBag.m_cust_name = (tbl.m_cust_id != 0 ? db.tb_m_cust.FirstOrDefault(x => x.m_cust_id == tbl.m_cust_id).m_cust_name : "");
            ViewBag.m_item_name = (tbl.m_item_id != 0 ? db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id).m_item_name : "");
            ViewBag.m_proyek_name = (tbl.m_proyek_id != 0 ? db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == tbl.m_proyek_id).m_proyek_name : "");
            var t_shm_account_id = new SelectList(GetCOAInterface(new List<string> { "PIUTANG SHM" }), "m_account_id", "m_account_name", tbl.t_shm_account_id);
            ViewBag.t_shm_account_id = t_shm_account_id;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_spr tbl)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var msg = ""; var result = "failed"; var hdrid = ""; 
            if (string.IsNullOrEmpty(tbl.t_shm_status)) tbl.t_shm_status = "In Process";
            if (string.IsNullOrEmpty(tbl.t_utj_type)) tbl.t_utj_type = "";
            if (string.IsNullOrEmpty(tbl.t_spr_promo_dp_type)) tbl.t_spr_promo_dp_type = "";
            if (string.IsNullOrEmpty(tbl.t_spr_promo_dp_note)) tbl.t_spr_promo_dp_note = "";
            if (string.IsNullOrEmpty(tbl.t_spr_note)) tbl.t_spr_note = "";
            if (string.IsNullOrEmpty(tbl.t_shm_status)) tbl.t_shm_status = "";
             
            if (tbl.t_shm_type == "Ya")
            {
                if (tbl.t_shm_type_bayar == "Angsuran" && tbl.t_shm_total_angsuran <= 0)
                    msg += "Total angsuran SHM tanah harus lebih dari 0!<br>";
                if (tbl.t_shm_amt <= 0) msg += "Jumlah SHM harus lebih dari 0!<br>";
            } 

            if (string.IsNullOrEmpty(msg))
            { 
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.updated_at = GetServerTime();
                        tbl.updated_by = Session["UserID"].ToString();
                        tbl.posted_at = GetServerTime();
                        tbl.posted_by = Session["UserID"].ToString();
                        tbl.t_dp_amt_old = 0;
                        tbl.t_piutang_amt_old = 0;
                        tbl.t_kt_amt_old = 0;
                        db.Entry(tbl).State = EntityState.Modified;
                        db.SaveChanges();

                        if (tbl.t_shm_status == "Post")
                        { 
                            // --> SHM
                            if (tbl.t_shm_type == "Ya" && tbl.t_shm_type_bayar == "Tunai" && tbl.t_shm_amt > 0)
                            {
                                db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "SHM", "tb_t_spr_shm", tbl.t_spr_id, tbl.t_spr_no, tbl.t_spr_date, tbl.t_spr_date, Convert.ToDateTime("1/1/1900".ToString()), tbl.t_shm_account_id, tbl.t_shm_amt, 0m, "SHM", tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0, tbl.t_spr_id, tbl.t_spr_awal_id));
                                tbl.t_shm_status = "Post";
                            } 
                            // --> Insert Jurnal
                            msg = PostJurnal(tbl);
                            if (!string.IsNullOrEmpty(msg))
                            {
                                objTrans.Rollback(); result = "failed";
                                return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        db.SaveChanges();
                        objTrans.Commit();

                        hdrid = tbl.t_spr_id.ToString(); result = "success";
                        if (tbl.t_shm_status == "Post") msg = $"data telah diposting dengan nomor transaksi {tbl.t_spr_no}";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }
         

        private string PostJurnal(tb_t_spr tbl)
        {
            var msg = "";
            try
            {
                var new_dt_gl = new List<dt_gl>();  
                var jual_account_id = GetCOAInterface(new List<string> { "HUTANG PENJUALAN" }).FirstOrDefault()?.m_account_id ?? 0;

                var total_ar = tbl.t_dp_amt + tbl.t_kt_amt + tbl.t_shm_amt + tbl.t_piutang_amt;
                new_dt_gl.Add(new dt_gl { r_gl_amt = tbl.t_shm_amt, m_account_id = tbl.t_shm_account_id, r_gl_db_cr = "D" });
                new_dt_gl.Add(new dt_gl { r_gl_amt = tbl.t_shm_amt, m_account_id = jual_account_id, r_gl_db_cr = "C" }); 

                var seq = 1; var gl_note = $"SPR (NO: {tbl.t_spr_no} | CUSTOMER: {db.tb_m_cust.FirstOrDefault(x => x.m_cust_id == tbl.m_cust_id)?.m_cust_name ?? ""}) - NOTE: {tbl.t_spr_note}";
                if (new_dt_gl != null && new_dt_gl.Count() > 0)
                { 
                    foreach (var item in new_dt_gl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, "tb_t_spr", tbl.t_spr_id, tbl.t_spr_no, tbl.updated_at, item.m_account_id, item.r_gl_db_cr, item.r_gl_amt, gl_note, tbl.created_by, tbl.created_at));
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.ToString();
            }
            db.SaveChanges();
            return msg;
        }

        #endregion
    }
}