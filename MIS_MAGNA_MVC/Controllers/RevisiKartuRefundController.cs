﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;

namespace MIS_MAGNA_MVC.Controllers
{
    public class RevisiKartuRefundController : Controller
    {
        // GET: RevisiKartuRefund
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        public RevisiKartuRefundController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        }

        #region Revisi Bayar Refund
        private void InitDDLAndField(tb_t_refund tbl)
        { 
            ViewBag.m_company_id = new SelectList(db.tb_m_company.Select(m => new { valuefield = m.m_company_id, textfield = m.m_company_name, flag = m.active_flag }).ToList(), "valuefield", "textfield", tbl.m_company_id);

            //Field
            ViewBag.t_angsuran_refund_date = tbl.t_refund_type_bayar == "Anguran" ? db.tb_t_angsuran_refund_h.FirstOrDefault(d => d.t_refund_id == tbl.t_refund_id).t_angsuran_refund_date : GetServerTime();
            ViewBag.m_item_name = db.tb_m_item.FirstOrDefault(w => w.m_item_id == tbl.m_item_id && w.m_proyek_id==tbl.m_proyek_id)?.m_item_name ?? "";
            ViewBag.m_proyek_name = db.tb_m_proyek.FirstOrDefault(w => w.m_proyek_id == tbl.m_proyek_id)?.m_proyek_name ?? "";
            ViewBag.m_cust_name = db.tb_m_cust.FirstOrDefault(w => w.m_cust_id == tbl.m_cust_id)?.m_cust_name ?? ""; 

            var t_spr= db.tb_t_spr.Where(s => s.m_item_id == tbl.m_item_id && s.m_cust_id == tbl.m_cust_id && s.t_spr_awal_id == tbl.t_spr_awal_id && s.t_spr_id==tbl.t_refund_ref_id).ToList();
            if(t_spr.Count> 0)
            {
                ViewBag.t_spr_no = t_spr.FirstOrDefault().t_spr_no.ToString();
                ViewBag.t_utj_amt = t_spr.FirstOrDefault().t_utj_amt;
            }            
        }

        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_t_refund", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE isapptrans=0 AND isposttrans=0 ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column() { name = "sAddFilter", data = "sAddFilter", search = new Search() { value = "", regex = "" }, searchable = true, orderable = false };
                model.columns.Add(sAddFilter);
            }

            sSql = "Select * From ( Select r.t_refund_id, r.m_item_id, r.m_cust_id, r.t_refund_no, i.m_item_name, c.m_cust_name, p.m_proyek_name, r.t_refund_date, r.t_refund_price, r.t_refund_pay_amt, r.t_refund_pay_amt_dp, r.t_refund_pay_amt_kpr, r.t_refund_pay_amt_kt, r.t_refund_cost_amt, r.t_refund_cost_other_amt, r.t_refund_total_amt, r.t_refund_total_angsuran, r.t_refund_status From tb_t_refund r Inner Join tb_m_item i ON i.m_item_id=r.m_item_id Inner join tb_m_cust c ON c.m_cust_id=r.m_cust_id Inner Join tb_m_proyek p ON p.m_proyek_id=r.m_proyek_id AND r.t_refund_type='Batal SPR' ) r ";
            var sFixedFilter = "";
            var sOrder_by = " r.t_refund_id";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile"); 
            tb_t_refund tbl = db.tb_t_refund.Find(id);  
            InitDDLAndField(tbl);
            return View(tbl);
        }

        public class sFilter
        {
            public DateTime t_angsuran_refund_date { get; set; }
            public int t_refund_total_angsuran { get; set; }         
            public int m_item_id { get; set; }
            public int m_cust_id { get; set; }            
            public string ddl_type_bayar { get; set; }
            public decimal t_refund_total_amt { get; set; }
            public decimal t_per_bln_angsuran { get; set; }
        }

        public class r_hutang_d : tb_r_hutang
        {
            public int r_hutang_seq { get; set; }
            public string m_item_name { get; set; }
            public string m_cust_name { get; set; }
            public string m_proyek_name { get; set; }
        }

        public class t_angsuran_refund_d : tb_t_angsuran_refund_d
        {
        }

        [HttpPost]
        public ActionResult FillDetailData(sFilter param)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select 0 r_hutang_seq, b.r_hutang_id, b.r_hutang_ref_id, b.r_hutang_ref_table, b.r_hutang_ref_due_date, b.r_hutang_ref_date, (Select r.r_hutang_amt from tb_r_hutang r Where r.r_hutang_ref_id=b.r_hutang_ref_id AND r.r_hutang_type<>'PAYMENT' AND b.r_hutang_ref_table=r.r_hutang_ref_table ) r_hutang_amt, r_hutang_pay_amt, b.r_hutang_pay_ref_id, b.r_hutang_pay_ref_no, b.r_hutang_pay_ref_date, b.r_hutang_pay_ref_no +' - '+ b.r_hutang_note r_hutang_note, b.r_hutang_pay_ref_table From tb_r_hutang b where m_item_id={param.m_item_id} AND b.r_hutang_type='PAYMENT' AND b.m_cust_id={param.m_cust_id} Order By r_hutang_ref_id";
                if(param.ddl_type_bayar== "Angsuran")
                    sSql = $"Select 0 r_hutang_seq, b.r_hutang_id, b.r_hutang_ref_id, b.r_hutang_ref_table, b.r_hutang_ref_due_date, r_hutang_amt, b.r_hutang_ref_date, Isnull((Select SUM(r.r_hutang_pay_amt) from tb_r_hutang r Where r.r_hutang_ref_id=b.r_hutang_ref_id AND r.r_hutang_type='PAYMENT' AND b.r_hutang_ref_table=r.r_hutang_ref_table ), 0.0) r_hutang_pay_amt, b.r_hutang_pay_ref_id, b.r_hutang_ref_no r_hutang_pay_ref_no, Isnull((Select MAX(r.r_hutang_pay_ref_date) from tb_r_hutang r Where r.r_hutang_ref_id=b.r_hutang_ref_id AND r.r_hutang_type='PAYMENT' AND b.r_hutang_ref_table=r.r_hutang_ref_table ), b.r_hutang_ref_due_date) r_hutang_pay_ref_date, b.r_hutang_ref_no +' - '+ r_hutang_note r_hutang_note, Isnull((Select TOP 1 (r.r_hutang_type) from tb_r_hutang r Where r.r_hutang_ref_id=b.r_hutang_ref_id AND r.r_hutang_type='PAYMENT' AND b.r_hutang_ref_table=r.r_hutang_ref_table ), '') r_hutang_type From tb_r_hutang b where m_item_id={param.m_item_id} AND b.r_hutang_type!='PAYMENT' AND b.m_cust_id={param.m_cust_id} Order By r_hutang_ref_id";
                var tbl = db.Database.SqlQuery<r_hutang_d>(sSql).ToList();
                //var tbl = db.Database.SqlQuery<r_hutang_d>(sSql).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = sSql + e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }
         
        [HttpPost]
        public ActionResult GetDataDetails(sFilter param, List<r_hutang_d> data)
        {
            List<tb_r_hutang> tbl = new List<tb_r_hutang>();
            var result = ""; 
            try
            {
                int maxloop = param.t_refund_total_angsuran;
                decimal dValue = Math.Round(param.t_per_bln_angsuran, 0);
                decimal amtTotal = Math.Round(param.t_refund_total_amt, 0);
                decimal amtGen = 0;

                for (var i = 0; i < maxloop; i++)
                { 
                    var r_hutang_seq = i + 1;
                    tbl.Add(new r_hutang_d()
                    {
                        r_hutang_seq = i + 1,
                        r_hutang_note = $"Refund {r_hutang_seq}",
                        r_hutang_ref_due_date = param.t_angsuran_refund_date.AddMonths(i), 
                        r_hutang_ref_date = param.t_angsuran_refund_date.AddMonths(i),
                        r_hutang_amt = (i == (maxloop - 1) ? (amtTotal - amtGen) : dValue),
                        r_hutang_pay_ref_date = ("1/1/1900").ToDateTime(),
                        r_hutang_pay_amt = 0,
                        r_hutang_pay_ref_id = 0, 
                        r_hutang_pay_account_id = 0,
                        r_hutang_account_id = 0,
                        r_hutang_ref_table = "tb_t_angsuran_refund_d",
                    });
                    amtGen += dValue;
                }

            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            JsonResult js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        // public ActionResult EditData(tb_t_refund tbl, List<r_hutang_d> dtDtl)
        public ActionResult Form(tb_t_refund tbl, List<r_hutang_d> dtDtl)
        {
            var msg = "";
            var result = "failed";
            var hdrid = new Guid();
            if (dtDtl == null || dtDtl.Count <= 0) msg += "Please fill detail Process!<br />";

            if (string.IsNullOrEmpty(msg))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (dtDtl != null && dtDtl.Count() > 0)
                        {
                            foreach (var item in dtDtl)
                            {
                                if (item.r_hutang_pay_ref_id > 0)
                                {
                                    int t_kbk_h_id = db.tb_t_kbk_d.Where(k => k.t_kbk_d_id == item.r_hutang_pay_ref_id).FirstOrDefault().t_kbk_h_id;
                                    var t_kbk_h = db.tb_t_kbk_h.Where(a => a.t_kbk_h_id == t_kbk_h_id).FirstOrDefault();
                                    t_kbk_h.t_kbk_no = "";
                                    t_kbk_h.updated_at = GetServerTime();
                                    t_kbk_h.updated_by = Session["UserID"].ToString();
                                    t_kbk_h.t_kbk_h_status = "In Process";
                                    db.Entry(t_kbk_h).State = EntityState.Modified;

                                    var r_kb = db.tb_r_kb.Where(r => r.r_kb_ref_id == item.r_hutang_pay_ref_id && r.m_item_id == tbl.m_item_id && r.r_kb_ref_table == "tb_t_kbk_d").ToList();
                                    if (r_kb.Count > 0) db.tb_r_kb.RemoveRange(r_kb);

                                    var r_gl = db.tb_r_gl.Where(r => r.r_gl_ref_id == t_kbk_h_id && r.r_gl_ref_table == "tb_t_kbk_h");
                                    db.tb_r_gl.RemoveRange(r_gl);
                                }

                                var r_hutang = db.tb_r_hutang.FirstOrDefault(r => r.r_hutang_pay_ref_id == item.r_hutang_pay_ref_id && r.m_item_id == tbl.m_item_id && r.m_cust_id == tbl.m_cust_id && r.r_hutang_type=="PAYMENT");
                                db.tb_r_hutang.Remove(r_hutang);
                            }
                        }
                        db.SaveChanges();
                        objTrans.Commit();
                        result = "success";
                        dtDtl = null;
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid, dtDtl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("GantiTenor")]
        [ValidateAntiForgeryToken]
        public ActionResult GantiTenor(int t_refund_id, tb_t_refund tbl, List<r_hutang_d> dtDtl, sFilter param)
        {
            var msg = ""; var result = "failed"; var hdrid = new Guid();
            tbl = db.tb_t_refund.Find(t_refund_id);
            var rHutang = db.tb_r_hutang.Where(a => a.m_item_id == tbl.m_item_id && a.m_cust_id==tbl.m_cust_id).ToList();
            if (param.ddl_type_bayar == "None") msg += "Pilih Type Bayar Revisi dulu!<br />";
            if ((dtDtl == null || dtDtl.Count <= 0) && tbl.t_refund_type_bayar == "Angsuran") msg += "Please fill detail Process!<br />";            
            if (param.ddl_type_bayar == "Angsuran" && rHutang.Where(r=> r.r_hutang_type=="PAYMENT").Count() > 0) msg += "Silahkan hapus pembayaran dulu!<br />";

            var t_refund_account_id = db.tb_t_refund.Where(t=> t.t_refund_id==tbl.t_refund_id).ToList();
            if(t_refund_account_id.Count==0) msg = "COA REFUND BELUM DI SETTING..<br />";

            var genNumber = GenerateTransNo($"GREF", "t_angsuran_refund_no", "tb_t_angsuran_refund_h", tbl.m_company_id, param.t_angsuran_refund_date.ToString("MM/dd/yyyy"));

            if (string.IsNullOrEmpty(msg))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.t_refund_status = tbl.t_refund_type_bayar == "Tunai" ? "Post":"Complete";
                        tbl.t_refund_account_id = t_refund_account_id.FirstOrDefault().t_refund_account_id;
                        tbl.t_refund_cost_account_id= t_refund_account_id.FirstOrDefault().t_refund_cost_account_id;
                        tbl.t_refund_type_bayar = tbl.t_refund_type_bayar;
                        tbl.t_refund_total_angsuran = tbl.t_refund_total_angsuran;
                        if (string.IsNullOrEmpty(tbl.t_refund_note)) tbl.t_refund_note = "";
                        tbl.posted_at = GetServerTime();
                        tbl.posted_by = Session["UserID"].ToString();
                        db.Entry(tbl).State = EntityState.Modified;
                        db.SaveChanges();

                        // Delete kartu Hutang  
                        var dt_h = db.tb_t_angsuran_refund_h.Where(a => a.t_refund_id == tbl.t_refund_id);
                        if (dt_h.Count() > 0)
                        {
                            var ref_h_id = dt_h.Select(a=> a.t_refund_id).Distinct();
                            var trndtl = db.tb_t_angsuran_refund_d.Where(a => ref_h_id.Contains(a.t_angsuran_refund_h_id)).ToList();
                            if(trndtl.Count()>0) db.tb_t_angsuran_refund_d.RemoveRange(trndtl);
                            db.tb_t_angsuran_refund_h.RemoveRange(dt_h);
                            db.SaveChanges();
                        } 

                        if (param.ddl_type_bayar == "Angsuran")
                        { 
                            if (tbl.t_refund_type_bayar == "Tunai")
                            { 
                                if (rHutang != null && rHutang.Count() > 0)
                                { 
                                    db.tb_r_hutang.RemoveRange(rHutang);
                                    db.SaveChanges();
                                }
                                db.tb_r_hutang.Add(Insert_R_Hutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "Refund", "tb_t_refund", tbl.t_refund_id, tbl.t_refund_no, tbl.t_refund_date, tbl.t_refund_date, tbl.t_refund_date, tbl.t_refund_account_id, tbl.t_refund_total_amt, 0m, tbl.t_refund_note, tbl.created_by, tbl.created_at, "", 0, "", 0, t_spr_id: tbl.t_refund_ref_id, t_spr_awal_id: tbl.t_spr_awal_id));
                                db.SaveChanges();
                            }
                            else if (tbl.t_refund_type_bayar == "Angsuran")
                            { 
                                if (rHutang != null && rHutang.Count() > 0)
                                {
                                    db.tb_r_hutang.RemoveRange(rHutang);
                                    db.SaveChanges();
                                }

                                tb_t_angsuran_refund_h tbl_h = new tb_t_angsuran_refund_h();
                                tbl_h.t_angsuran_refund_uid = Guid.NewGuid();
                                tbl.t_refund_id = tbl.t_refund_id;
                                tbl_h.t_spr_awal_id = tbl.t_spr_awal_id;
                                tbl_h.m_cust_id = tbl.m_cust_id;
                                tbl_h.m_item_id = tbl.m_item_id;
                                tbl_h.m_company_id = tbl.m_company_id;
                                tbl_h.m_proyek_id = tbl.m_proyek_id;
                                tbl_h.t_angsuran_refund_date = param.t_angsuran_refund_date;
                                tbl_h.t_angsuran_refund_no = genNumber;
                                tbl_h.t_angsuran_refund_h_status = "Post";
                                tbl_h.t_angsuran_refund_h_note = "Tambah Kartu";
                                tbl_h.created_at = GetServerTime();
                                tbl_h.created_by = Session["UserID"].ToString();
                                tbl_h.updated_at = param.t_angsuran_refund_date;
                                tbl_h.updated_by = Session["UserID"].ToString();
                                db.tb_t_angsuran_refund_h.Add(tbl_h);
                                db.SaveChanges();

                                tb_t_angsuran_refund_d tbl_d;
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    tbl_d = (tb_t_angsuran_refund_d)MappingTable(new tb_t_angsuran_refund_d(), dtDtl[i]);
                                    tbl_d.t_angsuran_refund_h_id = tbl_h.t_angsuran_refund_h_id;
                                    tbl_d.t_angsuran_refund_d_seq = i + 1;
                                    tbl_d.t_angsuran_refund_due_date = dtDtl[i].r_hutang_ref_due_date;
                                    tbl_d.t_angsuran_refund_d_amt = dtDtl[i].r_hutang_amt;
                                    tbl_d.t_angsuran_refund_d_amt_accum = 0;
                                    tbl_d.t_angsuran_refund_d_status = "Post";
                                    tbl_d.t_angsuran_refund_d_note = dtDtl[i].r_hutang_note ?? "";
                                    tbl_d.created_by = Session["UserID"].ToString();
                                    tbl_d.created_at = GetServerTime();
                                    db.tb_t_angsuran_refund_d.Add(tbl_d);
                                }
                                db.SaveChanges(); 
                                var dtldata = db.tb_t_angsuran_refund_d.Where(d => d.t_angsuran_refund_h_id == tbl_h.t_angsuran_refund_h_id).ToList();
                                foreach (var item in dtldata)
                                {
                                    db.tb_r_hutang.Add(Insert_R_Hutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, "Refund", "tb_t_angsuran_refund_d", item.t_angsuran_refund_d_id, tbl.t_refund_no, item.t_angsuran_refund_due_date, item.t_angsuran_refund_due_date, Convert.ToDateTime("1/1/1900".ToString()), tbl.t_refund_account_id, item.t_angsuran_refund_d_amt, 0m, item.t_angsuran_refund_d_note ?? "", tbl.created_by, tbl.created_at, "", 0, "", 0, t_spr_id: tbl.t_refund_ref_id, t_spr_awal_id: tbl.t_spr_awal_id));
                                } 
                            }
                        }
                        db.SaveChanges();
                        objTrans.Commit();
                        result = "success";
                        dtDtl = null;
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid, dtDtl }, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}