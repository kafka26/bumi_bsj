﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MIS_MAGNA_MVC.Models;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine; 
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using System.Web.UI.WebControls;

namespace MIS_MAGNA_MVC.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        private QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        #region Master Customer
        public class tb_hist : tb_m_cust_hist
        { 
        }

        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            InitAdvFilterIndex(modfil, "tb_m_cust", false);
            return View();
        }

        private void InitAdvFilterIndex(ModelFilter modfil, string tblname, bool isapproval)
        {
            var filterddl = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT tblfield valuefield, fieldlabel textfield FROM tb_formfilterddl WHERE tblname='" + tblname + "' ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterddl);
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = modfil.filtertext;
            ViewBag.isperiodchecked = modfil.isperiodchecked;
            if (modfil.filterperiodfrom == null || modfil.filterperiodfrom.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ViewBag.filterperiodfrom = modfil.filterperiodfrom;
            if (modfil.filterperiodto == null || modfil.filterperiodto.ToString("MM/dd/yyyy") == "01/01/0001")
                modfil.filterperiodto = ClassFunction.GetServerTime();
            ViewBag.filterperiodto = modfil.filterperiodto;

            var filterstatus = new SelectList(db.Database.SqlQuery<ReportModels.DDLDoubleFieldString>("SELECT statuslabel valuefield, statuslabel textfield FROM tb_formfilterstatus WHERE " + (isapproval ? "isapptrans=1" : "isposttrans=1") + " ORDER BY oid").ToList(), "valuefield", "textfield", modfil.filterstatus);
            ViewBag.filterstatus = filterstatus;
        }

        public JsonResult getListDataTable(DataTableAjaxPostModel model)
        {
            // Add Custom Filter
            var sAddFilter = model.columns.Where(w => w.name == "sAddFilter").FirstOrDefault();
            if (sAddFilter == null)
            {
                sAddFilter = new Column()
                {
                    name = "sAddFilter",
                    data = "sAddFilter",
                    search = new Search()
                    {
                        value = "",
                        regex = ""
                    },
                    searchable = true,
                    orderable = false
                };
                model.columns.Add(sAddFilter);
            }
            // end Of custom Filter

            sSql = "SELECT * FROM ( SELECT c.m_cust_id, c.m_cust_uid, c.m_cust_code, c.m_cust_name, c.m_cust_addr, c.m_cust_nik, c.m_cust_gender, m_cust_npwp, c.created_by, c.created_at, c.last_edited_by, c.last_edited_at, c.m_cust_flag FROM tb_m_cust c ) AS t";
            var sFixedFilter = "";
            var sOrder_by = " t.m_cust_code";

            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            var res = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sSql, sFixedFilter, sOrder_by);

            var result = new List<Dictionary<string, object>>(res.Count);
            foreach (var s in res) result.Add(s);
            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }

        public ActionResult Form(Guid? id)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_m_cust tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_m_cust();
                tbl.m_cust_uid = Guid.NewGuid();
                tbl.m_company_id = db.tb_m_company.FirstOrDefault().m_company_id;
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.m_cust_birth_date = GetServerTime();
                tbl.m_company_id = db.tb_m_company.FirstOrDefault(d=>d.active_flag=="ACTIVE").m_company_id;
                tbl.m_cust_work = "-";
            }
            else
            { 
                action = "Update Data";
                tbl = db.tb_m_cust.FirstOrDefault(p => p.m_cust_uid == id);
            }
            if (tbl == null) return HttpNotFound();
            ViewBag.action = action;
            var dtHist = db.Database.SqlQuery<tb_hist>($"select top 1 * from tb_m_cust_hist Where m_cust_id={tbl.m_cust_id} Order By m_cust_d_id desc").ToList();
            ViewBag.m_cust_name_new = tbl.m_cust_name;
            ViewBag.m_cust_old_name = tbl.m_cust_name;
            ViewBag.m_cust_nik_new = tbl.m_cust_nik;
            ViewBag.m_cust_nik_old = tbl.m_cust_nik;
            ViewBag.m_city_id = new SelectList(db.tb_m_city.Where(a => a.m_city_flag == "ACTIVE").ToList(), "m_city_id", "m_city_name", tbl.m_city_id);
            ViewBag.m_cust_city_id = new SelectList(db.tb_m_city.Where(a => a.m_city_flag == "ACTIVE").ToList(), "m_city_id", "m_city_name", tbl.m_cust_city_id);
            ViewBag.m_employee_id = new SelectList(db.tb_m_user.Where(a => a.m_employee_id != 0).ToList(), "m_user_id", "m_user_login", tbl.m_employee_id);
            return View(tbl);
        }

        private string GenerateCode(string m_cust_name)
        {
            var length = 3;
            string sNo = m_cust_name.Left(1).ToUpper();
            sSql = $"SELECT ISNULL(MAX(CAST(RIGHT(m_cust_code, {length}) AS INTEGER)) + 1, 1) AS IDNEW FROM tb_m_cust WHERE m_cust_code LIKE '{ sNo }%' AND ISNUMERIC(RIGHT(m_cust_code, {length})) = 1";
            return sNo + GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), length);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_m_cust tbl, string action, tb_hist param )
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile"); 
            var msg = ""; var result = "failed"; var hdrid = ""; var ref_code = tbl.m_cust_code;
            var servertime = GetServerTime();
            if (string.IsNullOrEmpty(tbl.m_cust_name)) msg += "Tolong, Isi Nama customer..!<br />";
            if (string.IsNullOrEmpty(tbl.m_cust_nik)) msg += "Tolong, Isi No. KTP customer..!<br />";
            if (string.IsNullOrEmpty(tbl.m_cust_addr)) msg += "Tolong, Isi Alamat customer..!<br />";
            if (string.IsNullOrEmpty(tbl.m_cust_phone1)) msg += "Tolong, Isi No. Telp/Hp1 customer..!<br />"; 
            if (string.IsNullOrEmpty(tbl.m_cust_office_name)) msg += "Tolong, Nama Perusahaan customer..!<br />";
            if (string.IsNullOrEmpty(tbl.m_cust_office_addr)) msg += "Tolong, Alamat Perusahaan customer..!<br />";
            if (string.IsNullOrEmpty(tbl.m_cust_birth_city)) tbl.m_cust_birth_city = "";
            if (string.IsNullOrEmpty(tbl.m_cust_code)) tbl.m_cust_code = GenerateCode(tbl.m_cust_name);
            if (string.IsNullOrEmpty(tbl.m_cust_work)) tbl.m_cust_work = "";
            
            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.last_edited_at = tbl.created_at;
                            tbl.last_edited_by = tbl.created_by;
                            db.tb_m_cust.Add(tbl);
                        }
                        else
                        { 
                            tbl.last_edited_at = servertime;
                            tbl.last_edited_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            if (tbl.m_cust_name != param.m_cust_old_name)
                            {
                                var sTbl = new tb_m_cust_hist();
                                sTbl.m_cust_id = tbl.m_cust_id;
                                sTbl.m_cust_name_new = param.m_cust_name_new;
                                sTbl.m_cust_old_name = param.m_cust_old_name;
                                sTbl.m_cust_nik_old = param.m_cust_nik_old;
                                sTbl.m_cust_nik_new = param.m_cust_nik_new;
                                sTbl.last_edited_at = servertime;
                                sTbl.last_edited_by = Session["UserID"].ToString();
                                db.tb_m_cust_hist.Add(sTbl);
                            }
                        }

                        db.SaveChanges(); objTrans.Commit();
                        hdrid = tbl.m_cust_uid.ToString();
                        msg = "Data Already Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, ref_code, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid? m_cust_uid)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            tb_m_cust tbl = db.tb_m_cust.FirstOrDefault(p => p.m_cust_uid == m_cust_uid);
            var servertime = GetServerTime(); 
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.tb_m_cust.Remove(tbl);
                        db.SaveChanges();
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            string rptname = "AccKartuKontrol";
            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), $"rpt{rptname}.rpt"));

            sSql = $"Select * From tb_v_report_kartu_kontrol r Where m_cust_id={ids} ORDER BY m_proyek_id, m_cust_id, m_item_id, seq, r_piutang_ref_id, r_piutang_pay_date";
            var dtRpt = new ClassConnection().GetDataTable(sSql, "data"); 
            report.SetDataSource(dtRpt);            
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose(); 
            base.Dispose(disposing);
        } 
        #endregion
    }
}