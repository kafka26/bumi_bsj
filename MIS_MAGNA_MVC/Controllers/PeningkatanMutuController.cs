﻿using MIS_MAGNA_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.GlobalFunctions;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using System.Collections;

namespace MIS_MAGNA_MVC.Controllers
{
    public class PeningkatanMutuController : Controller
    {
        // GET: PeningkatanMutu
        private QL_MIS_MAGNAEntities db;
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        public string sSql = "";
        public string sWhere = "";
        public PeningkatanMutuController()
        {
            db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
        } 

        public class t_titipan : tb_t_titipan
        {
            public decimal t_titipan_debet_amt { get; set; }
            public decimal t_titipan_credit_amt { get; set; }
            public decimal t_titipan_os_amt { get; set; }
        }

        public class dt_gl
        {
            public decimal r_gl_amt { get; set; }
            public int m_account_id { get; set; }
            public string r_gl_db_cr { get; set; }
        }

        #region Peningkatan Mutu 

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(ModelFilter modfil)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile"); 
            ViewBag.periodstart = GetServerTime().ToString("01/dd/yyyy");
            ViewBag.periodend = GetServerTime().ToString("MM/dd/yyyy");
            ViewBag.Title = "Peningkatan Mutu";
            return View();
        }

        [HttpPost]
        public ActionResult GetDataIndex(string periodstart, string periodend)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT * FROM ( SELECT h.*, p.m_proyek_name, c.m_cust_name, i.m_item_name, s.t_spr_no FROM tb_t_titipan h INNER JOIN tb_m_company cmp ON cmp.m_company_id=h.m_company_id INNER JOIN tb_m_proyek p ON p.m_proyek_id = h.m_proyek_id INNER JOIN tb_m_cust c ON c.m_cust_id = h.m_cust_id INNER JOIN tb_m_item i ON i.m_item_id = h.m_item_id INNER JOIN tb_t_spr s ON s.t_spr_id = h.t_spr_id WHERE h.t_titipan_trans_type='PENINGKATAN' AND Isnull(h.t_titipan_flag, '') = '') AS t Order By created_at DESC";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        } 

        [HttpPost]
        public ActionResult GetDataCust(string t_titipan_type_bayar, string t_titipan_type)
        {
            JsonResult js = null;
            try
            {
                sSql = $"Select distinct c.m_cust_id, c.m_cust_code, c.m_cust_name, c.m_cust_addr, s.m_proyek_id, s.t_spr_id, s.t_spr_awal_id, s.t_spr_no, i.m_item_id, i.m_item_name, (select p.m_proyek_name from tb_m_proyek p where p.m_proyek_id=s.m_proyek_id) m_proyek_name from tb_m_cust c inner join tb_t_spr s on s.m_cust_id = c.m_cust_id Inner Join tb_m_item i ON i.m_item_id=s.m_item_id AND i.m_proyek_id=s.m_proyek_id ";
                if (t_titipan_type_bayar != "SALDO")
                {
                    sSql += $"Inner Join ( Select r.m_cust_id, r.m_item_id, r.t_spr_id, r.r_piutang_amt - Isnull(SUM(b.r_piutang_pay_amt), 0.0) r_piutang_sisa_amt From tb_r_piutang r Left Join tb_r_piutang b ON b.r_piutang_ref_id=r.r_piutang_ref_id AND r.r_piutang_ref_table=b.r_piutang_ref_table AND b.r_piutang_type='PENERIMAAN' Where r.r_piutang_ref_table='tb_t_titipan' AND r.r_piutang_type='SALDO' AND r.r_piutang_trans_type='PENINGKATAN' AND r.r_piutang_status='Post' Group By r.m_cust_id, r.m_item_id, r.t_spr_id, r.r_piutang_amt ) r ON r.m_cust_id=c.m_cust_id AND r.m_item_id=i.m_item_id AND r.t_spr_id=s.t_spr_id {(t_titipan_type_bayar == "PENERIMAAN" ? $" AND r_piutang_sisa_amt > 0" : $" AND r_piutang_sisa_amt <= 0")}";
                }
                sSql += $" where s.t_spr_status = 'Post' and c.m_cust_flag = 'ACTIVE' {sWhere} Order By c.m_cust_code";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataItem(int m_company_id, int m_cust_id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"select c.m_cust_id, c.m_cust_name, i.m_proyek_id, p.m_proyek_name, i.m_item_id, i.m_item_name, tu.m_type_unit_name, isnull(s.t_spr_awal_id, 0) t_spr_awal_id, s.t_spr_id, s.t_spr_no, format(s.t_spr_date, 'dd/MM/yyyy') t_spr_date from tb_m_cust c inner join tb_t_spr s on s.m_cust_id = c.m_cust_id inner join tb_m_item i on i.m_item_id = s.m_item_id inner join tb_m_type_unit tu on tu.m_type_unit_id = i.m_item_type_unit_id inner join tb_m_proyek p on p.m_proyek_id = i.m_proyek_id where s.t_spr_status = 'Post' and s.m_company_id = {m_company_id} and s.m_cust_id = {m_cust_id}";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataRef(string t_titipan_type_bayar, string t_titipan_type, int m_item_id, int t_titipan_id)
        {
            JsonResult js = null;
            try
            {
                var t_titipan_os_amt = "Isnull(r_piutang_sisa_amt, 0.0)"; var t_titipan_pay_amt = " Isnull(r_piutang_pay_amt, 0.0) ";
                if (t_titipan_type_bayar == "REALISASI")
                {
                    t_titipan_os_amt = $" t_titipan_amt-Isnull((select SUM(t_titipan_amt) from tb_t_titipan r where r.t_titipan_type_min=-1 AND r.t_titipan_type_bayar='{t_titipan_type_bayar}' AND r.m_item_id=s.m_item_id AND s.m_cust_id=r.m_cust_id AND r.t_titipan_ref_id=t.t_titipan_id AND r.t_titipan_id!={t_titipan_id}), 0.0) ";
                    t_titipan_pay_amt = $" Isnull((select SUM(t_titipan_amt) from tb_t_titipan r where r.t_titipan_type_min=-1 AND r.t_titipan_type_bayar='{t_titipan_type_bayar}' AND r.m_item_id=s.m_item_id AND s.m_cust_id=r.m_cust_id AND r.t_titipan_ref_id=t.t_titipan_id AND r.t_titipan_id!={t_titipan_id}), 0.0)";
                }

                sSql = $"Select t.t_titipan_id, t.t_titipan_no, format(t.t_titipan_date, 'dd/MM/yyyy') t_titipan_date, t.t_titipan_type_bayar, t.t_titipan_type, t.t_titipan_amt t_titipan_masuk_amt, r_piutang_amt t_titipan_amt, {t_titipan_pay_amt} t_titipan_pay_amt, {t_titipan_os_amt} t_titipan_os_amt, t.t_titipan_note, s.t_spr_no, s.t_spr_id, s.t_spr_awal_id from tb_t_titipan t Inner Join tb_t_spr s ON s.m_item_id = t.m_item_id AND s.m_cust_id = t.m_cust_id AND s.t_spr_id = t.t_spr_id Inner Join ( Select r.m_cust_id, r.m_item_id, r.t_spr_id, r.r_piutang_amt, Isnull(SUM(b.r_piutang_pay_amt), 0.0) r_piutang_pay_amt, r.r_piutang_amt - Isnull(SUM(b.r_piutang_pay_amt), 0.0) r_piutang_sisa_amt From tb_r_piutang r Left Join tb_r_piutang b ON b.r_piutang_ref_id=r.r_piutang_ref_id AND r.r_piutang_ref_table=b.r_piutang_ref_table AND r.r_piutang_trans_type=b.r_piutang_trans_type AND b.r_piutang_type='PENERIMAAN' Where r.r_piutang_ref_table='tb_t_titipan' AND r.r_piutang_type='SALDO' AND r.r_piutang_trans_type='PENINGKATAN' AND r.r_piutang_status='Post' Group By r.m_cust_id, r.m_item_id, r.t_spr_id, r.r_piutang_amt ) r ON r.m_cust_id=t.m_cust_id AND r.m_item_id=t.m_item_id AND r.t_spr_id=s.t_spr_id ";
                if (t_titipan_type_bayar != "SALDO")
                    sWhere += (t_titipan_type_bayar == "PENERIMAAN" ? $" AND r_piutang_sisa_amt > 0 " : $" AND r_piutang_sisa_amt <= 0 ");

                sSql += $"Where t.t_titipan_status = 'Post' {sWhere} And t.m_item_id = {m_item_id} AND t.t_titipan_type='{t_titipan_type}' AND t.t_titipan_type_bayar='SALDO' AND t.t_titipan_trans_type='PENINGKATAN'";
                var tbl = toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetAccount(string t_titipan_type_bayar, string t_titipan_type)
        {
            JsonResult js = null;
            try
            {
                var sList = new List<string>() { "PENINGKATAN" };
                var var_db = new List<string>(); var var_cr = new List<string>();
                var var_profit = new List<string>() { "PENDAPATAN" };
                if (t_titipan_type_bayar == "REALISASI")
                {
                    var_db.Add("UM TITIPAN");
                    var_cr.Add("BIAYA TITIPAN");
                }
                else if (t_titipan_type_bayar == "PENGEMBALIAN")
                {
                    var_db.Add("UM TITIPAN");
                    var_cr.Add("BANK_SISTEM");
                    var_cr.Add("KAS_SISTEM");
                }
                else if (t_titipan_type_bayar == "PENERIMAAN")
                {
                    if (sList.Contains(t_titipan_type))
                        var_cr.Add("PIUTANG_PENINGKATAN_BANGUNAN");
                    else
                        var_cr.Add("PIUTANG_PENINGKATAN_BANGUNAN");
                    var_db.Add("BANK_SISTEM");
                    var_db.Add("KAS_SISTEM");
                }
                else if (t_titipan_type_bayar == "SALDO")
                {
                    var_cr.Add("HUTANG_PENINGKATAN_BANGUNAN");
                    var_db.Add("PIUTANG_PENINGKATAN_BANGUNAN");
                }
                var db = GetCOAInterface(var_db);
                var cr = GetCOAInterface(var_cr);
                var crp = GetCOAInterface(var_profit);

                js = Json(new { result = "", db, cr, crp }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;

                //if ((db == null || db.Count <= 0) && (cr == null || cr.Count <= 0))
                //    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                //else
                //{
                //    js = Json(new { result = "", db, cr }, JsonRequestBehavior.AllowGet);
                //    js.MaxJsonLength = Int32.MaxValue;
                //}
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        private void InitDDL(tb_t_titipan tbl, string action)
        {
            var sList = new List<string> { "PENINGKATAN", "JASA BANGUN" };
            //ViewBag.t_titipan_type = new SelectList(GetTypeTitipan().Where(x => sList.Contains(x.Value)).ToList(), "Value", "Text", tbl.t_titipan_type);
            var var_db = new List<string>();
            var var_cr = new List<string>();
            var var_profit = new List<string>() { "PENDAPATAN" };
            if (tbl.t_titipan_type_bayar == "REALISASI")
            {
                var_db.Add("UM TITIPAN");
                var_cr.Add("BIAYA TITIPAN");
            }
            else if (tbl.t_titipan_type_bayar == "PENGEMBALIAN")
            {
                var_db.Add("UM TITIPAN");
                var_cr.Add("BANK_SISTEM");
                var_cr.Add("KAS_SISTEM");
            }
            else if (tbl.t_titipan_type_bayar == "PENERIMAAN")
            {
                if (sList.Contains(tbl.t_titipan_type))
                    var_cr.Add("PIUTANG_PENINGKATAN_BANGUNAN");
                else
                    var_cr.Add("PIUTANG_PENINGKATAN_BANGUNAN");
                var_db.Add("BANK_SISTEM");
                var_db.Add("KAS_SISTEM");
            }
            else if (tbl.t_titipan_type_bayar == "SALDO")
            {
                var_cr.Add("HUTANG_PENINGKATAN_BANGUNAN");
                var_db.Add("PIUTANG_PENINGKATAN_BANGUNAN");
            }

            ViewBag.t_titipan_debet_account_id = new SelectList(GetCOAInterface(var_db), "m_account_id", "m_account_name", tbl.t_titipan_debet_account_id);
            ViewBag.t_titipan_credit_account_id = new SelectList(GetCOAInterface(var_cr), "m_account_id", "m_account_name", tbl.t_titipan_credit_account_id);
            ViewBag.t_titipan_profit_account_id = new SelectList(GetCOAInterface(var_profit), "m_account_id", "m_account_name", tbl.t_titipan_profit_account_id);
        }

        private static string GenTransNo(string prefix, string field_no, string tablename, int m_company_id, string transdate = "", QL_MIS_MAGNAEntities db = null)
        {
            var sNo = $"{prefix}-{(string.IsNullOrEmpty(transdate) ? GetServerTime().ToString("yyyy.MM") : DateTime.Parse(transdate).ToString("yyyy.MM"))}-";
            var sSql = $"SELECT ISNULL(MAX(CAST(RIGHT({field_no}, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM {tablename} WHERE {field_no} LIKE '{sNo}%' ";
            if (db == null) db = new QL_MIS_MAGNAEntities();
            var result = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (result == 0) result = 1;
            return sNo + GenNumberString(result, 4);
        }

        private void FillAdditionalField(tb_t_titipan tbl, string action)
        {
            var sList = new List<string> { "PENINGKATAN", "JASA BANGUN" };
            var t_titipan_debet_amt = GetDataTitipan(tbl, 1).FirstOrDefault()?.t_titipan_debet_amt ?? 0m;
            var t_titipan_credit_amt = GetDataTitipan(tbl, 1).FirstOrDefault()?.t_titipan_credit_amt ?? 0m;
            ViewBag.m_cust_name = (tbl.m_cust_id != 0 ? db.tb_m_cust.FirstOrDefault(x => x.m_cust_id == tbl.m_cust_id).m_cust_name : "");
            ViewBag.m_item_name = (tbl.m_item_id != 0 ? db.tb_m_item.FirstOrDefault(x => x.m_item_id == tbl.m_item_id).m_item_name : "");
            ViewBag.m_proyek_name = (tbl.m_proyek_id != 0 ? db.tb_m_proyek.FirstOrDefault(x => x.m_proyek_id == tbl.m_proyek_id).m_proyek_name : "");
            ViewBag.t_spr_no = (tbl.t_spr_id != 0 ? db.tb_t_spr.FirstOrDefault(x => x.t_spr_id == tbl.t_spr_id).t_spr_no : "");
            ViewBag.t_titipan_ref_no = GetDataTitipan(tbl, 1).FirstOrDefault()?.t_titipan_no ?? "";
            ViewBag.t_kbm_h_id = db.tb_t_kbm_h.FirstOrDefault(a=> a.t_kbm_ref_id==tbl.t_titipan_id && a.t_kbm_no==tbl.t_kb_no)?.t_kbm_h_id ?? 0;
            if (action != "New Data" && tbl.t_titipan_type_bayar == "PENERIMAAN")
                ViewBag.tableoid = db.tb_r_kb.FirstOrDefault(r => r.r_kb_no == tbl.t_kb_no)?.r_kb_h_ref_id ?? 0;

            if (tbl.t_titipan_type_bayar == "PENERIMAAN" && sList.Contains(tbl.t_titipan_type))
            {
                t_titipan_debet_amt = GetDataTitipan(tbl, 1).FirstOrDefault()?.t_titipan_debet_amt ?? 0m;
                t_titipan_credit_amt = GetDataTitipan(tbl, -1).FirstOrDefault()?.t_titipan_credit_amt ?? 0m;
            }
            ViewBag.t_titipan_masuk_amt = t_titipan_debet_amt;
            ViewBag.t_titipan_pay_amt = t_titipan_credit_amt;
            ViewBag.t_titipan_os_amt = t_titipan_debet_amt - t_titipan_credit_amt;
        }

        [HttpPost]
        public ActionResult GetType(string t_titipan_type_bayar)
        {
            JsonResult js = null;
            try
            {
                var sList = new List<string> { "PENINGKATAN", "JASA BANGUN" };
                var tbl = GetTypeTitipan().Where(x => sList.Contains(x.Value)).ToList();
                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            tb_t_titipan tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new tb_t_titipan();
                tbl.t_titipan_uid = Guid.NewGuid();
                tbl.t_titipan_date = GetServerTime();
                tbl.created_by = Session["UserID"].ToString();
                tbl.created_at = GetServerTime();
                tbl.t_titipan_status = "In Process";
                tbl.m_company_id = db.tb_m_company.FirstOrDefault()?.m_company_id ?? 0;
                tbl.t_titipan_trans_type = "PENINGKATAN";
            }
            else
            {
                action = "Update Data";
                tbl = db.tb_t_titipan.Find(id);
            }

            ViewBag.action = action;
            InitDDL(tbl, action);
            FillAdditionalField(tbl, action);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_t_titipan tbl, string action)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile"); 
            var msg = ""; var result = "failed"; var type_bayar = "";
            var hdrid = ""; var servertime = GetServerTime();
            var m_account_id = tbl.t_titipan_debet_account_id;
            if (tbl.t_titipan_date < GetCutOffdate())
                msg += $"Silahkan pilih Tgl. Trnsfer {tbl.t_titipan_date.ToString("dd/MM/yyy")} diatas Tgl. Cut Off {GetCutOffdate().ToString("dd/MM/yyyy")}..!<br />";
            if (tbl.m_cust_id == 0) msg += "Silahkan pilih customer !<br/>";
            if (tbl.m_item_id == 0) msg += "Silahkan pilih unit !<br/>";
            if (string.IsNullOrEmpty(tbl.t_titipan_type)) msg += "Silahkan pilih tipe !<br/>";
            if (string.IsNullOrEmpty(tbl.t_titipan_type_bayar)) msg += "Silahkan pilih tipe bayar !<br/>";
            else if (tbl.t_titipan_ref_id == 0)
            {
                if (tbl.t_titipan_type_bayar == "PENERIMAAN")
                    msg += "Silahkan pilih no. referensi !<br/>";
                if (tbl.t_titipan_type_bayar == "REALISASI")
                    msg += "Silahkan pilih no. referensi !<br/>";
            }

            if (tbl.t_titipan_amt <= 0) msg += "Nominal harus lebih dari 0 !<br/>";
            if (tbl.t_titipan_debet_account_id == 0) msg += "Silahkan pilih account debet !<br/>";
            if (tbl.t_titipan_credit_account_id == 0) msg += "Silahkan pilih account credit !<br/>";
            if (tbl.t_titipan_type_bayar == "PENERIMAAN") m_account_id = tbl.t_titipan_debet_account_id;
            if (tbl.t_titipan_type_bayar == "PENGEMBALIAN") m_account_id = tbl.t_titipan_credit_account_id;

            var dt_t = db.Database.SqlQuery<t_titipan>($"Select *, t_titipan_debet_amt-(t_titipan_credit_amt + {tbl.t_titipan_amt}) t_titipan_os_amt from ( select s.t_titipan_id, t_titipan_amt t_titipan_debet_amt, Isnull(( select SUM(t_titipan_amt) from tb_t_titipan r where r.t_titipan_type_min=-1 AND r.t_titipan_type_bayar='{tbl.t_titipan_type_bayar}' AND r.m_item_id=s.m_item_id AND s.m_cust_id=r.m_cust_id AND r.t_titipan_ref_id=s.t_titipan_id AND r.t_titipan_id!={tbl.t_titipan_id} ), 0.0) t_titipan_credit_amt, s.t_titipan_status from tb_t_titipan s where t_titipan_type_bayar='SALDO' AND t_titipan_type_min=1 AND s.t_titipan_id=0{tbl.t_titipan_ref_id} AND s.m_item_id={tbl.m_item_id} AND s.m_cust_id={tbl.m_cust_id}) t").ToList();

            var kb_no = db.Database.SqlQuery<listcoa>($"select h.m_interface_name m_account_var, d.m_account_id from tb_m_interface_h h Inner Join tb_m_interface_d d ON h.m_interface_h_id=d.m_interface_h_id WHere h.m_interface_name <> 'KAS_BANK_TITIPAN'").ToList();

            if (tbl.t_titipan_type_bayar == "PENERIMAAN")
                type_bayar = kb_no.Where(d => d.m_account_id == tbl.t_titipan_debet_account_id).FirstOrDefault().m_account_var == "BANK_SISTEM" ? "BM" : "KM";
            else if (tbl.t_titipan_type_bayar == "PENGEMBALIAN")
                type_bayar = kb_no.Where(d => d.m_account_id == tbl.t_titipan_credit_account_id).FirstOrDefault().m_account_var == "BANK_SISTEM" ? "BK" : "KK";
            else if (tbl.t_titipan_type_bayar == "SALDO") tbl.t_kb_no = "";

            if (tbl.t_titipan_type_bayar == "PENGEMBALIAN")
            {
                var t_titipan_pay_amt = GetDataTitipan(tbl, -1).FirstOrDefault()?.t_titipan_credit_amt ?? 0m;
                var t_titipan_masuk_amt = GetDataTitipan(tbl, 1).FirstOrDefault()?.t_titipan_debet_amt ?? 0m;
                var sCek = t_titipan_masuk_amt - (t_titipan_pay_amt + tbl.t_titipan_amt);
                if (sCek <= 0) msg += $"Nominal sisa penerimaan {GetDataTitipan(tbl, 1).FirstOrDefault()?.t_titipan_no.ToString() ?? ""} sudah habis {toMoney(sCek)} atau melebihi penerimaan {toMoney(t_titipan_masuk_amt)}!<br/>";
            }

            if (string.IsNullOrEmpty(msg))
            {
                tbl.t_titipan_note = tbl.t_titipan_note ?? "";
                if (tbl.t_titipan_type_bayar == "SALDO") tbl.t_titipan_type_min = 1;
                else if (tbl.t_titipan_type_bayar == "PENERIMAAN") tbl.t_titipan_type_min = 1;
                else if (tbl.t_titipan_type_bayar == "REALISASI") tbl.t_titipan_type_min = -1;

                if (tbl.t_titipan_status == "Revised") tbl.t_titipan_status = "In Process";
                if (tbl.t_titipan_status == "Post")
                {
                    tbl.t_titipan_no = GenTransNo($"TC", "t_titipan_no", "tb_t_titipan", tbl.m_company_id, tbl.t_titipan_date.ToString("MM/dd/yyyy"));
                    tbl.posted_by = Session["UserID"].ToString();
                    tbl.posted_at = servertime;
                    if (tbl.t_titipan_type_bayar == "PENERIMAAN")
                        tbl.t_kb_no = GenerateTransNo($"{type_bayar}", "r_kb_no", "tb_r_kb", tbl.m_company_id, tbl.t_titipan_date.ToString("MM/dd/yyyy"));
                }
                else
                {
                    tbl.t_titipan_no = "";
                    tbl.t_kb_no = "";
                }

                tbl.t_titipan_profit_account_id = (tbl.t_titipan_type_bayar != "REALISASI" ? 0 : tbl.t_titipan_profit_account_id);
                if (tbl.t_titipan_type_bayar == "REALISASI" && dt_t.Count() > 0)
                {
                    var dt_ref = dt_t.FirstOrDefault();
                    tbl.t_titipan_profit_amt = dt_ref.t_titipan_os_amt < 0 ? dt_ref.t_titipan_os_amt * -1 : 0m;
                    tbl.t_titipan_profit_account_id = dt_ref.t_titipan_os_amt < 0 ? tbl.t_titipan_profit_account_id : 0;
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            tbl.updated_at = tbl.created_at;
                            tbl.updated_by = tbl.created_by;
                            tbl.r_hutang_ref_id = 0;
                            tbl.t_titipan_flag = "";
                            db.tb_t_titipan.Add(tbl);
                        }
                        else
                        {
                            tbl.updated_at = GetServerTime();
                            tbl.updated_by = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                        }
                        db.SaveChanges();

                        if (tbl.t_titipan_status == "Post")
                        {    
                            //Block Post Transaction
                            db.tb_post_trans.Add(InsertPostTrans(tbl.m_company_id, tbl.t_titipan_uid, "tb_t_titipan", tbl.t_titipan_id, tbl.updated_by, tbl.updated_at));
                            var kbm = new tb_t_kbm_h(); var kbmd = new tb_t_kbm_d();
                            if (tbl.t_titipan_type_bayar == "PENERIMAAN")
                            {  
                                kbm.t_kbm_uid = tbl.t_titipan_uid;
                                kbm.t_kbm_no = tbl.t_kb_no;
                                kbm.m_company_id = tbl.m_company_id;
                                kbm.t_kbm_ref_id = tbl.t_titipan_id;
                                kbm.t_kbm_ref_type = "TITIPAN";
                                kbm.t_kbm_ref_no = tbl.t_titipan_no;
                                kbm.t_kbm_type_bayar = type_bayar;
                                kbm.t_kbm_date = tbl.t_titipan_date;
                                kbm.t_spr_id = tbl.t_spr_id;
                                kbm.t_spr_awal_id = tbl.t_spr_awal_id;
                                kbm.m_cust_id = tbl.m_cust_id;
                                kbm.m_item_id = tbl.m_item_id;
                                kbm.m_proyek_id = tbl.m_proyek_id;
                                kbm.t_kbm_account_id = tbl.t_titipan_debet_account_id;
                                kbm.t_kbm_h_amt = tbl.t_titipan_amt;
                                kbm.t_kbm_h_status = tbl.t_titipan_status;
                                kbm.t_kbm_h_note = tbl.t_titipan_note;
                                kbm.t_kbm_type = "Proyek";                               
                                kbm.posted_at = GetServerTime();
                                kbm.posted_by = tbl.posted_by;
                                kbm.created_at = tbl.t_titipan_date;
                                kbm.created_by = tbl.created_by;
                                kbm.updated_at = tbl.updated_at;
                                kbm.updated_by = tbl.updated_by;
                                kbm.m_promo_id = 0;
                                db.tb_t_kbm_h.Add(kbm);
                                db.SaveChanges(); 
                              
                                kbmd.t_kbm_d_account_id = tbl.t_titipan_credit_account_id;
                                kbmd.t_kbm_ref_id = tbl.t_titipan_id;
                                kbmd.t_kbm_h_id = kbm.t_kbm_h_id;
                                kbmd.t_kbm_ref_table = "tb_t_titipan";
                                kbmd.t_kbm_d_seq = 1;
                                kbmd.t_kbm_d_amt = tbl.t_titipan_amt;
                                kbmd.t_kbm_disc_amt = 0m;
                                kbmd.t_kbm_d_netto = tbl.t_titipan_amt;
                                kbmd.t_kbm_d_note = tbl.t_titipan_note;
                                kbmd.created_at = tbl.created_at;
                                kbmd.created_by = tbl.created_by;
                                db.tb_t_kbm_d.Add(kbmd);
                                db.SaveChanges();

                                var kb_account_id = (tbl.t_titipan_type_bayar == "PENERIMAAN" ? tbl.t_titipan_debet_account_id : tbl.t_titipan_credit_account_id);
                                var kb_d_account_id = (tbl.t_titipan_type_bayar == "PENERIMAAN" ? tbl.t_titipan_credit_account_id : tbl.t_titipan_debet_account_id);
                                var ref_table = (tbl.t_titipan_type_bayar == "PENERIMAAN" ? "tb_t_kbm_d" : "tb_t_titipan");

                                db.tb_r_kb.Add(Insert_R_KB(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, ref_table, kbmd.t_kbm_d_id, type_bayar, tbl.t_kb_no, tbl.t_titipan_date, kb_account_id, kb_d_account_id, (tbl.t_titipan_type_bayar == "PENERIMAAN" ? tbl.t_titipan_amt : 0m), (tbl.t_titipan_type_bayar == "PENGEMBALIAN" ? tbl.t_titipan_amt : 0m), tbl.t_titipan_note ?? "", tbl.created_by, tbl.t_titipan_date, tbl.t_spr_id, tbl.t_spr_awal_id, kbm.t_kbm_h_id, kbm.t_kbm_type));
                            }
                           
                            if (tbl.t_titipan_type_bayar != "REALISASI")
                            {
                                //var dt_ref = dt_t.FirstOrDefault();                                
                                if (dt_t.Count() > 0)
                                {
                                    var dt = db.tb_t_titipan.FirstOrDefault(x => x.t_titipan_type_bayar == "Saldo" && x.t_titipan_id == tbl.t_titipan_ref_id);
                                    dt.t_titipan_status = dt_t.FirstOrDefault().t_titipan_os_amt <= 0 ? "Complete" : dt.t_titipan_status;
                                    db.Entry(dt).State = EntityState.Modified;
                                }
                            }
                            db.SaveChanges();
                            // --> Insert Jurnal
                            msg = PostJurnal(ref db, tbl, kbm, kbmd);
                            if (!string.IsNullOrEmpty(msg))
                            {
                                objTrans.Rollback(); result = "failed";
                                return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        db.SaveChanges();
                        objTrans.Commit();

                        hdrid = tbl.t_titipan_id.ToString(); result = "success";
                        if (tbl.t_titipan_status == "Post") msg = $"data telah diposting dengan nomor transaksi {tbl.t_titipan_no}";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            msg += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                                msg += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                        }
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        private string PostJurnal(ref QL_MIS_MAGNAEntities db, tb_t_titipan tbl, tb_t_kbm_h kbm, tb_t_kbm_d kbmd)
        {
            var msg = ""; var seq = 1; var gl_note = "";
            decimal t_titipan_amt = tbl.t_titipan_amt;
            decimal t_titipan_profit_amt = tbl.t_titipan_profit_amt;
            var ref_table = "tb_t_titipan";
            var ref_no = tbl.t_titipan_no;
            int ref_id = tbl.t_titipan_id;
            try
            {
                //inser r_piutang
                if (tbl.t_titipan_type_bayar == "SALDO")
                    db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, tbl.t_titipan_type_bayar, "tb_t_titipan", tbl.t_titipan_id, tbl.t_titipan_no, tbl.t_titipan_date, tbl.t_titipan_date, Convert.ToDateTime("1/1/1900".ToString()), tbl.t_titipan_debet_account_id, tbl.t_titipan_amt, 0m, tbl.t_titipan_note, tbl.posted_by, (DateTime)tbl.posted_at, "", 0, "", 0, 0, tbl.t_spr_id, tbl.t_spr_awal_id, tbl.t_titipan_type, "PENINGKATAN"));
                else if (tbl.t_titipan_type_bayar == "PENERIMAAN")
                {
                    ref_table = "tb_t_kbm_h"; ref_no = tbl.t_kb_no; ref_id = kbm.t_kbm_h_id;
                    var dt_ref = db.tb_t_titipan.FirstOrDefault(t => t.t_titipan_id == tbl.t_titipan_ref_id && t.t_titipan_type_bayar == "SALDO");
                    var sCount = db.tb_r_piutang.Where(r => r.r_piutang_ref_id == tbl.t_titipan_ref_id && r.r_piutang_type == tbl.t_titipan_type_bayar);

                    db.tb_r_piutang.Add(Insert_R_Piutang(tbl.m_company_id, tbl.m_cust_id, tbl.m_proyek_id, tbl.m_item_id, tbl.t_titipan_type_bayar, "tb_t_titipan", tbl.t_titipan_ref_id, dt_ref.t_titipan_no, dt_ref.t_titipan_date, dt_ref.t_titipan_date, tbl.t_titipan_date, tbl.t_titipan_credit_account_id, 0m, tbl.t_titipan_amt, tbl.t_titipan_note == "" ? $"Penerimaan {sCount.Count() + 1}" : tbl.t_titipan_note, tbl.created_by, tbl.created_at, "tb_t_kbm_d", kbmd.t_kbm_d_id, tbl.t_kb_no, tbl.t_titipan_debet_account_id, 0m, tbl.t_spr_id, tbl.t_spr_awal_id, tbl.t_titipan_type, "PENINGKATAN"));
                }

                var new_dt_gl = new List<dt_gl>();
                new_dt_gl.Add(new dt_gl { r_gl_amt = t_titipan_amt, m_account_id = tbl.t_titipan_debet_account_id, r_gl_db_cr = "D" });
                new_dt_gl.Add(new dt_gl { r_gl_amt = t_titipan_amt, m_account_id = tbl.t_titipan_credit_account_id, r_gl_db_cr = "C" });

                if (tbl.t_titipan_type_bayar == "REALISASI" && tbl.t_titipan_profit_amt > 0m)
                {
                    new_dt_gl.Add(new dt_gl { r_gl_amt = tbl.t_titipan_amt - tbl.t_titipan_profit_amt, m_account_id = tbl.t_titipan_credit_account_id, r_gl_db_cr = "C" });
                    new_dt_gl.Add(new dt_gl { r_gl_amt = t_titipan_profit_amt, m_account_id = tbl.t_titipan_profit_account_id, r_gl_db_cr = "C" });
                }

                gl_note = tbl.t_titipan_note == "" ? $"PENINGKATAN {tbl.t_titipan_type_bayar}" : tbl.t_titipan_note; 
                if (new_dt_gl != null && new_dt_gl.Count() > 0)
                {
                    foreach (var item in new_dt_gl)
                    {
                        db.tb_r_gl.Add(Insert_R_GL(tbl.m_company_id, tbl.m_proyek_id, tbl.m_item_id, seq++, ref_table, ref_id, ref_no, tbl.t_titipan_date, item.m_account_id, item.r_gl_db_cr, item.r_gl_amt, item.r_gl_db_cr == "D" ? tbl.t_kb_no : gl_note, tbl.created_by, tbl.created_at, tbl.m_cust_id));
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.ToString();
            }
            return msg;
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string revisi)
        {

            tb_t_titipan tbl = db.tb_t_titipan.Find(id);
            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    { 
                        if(tbl.t_titipan_type_bayar != "SALDO")
                        {
                            var dt = db.tb_t_titipan.FirstOrDefault(x => x.t_titipan_type_bayar == "Saldo" && x.t_titipan_id == tbl.t_titipan_ref_id);
                            dt.t_titipan_status = "Post";
                            db.Entry(dt).State = EntityState.Modified;
                        }
                        db.tb_t_titipan.Remove(tbl);
                        db.SaveChanges(); objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Print Out
        public ActionResult PrintReport(int ids)
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");

            var sCek = db.tb_t_titipan.Where(x => x.t_titipan_id == ids);
            if (sCek.FirstOrDefault().t_titipan_type_bayar != "PENERIMAAN")
                ids = sCek.FirstOrDefault().t_titipan_ref_id;

            string rptname = $"PrintKBM";
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("userID", Session["UserID"].ToString());

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), $"rpt{rptname}.rpt"));
            sSql = $"SELECT * FROM tb_v_print_peningkatan WHERE t_kbm_h_id IN ({ids}) ORDER BY t_kbm_h_id";
            var dt = new ClassConnection().GetDataTable(sSql, "datatable");
            report.SetDataSource(dt);
            if (rptparam.Count > 0) foreach (var item in rptparam) report.SetParameterValue(item.Key, item.Value);

            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", $"inline; filename={rptname}.pdf");
            return new FileStreamResult(stream, "application/pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
        #endregion

        public class sFilter
        {
            public string ddltype { get; set; }
            public int ddlproyek { get; set; }
            public string periodstart { get; set; }
            public string periodend { get; set; }
            public string filterspr { get; set; }
            public string ddlfilter { get; set; }
        }

        #region Kartu Peningkatan

        public ActionResult KartuPeningkatan()
        {
            if (Session["UserID"] == null) return RedirectToAction("Login", "Profile");
            if (checkPagePermissionForReport(this.ControllerContext.RouteData.Values["controller"].ToString() + "/KartuPeningkatan", (List<RoleDetail>)Session["Role"])) return RedirectToAction("NotAuthorize", "Profile");
            var proyek = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "ALL" } }.Union(db.tb_m_proyek.Where(x => x.m_proyek_flag == "OPEN").Select(x => new SelectListItem() { Value = x.m_proyek_id.ToString(), Text = x.m_proyek_name })).ToList();
            ViewBag.ddlproyek = new SelectList(proyek, "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult GetModalData(sFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                if (modaltype.ToUpper() == "SPR" && param.ddltype == "TITIPAN")
                    sSql = $"select 0 seq, i.m_item_name [Unit], t.t_titipan_no [No. Transaksi], FORMAT(t.t_titipan_date, 'MM/dd/yyyy') [Tanggal], m_cust_code [Kode], m_cust_name [Customer] from tb_t_titipan t Inner Join tb_m_cust c ON c.m_cust_id=t.m_cust_id Inner Join tb_m_item i ON i.m_item_id=t.m_item_id Inner Join tb_m_proyek p ON p.m_proyek_id=t.m_proyek_id where t_titipan_type_bayar='SALDO'";
                if (modaltype.ToUpper() == "SPR" && param.ddltype == "PENINGKATAN")
                    sSql = $"select 0 seq, i.m_item_name [Unit], t.t_peningkatan_no [No. Transaksi], FORMAT(t.t_peningkatan_date, 'MM/dd/yyyy') [Tanggal], m_cust_code [Kode], m_cust_name [Customer] from tb_t_peningkatan_h t Inner Join tb_m_cust c ON c.m_cust_id=t.m_cust_id Inner Join tb_m_item i ON i.m_item_id=t.m_item_id Inner Join tb_m_proyek p ON p.m_proyek_id=t.m_proyek_id where t_peningkatan_trans_type='SALDO'";
                else if (modaltype.ToUpper() == "SPR" && param.ddltype == "LEBIH_BAYAR")
                    sSql = $"SELECT 0seq, m_item_name [Unit], t_spr_no [No. Transaksi], FORMAT(t_spr_date, 'dd/MM/yyyy') [Tanggal], m_cust_code [Kode], m_cust_name [Customer] FROM tb_v_Kartu_lebih_bayar r Where 1=1 ";
                if (param.ddlproyek != 0) { sSql += $" AND m_proyek_id = {param.ddlproyek}"; }
                if (param.ddltype == "LEBIH_BAYAR") { sSql += "Group BY t_spr_no, m_item_name, FORMAT(t_spr_date, 'dd/MM/yyyy'), m_cust_code, m_cust_name Order By t_spr_no"; }
                    
                DataTable tbl = new ClassConnection().GetDataTable(sSql, $"tblModal{modaltype}");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString() + sSql;
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ViewReport(sFilter param, string reporttype)
        {    
            var Periode = $"{param.periodstart.ToDateTime().ToString("dd/MM/yyyy")}-{param.periodend.ToDateTime().ToString("dd/MM/yyyy")}";
            var rptfile = $"RptKartuTitipan";
            var rptname = $"KARTU_{(param.ddltype.ToUpper()=="TITIPAN" ? "DANA" : "")}_{param.ddltype.ToUpper()}_{(param.ddltype.ToUpper() == "TITIPAN" ? "" : "MUTU")}_{Periode}";
            rptfile = rptfile.Replace("PDF", "");
            if (param.ddltype == "PENINGKATAN")
            {
                rptfile = $"RptKartuPeningkatan";
                sSql = $"select *, Case When r_piutang_termin <= 0 AND amt_sisa > 0 then amt_sisa Else 0 End amt_sisa_piutang from ( Select *, Cast(format(r.r_piutang_amt - Isnull((Select sum((r_piutang_pay_amt)) from tb_r_piutang x where r.r_piutang_ref_id = x.r_piutang_ref_id AND x.r_piutang_status = r.r_piutang_status AND r.r_piutang_ref_table = x.r_piutang_ref_table AND r.t_spr_id = x.t_spr_id AND r.m_item_id = x.m_item_id), 0.0), '0') as decimal) amt_sisa FROM tb_v_Kartu_Peningkatan r ) r Where 1 = 1";
            }
            else if (param.ddltype == "TITIPAN")
            {
                rptfile = $"RptKartuTitipan";
                sSql = $"select *, Case When r_piutang_termin <= 0 AND amt_sisa > 0 then amt_sisa Else 0 End amt_sisa_piutang from ( Select *, Cast(format(r.r_piutang_amt - Isnull((Select sum((r_piutang_pay_amt)) from tb_r_piutang x where r.r_piutang_ref_id = x.r_piutang_ref_id AND x.r_piutang_status = r.r_piutang_status AND r.r_piutang_ref_table = x.r_piutang_ref_table AND r.t_spr_id = x.t_spr_id AND r.m_item_id = x.m_item_id), 0.0), '0') as decimal) amt_sisa FROM tb_v_Kartu_Titipan r ) r Where 1 = 1";
            }
            else if (param.ddltype == "LEBIH_BAYAR")
            {
                rptfile = $"RptKartuLebihBayar"; 
                rptname = $"KARTU_KELEBIHAN_BAYAR_{Periode}";
                sSql = $"select * From tb_v_Kartu_lebih_bayar r where 1=1 ";
            }

            if (param.ddlproyek != 0) sSql += $" AND r.m_proyek_id = {param.ddlproyek}";
            if (!string.IsNullOrEmpty(param.filterspr)) sSql += $" AND r.{param.ddlfilter} IN ('{param.filterspr.Replace(";", "', '")}')";
            sSql += $" AND t_spr_date >= Cast('{param.periodstart} 0:0:0' as Datetime) AND t_spr_date <= Cast('{param.periodend} 23:59:59' as datetime)";

            sSql += (param.ddltype == "LEBIH_BAYAR" ? "Order By r.r_hutang_ref_date, r.m_proyek_name, r.r_hutang_pay_ref_date" : " Order By r.m_cust_id, r.t_spr_id, r.m_item_id, seq, r_piutang_ref_id, r_piutang_ref_date, r.created_at");

            var PaperSize = 9;
            var irequest = new ReportRequest()
            {
                rptQuery = sSql,
                rptFile = rptfile,
                rptPaperSizeEnum = PaperSize,
                rptPaperOrientationEnum = 1,
                rptExportType = reporttype,
                rptParam = new Dictionary<string, string>()
            };

            irequest.rptParam.Add("Periode", Periode);
            irequest.rptParam.Add("UserID", Session["UserID"].ToString());
            irequest.rptParam.Add("sTitle", $"KARTU {(param.ddltype.ToUpper() == "TITIPAN" ? "DANA" : "")} {param.ddltype.ToUpper()} {(param.ddltype.ToUpper() == "TITIPAN" ? "" : "MUTU")}");
            var rpt_id = GenerateReport(irequest);
            return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}