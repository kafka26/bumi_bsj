﻿using System.Web;
using System.Web.Optimization;

namespace MIS_MAGNA_MVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                     "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/login/css").Include(
                    "~/login/vendor/bootstrap/css/bootstrap.min.css",
                    "~/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css",
                    "~/login/fonts/iconic/css/material-design-iconic-font.min.css",
                    "~/login/vendor/animate/animate.css",
                    "~/login/vendor/css-hamburgers/hamburgers.min.css",
                    "~/login/vendor/animsition/css/animsition.min.css",
                    "~/login/css/util.css",
                    "~/login/css/main.css"));

            bundles.Add(new ScriptBundle("~/login/js").Include(
                    "~/login/vendor/jquery/jquery-3.2.1.min.js",
                    "~/login/vendor/animsition/js/animsition.min.js",
                    "~/login/vendor/bootstrap/js/popper.js",
                    "~/login/vendor/bootstrap/js/bootstrap.min.js",
                    "~/login/js/main.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                    "~/Content/bootstrap/dist/css/bootstrap.min.css",
                    "~/Content/font-awesome/css/font-awesome.min.css",
                    "~/Content/ionicons.min.css",
                    "~/Content/dataTables.bootstrap.min.css",
                    "~/Content/dataTables.checkboxes.css",
                    "~/Content/dataTables.fixedHeader.bootstrap.min.css",
                    "~/Content/scroller.bootstrap.min.css",
                    "~/Content/scroller.dataTables.min.css",
                    "~/Content/rowReorder.dataTables.min.css",
                    "~/Content/dist/css/util.css",
                    "~/Content/dist/css/AdminLTE.min.css",
                    "~/Content/_all-skins.min.css",
                    "~/Content/daterangepicker.css",
                    "~/Content/bootstrap3-wysihtml5.min.css",
                    "~/Content/select2/dist/css/select2.min.css",
                    "~/Content/select2/dist/css/select2-bootstrap.min.css",
                    "~/Content/bootstrap-datepicker.min.css",
                    "~/Content/timepicker.less",
                    "~/Content/bootstrap-timepicker.min.css",
                    "~/Content/iCheck/icheck-all.css",
                    "~/Content/bootstrap-colorpicker.min.css",
                    "~/Content/dist/css/style.css",
                    "~/Content/sweetalert2.min.css"));

            bundles.Add(new ScriptBundle("~/Scripts/js").Include(
                    "~/Scripts/jquery.min.js",
                    "~/Scripts/bootstrap.min.js",
                    "~/Scripts/jquery.dataTables.min.js",
                    "~/Scripts/dataTables.bootstrap.min.js",
                    "~/Scripts/dataTables.checkboxes.min.js",
                    "~/Scripts/dataTables.scroller.min.js",
                    "~/Scripts/dataTables.fixedHeader.min.js",
                    "~/Scripts/dataTables.rowReorder.min.js",
                    "~/Scripts/jquery.inputmask.bundle.min.js",
                    "~/Scripts/jquery.sparkline.min.js",
                    "~/Scripts/jquery.knob.min.js",
                    "~/Scripts/moment.min.js",
                    "~/Scripts/daterangepicker.js",
                    "~/Scripts/bootstrap3-wysihtml5.all.min.js",
                    "~/Content/select2/dist/js/select2.full.min.js",
                    "~/Scripts/jquery.slimscroll.min.js",
                    "~/Scripts/icheck.min.js",
                    "~/Scripts/fastclick.js",
                    "~/Scripts/adminlte.min.js",
                    "~/Scripts/bootstrap-datepicker.min.js",
                    "~/Scripts/bootstrap-colorpicker.min.js",
                    "~/Scripts/bootstrap-timepicker.min.js",
                    "~/Scripts/sweetalert2.min.js",
                    "~/Scripts/jquery.signalR-2.2.2.min.js",
                    "~/Scripts/jquery.blockUI.js"));
        }
    }
}
