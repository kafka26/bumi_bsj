﻿using System;
using System.Collections.Generic;
using System.Web;
using CrystalDecisions.CrystalReports.Engine;

namespace MIS_MAGNA_MVC.Other
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        ReportDocument rd = new ReportDocument();
        string rpttype = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["UserID"] == null)
                Response.Redirect("~/Profile/Login");

            var rptSource = HttpContext.Current.Session["rptsource"];
            var rptPaper = HttpContext.Current.Session["rptpaper"];
            Dictionary<string, object> rptParam = (Dictionary<string, object>)HttpContext.Current.Session["rptparam"];
            var rptfile = Request["rptfile"].Trim();
            var rptLogon = HttpContext.Current.Session["rptlogon"];
            string[] rptexctblname = (string[])HttpContext.Current.Session["rptexctblname"];
            string rptname = Request["rptname"]?.Trim();
            rpttype = Request["rpttype"]?.Trim();

            string strRptPath = Server.MapPath("~/") + "Report//" + rptfile;
            //Loading Report
            rd.Load(strRptPath);
            if (rptPaper != null)
                rd.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)rptPaper;

            // Setting report data source
            if (rptSource != null && rptSource.GetType().ToString() != "System.String")
            {
                rd.SetDataSource(rptSource);
                if (rptLogon != null && !string.IsNullOrEmpty(rptLogon.ToString()))
                {
                    if (rptexctblname != null)
                        Controllers.ClassProcedure.SetDBLogonForReport(rd, rptexctblname);
                    else
                        Controllers.ClassProcedure.SetDBLogonForReport(rd);
                }
            }
            else
            {
                Controllers.ClassProcedure.SetDBLogonForReport(rd);
            }

            if (rptParam != null && rptParam.Count > 0)
                foreach (var item in rptParam)
                    rd.SetParameterValue(item.Key, item.Value);

            if (rpttype == "PDF")
            {
                Response.Buffer = false; Response.ClearContent(); Response.ClearHeaders();
                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, true, rptname + ".pdf");
                rd.Close(); rd.Dispose();
            }
            else if (rpttype == "XLS")
            {
                Response.Buffer = false; Response.ClearContent(); Response.ClearHeaders();
                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.Excel, Response, true, rptname + ".xls");
                rd.Close(); rd.Dispose();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            CRViewer.ReportSource = rd;
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            try
            {
                if (rd != null)
                {
                    if (rd.IsLoaded)
                    {
                        rd.Dispose();
                        rd.Close();
                    }
                }
            }
            catch
            {
                rd.Dispose();
                rd.Close();
            }
        }
    }
}