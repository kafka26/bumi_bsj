﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MIS_MAGNA_MVC
{
    public class MustBeAuthorizedFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string action = filterContext.ActionDescriptor.ActionName;
            string controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string userId = filterContext.HttpContext.Session["UserID"]?.ToString();
            if (string.IsNullOrEmpty(userId)) filterContext.HttpContext.Session["UserID"] = "admin";
        }
    }
}