﻿using System;
using System.Linq;
using System.Web.Hosting;
using System.IO;
using System.Data;
using System.Web.Mvc;
using System.Reflection;
using System.Reflection.Emit;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using static MIS_MAGNA_MVC.GlobalClass;
using static MIS_MAGNA_MVC.Controllers.ClassFunction;
using static MIS_MAGNA_MVC.GlobalExtension;
using System.Drawing;
using MIS_MAGNA_MVC.Models;
using MIS_MAGNA_MVC.Controllers;

namespace MIS_MAGNA_MVC
{
    public class GlobalFunctions
    {
        private static string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private static Dictionary<string, string> filterStatus = new Dictionary<string, string> { { "1", "'In Process', 'Revised', 'Active', 'Used'" }, { "2", "'In Approval', 'Inactive', 'Unused'" }, { "3", "'Approved', 'Post'" } };

        public static string GetIndexFilter(string cmpcode, string filter, ModelFilter modfil, string alias, string dateField, string statusField, bool specialAccess, string userId)
        {
            var result = " WHERE ";
            if (cmpcode != CompnyCode) result += alias + ".cmpcode IN ('" + cmpcode + "')";
            else result += alias + ".cmpcode LIKE '%'";
            if (filter == "POST")
            {
                if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                if (!string.IsNullOrEmpty(modfil.filterddl)) result += " AND " + modfil.filterddl + " LIKE '%" + Tchar(modfil.filtertext) + "%'";
                if (modfil.isperiodchecked && modfil.filterperiodfrom != null && modfil.filterperiodto != null) result += " AND " + dateField + ">=CAST('" + modfil.filterperiodfrom.ToString("MM/dd/yyyy") + " 00:00:00' AS DATETIME) AND " + dateField + "<=CAST('" + modfil.filterperiodto.ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME)";
                if (!string.IsNullOrEmpty(modfil.filterstatus) && modfil.filterstatus != "ALL") result += " AND " + statusField + "='" + modfil.filterstatus + "'";
            }
            else if ((new string[] { "1", "2", "3" }).Contains(filter)) result += " AND " + statusField + " IN (" + filterStatus[filter] + ")";
            if (!specialAccess) result += " AND " + alias + ".createuser='" + userId + "'";
            return result;
        }

        public static List<SelectListItem> GetTypeTitipan()
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem { Text = "-- Pilih Tipe --", Value = "0" },
                new SelectListItem { Text = "TITIPAN PAJAK PEMBELIAN", Value = "PPN" },
                //new SelectListItem { Text = "BPHTB", Value = "BPHTB" },
                //new SelectListItem { Text = "BALIK NAMA", Value = "BL" },
                new SelectListItem { Text = "PENINGKATAN BANGUNAN", Value = "PB" },
                new SelectListItem { Text = "JASA BANGUN", Value = "JB" }
            };
            return result;
        }

        public static string GetIndexFilterMaster(string cmpcode, string filter, ModelFilter modfil, string alias, string statusField, bool specialAccess, string userId)
        {
            var result = $" WHERE ";
            if (cmpcode != CompnyCode) result += alias + ".cmpcode IN ('" + cmpcode + "')";
            else result += alias + ".cmpcode LIKE '%'";
            if (filter == "POST")
            {
                if (string.IsNullOrEmpty(modfil.filtertext)) modfil.filtertext = "";
                if (!string.IsNullOrEmpty(modfil.filterddl)) result += $" AND {modfil.filterddl} LIKE '%{Tchar(modfil.filtertext)}%'";
                if (!string.IsNullOrEmpty(modfil.filterstatus) && modfil.filterstatus != "ALL") result += $" AND {alias}.{statusField}='{modfil.filterstatus}'";
            }
            else if ((new string[] { "1", "2", "3" }).Contains(filter)) result += $" AND {alias}.{statusField} IN ({filterStatus[filter]})";
            if (!specialAccess) result += $" AND {alias}.createuser='{userId}'";
            return result;
        }
        
        public static string GetErrorFromModelState(ModelStateDictionary ModelState)
        {
            var sError = "";
            foreach (var modelStateVal in ModelState.Values)
                foreach (var error in modelStateVal.Errors)
                    if (!error.ErrorMessage.ToLower().Contains("field is required"))
                        sError += error.ErrorMessage + (!string.IsNullOrEmpty(error.ErrorMessage) ? "<br />" : "");
            return sError;
        }

        public static string GetFilterQueryFromMultiFilterText(string filter_field, string filter_text, char separator = ';')
        {
            var result = "";
            var filter_like = ""; var filter_in = "";
            foreach (var item in filter_text.Split(separator))
                if (!string.IsNullOrEmpty(item))
                {
                    if (item.Contains("%"))
                        filter_like += $"{filter_field} LIKE '{item}' OR ";
                    else
                        filter_in += $"'{item}',";
                }
            if (!string.IsNullOrEmpty(filter_like))
                result += $"({Left(filter_like, filter_like.Length - 4)})";
            if (!string.IsNullOrEmpty(filter_in))
                result += $"{(!string.IsNullOrEmpty(result) ? " AND " : "")}({filter_field} IN ({Left(filter_in, filter_in.Length - 1)}))";
            return result;
        }

        public static bool IsNewDataExists(string tbl, string keyField, string keyValue, out string warningMessage, string addFilter = "")
        {
            QL_MIS_MAGNAEntities db = new QL_MIS_MAGNAEntities();
            db.Database.CommandTimeout = 0;
            warningMessage = "";
            if (db.Database.SqlQuery<int>($"SELECT COUNT(*) FROM {tbl} WHERE {keyField}='{keyValue}'{addFilter}").FirstOrDefault() > 0)
                warningMessage = "This data has been inserted before. Please cancel this transaction!";
            return !string.IsNullOrEmpty(warningMessage);
        }

        public static List<ReportModels.DDLSingleField> GetActiveFlagDataToList()
        {
            var result = new List<ReportModels.DDLSingleField>();
            result.Add(new ReportModels.DDLSingleField { sfield = "ACTIVE" });
            result.Add(new ReportModels.DDLSingleField { sfield = "INACTIVE" });
            return result;
        }

        public static Dictionary<string, Type> GetFieldFromQuery(string sqlQuery)
        {
            sqlQuery = $"SELECT TOP 1{sqlQuery.RightText(sqlQuery.Length - 6)}";
            var tbl = new Controllers.ClassConnection().GetDataTable(sqlQuery, "tblTmp");
            var result = new Dictionary<string, Type>();
            if (tbl != null && tbl.Rows.Count > 0)
            {
                for (int i = 0; i < tbl.Columns.Count; i++)
                {
                    var tipe = typeof(string);
                    if (tbl.Columns[i].DataType.Name == "Int32") tipe = typeof(int);
                    else if (tbl.Columns[i].DataType.Name == "Int32") tipe = typeof(int);
                    else if (tbl.Columns[i].DataType.Name == "DateTime") tipe = typeof(DateTime);
                    else if (tbl.Columns[i].DataType.Name == "Decimal") tipe = typeof(decimal);
                    else if (tbl.Columns[i].DataType.Name == "Boolean") tipe = typeof(bool);
                    result.Add(tbl.Columns[i].ColumnName, tipe);
                }
            }
            return result;
        }

        public static Type CreateDynamicModel(Dictionary<string, Type> prop)
        {
            TypeBuilder builder = CreateTypeBuilder("MyDynamicAssembly", "MyModule", "MyType");
            foreach (var item in prop) { CreateAutoImplementedProperty(builder, item.Key, item.Value); } 
            return builder.CreateType();
        }

        private static TypeBuilder CreateTypeBuilder(string assemblyName, string moduleName, string typeName)
        {
            TypeBuilder typeBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName(assemblyName), AssemblyBuilderAccess.Run).DefineDynamicModule(moduleName).DefineType(typeName, TypeAttributes.Public);
            typeBuilder.DefineDefaultConstructor(MethodAttributes.Public);
            return typeBuilder;
        }

        private static void CreateAutoImplementedProperty(TypeBuilder builder, string propertyName, Type propertyType)
        {
            const string PrivateFieldPrefix = "m_";
            const string GetterPrefix = "get_";
            const string SetterPrefix = "set_";
            FieldBuilder fieldBuilder = builder.DefineField(string.Concat(PrivateFieldPrefix, propertyName), propertyType, FieldAttributes.Private);
            PropertyBuilder propertyBuilder = builder.DefineProperty(propertyName, System.Reflection.PropertyAttributes.HasDefault, propertyType, null);
            MethodAttributes propertyMethodAttributes = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig;
            MethodBuilder getterMethod = builder.DefineMethod(string.Concat(GetterPrefix, propertyName), propertyMethodAttributes, propertyType, Type.EmptyTypes);
            ILGenerator getterILCode = getterMethod.GetILGenerator();
            getterILCode.Emit(OpCodes.Ldarg_0);
            getterILCode.Emit(OpCodes.Ldfld, fieldBuilder);
            getterILCode.Emit(OpCodes.Ret);
            MethodBuilder setterMethod = builder.DefineMethod(string.Concat(SetterPrefix, propertyName), propertyMethodAttributes, null, new Type[] { propertyType });
            ILGenerator setterILCode = setterMethod.GetILGenerator();
            setterILCode.Emit(OpCodes.Ldarg_0);
            setterILCode.Emit(OpCodes.Ldarg_1);
            setterILCode.Emit(OpCodes.Stfld, fieldBuilder);
            setterILCode.Emit(OpCodes.Ret);
            propertyBuilder.SetGetMethod(getterMethod);
            propertyBuilder.SetSetMethod(setterMethod);
        }

        public static string GenerateReport(ReportRequest request)
        {
            var report = new ReportDocument();
            report.Load(Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~"), "Report/" + request.rptFile + ".rpt"));
            if (string.IsNullOrEmpty(request.rptQuery) && request.rptDataSource != null)
                report.SetDataSource(request.rptDataSource);
            else
            {
                DataTable dtRpt = new ClassConnection().GetDataTable(request.rptQuery, request.rptFile);
                report.SetDataSource(dtRpt);
            }
            if (request.rptParam != null && request.rptParam.Count > 0)
                foreach (var item in request.rptParam)
                    report.SetParameterValue(item.Key, item.Value);
            if (request.rptPaperSizeEnum != 0)
                report.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)request.rptPaperSizeEnum;
            if (request.rptPaperOrientationEnum != 0)
                report.PrintOptions.PaperOrientation = (CrystalDecisions.Shared.PaperOrientation)request.rptPaperOrientationEnum;
            var reportId = Guid.NewGuid();
            var saveAs = reportId.ToString().AsTempPath(StringExt.FileExt_Report);
            report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.CrystalReport, saveAs);
            report.Dispose(); report.Close();

            var sdir = System.IO.Path.GetTempPath();
            if (Directory.Exists(sdir))
            {
                foreach (var file in Directory.GetFiles(sdir))
                {
                    var updtime = File.GetLastWriteTime(file);
                    if (updtime < DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy 00:00:00")) && file.EndsWith(".exported-rpt")) 
                        File.Delete(file); 
                }
            }

            return reportId.ToString();
        }

        public static ActionResult setPrintOut(PrintOutRequest request)
        {
            string error = string.Empty; string rpt_id = string.Empty; string err_query = string.Empty;
            var report = getReportDoc(request, ref error, ref err_query);
            var reportId = Guid.NewGuid();
            var saveAs = reportId.ToString().AsTempPath(FileExt_Report);
            report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.CrystalReport, saveAs);
            report.Dispose(); report.Close();
            var sdir = Path.GetTempPath();
            if (Directory.Exists(sdir))
            {
                foreach (var file in Directory.GetFiles(sdir))
                {
                    var updtime = File.GetLastWriteTime(file);
                    if (updtime < DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy 00:00:00")) && file.EndsWith(".exported-rpt")) File.Delete(file);
                }
            }
            rpt_id = reportId.ToString();
            return new JsonResult()
            {
                Data = new { rpt_id, error, rptname = request.file_name, err_query },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue,
            };
        }

        public static string GenerateReport_New(ReportRequest_New request)
        {
            var report = new ReportDocument();
            report.Load(Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~"), "Report/" + request.rptFile + ".rpt"));
            if (string.IsNullOrEmpty(request.rptQuery) && request.rptDataSource != null)
            {
                var i = 1; var x = 0;
                foreach (var item in request.rptDataSource)
                {
                    if (i == 1) report.SetDataSource(item);
                    else
                    {
                        report.Subreports[x].SetDataSource(item);
                        x++;
                    }
                    i++;
                }
            }
            else
            {
                DataTable dtRpt = new ClassConnection().GetDataTable(request.rptQuery, request.rptFile);
                report.SetDataSource(dtRpt);
            }
            if (request.rptParam != null && request.rptParam.Count > 0)
                foreach (var item in request.rptParam)
                    report.SetParameterValue(item.Key, item.Value);
            if (request.rptPaperSizeEnum != 0) report.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)request.rptPaperSizeEnum;
            if (request.rptPaperOrientationEnum != 0) report.PrintOptions.PaperOrientation = (CrystalDecisions.Shared.PaperOrientation)request.rptPaperOrientationEnum;
            var reportId = Guid.NewGuid();
            var saveAs = reportId.ToString().AsTempPath(StringExt.FileExt_Report);
            report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.CrystalReport, saveAs);
            report.Dispose(); report.Close();

            var sdir = System.IO.Path.GetTempPath();
            if (Directory.Exists(sdir))
            {
                foreach (var file in Directory.GetFiles(sdir))
                {
                    var updtime = File.GetLastWriteTime(file);
                    if (updtime < DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy 00:00:00")) && file.EndsWith(".exported-rpt"))
                    {
                        File.Delete(file);
                    }
                }
            }

            return reportId.ToString();
        }

        public static JsonResult GetJsonResult(string query, string fSeq = "")
        {
            string result = string.Empty; List<Dictionary<string, object>> tbl = null;
            try
            {  // SQL server 2016
                tbl = toObject(new ClassConnection().GetDataTable(query, "data"));
                if (tbl == null || tbl.Count <= 0)
                    result = "Data not found!";
                else if (!string.IsNullOrEmpty(fSeq))
                    for (int i = 0; i < tbl.Count; i++)
                        tbl[i][fSeq] = i + 1;
            }
            catch (Exception e)
            {
                result = e.ToString();
            }
            return new JsonResult()
            {
                Data = new { result, tbl },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue,
            };
        }

        #region FGlobal Function CRV
        public static Byte[] getImages(string sPath)
        {
            Byte[] arrImage = null;
            if (File.Exists(sPath))
            {
                Image img = Image.FromFile(sPath);
                MemoryStream ms = new MemoryStream();
                img.Save(ms, img.RawFormat);
                arrImage = ms.GetBuffer();
                ms.Close();
            }
            return arrImage;
        }

        public static ReportDocument getReportDoc(PrintOutRequest request, ref string error, ref string err_query)
        {
            string query = string.Empty;
            var CompanyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            var path_logo = $"~/Images/{CompanyCode}_logo_no_bg.jpg";
            ReportDocument report = new ReportDocument();
            try
            {
                report.Load(Path.Combine(HostingEnvironment.MapPath("~/Report"), request.rpt_name + ".rpt"));
                if (request.view_param != null) { query = $"select * from {request.view_param.view_name} where {request.view_param.view_filter}"; }
                else if (!string.IsNullOrEmpty(request.query)) { query = request.query; }
                else if (request.rpt_src != null)
                {
                    if (request.rpt_logo) { foreach (DataRow item in request.rpt_src.Rows) item["company_img"] = getImages(HostingEnvironment.MapPath(path_logo)); } 
                    report.SetDataSource(request.rpt_src);
                }
                if (request.sub_view_param != null && request.sub_view_param.Count > 0)
                {
                    request.query_sub = new List<string>();
                    foreach (var item in request.sub_view_param) { request.query_sub.Add($"select * from {item.view_name} where {item.view_filter}"); }
                }
                if (!string.IsNullOrEmpty(query))
                {
                    err_query = query;
                    var dt = new ClassConnection().GetDataTable(query, "data");
                    if (request.rpt_logo) foreach (DataRow item in dt.Rows) { item["company_img"] = getImages(HostingEnvironment.MapPath(path_logo)); }
                    report.SetDataSource(dt);
                    query = string.Empty;
                }
                if (request.query_sub != null && request.query_sub.Count > 0)
                {
                    for (int i = 0; i < request.query_sub.Count; i++)
                    {
                        err_query = request.query_sub[i];
                        var dt = new ClassConnection().GetDataTable(request.query_sub[i], "data");
                        report.Subreports[i].SetDataSource(dt);
                    }
                }
                else if (request.rpt_sub_src != null && request.rpt_sub_src.Count > 0) for (int i = 0; i < request.rpt_sub_src.Count; i++) report.Subreports[i].SetDataSource(request.rpt_sub_src[i]);
                if (request.rpt_param != null && request.rpt_param.Count > 0) {
                    foreach (var item in request.rpt_param) { report.SetParameterValue(item.Key, item.Value); }
                }
                report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            }
            catch (Exception ex)
            {
                error = ex.ToString();
            }
            return report;
        }  
        #endregion
    }
}