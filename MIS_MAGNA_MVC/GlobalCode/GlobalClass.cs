﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS_MAGNA_MVC
{
    public class GlobalClass
    {
        public class GLParam
        {
            public int acctgoid { get; set; }
            public decimal amtdebet { get; set; }
            public decimal amtcredit { get; set; }

            public GLParam(int acctgoid = 0, decimal amtdebet = 0, decimal amtcredit = 0)
            {
                this.acctgoid = acctgoid;
                this.amtdebet = amtdebet;
                this.amtcredit = amtcredit;
            }
        }

        public class notifList
        {
            public string cmpcode { get; set; }
            public int oid { get; set; }
            public int oid2 { get; set; }
            public string action_link { get; set; }
            public string strfield_1 { get; set; }
            public string strfield_2 { get; set; }
            public string strfield_3 { get; set; }
            public string strfield_4 { get; set; }
            public string note { get; set; }
            public string createuser { get; set; }
            public string postapptime { get; set; }
            public DateTime postapptime_dt { get; set; }
            public int days_count { get; set; }
        }

        public class JQDTParams
        {
            public int draw { get; set; }
            public int start { get; set; }
            public int length { get; set; }
            public List<JQDTColumn> columns { get; set; }
            public JQDTColumnSearch search { get; set; }
            public List<JQDTColumnOrder> order { get; set; }
        }

        public class JQDTColumn
        {
            public string data { get; set; }
            public string name { get; set; }
            public bool searchable { get; set; }
            public bool orderable { get; set; }
            public JQDTColumnSearch search { get; set; }
        }

        public class JQDTColumnSearch
        {
            public string value { get; set; }
            public string regex { get; set; }
        }

        public class JQDTColumnOrder
        {
            public int column { get; set; }
            public string dir { get; set; }
        }

        public enum JQDTColumnOrderDirection { asc, desc }
        #region Class Crystal Report

        public class ReportRequest
        {
            public string rptQuery { get; set; }
            public string rptFile { get; set; }
            /// <summary>
            /// Paper Size; 0-Default, 1-Letter, 4-Ledger, 5-Legal, 8-A3, 9-A4, 11-A5, 14-Folio
            /// </summary>
            public int rptPaperSizeEnum { get; set; }
            /// <summary>
            /// Paper Orientation: 1-Portrait, 2-Landscape
            /// </summary>
            public int rptPaperOrientationEnum { get; set; }
            public string rptExportType { get; set; }
            public Dictionary<string, string> rptParam { get; set; }
            public System.Data.DataTable rptDataSource { get; set; }
        }

        public class ReportRequest_New
        {
            public string rptQuery { get; set; }
            public string rptFile { get; set; }
            /// <summary>
            /// Paper Size; 0-Default, 1-Letter, 4-Ledger, 5-Legal, 8-A3, 9-A4, 11-A5, 14-Folio
            /// </summary>
            public int rptPaperSizeEnum { get; set; }
            /// <summary>
            /// Paper Orientation: 1-Portrait, 2-Landscape
            /// </summary>
            public int rptPaperOrientationEnum { get; set; }
            public string rptExportType { get; set; }
            public Dictionary<string, string> rptParam { get; set; }
            public List<System.Data.DataTable> rptDataSource { get; set; }
        }

        public class PrintOutSysView
        {
            public string view_name { get; set; }
            public string view_filter { get; set; }

            public PrintOutSysView(string viewName, string viewFilter)
            {
                view_name = viewName;
                view_filter = viewFilter;
            }
        }

        public class PrintOutRequest
        {
            public string rpt_name { get; set; }
            public PrintOutSysView view_param { get; set; }
            public List<PrintOutSysView> sub_view_param { get; set; }
            public string file_name { get; set; }
            public Dictionary<string, string> rpt_param { get; set; }
            public System.Data.DataTable rpt_src { get; set; }
            public List<System.Data.DataTable> rpt_sub_src { get; set; }
            public string query { get; set; }
            public List<string> query_sub { get; set; }
            public bool rpt_logo { get; set; }
        }

        #endregion
    }
}