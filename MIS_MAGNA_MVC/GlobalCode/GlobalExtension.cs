﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace MIS_MAGNA_MVC
{
    public static class GlobalExtension
    {
        public const string FileExt_Report = ".exported-rpt";
        public static decimal ToDecimal(this String val)
        {
            decimal result = 0;
            if (!string.IsNullOrEmpty(val))
            {
                try { Decimal.TryParse(val, out result); }
                catch (Exception) { }
            }
            return result;
        }

        public static string LeftText(this string input, int length)
        {
            var result = "";
            if ((input.Length <= 0)) return result;
            if ((length > input.Length)) length = input.Length;
            result = input.Substring(0, length);
            return result;
        }

        public static string RightText(this string input, int length)
        {
            var result = "";
            if ((input.Length <= 0)) return result;
            if ((length > input.Length)) length = input.Length;
            result = input.Substring((input.Length - length), length);
            return result;
        }

        public static int ToInteger(this String val)
        {
            int result = 0;
            if (!string.IsNullOrEmpty(val))
            {
                try { int.TryParse(val, out result); }
                catch (Exception) { }
            }
            return result;
        }

        public static string EmptyIfNull(this String val)
        {
            if (string.IsNullOrEmpty(val)) return "";
            else return val;
        }

        public static DateTime ToDateTime(this String val)
        {
            DateTime result = new DateTime(1900, 1, 1);
            if (!string.IsNullOrEmpty(val))
            {
                try { DateTime.TryParse(val, out result); }
                catch (Exception) { }
            }
            return result;
        }

        public static string ToTitleCase(this String val)
        {
            var textInfo = new System.Globalization.CultureInfo("en-US", false).TextInfo;
            return textInfo.ToTitleCase(val);
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> query, string memberName, bool asc = true)
        {
            ParameterExpression[] typeParams = new ParameterExpression[] { Expression.Parameter(typeof(T), "") };
            System.Reflection.PropertyInfo pi = typeof(T).GetProperty(memberName);

            return (IOrderedQueryable<T>)query.Provider.CreateQuery(
                Expression.Call(
                    typeof(Queryable),
                    asc ? "OrderBy" : "OrderByDescending",
                    new Type[] { typeof(T), pi.PropertyType },
                    query.Expression,
                    Expression.Lambda(Expression.Property(typeParams[0], pi), typeParams))
                );
        }
    }

    public static class StringExt
    {
        public const string FileExt_Report = ".exported-rpt";

        public static string AsTempPath(this string filenameNoExt, string ext = "")
        {
            return System.IO.Path.GetTempPath() + filenameNoExt + ext;
        }
    }
}