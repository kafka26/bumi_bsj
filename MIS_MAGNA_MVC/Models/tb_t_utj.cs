//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MIS_MAGNA_MVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_t_utj
    {
        public int t_utj_id { get; set; }
        public System.Guid t_utj_uid { get; set; }
        public int m_company_id { get; set; }
        public string t_utj_no { get; set; }
        public System.DateTime t_utj_date { get; set; }
        public System.DateTime t_utj_due_date { get; set; }
        public int m_cust_id { get; set; }
        public int m_proyek_id { get; set; }
        public int m_item_id { get; set; }
        public int m_sales_id { get; set; }
        public int m_manager_id { get; set; }
        public string t_utj_type_bayar { get; set; }
        public int t_utj_account_id { get; set; }
        public decimal t_utj_amt { get; set; }
        public string t_utj_status { get; set; }
        public string t_utj_note { get; set; }
        public string created_by { get; set; }
        public System.DateTime created_at { get; set; }
        public string updated_by { get; set; }
        public System.DateTime updated_at { get; set; }
        public string posted_by { get; set; }
        public Nullable<System.DateTime> posted_at { get; set; }
        public int m_marketing_id { get; set; }
        public string m_marketing_name { get; set; }
    }
}
