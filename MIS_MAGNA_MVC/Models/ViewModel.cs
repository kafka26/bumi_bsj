﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS_MAGNA_MVC.Models
{
    public class DataLogin
    {
        public string userid { get; set; }
        public string userpwd { get; set; }
    }

    public class RoleDetail
    {
        public string formtype { get; set; }
        public string formname { get; set; }
        public string formaddress { get; set; }
        public string formmodule { get; set; }
        public int formnumber { get; set; }
        public string formmenu { get; set; }
        public string formimage { get; set; }
    }

    public class RoleSpecial
    {
        public string formaddress { get; set; }
        public string special { get; set; }
    }

    public class ModelFilter
    {
        public DateTime filterperiodfrom { get; set; }
        public DateTime filterperiodto { get; set; }
        public string filterstatus { get; set; }
        public string filterddl { get; set; }
        public string filtertext { get; set; }
        public bool isperiodchecked { get; set; }
    }

    public class dtFiles
    {
        public int dt_fileseq { get; set; }
        public string dt_filepath { get; set; }
        public string dt_fileurl { get; set; }
        public string dt_filename { get; set; }
        public string dt_filectrl { get; set; }
    }

    public class DataTableAjaxPostModel
    {
        // properties are not capital due to json mapping
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public List<Column> columns { get; set; }
        public Search search { get; set; }
        public List<Order> order { get; set; }
    }

    public class Column
    {
        public string data { get; set; }
        public string name { get; set; }
        public bool searchable { get; set; }
        public bool orderable { get; set; }
        public Search search { get; set; }
    }

    public class Search
    {
        public string value { get; set; }
        public string regex { get; set; }
    }

    public class Order
    {
        public int column { get; set; }
        public string dir { get; set; }
    }

    public class PostAJExpenseReceipt
    {
        public int coa_hdr { get; set; }
        public string trn_type { get; set; }
        public string trn_note { get; set; }
        public List<PostAJExpenseReceiptDtl> detail { get; set; }
    }

    public class PostAJExpenseReceiptDtl
    {
        public int coa_dtl { get; set; }
        public decimal amt { get; set; }
        public string note { get; set; }
    }

    public class PostAJSalesInvoice
    {
        public string trn_type { get; set; }
        public int coa_dp { get; set; }
        public decimal amt_stock { get; set; }
        public decimal amt_jual { get; set; }
        public decimal amt_ppn { get; set; }
        public decimal amt_dp { get; set; }
        public decimal amt_piutang { get; set; }
        public bool t_ar_type_jasa { get; set; }
    }

    public class PostAJPurchaseInvoice
    {
        public string trn_type { get; set; }
        public int coa_dp { get; set; }
        public decimal amt_stock { get; set; }
        public decimal amt_ppn { get; set; }
        public decimal amt_hutang { get; set; }
        public decimal amt_dp { get; set; }
    }

    public class PostAJPurchaseInvoiceSrv
    {
        public string trn_type { get; set; }
        public int coa_cost { get; set; }
        public decimal amt_stock { get; set; }
        public decimal amt_ppn { get; set; }
        public decimal amt_hutang { get; set; }
        public List<PostAJPurchaseInvoiceSrvDtl> detail { get; set; }
    }
    public class PostAJPurchaseInvoiceSrvDtl
    {
        public int m_account_id { get; set; }
        public decimal amt_dtl { get; set; }
        public string note_dtl { get; set; }
    }

    public class PostAJPurchaseRetur
    {
        public string trn_type { get; set; }
        public string trn_h_type { get; set; }
        public decimal amt_stock { get; set; }
        public decimal amt_ppn { get; set; }
        public decimal amt_hutang { get; set; }
        public decimal amt_dp { get; set; }
        public List<PostAJPurchaseInvoiceSrvDtl> detail { get; set; }
    }

    public class PostAJAssetConfirm
    {
        public string trn_type { get; set; }
        public int coa_debet { get; set; }
        public int coa_credit { get; set; }
        public decimal amt { get; set; }
        public string note { get; set; }
    }

    public class PostMemorialJournal
    {
        public List<PostMemorialJournalDtl> detail { get; set; }
    }

    public class PostMemorialJournalDtl
    {
        public string t_memo_d_note { get; set; }
        public int t_memo_account_id { get; set; }
        public decimal t_memo_debet_amt { get; set; }
        public decimal t_memo_credit_amt { get; set; }
    }

    public class PostDebetNote
    {
        public List<PostDebetNoteDtl> detail { get; set; }
    }

    public class PostDebetNoteDtl
    {        
        public int t_dn_d_debet_account_id { get; set; }
        public int t_dn_d_credit_account_id { get; set; }
        public decimal t_dn_d_amt { get; set; }
        public string t_dn_d_note { get; set; }
    }
     
    public class PostCreditNote
    {
        public List<PostCreditNoteDtl> detail { get; set; }
    }

    public class PostCreditNoteDtl
    {
        public int t_cn_d_debet_account_id { get; set; }
        public int t_cn_d_credit_account_id { get; set; }
        public decimal t_cn_d_amt { get; set; }
        public string t_cn_d_note { get; set; }
    }

    public class PostRgmHeader
    {
        public int coa_hdr { get; set; }
        public string t_rgm_type { get; set; }
        public string t_rgm_note { get; set; }
        public decimal t_rgm_h_amt { get; set; }
        public List<PostRgmDtl> detail { get; set; }
    }

    public class PostRgmDtl
    {
        public int coa_dtl { get; set; }
        public decimal t_rgm_d_amt { get; set; }
        public string t_rgm_d_note { get; set; }
    }

    public class PostRgkHeader
    {
        public int coa_hdr { get; set; }
        public string t_rgk_type { get; set; }
        public string t_rgk_note { get; set; }
        public decimal t_rgk_h_amt { get; set; }
        public List<PostRgkDtl> detail { get; set; }
    }

    public class PostRgkDtl
    {
        public int coa_dtl { get; set; }
        public decimal t_rgk_d_amt { get; set; }
        public string t_rgk_d_note { get; set; }
    }

    public class PostARPaymentHeader
    {
        public int coa_hdr { get; set; }
        public string t_payar_h_note { get; set; }
        public decimal t_payar_h_amt { get; set; }
        public List<PostARPaymentDtl> detail { get; set; }
    }

    public class PostARPaymentDtl
    {
        public int coa_dtl { get; set; }
        public decimal t_payar_d_amt { get; set; }
        public string t_payar_d_note { get; set; }
    }

    public class PostFADepreciationDetail
    {
        public int m_asset_h_cost_accum_account_id { get; set; }
        public int m_asset_h_accum_account_id { get; set; }
        public decimal t_fa_depr_d_value { get; set; }
        public string t_fa_depr_h_note { get; set; }
    }

    public class PostFADepreciationHeader
    {
        public List<PostFADepreciationDetail> detail { get; set; }
    } 

    public class PostAJSalesInvoiceOther
    {
        public int coa_header { get; set; }
        public string trn_type { get; set; }
        public decimal amt_ppn { get; set; }
        public decimal amt_jual { get; set; }
        public List<PostAJSalesInvoiceOtherDetail> detail { get; set; }
    }

    public class PostAJSalesInvoiceOtherDetail
    {
        public int coa_dtl { get; set; }        
        public decimal amt_piutang { get; set; }
    }

    public class PostAJSalesReturn
    {
        public string trn_type { get; set; } 
        public decimal amt_stock { get; set; }
        public decimal amt_return { get; set; }
        public decimal amt_ppn { get; set; } 
        public decimal amt_piutang { get; set; }
    }
}