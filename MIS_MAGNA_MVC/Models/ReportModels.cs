﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MIS_MAGNA_MVC.Models
{
    public class ReportModels
    {
        public class DDLSingleField
        {
            public string sfield { get; set; }
        }

        public class DDLDoubleField
        {
            public int ifield { get; set; }
            public string sfield { get; set; }
        }

        public class DDLDoubleFieldString
        {
            public string valuefield { get; set; }
            public string textfield { get; set; }
        }

        public class ModelAccount
        {
            public int m_account_id { get; set; }
            public string m_account_code { get; set; }
            public string m_account_name { get; set; }
        }

        public class dropdownlistdata
        {
            public string ddlvalue { get; set; }
            public string ddltext { get; set; }

            public dropdownlistdata(string dval, string dtext)
            {
                ddlvalue = dval;
                ddltext = dtext;
            }
        }
    }
}