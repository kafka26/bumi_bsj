//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MIS_MAGNA_MVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_m_account
    {
        public System.Guid m_account_uid { get; set; }
        public int m_company_id { get; set; }
        public int m_account_id { get; set; }
        public string m_account_code { get; set; }
        public string m_account_desc { get; set; }
        public string m_account_dbcr { get; set; }
        public string m_account_type { get; set; }
        public int m_account_level { get; set; }
        public string m_account_group { get; set; }
        public int m_account_parent_id { get; set; }
        public string m_account_report_type { get; set; }
        public string m_account_report_lvl { get; set; }
        public int m_curr_id { get; set; }
        public string m_account_note { get; set; }
        public string active_flag { get; set; }
        public string created_by { get; set; }
        public System.DateTime created_at { get; set; }
        public string last_edited_by { get; set; }
        public System.DateTime last_edited_at { get; set; }
    }
}
